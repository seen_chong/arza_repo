<?php
$donation_type = [1=>'fixed',2=>'random',3=>'free'];
?>

<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
    <div role="tabpanel">
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="test">
                <input type="hidden" name="id" value="<?php echo $model->member_type->id; ?>"/>
                <input name="token" type="hidden" value="<?php echo get_token(); ?>"/>

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>Name</label>
                                <?php echo $model->form->editorFor("name"); ?>
                            </div>
                            <div class="form-group">
                                <label>Parent Member Type</label>
                                <select name="parent_type">
                                    <option value="0">None</option>
                                    <?php foreach ($model->member_types as $types) {
                                        $select = ($types->id == $model->member_type->parent_type) ? " selected='selected'" : "";
                                        ?>
                                        <option
                                            value="<?php echo $types->id; ?>" <?php echo $select; ?> ><?php echo $types->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <?php echo $model->form->textAreaFor("description", ["class" => "ckeditor"]); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Donation Amount</h4>
                            <label>Donation Type</label>
                            <select name="donation_type">
                                <option value="0">Select</option>
                                <?php foreach($donation_type as $key=>$val) {
                                    $select = ($model->member_type->donation_type==$key) ? " selected='selected'" : "";
                                    ?>
                                    <option value="<?php echo $key;?>"<?php echo $select;?>><?php echo $val;?></option>
                                <?php } ?>
                            </select>
                            <div class="amount" data-type_id="1" style="<?=($model->member_type->donation_type==1)?"":"display: none"?>">
                            <div class="form-group">
                                <label>Amount</label><br/>
                                <?php echo $model->form->editorFor('amount'); ?>
                            </div>
                            </div>
                            <div class="amount" data-type_id="2" style="<?=($model->member_type->donation_type==2)?"":"display: none"?>">
                                <div class="form-group">
                                    <label>Minimum</label>
                                    <?php echo $model->form->editorFor('minimum'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Interval</label>
                                    <?php echo $model->form->editorFor('amount_interval'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
<script type='text/javascript'>
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        $("select[name=donation_type]").on('change',function(e){
            var type = $(this).val();
            $('.amount').hide();
            $('.amount').each(function(){
                if($(this).attr("data-type_id")==type){
                    $(this).show();
                }
            })

        })
    });
</script>