<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#test" aria-controls="general" role="tab" data-toggle="tab">General</a>
            </li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="test">
                <input type="hidden" name="id" value="<?php echo $model->blog_category->id; ?>"/>
                <input name="token" type="hidden" value="<?php echo get_token(); ?>"/>

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>Name</label>
                                <?php echo $model->form->editorFor("name"); ?>
                            </div>

                            <div class="box">
                                <div class="form-group">
                                    <h4>Featured Image</h4>

                                    <p>
                                        <small>(ideal image size is 720 x 255)</small>
                                    </p>
                                    <?php
                                    $img_path = "";
                                    if ($model->blog_category->featured_image != ""&&file_exists(UPLOAD_PATH . 'blog_categories' . DS . $model->blog_category->featured_image)) {
                                        $img_path = UPLOAD_URL . 'blog_categories/' . $model->blog_category->featured_image;
                                    }
                                    ?>
                                    <p><input type="file" name="featured_image" class='image'/></p>
                                    <?php if ($model->blog_category->featured_image != "") { ?>
                                        <div class="well well-sm pull-left">
                                            <div id='image-preview'>
                                                <img src="<?php echo $img_path; ?>" width="360" height="127.5"/>
                                                <br/>
                                                <a href=<?= ADMIN_URL . 'blog_categories/delete_image/' . $model->blog_category->id . '?featured_image=1'; ?> class="btn
                                                   btn-default btn-xs">Delete</a>
                                                <input type="hidden" name="featured_image"
                                                       value="<?= $model->blog_category->featured_image ?>"/>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class='preview-container'></div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>SEO</h4>

                            <div class="form-group">
                                <label>Slug</label>
                                <?php echo $model->form->editorFor('slug'); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Title</label>
                                <?php echo $model->form->editorFor('meta_title'); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Keywords</label>
                                <?php echo $model->form->editorFor('meta_keywords'); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Description</label>
                                <?php echo $model->form->textAreaFor('meta_description'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
<script type='text/javascript'>
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '360');
                    img.attr('height', '127.5');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
        });
    });
</script>