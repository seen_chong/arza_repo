<div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#account-info-tab" aria-controls="general" role="tab"
                                                  data-toggle="tab">Account Information</a></li>
        <li role="presentation"><a href="#user-list-tab" aria-controls="user" role="tab" data-toggle="tab">Users
                List</a></li>
        <li role="presentation"><a href="#credit-list-tab" aria-controls="credit" role="tab" data-toggle="tab">Credit
                History</a></li>
    </ul>
    <form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $model->corporate->id; ?>"/>
        <input type="hidden" name="insert_time" value="<?php echo $model->corporate->insert_time; ?>"/>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="account-info-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <!--							--><? //foreach($model->users as $obj){dd(\Model\User::getItem($obj->employee_id));}?>
                            <h4>Company Information</h4>

                            <div class="form-group">
                                <label>Company Name</label>
                                <?php echo $model->form->editorFor('company'); ?>
                            </div>
                            <div class="form-group">
                                <label>Admin First Name</label>
                                <?php echo $model->form->editorFor('first_name'); ?>
                            </div>
                            <div class="form-group">
                                <label>Admin Last Name</label>
                                <?php echo $model->form->editorFor('last_name'); ?>
                            </div>
                            <div class="form-group">
                                <label>Admin Email Address</label>
                                <?php echo $model->form->editorFor('email'); ?>
                            </div>
                            <div class="form-group">
                                <label>Distributable Credit Amount</label>
                                <input name="credit" type="text" value="<?=$model->corporate->credit?>">
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div role="tabpanel" class="tab-pane" id="user-list-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <table id="data-list" class="table">
                                <thead>
                                <tr>
                                    <th width="30%">Date time</th>
                                    <th width="30%">Email</th>
                                    <th width="40%">Name</th>
                                    <th width="15%" class="text-center">Edit</th>
                                    <th width="15%" class="text-center">Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <? foreach ($model->users as $obj) {
                                    $user = \Model\User::getItem($obj->employee_id) ?>
                                    <tr class="originalProducts">
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>users/update/<?php echo $user->id; ?>">
                                                <?php echo $user->insert_time; ?>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>users/update/<?php echo $user->id; ?>">
                                                <?php echo $user->email; ?>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>users/update/<?php echo $user->id; ?>">
                                                <?php echo $user->full_name(); ?>
                                            </a>
                                        </td>

                                        <td class="text-center">
                                            <a class="btn-actions" href="<?= ADMIN_URL ?>users/update/<?= $obj->id ?>">
                                                <i class="icon-pencil"></i>
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <a class="btn-actions"
                                               href="<?= ADMIN_URL ?>users/delete/<?= $obj->id ?>?token_id=<?php echo get_token(); ?>"
                                               onClick="return confirm('Are You Sure?');">
                                                <i class="icon-cancel-circled"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <? } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>

            <div role="tabpanel" class="tab-pane" id="credit-list-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <table id="data-list" class="table">
                                <thead>
                                <tr>
                                    <th width="33%">Credits Sent</th>
                                    <th width="40%">Sent To</th>
                                    <th width="40%">Date and Time</th>
                                    <!--									<th width="15%" class="text-center">Edit</th>-->
                                    <!--									<th width="15%" class="text-center">Delete</th>-->
                                </tr>
                                </thead>
                                <tbody>
                                <? foreach ($model->credits as $obj) {
                                    foreach ($obj as $cCredit) {
                                        $emp = \Model\Corporate_Employee::getItem($cCredit->corporate_employee_id);
                                        $user = \Model\User::getItem($emp->employee_id); ?>
                                        <tr class="originalProducts">
                                            <td>
                                                <!--											<a href="-->
                                                <?php //echo ADMIN_URL;
                                                ?><!--credits/update/--><?php //echo $cCredit->id;
                                                ?><!--">-->
                                                <?php echo $cCredit->credit; ?>
                                                <!--											</a>-->
                                            </td>
                                            <td>
                                                <!--											<a href="-->
                                                <?php //echo ADMIN_URL;
                                                ?><!--credits/update/--><?php //echo $cCredit->id;
                                                ?><!--">-->
                                                <?php echo $user->email; ?>
                                                <!--											</a>-->
                                            </td>
                                            <td>
                                                <!--											<a href="-->
                                                <?php //echo ADMIN_URL;
                                                ?><!--credits/update/--><?php //echo $cCredit->id;
                                                ?><!--">-->
                                                <?php echo $cCredit->insert_time; ?>
                                                <!--											</a>-->
                                            </td>
                                            <!--										<td class="text-center">-->
                                            <!--											<a class="btn-actions" href="-->
                                            <? //= ADMIN_URL
                                            ?><!--credits/update/--><? //= $cCredit->id
                                            ?><!--">-->
                                            <!--												<i class="icon-pencil"></i>-->
                                            <!--											</a>-->
                                            <!--										</td>-->
                                            <!--										<td class="text-center">-->
                                            <!--											<a class="btn-actions" href="-->
                                            <? //= ADMIN_URL
                                            ?><!--credits/delete/--><? //= $cCredit->id
                                            ?><!--?token_id=--><?php //echo get_token();
                                            ?><!--" onClick="return confirm('Are You Sure?');">-->
                                            <!--												<i class="icon-cancel-circled"></i>-->
                                            <!--											</a>-->
                                            <!--										</td>-->
                                        </tr>
                                    <? } ?>
                                <? } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>


        </div>
</div>
<div class="form-group">
    <button type="submit" class="btn btn-save">Save</button>
</div>
</form>
</div>

<?php footer(); ?>

<script>

    var services = <?php echo json_encode($model->provider->services); ?>;

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var img = $("<img />");
            reader.onload = function (e) {
                img.attr('src', e.target.result);
                img.attr('alt', 'Uploaded Image');
                img.attr("width", '100%');
                img.attr('height', '100%');
            };
            $(input).parent().parent().find('.preview-container').html(img);
            $(input).parent().parent().find('input[type="hidden"]').remove();

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(function () {
        $("select[name='provider_service[]']").val(services);

        $("select.multiselect").each(function (i, e) {
            //$(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });

        $("input.image").change(function () {
            readURL(this);
        });
    })

</script>