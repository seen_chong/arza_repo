<?php if(count($model->newsletters)>0) { ?>
    <a class="btn-actions" href="/admin/newsletter/export"><i class="fa fa-download"></i> Export</a>
<div class="box box-table">
  <table class="table">
    <thead>
      <tr>
        <th>Email</th>
          <th>Time</th>
          <th>Delete</th>
      </tr>
    </thead>
    <tbody>
     <?php foreach($model->newsletters as $newsletter): ?>	
      <tr>
       <td><?= $newsletter->email; ?></td>
       <td><?php $newsletter->insert_time = new DateTime($newsletter->insert_time);
           $newsletter->insert_time = $newsletter->insert_time->format('m-d-Y H:i:s');
           echo $newsletter->insert_time; ?></td>
        <td><a class="btn-actions" href="<?php echo ADMIN_URL; ?>newsletter/delete/<?php echo $newsletter->id; ?>" onClick="return confirm('Are You Sure?');">
                <i class="icon-cancel-circled"></i>
            </a></td>
     </tr>
   <?php endforeach; ?>
 </tbody>
</table>
<div class="box-footer">
	<div class='paginationContent'></div>
</div>
</div>
<?php };?>

<?= footer();?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'newsletter';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>