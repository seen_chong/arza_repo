<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<div class="row">
    <div class="col-md-8">
        <div class="box">
            <div class="input-group">
                <input id="search" type="text" name="search" class="form-control" placeholder="Search by Id or Name"/>
            <span class="input-group-addon">
                <i class="icon-search"></i>
            </span>
            </div>
        </div>
    </div>

</div>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<?php if (count($model->ebooks) > 0) { ?>
    <div class="box box-table">
        <table id="data-list" class="table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Author</th>
                <th>Link</th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->ebooks as $obj) { ?>
                <tr class="originalProducts">
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>ebooks/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->name; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>ebooks/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->author; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>ebooks/update/<?php echo $obj->id; ?>">
                            <?php echo 'arza.org/resource/'.$obj->id ; ?>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>ebooks/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>ebooks/delete/<?php echo $obj->id; ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>

                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>

<?php footer(); ?>

<script>
    var site_url = '<?= ADMIN_URL.'ebooks';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script type="text/javascript">
    $(function () {
        $("#search").keyup(function () {
            var url = "<?php echo ADMIN_URL; ?>ebooks/search";
            var keywords = $(this).val();
            if (keywords.length > 0) {
                $.get(url, {keywords: keywords}, function (data) {
                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').hide();
                    $('.originalProducts').hide();
                    var list = JSON.parse(data);
                    for (key in list) {
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html(list[key].name);
                        $('<td />').appendTo(tr).html(list[key].author);
                        $('<td />').appendTo(tr).html(list[key].publish_date);
//                        $('<td />').appendTo(tr).html(list[key].name);
//                        $('<td />').appendTo(tr).html(list[key].email);
//                        $('<td />').appendTo(tr).html(list[key].phone);
//                        $('<td />').appendTo(tr).html(list[key].type);
                        var editTd = $('<td />').addClass('text-center').appendTo(tr);
                        var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>ebooks/update/' + list[key].id);
                        var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                        var deleteTd = $('<td />').addClass('text-center').appendTo(tr);
                        var deleteLink = $('<a />').appendTo(deleteTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>ebooks/delete/' + list[key].id);
                        deleteLink.click(function () {
                            return confirm('Are You Sure?');
                        });
                        var deleteIcon = $('<i />').appendTo(deleteLink).addClass('icon-cancel-circled');
                        tr.appendTo($("#data-list tbody"));
                    }

                });
            } else {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').show();
                $('.originalProducts').show();
            }
        });
    })
</script>