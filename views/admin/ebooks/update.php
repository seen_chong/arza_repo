<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->ebook->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div class="tab-content">
        <div role="tabpanel">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <h4>General</h4>
                        <div class="form-group">
                            <label>Name</label>
                           <?echo $model->form->editorFor("name")?>
                        </div>
                        <div class="form-group">
                            <label>Author</label>
                            <?echo $model->form->editorFor("author")?>
                        </div>
                        <div class="form-group">
                            <label>Publish Date</label>
                            <?php echo $model->form->editorFor("publish_date"); ?>
                        </div>

                        <div class="form-group">
                            <label>Description</label>
                            <?echo $model->form->textAreaFor("description", ["class" => "ckeditor"]);?>
                        </div>
                        <div class="form-group">
                            <label>Accessibility</label>
                            <select name="member_type[]" class="multiselect" data-placeholder="Members" multiple>
                                <?$members = \Model\Member_Type::getParentList();?>
                                <?php foreach ($members as $member) {?>
                                    <option value="<?php echo $member->id; ?>"><?php echo $member->name; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="box">
                        <h4>Resource Information</h4>
                        <div class="form-group">
                            <label>Upload Book</label>
                            <p><input type="file" name="file_name" class='file'/></p>
                            <?php if($model->ebook->file_name!=''){?>
                                <p><?=$model->ebook->file_name?></p>
                                <input type="hidden" name="file_name" value="<?=$model->ebook->file_name?>">
                                <a href="<?=ADMIN_URL.'ebooks/delete_image/' . $model->ebook->id .'?type=file_name'?>"class="btn btn-default btn-xs">Delete</a>
                            <?}?>

                        </div>

                        <div class="form-group">
                            <label>Featured image</label>

                            <p>
                                <small>(ideal featured image size is 1920 x 300)</small>
                            </p>
                            <?php
                            $img_path = "";
                            if ($model->ebook->featured_image != "") {
                                $img_path = UPLOAD_URL. 'ebooks/' . $model->ebook->featured_image;
                            }
                            ?>
                            <p><input type="file" name="featured_image" class='image'/></p>
                            <?php if ($model->ebook->featured_image != "") { ?>
                                <div class="well well-sm pull-left">
                                    <img src="<?php echo $img_path; ?>" width="100"/>
                                    <br/>
                                    <a href="<?= ADMIN_URL . 'ebooks/delete_image/' . $model->ebook->id . '/?featured_image=1'; ?>"
                                       class="btn btn-default btn-xs">Delete</a>
                                    <input type="hidden" name="featured_image" value="<?= $model->ebook->featured_image ?>"/>
                                </div>
                            <?php } ?>
                            <div class='preview-container'></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

    <input type="hidden" id="act_for_click" name="redirectTo" value="<?=ADMIN_URL.'ebooks/'?>">
    <center style="    margin-bottom: 112px;">
        <button type="submit" id="save_close" class="btn btn-save">Save and close</button>
        <button type="submit" id="save" class="btn btn-save">Save</button>
    </center>
</form>
<?php footer(); ?>

<style>
    div.mouseover-thumbnail-holder {
        position: relative;
        display: block;
        float: left;
        margin-right: 10px;
    }

    .large-thumbnail-style {
        display: block;
        border: 2px solid #fff;
        box-shadow: 0px 0px 5px #aaa;
    }

    div.mouseover-thumbnail-holder .large-thumbnail-style {
        position: absolute;
        top: 0;
        left: -9999px;
        z-index: 1;
        opacity: 0;
        transition: opacity .5s ease-in-out;
        -moz-transition: opacity .5s ease-in-out;
        -webkit-transition: opacity .5s ease-in-out;
    }

    div.mouseover-thumbnail-holder:hover .large-thumbnail-style {
        width: 100% !important;
        top: 0;
        left: 105%;
        z-index: 1;
        opacity: 1;

    }
</style>

<script type="text/javascript">
    var member = <?php echo $model->ebook->member_type ? : json_encode([]) ?>;

    $(document).ready(function () {

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '120');
                    img.attr('height', '180');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
            $('#previewupload').show();
        });

        $("select.multiselect").each(function (i, e) {
            $(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });
        $("select[name='member_type[]']").val(member);
        $("select.multiselect").multiselect("rebuild");

    })

    $(function () {
        var date = $("input[name='publish_date']");
        date.datepicker({
            format: "MM d, yyyy",
            selectOnBlur:true,
        });
    });
    $(function () {
        $('#save_close').mouseover(function () {
            $("#act_for_click").val("<?=ADMIN_URL.'ebooks/'?>");

        });

        $('#save').mouseover(function () {
            var id = $("input[name=id]").val();
            $("#act_for_click").val("<?=ADMIN_URL.'ebooks/update/'?>" + id);
        });


    });
</script>




























