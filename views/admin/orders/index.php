<div class="row">
    <div class="col-md-8">
        <div class="box">
            <div class="input-group">
                <input id="search" type="text" name="search" class="form-control" placeholder="Search by Id or Name"/>
            <span class="input-group-addon">
                <i class="icon-search"></i>
            </span>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="box">
            Show on page:
            <select class="how_many" name="how_many" style="cursor:pointer">

                <option <? if (isset($_GET['how_many'])) {
                    if ($_GET['how_many'] == 10) {
                        echo "selected";
                    }
                } ?> selected value="10">10
                </option>
                <option <? if (isset($_GET['how_many'])) {
                    if ($_GET['how_many'] == 50) {
                        echo "selected";
                    }
                } ?> value="50">50
                </option>
                <option <? if (isset($_GET['how_many'])) {
                    if ($_GET['how_many'] == 100) {
                        echo "selected";
                    }
                } ?> value="100">100
                </option>
                <option <? if (isset($_GET['how_many'])) {
                    if ($_GET['how_many'] == 500) {
                        echo "selected";
                    }
                } ?> value="500">500
                </option>
                <option <? if (isset($_GET['how_many'])) {
                    if ($_GET['how_many'] == 1000) {
                        echo "selected";
                    }
                } ?> value="1000">1000
                </option>
            </select>


            Type:
            <select class="type" name="type" style="cursor:pointer">
                <option <? if (!isset($_GET['type'])) {
                    echo "selected";
                } ?> value="0">All
                </option>
                <option <? if (isset($_GET['type'])) {
                    if ($_GET['type'] == 2) {
                        echo "selected";
                    }
                } ?> value="2">Background check
                </option>
                <option <? if (isset($_GET['type'])) {
                    if ($_GET['type'] == 1) {
                        echo "selected";
                    }
                } ?> value="1">Products & packages
                </option>

            </select>
        </div>


    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
</div>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script>
    $(document).ready(function () {
        $('body').on('change', '.how_many', function () {
            var how_many = $(this).val();
            window.location.href = '/admin/orders?how_many=' + how_many;
        });
        $('body').on('change', '.type', function () {
            var type = $(this).val();
            window.location.href = '/admin/orders?type=' + type;
        });
    });
</script>
<?php if (count($model->orders) > 0) { ?>
    <div class="box box-table">
        <table id="data-list" class="table">
            <thead>
            <tr>
                <th width="5%"></th>
                <th>Order #</th>
                <th>Date</th>

                <th>Status</th>
                <th>Tracking #</th>

                <th>Bill Name</th>
                <th>Type</th>
                <th>Coupon</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->orders as $obj) { ?>
                <tr class="originalProducts">
                    <td>
                        <? if (is_null($obj->viewed) || !$obj->viewed) { ?>
                            <button type="button" class="btn btn-success" disabled="disabled">New</button>
                        <? } ?>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->id; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->insert_time; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                            <?php echo \Model\Order::$transactionStatus[$obj->status]; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->tracking_number; ?>
                        </a>
                    </td>

                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                            <? $shp = array($obj->ship_first_name, $obj->ship_last_name, $obj->ship_address, $obj->ship_address2,
                                $obj->ship_country, $obj->ship_city, $obj->ship_state, $obj->ship_zip);


                            $blng = array($obj->bill_first_name, $obj->bill_last_name, $obj->bill_address, $obj->bill_address2,
                                $obj->bill_country, $obj->bill_city, $obj->bill_state, $obj->bill_zip);


                            if (count(array_diff($shp, $blng)) == 0) {
                                ?><?php echo $obj->bill_first_name . ' ' . $obj->bill_last_name; ?>
                            <? } else {
                                ?>
                                <button type="button" class="btn btn-success" style="position:relative;background:red;"
                                        disabled="disabled"> <?php echo $obj->bill_first_name . ' ' . $obj->bill_last_name; ?></button>
                            <? } ?>
                        </a>
                    </td>
                    <td>
                        <? if ($obj->type == 2) {
                            echo "Background check";
                        } else {
                            echo "Credits";
                        } ?>
                    </td>

                    <td>
                        <?=$obj->coupon_code ?>
                    </td>

                    <td>
                        <a href="<?php echo ADMIN_URL; ?>orders/update/<?php echo $obj->id; ?>">
                            <? if ($obj->payment_method == 2) { ?>
                                <button type="button" class="btn btn-success" style="position:relative;"
                                        disabled="disabled">$<?php echo number_format($obj->total, 2); ?></button>
                            <? } else { ?>$<?php echo number_format($obj->total, 2);
                            } ?>
                        </a>
                    </td>

                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>

<?php footer(); ?>

<script>
    var site_url = '<?= ADMIN_URL.'orders';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script type="text/javascript">
    $(function () {
        $("#search").keyup(function () {
            var url = "<?php echo ADMIN_URL; ?>orders/search";
            var keywords = $(this).val();
            if (keywords.length > 0) {
                $.get(url, {keywords: keywords}, function (data) {
                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').hide();
                    $('.originalProducts').hide();
                    var list = JSON.parse(data);
                    for (key in list) {
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html();
                        $('<td />').appendTo(tr).html(list[key].id);
                        $('<td />').appendTo(tr).html(list[key].insert_time);
                        $('<td />').appendTo(tr).html(list[key].tracking_number);
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>orders/update/' + list[key].id).html(list[key].status));
                        $('<td />').appendTo(tr).html(list[key].bill_name);
                        $('<td />').appendTo(tr).html(list[key].products);
                        $('<td />').appendTo(tr).html(list[key].brands);
                        $('<td />').appendTo(tr).html("$" + list[key].total);
                        var editTd = $('<td />').addClass('text-center').appendTo(tr);
                        var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>orders/update/' + list[key].id);
                        var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                        tr.appendTo($("#data-list tbody"));
                    }

                });
            } else {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').show();
                $('.originalProducts').show();
            }
        });
    })
</script>