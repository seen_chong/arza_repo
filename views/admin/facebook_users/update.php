<div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#account-info-tab" aria-controls="general" role="tab"
                                                  data-toggle="tab">Account Information</a></li>
        <li role="presentation"><a href="#credit-list-tab" aria-controls="credit" role="tab" data-toggle="tab">Credit
                History</a></li>
        <li role="presentation"><a href="#session-list-tab" aria-controls="session" role="tab" data-toggle="tab">Sessions</a>
        </li>
    </ul>
    <form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $model->user->id; ?>"/>
        <input type="hidden" name="insert_time" value="<?php echo $model->user->insert_time; ?>"/>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="account-info-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Account Information</h4>

                            <div class="form-group">
                                <label>First name</label>
                                <?php echo $model->form->editorFor('first_name'); ?>
                            </div>
                            <div class="form-group">
                                <label>Last name</label>
                                <?php echo $model->form->editorFor('last_name'); ?>
                            </div>
                            <div class="form-group">
                                <label>Email Address</label>
                                <?php echo $model->form->editorFor('email'); ?>
                            </div>
                            <div class="form-group">
                                <label>Facebook ID</label>
<!--                                --><?php //echo $model->form->editorFor('fb_id'); ?>
                                <input readonly type="text" value="<?=$model->user->fb_id?>" name="fb_id"/>

                            </div>
                            <div class="form-group">
                                <label>Company</label>
<!--                                --><?php //echo $model->form->editorFor('company'); ?>
                                <input readonly type="text" value="<?if($model->user->getCorporateParent()){echo $model->user->getCorporateParent()->company;} else{"";}?>" name="company"/>
                            </div>

                            <div class="form-group">
                                <label>Password (leave blank to keep current password)</label>
                                <input type="password" name="password"/>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div role="tabpanel" class="tab-pane" id="credit-list-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Credit Purchases</h4>
                            <table id="data-list" class="table">
                                <thead>
                                <tr>
                                    <th width="30%">Credit</th>
                                    <th width="30%">Price</th>
                                    <th width="20%">Date</th>
                                    <!--									<th width="20%">Price</th>-->
                                    <th width="15%" class="text-center">Edit</th>
                                    <th width="15%" class="text-center">Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <? if (isset($model->creditHistory)) {
                                    foreach ($model->creditHistory as $obj) {
                                        ?>
                                        <tr class="originalProducts">
                                            <td>
                                                <a href="<?php echo ADMIN_URL; ?>credits/update/<?php echo $obj->id; ?>">
                                                    <?php echo \Model\Credit::getItem($obj->credit_id)->name; ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="<?php echo ADMIN_URL; ?>credits/update/<?php echo $obj->id; ?>">
                                                    <?php echo \Model\Order::getItem($obj->order_id)->total; ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="<?php echo ADMIN_URL; ?>credits/update/<?php echo $obj->id; ?>">
                                                    <?php echo $obj->insert_time; ?>
                                                </a>
                                            </td>

                                            <td class="text-center">
                                                <a class="btn-actions"
                                                   href="<?= ADMIN_URL ?>credits/update/<?= $obj->id ?>">
                                                    <i class="icon-pencil"></i>
                                                </a>
                                            </td>
                                            <td class="text-center">
                                                <a class="btn-actions"
                                                   href="<?= ADMIN_URL ?>credits/delete/<?= $obj->id ?>?token_id=<?php echo get_token(); ?>"
                                                   onClick="return confirm('Are You Sure?');">
                                                    <i class="icon-cancel-circled"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?
                                    }
                                } ?>
                                </tbody>
                            </table>

                            <h4>Corporate Offers</h4>
                            <table id="data-list" class="table">
                                <thead>
                                <tr>
                                    <th width="30%">Corporate Name</th>
                                    <th width="30%">Credits Sent</th>
                                    <th width="20%">Date</th>
                                    <!--									<th width="20%">Price</th>-->
                                    <th width="15%" class="text-center">Edit</th>
                                    <th width="15%" class="text-center">Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <? if (isset($model->corporateCredit)) {
                                    foreach ($model->corporateCredit as $obj) {
                                        ?>
                                        <tr class="originalProducts">
                                            <td>
                                                <a href="<?php echo ADMIN_URL; ?>credits/update/<?php echo $obj->id; ?>">
                                                    <?php echo \Model\User::getItem($model->corporateEmployee->employer_id)->company ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="<?php echo ADMIN_URL; ?>credits/update/<?php echo $obj->id; ?>">
                                                    <?php echo $obj->credit; ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="<?php echo ADMIN_URL; ?>credits/update/<?php echo $obj->id; ?>">
                                                    <?php echo $obj->insert_time; ?>
                                                </a>
                                            </td>

                                            <td class="text-center">
                                                <a class="btn-actions"
                                                   href="<?= ADMIN_URL ?>credits/update/<?= $obj->id ?>">
                                                    <i class="icon-pencil"></i>
                                                </a>
                                            </td>
                                            <td class="text-center">
                                                <a class="btn-actions"
                                                   href="<?= ADMIN_URL ?>credits/delete/<?= $obj->id ?>?token_id=<?php echo get_token(); ?>"
                                                   onClick="return confirm('Are You Sure?');">
                                                    <i class="icon-cancel-circled"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?
                                    }
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>

            <div role="tabpanel" class="tab-pane" id="session-list-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Sessions</h4>
                            <table id="data-list" class="table">
                                <thead>
                                <tr>
                                    <th width="30%">Service</th>
                                    <th width="30%">Provider</th>
                                    <th width="20%">Location</th>
                                    <th width="20%">Date</th>
                                    <th width="15%" class="text-center">Edit</th>
                                    <th width="15%" class="text-center">Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <? if (isset($model->sessionHistory)) {
                                    foreach ($model->sessionHistory as $obj) {
                                        ?>
                                        <tr class="originalProducts">
                                            <td>
                                                <a href="<?php echo ADMIN_URL; ?>session_history/update/<?php echo $obj->id; ?>">
                                                    <?php echo \Model\Service::getItem($obj->service_id)->name; ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="<?php echo ADMIN_URL; ?>session_history/update/<?php echo $obj->id; ?>">
                                                    <?php echo \Model\Provider::getItem($obj->provider_id)->full_name(); ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="<?php echo ADMIN_URL; ?>session_history/update/<?php echo $obj->id; ?>">
                                                    <?php echo $obj->getLocation(); ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="<?php echo ADMIN_URL; ?>session_history/update/<?php echo $obj->id; ?>">
                                                    <?php echo $obj->date; ?>
                                                </a>
                                            </td>

                                            <td class="text-center">
                                                <a class="btn-actions"
                                                   href="<?= ADMIN_URL ?>session_history/update/<?= $obj->id ?>">
                                                    <i class="icon-pencil"></i>
                                                </a>
                                            </td>
                                            <td class="text-center">
                                                <a class="btn-actions"
                                                   href="<?= ADMIN_URL ?>session_history/delete/<?= $obj->id ?>?token_id=<?php echo get_token(); ?>"
                                                   onClick="return confirm('Are You Sure?');">
                                                    <i class="icon-cancel-circled"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?
                                    }
                                } ?>
                                </tbody>
                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
</div>
<div class="form-group">
    <button type="submit" class="btn btn-save">Save</button>
</div>
</form>
</div>

<?php footer(); ?>

<script>

    var services = <?php echo json_encode($model->provider->services); ?>;

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var img = $("<img />");
            reader.onload = function (e) {
                img.attr('src', e.target.result);
                img.attr('alt', 'Uploaded Image');
                img.attr("width", '100%');
                img.attr('height', '100%');
            };
            $(input).parent().parent().find('.preview-container').html(img);
            $(input).parent().parent().find('input[type="hidden"]').remove();

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(function () {
        $("select[name='provider_service[]']").val(services);

        $("select.multiselect").each(function (i, e) {
            //$(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });

        $("input.image").change(function () {
            readURL(this);
        });
    })

</script>