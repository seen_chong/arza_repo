<div id="page-wrapper">
<!--    <div class="row">-->
<!--        <div class="col-lg-24 dashboardnotify">-->
<!--            <div class="top_panels">-->
<!---->
<!---->
<!--                <div class="col-lg-6 col-md-6">-->
<!--                    <div class="panel panel-green">-->
<!--                        <div class="panel-heading">-->
<!--                            <div class="row">-->
<!--                                <div class="col-xs-3">-->
<!--                                    <i class="fa fa-shopping-cart fa-5x"></i>-->
<!--                                </div>-->
<!--                                <div class="col-xs-20 text-right">-->
<!--                                    <div class="huge">$--><?//=number_format($model->monthly_sum, 2)?><!--</div>-->
<!--                                    <div class="panel_text">-->
<!--                                        <p>-->
<!--                                            Monthly Sales-->
<!--                                        </p>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <a href="--><?//=ADMIN_URL?><!--orders">-->
<!--                            <div class="panel-footer viewsales">-->
<!--                                <span class="pull-left"><p>View Details</p></span>-->
<!--                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>-->
<!--                                <div class="clearfix"></div>-->
<!--                            </div>-->
<!--                        </a>-->
<!--                    </div>-->
<!--                </div>-->
<!---->
<!---->
<!--                <div class="col-lg-6 col-md-6">-->
<!--                    <div class="panel panel-orange">-->
<!--                        <div class="panel-heading">-->
<!--                            <div class="row">-->
<!--                                <div class="col-xs-3">-->
<!--                                    <i class="fa fa-line-chart fa-5x"></i>-->
<!--                                </div>-->
<!--                                <div class="col-xs-20 text-right">-->
<!---->
<!--                                    <div class="huge">555%-->
<!--                                        <!-- £--><?php //echo number_format($model->month_data->gbp_total, 0); ?><!-- -->
<!--                                    </div>-->
<!--                                    <div class="panel_text">-->
<!--                                        <p>Gross Margin</p>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <a href="--><?//=ADMIN_URL?><!--orders">-->
<!--                            <div class="panel-footer viewsales">-->
<!--                                <span class="pull-left"><p>View Details</p></span>-->
<!--                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>-->
<!--                                <div class="clearfix"></div>-->
<!--                            </div>-->
<!--                        </a>-->
<!--                    </div>-->
<!--                </div>-->
<!---->

<!---->
<!---->
<!--                <div class="col-lg-6 col-md-6">-->
<!--                    <div class="panel panel-blue">-->
<!--                        <div class="panel-heading">-->
<!--                            <div class="row">-->
<!--                                <div class="col-xs-3">-->
<!--                                    <i class="fa fa-comments fa-5x"></i>-->
<!--                                </div>-->
<!--                                <div class="col-xs-20 text-right">-->
<!---->
<!--                                    <div class="huge">--><?//= (count($model->new_orders)>0)?$model->new_orders:0; ?>
<!--                                    </div>-->
<!--                                    <div class="panel_text">-->
<!--                                        <p>New Orders</p>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <a href="--><?//=ADMIN_URL?><!--orders">-->
<!--                            <div class="panel-footer viewsales">-->
<!--                                <span class="pull-left"><p>View Details</p></span>-->
<!--                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>-->
<!--                                <div class="clearfix"></div>-->
<!--                            </div>-->
<!--                        </a>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!---->
<!--    </div> <!-- top_panels -->
<!---->
<!---->
<!--    <div class="col-md-8 chartsales">-->
<!--        <div class="panel panel-default" style="min-height: 390px;">-->
<!--            <div class="panel-heading">-->
<!--                <h3 class="panel-title"><i class="fa fa-money"></i> Monthly Sales</h3>-->
<!--            </div>-->
<!---->
<!--            <!-- html executes chart javascript -->
<!--            <div id="company-sales" style="width: 100%">-->
<!--                <canvas id="A" height="400"></canvas>-->
<!--            </div>-->
<!---->
<!--        </div>-->
<!--    </div>-->
<!---->
<!---->
<!--    <div class="col-md-8 chartgross">-->
<!--        <div class="panel panel-default" style="min-height: 390px;">-->
<!--            <div class="panel-heading">-->
<!--                <h3 class="panel-title"><i class="fa fa-area-chart"></i> Gross Margin</h3>-->
<!--            </div>-->
<!---->
<!--            <div id="gross-margin" style="width: 100%">-->
<!--                <canvas id="B" height="400"></canvas>-->
<!--            </div>-->
<!---->
<!--        </div>-->
<!--    </div>-->
<!---->
<!--    <div class="col-md-8 chartorders">-->
<!--        <div class="panel panel-default" style="min-height: 390px;">-->
<!--            <div class="panel-heading">-->
<!--                <h3 class="panel-title"><i class="fa fa-truck"></i> Monthly Orders</h3>-->
<!--            </div>-->
<!---->
<!--            <!-- html executes chart javascript -->
<!--            <div id="monthly-orders" style="width: 100%">-->
<!--                <canvas id="C" height="400"></canvas>-->
<!--            </div>-->
<!---->
<!---->
<!--        </div>-->
<!--    </div>-->


    <div class="row">
        <div class="col-lg-24 dashboardnotify">
            <div class="top_panels">
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-support fa-5x"></i>
                                </div>
                                <div class="col-xs-20 text-right">

                                    <div class="huge"><?=(count($model->new_contact)>0)?$model->new_contact:0;?></div>
                                    <div class="panel_text">
                                        <p>New Contact<?=(count($model->new_contact)>0)?'s':'';?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="<?=ADMIN_URL?>contacts">
                            <div class="panel-footer viewsales">
                                <span class="pull-left"><p>View Details</p></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-user fa-5x"></i>
                                </div>
                                <div class="col-xs-20 text-right">
                                    <div class="huge"><?=$model->members?></div>
                                    <div class="panel_text">
                                        <p>
                                            Total Members
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="<?=ADMIN_URL?>users">
                            <div class="panel-footer viewsales">
                                <span class="pull-left"><p>View Details</p></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-gift fa-5x"></i>
                                </div>
                                <div class="col-xs-20 text-right">
                                    <div class="huge">$ <?=number_format($model->total_donation,2)?></div>
                                    <div class="panel_text">
                                        <p>
                                            Total Donations
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="<?=ADMIN_URL?>donations">
                            <div class="panel-footer viewsales">
                                <span class="pull-left"><p>View Details</p></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-heart fa-5x"></i>
                                </div>
                                <div class="col-xs-20 text-right">
                                    <div class="huge">$ <?=number_format($model->total_kehilat_donation,2)?></div>
                                    <div class="panel_text">
                                        <p>
                                            Total Donations for <br>Kehilat Ra'anan
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="<?=ADMIN_URL?>donations?type=3">
                            <div class="panel-footer viewsales">
                                <span class="pull-left"><p>View Details</p></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class=" charts-list">
            <div class="col-md-24">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-link"></i> Shortcuts</h3>
                    </div>
                    <div class="panel-body">
<!--                        <div class="top_panels">-->
                            <div class="col-lg-6 col-md-6">
                                <div class="panel panel-red">
                                    <a href="<?=ADMIN_URL?>latest">
                                        <div class="panel-footer">
                                            <span class="pull-right"><p>Add a Latest</p></span>
                                            <span class="pull-left"><i class="fa fa-bullhorn"></i></span>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="panel panel-red">
                                <a href="<?=ADMIN_URL?>events">
                                    <div class="panel-footer viewsales">
                                        <span class="pull-right"><p>Add an Event</p></span>
                                        <span class="pull-left"><i class="fa fa-calendar"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="panel panel-red">
                                <a href="<?=ADMIN_URL?>news">
                                    <div class="panel-footer viewsales">
                                        <span class="pull-right"><p>Add a Press Coverage</p></span>
                                        <span class="pull-left"><i class="fa fa-newspaper-o"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="panel panel-red">
                                <a href="<?=ADMIN_URL?>blogs">
                                    <div class="panel-footer viewsales">
                                        <span class="pull-right"><p>Add a Blog</p></span>
                                        <span class="pull-left"><i class="fa fa-keyboard-o"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="panel panel-red">
                                <a href="<?=ADMIN_URL?>blog_categories">
                                    <div class="panel-footer viewsales">
                                        <span class="pull-right"><p>Add a Blog Category</p></span>
                                        <span class="pull-left"><i class="fa fa-bars"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="panel panel-red">
                                <a href="<?=ADMIN_URL?>israels">
                                    <div class="panel-footer viewsales">
                                        <span class="pull-right"><p>Add a Statement</p></span>
                                        <span class="pull-left"><i class="fa fa-keyboard-o"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>

<!--                        <div class="col-lg-6 col-md-6">-->
<!--                            <div class="panel panel-red">-->
<!--                                <a href="--><?//=ADMIN_URL?><!--contents">-->
<!--                                    <div class="panel-footer viewsales">-->
<!--                                        <span class="pull-right"><p>Add a Content</p></span>-->
<!--                                        <span class="pull-left"><i class="fa fa-check"></i></span>-->
<!--                                        <div class="clearfix"></div>-->
<!--                                    </div>-->
<!--                                </a>-->
<!--                            </div>-->
<!--                        </div>-->
                        <div class="col-lg-6 col-md-6">
                            <div class="panel panel-red">
                                    <div class="panel-footer viewsales">
                                        <span class="pull-right">
                                            <select name="value" data-config_id="<?=$model->statement->id?>">
                                                <?foreach (\Model\Config::$status as $key=>$value){
                                                    $select = $model->statement->value == $key? " selected='selected'" : ""?>
                                                    <option value="<?=$key?>" <?=$select?>><?=$value?></option>
                                                <?}?>
                                            </select>
                                        </span>
                                        <span class="pull-left"><i class="fa fa-home"></i> Statement</span>
                                        <div class="clearfix"></div>
                                    </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="panel panel-red">
                                <a href="<?=ADMIN_URL?>donate/campaign">
                                    <div class="panel-footer viewsales">
                                        <span class="pull-right"><p>Add a Donate Campaign</p></span>
                                        <span class="pull-left"><i class="fa fa-gift"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="panel panel-red">
                                <a href="<?=ADMIN_URL?>donate/landing">
                                    <div class="panel-footer viewsales">
                                        <span class="pull-right"><p>Donate Landing Page</p></span>
                                        <span class="pull-left"><i class="fa fa-wrench"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="panel panel-red">
                                <a href="<?=ADMIN_URL?>homes">
                                    <div class="panel-footer viewsales">
                                        <span class="pull-right"><p>Update Home Page</p></span>
                                        <span class="pull-left"><i class="fa fa-home"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>

<!--                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <div class="row">


        <div class=" charts-list">
            <div class="col-md-12">
                <div class="panel panel-default" style="min-height: 290px;">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-money"></i> Recent Donations</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped tablesorter">
                                <thead>
                                <tr>
                                    <th class="header">Name</th>
                                    <th class="header">Date</th>
                                    <th class="header">Member</th>
                                    <th class="header">Amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($model->recent_donations as $donation): ?>
                                    <tr>
                                        <td><a href="<?=ADMIN_URL?>donations/update/<?=$donation->id?>"><?php echo $donation->full_name(); ?></a></td>
                                        <td><a href="<?=ADMIN_URL?>donations/update/<?=$donation->id?>"><?php echo date("m/d/Y", strtotime($donation->insert_time)); ?></a></td>
                                        <td><a href="<?=ADMIN_URL?>donations/update/<?=$donation->id?>">
                                                <?if($donation->user_id==0){?>
                                                    Guest
                                                <?}else{?>
                                                    <?=\Model\User::getItem($donation->user_id)->getMemberType();?>
                                                <?}?>
                                            </a></td>
                                        <td><a href="<?=ADMIN_URL?>donations/update/<?=$donation->id?>">$<?= number_format($donation->amount,2);?></a></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo ADMIN_URL; ?>donations">View All Donations <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-users"></i> Recent Registers</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped tablesorter">
                            <thead>
                            <tr>
                                <th class="header">Full Name </th>
                                <th class="header">Email </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($model->recent_users as $user): ?>
                                <tr>
                                    <td><a href="<?=ADMIN_URL?>users/update/<?=$user->id?>"><?php echo $user->full_name(); ?></a></td>
                                    <td><a href="<?=ADMIN_URL?>users/update/<?=$user->id?>"><?php echo $user->email; ?></a></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="text-right">
                        <a href="<?php echo ADMIN_URL; ?>users">View All Users <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-envelope"></i> Recent Newsletters</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped tablesorter">
                            <thead>
                            <tr>
                                <th class="header">Email Address </th>
                                <th class="header">Date </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($model->recent_newsletters as $newsletter): ?>
                                <tr>
                                    <td><a href="<?=ADMIN_URL?>newsletters/update/<?=$newsletter->id?>"><?php echo $newsletter->email; ?></a></td>
                                    <td><a href="<?=ADMIN_URL?>newsletters/update/<?=$newsletter->id?>"><?php echo date("m/d/Y g:iA",  strtotime($newsletter->insert_time)) ?></a></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="text-right">
                        <a href="<?php echo ADMIN_URL; ?>users">View All Newsletters <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-calendar"></i> Recent Events</h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped tablesorter">
                            <thead>
                            <tr>
                                <th class="header">Event Name </th>
                                <th class="header">Start Date </th>
                                <th class="header">Capacity </th>
                                <th class="header">RSVP </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($model->events as $event): ?>
                                <tr>
                                    <td><a href="<?=ADMIN_URL?>events/update/<?=$event->id?>"><?php echo $event->name; ?></a></td>
                                    <td><a href="<?=ADMIN_URL?>events/update/<?=$event->id?>"><?php echo date("m/d/Y g:iA",  $event->start_time); ?></a></td>
                                    <td><a href="<?=ADMIN_URL?>events/update/<?=$event->id?>"><?php echo $event->capacity ?></a></td>
                                    <td><a href="<?=ADMIN_URL?>events/update/<?=$event->id?>"><?php echo $event->getRsvpNumber(); ?></a></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="text-right">
                        <a href="<?php echo ADMIN_URL; ?>users">View All Events <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

<script>
//    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
//    var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
//    var dataSet = <?//=json_encode($model->graphsData)?>//;
//
//    var months = [];
//    for (key in dataSet.previous){
//        var month = new Date(key.split('-')[0], key.split('-')[1]-1);
//        months.push(monthNames[month.getMonth()]);
//    }
//
//    var set = [[],[]];
//    for (key in dataSet.previous){
//        set[0].push(dataSet.previous[key].monthly_sales);
//    }
//    for (key in dataSet.current){
//        set[1].push(dataSet.current[key].monthly_sales);
//    }
//    var barChartData1 = {
//        labels : months,
//        datasets : [
//            {
//                fillColor : "rgba(220,220,220,0.5)",
//                strokeColor : "rgba(220,220,220,0.8)",
//                highlightFill: "rgba(220,220,220,0.75)",
//                highlightStroke: "rgba(220,220,220,1)",
//                data : set[0]
//            },
//            {
//                fillColor : "rgba(151,187,205,0.5)",
//                strokeColor : "rgba(151,187,205,0.8)",
//                highlightFill : "rgba(151,187,205,0.75)",
//                highlightStroke : "rgba(151,187,205,1)",
//                data : set[1]
//            }
//        ]
//
//    }
//
//    var lineChartData = {
//        labels : months,
//        datasets : [
//            {
//                label: "Previous",
//                fillColor : "rgba(220,220,220,0.2)",
//                strokeColor : "rgba(220,220,220,1)",
//                pointColor : "rgba(220,220,220,1)",
//                pointStrokeColor : "#fff",
//                pointHighlightFill : "#fff",
//                pointHighlightStroke : "rgba(220,220,220,1)",
//                data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
//            },
//            {
//                label: "Current",
//                fillColor : "rgba(151,187,205,0.2)",
//                strokeColor : "rgba(151,187,205,1)",
//                pointColor : "rgba(151,187,205,1)",
//                pointStrokeColor : "#fff",
//                pointHighlightFill : "#fff",
//                pointHighlightStroke : "rgba(151,187,205,1)",
//                data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
//            }
//        ]
//
//    }
//
//    set = [[],[]];
//    for (key in dataSet.previous){
//        set[0].push(dataSet.previous[key].monthly_orders);
//    }
//    for (key in dataSet.current){
//        set[1].push(dataSet.current[key].monthly_orders);
//    }
//    var barChartData2 = {
//        labels : months,
//        datasets : [
//            {
//                fillColor : "rgba(220,220,220,0.5)",
//                strokeColor : "rgba(220,220,220,0.8)",
//                highlightFill: "rgba(220,220,220,0.75)",
//                highlightStroke: "rgba(220,220,220,1)",
//                data : set[0]
//            },
//            {
//                fillColor : "rgba(151,187,205,0.5)",
//                strokeColor : "rgba(151,187,205,0.8)",
//                highlightFill : "rgba(151,187,205,0.75)",
//                highlightStroke : "rgba(151,187,205,1)",
//                data : set[1]
//            }
//        ]
//
//    }
//
//    window.onload = function(){
//        var ctxB = document.getElementById("B").getContext("2d");
//        window.myLine = new Chart(ctxB).Line(lineChartData, {
//            responsive: true
//        });
//        var ctxA = document.getElementById("A").getContext("2d");
//        window.myBar = new Chart(ctxA).Bar(barChartData1, {
//            responsive : true
//        });
//        var ctxC = document.getElementById("C").getContext("2d");
//        window.myBar = new Chart(ctxC).Bar(barChartData2, {
//            responsive : true
//        });
//    }
$(document).ready(function () {
    $('select[name="value"]').on('change',function(e){
        var value = $(this).val();
        var id = $(this).attr('data-config_id');
        $.post('/dashboard/updateConfig',{value:value,id:id},function (e) {
           if(e.status == 'success'){
               alert('Success');
               location.reload();
           }else{
               alert('Fail, please try again');
               location.reload();

           }
        })

    })
})
</script>
<?php echo footer(); ?>