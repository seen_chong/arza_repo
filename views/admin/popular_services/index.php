<?php
if (count($model->popular_services) > 0): ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="10%">Image</th>
                <th width="5%">Title</th>
                <th width="5%">Service</th>
                <th width="20%">Description</th>
                <th width="5%">URL</th>
                <th width="5%">Display</th>
                <th width="15%" class="text-center">Edit</th>
                <th width="15%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->popular_services as $obj) { ?>
                <tr>
                    <td><a href="<?php echo ADMIN_URL; ?>popular_services/update/<?php echo $obj->id; ?>"><img src="<?=S3_URL?><?php echo $obj->featured_image; ?>" height="60px"/></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>popular_services/update/<?php echo $obj->id; ?>"><?php echo $obj->title; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>popular_services/update/<?php echo $obj->id; ?>"><?php echo $obj->getService()->name; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>popular_services/update/<?php echo $obj->id; ?>"><?php echo $obj->description; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>popular_services/update/<?php echo $obj->id; ?>"><?php echo $obj->url; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>popular_services/update/<?php echo $obj->id; ?>"><?php echo \Model\Popular_Service::$display_state[$obj->display_state]; ?></a></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?= ADMIN_URL ?>popular_services/update/<?= $obj->id ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?= ADMIN_URL ?>popular_services/delete/<?= $obj->id ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'popular_services';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>