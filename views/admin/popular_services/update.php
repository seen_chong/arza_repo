<div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#account-info-tab" aria-controls="general" role="tab" data-toggle="tab">Account Information</a>
        </li>
    </ul>
    <form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $model->popular_service->id; ?>"/>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="account-info-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Account Information</h4>

                            <div class="form-group">
                                <label>Title</label>
                                <? echo $model->form->editorFor("title"); ?>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <? echo $model->form->textAreaFor("description", ['row'=>4]); ?>
                            </div>
                            <div class="form-group">
                                <label>Display</label>
                                <? echo $model->form->dropDownListFor("display_state", \Model\Popular_Service::$display_state,'',['class'=>'multiselect']); ?>
                            </div>
                            <div class="form-group">
                                <label>Service</label>
                                <select name="service_id" class="multiselect">
                                    <?foreach($model->services as $service){?>
                                        <option value="<?=$service->id?>" data-slug="<?=$service->slug?>"><?=$service->name?></option>
                                    <?}?>
                                </select>
<!--                                --><?// echo $model->form->dropDownListFor("service_id",$model->services,'', ['class'=>'multiselect']); ?>
                            </div>
                            <div class="form-group">
                                <label>Url</label>
                                <input name="url" type="text" placeholder="/service/{service}" value="<?=$model->popular_service->url?>"/>
                            </div>
                            <div class="form-group">
                                <label>Image</label>

                                <p>
                                    <small>(ideal profile photo size is 850 x 850)</small>
                                </p>
                                <p><input type="file" name="featured_image" class='image'/></p>

                                <div style="display:inline-block">
                                    <?php
                                    $img_path = "";
                                    if ($model->popular_service->featured_image != "" && \s3Bucket\s3Handler::doesObjectExist($model->popular_service->featured_image)) {
                                        $img_path = S3_URL . $model->popular_service->featured_image;
                                        ?>
                                        <div class="well well-sm pull-left"
                                             style="max-width:106.25px;max-height:106.25px;">
                                            <img src="<?php echo $img_path; ?>"/>
                                            <br/>
                                            <a href="<?= ADMIN_URL . 'banners/delete_image/' . $model->popular_service->id; ?>?photo=1"
                                               onclick="return confirm('Are you sure?');"
                                               class="btn btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="featured_image" value="<?= $model->popular_service->featured_image ?>"/>
                                        </div>
                                    <?php } ?>
                                    <div class='preview-container' style="max-width:106.25px;max-height:106.25px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
</div>
<div class="form-group">
    <button type="submit" class="btn btn-save">Save</button>
</div>
</form>
</div>

<?php footer(); ?>

<script>

    var service = <?php echo json_encode($model->popular_service->service_id); ?>;
    var display = <?php echo json_encode($model->popular_service->display_state); ?>;

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var img = $("<img />");
            reader.onload = function (e) {
                img.attr('src', e.target.result);
                img.attr('alt', 'Uploaded Image');
                img.attr("width", '100%');
                img.attr('height', '100%');
            };
            $(input).parent().parent().find('.preview-container').html(img);
            $(input).parent().parent().find('input[type="hidden"]').remove();

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("select[name='service_id']").on('change',function(){
        var slug = $(this).find(':selected').attr('data-slug');
        $('input[name=url]').val('/service/'+slug);
    });

    $(function () {
        $("select[name='service_id']").val(service);
        $("select[name='display_state']").val(display);

        $("select.multiselect").each(function (i, e) {
            //$(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });

        $("input.featured_image").change(function () {
            readURL(this);
        });
        $("input.slider_1").change(function () {
            readURL(this);
        });
        $("input.slider_2").change(function () {
            readURL(this);
        });
        $("input.slider_3").change(function () {
            readURL(this);
        });
    })

</script>