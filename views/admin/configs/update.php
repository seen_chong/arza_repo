<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->config->id; ?>"/>

    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>General</h4>

                <div class="form-group">
                    <label>Id</label>
                    <input type="text" value="<?= $model->config->id ?>" disabled="disabled"/>
                </div>
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" value="<?= $model->config->name ?>" disabled="disabled"/>
                </div>
                <?if($model->config->id == 4){?>
                    <div class="form-group">
                        <label>Value</label>
                        <select name="value">
                            <?foreach (\Model\Config::$status as $key=>$value){
                                $select = $model->config->value == $key? " selected='selected'" : ""?>
                                <option value="<?=$key?>" <?=$select?>><?=$value?></option>
                            <?}?>
                        </select>
                    </div>
                <?}else{?>
                <div class="form-group">
                    <label>Value</label>
                    <?php echo $model->form->editorFor("value"); ?>
                </div>
                <?}?>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>
</form>
<?php footer(); ?>
