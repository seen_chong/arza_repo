<?php
if (count($model->referrals) > 0): ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="15%">Time</th>
                <th width="20%">Referral</th>
                <th width="20%">New client</th>

                <th width="15%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->referrals as $obj) { ?>
                <? if ($obj->user_type_from == "provider") {
                    $from = \Model\Provider::getItem($obj->user_id_from);
                } else {
                    $from = \Model\User::getItem($obj->user_id_from);
                }
                ?>

                <? if ($obj->user_type_to == "provider") {
                    $to = \Model\Provider::getItem($obj->user_id_to);
                } else {
                    $to = \Model\User::getItem($obj->user_id_to);
                }
                ?>


                <tr>
                    <td>
                        <?= date("F d, Y g:ia", strtotime($obj->insert_time)) ?>
                    </td>
                    <td>
                        <a href="/admin/<?= $obj->user_type_from ?>s/update/<?= $from->id ?>"><?= $from->first_name ?> <?= $from->last_name ?></a>
                    </td>
                    <td>
                        <a href="/admin/<?= $obj->user_type_to ?>s/update/<?= $to->id ?>"><?= $to->first_name ?> <?= $to->last_name ?></a>
                    </td>


                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?= ADMIN_URL ?>referrals/delete/<?= $obj->id ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'referrals';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>