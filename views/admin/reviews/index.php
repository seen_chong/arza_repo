<?php
if (count($model->reviews) > 0): ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="10%">Time</th>
                <th width="10%">Client</th>
                <th width="10%">Provider</th>
                <th width="30%">Session info</th>
                <th width="30%">Review</th>
                <th width="10%">Rating</th>
                <th width="20%">Problem</th>
                <th width="15%" class="text-center">Edit</th>
                <th width="15%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->reviews as $obj) { ?>
                <? $session_info = \Model\Schedule::getItem($obj->session_id) ?>
                <? $user = \Model\User::getItem($session_info->user_id); ?>
                <? $provider = \Model\Provider::getItem($session_info->provider_id); ?>
                <? $service = \Model\Service::getItem($session_info->service); ?>
                <tr>
                    <td>
                        <?= date("F d, Y g:ia", strtotime($obj->insert_time)) ?>
                    </td>
                    <td>
                        <a href="/admin/users/update/<?= $user->id ?>"><?= $user->first_name ?> <?= $user->last_name ?></a>
                    </td>
                    <td>
                        <a href="/admin/providers/update/<?= $provider->id ?>"><?= $provider->first_name ?> <?= $provider->last_name ?></a>
                    </td>
                    <td> 1x <?= strtolower($service->name) ?> session
                        <?= date("F d, Y", strtotime($session_info->day)) ?> at <?= $session_info->time ?>
                        | <?= $session_info->session_location ?>
                    </td>
                    <td><?= $obj->review ?></td>
                    <td><?= $session_info->rating ?></td>
                    <td><? if (strlen($session_info->problem) > 0) {
                            echo $session_info->problem;
                        } else {
                            echo "No problems";
                        } ?></td>

                    <td class="text-center">
                        <a class="btn-actions" href="<?= ADMIN_URL ?>reviews/update/<?= $obj->id ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?= ADMIN_URL ?>reviews/delete/<?= $obj->id ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'reviews';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>