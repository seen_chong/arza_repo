<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#test" aria-controls="general" role="tab" data-toggle="tab">General</a>
            </li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="test">
                <input type="hidden" name="id" value="<?php echo $model->review->id; ?>"/>
                <? $session_info = \Model\Schedule::getItem($model->review->session_id) ?>
                <? $user = \Model\User::getItem($session_info->user_id); ?>
                <? $provider = \Model\Provider::getItem($session_info->provider_id); ?>
                <? $service = \Model\Service::getItem($session_info->service); ?>
                <input name="token" type="hidden" value="<?php echo get_token(); ?>"/>
                <input name="session_id" type="hidden" value="<?= $model->review->session_id ?>"/>

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>Reviews text</label>
                                <?php echo $model->form->textAreaFor("review", ["class" => "ckeditor"]); ?>
                            </div>
                            <? if (strlen($session_info->problem) > 0) { ?>
                                <div class="form-group">
                                    <label>Problem</label>
                                    <textarea name="problem"><?= $session_info->problem ?></textarea>
                                </div>
                            <? } ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Session information</h4>

                            <div class="form-group">
                                <label>User</label>
                                <a href="/admin/users/update/<?= $user->id ?>"><?= $user->first_name ?> <?= $user->last_name ?></a>
                            </div>
                            <div class="form-group">
                                <label>Provider</label>
                                <a href="/admin/providers/update/<?= $provider->id ?>"><?= $provider->first_name ?> <?= $provider->last_name ?></a>
                            </div>
                            <div class="form-group">
                                <label>Session info</label>
                                1x <?= strtolower($service->name) ?> session
                                <?= date("F d, Y", strtotime($session_info->day)) ?> at <?= $session_info->time ?>
                                | <?= $session_info->session_location ?>
                            </div>
                            <div class="form-group">
                                <label>Rating</label>
                                <?= $session_info->rating ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
<script type='text/javascript'>
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '360');
                    img.attr('height', '127.5');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
        });
    });
</script>