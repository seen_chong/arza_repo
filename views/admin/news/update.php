<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->news->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div class="tab-content">
        <div role="tabpanel">
            <div class="row">
                <div class="col-md-24">
                    <div class="box">
                        <h4>General</h4>

                        <div class="form-group">
                            <label>Title</label>
                            <?php echo $model->form->textBoxFor("title"); ?>
                        </div>
                        <div class="form-group">
                            <label>Date</label>
                            <input name="time" id="startDate" class="time" value="<?=($model->news->id)?($model->news->time):""?>" />
                        </div>
                        <div class="form-group">
                            <label>Author</label>
                            <?php echo $model->form->textBoxFor("author"); ?>
                        </div>
                        <div class="form-group">
                            <label>Url</label>
                            <?php echo $model->form->textBoxFor("url"); ?>
                        </div>
                    </div>
                    <div class="box">
                        <div class="form-group">
                            <label>News Logo</label>

                            <p>
                                <small>(ideal featured image size is 1920 x 300)</small>
                            </p>
                            <?php
                            $img_path = "";
                            if ($model->news->featured_image != "" && file_exists(UPLOAD_PATH . 'news' . DS . $model->news->featured_image)) {
                                $img_path = UPLOAD_URL . 'news/' . $model->news->featured_image;
                            }
                            ?>
                            <p><input type="file" name="featured_image" class='image'/></p>
                            <?php if ($model->news->featured_image != "") { ?>
                                <div class="well well-sm pull-left">
                                    <img src="<?php echo $img_path; ?>" width="1200"/>
                                    <br/>
                                    <a href="<?= ADMIN_URL . 'news/delete_image/' . $model->news->id . '/?featured_image=1'; ?>"
                                       onclick="return confirm('Are you sure?');"
                                       class="btn btn-default btn-xs">Delete</a>
                                    <input type="hidden" name="featured_image"
                                           value="<?= $model->news->featured_image ?>"/>
                                </div>
                            <?php } ?>
                            <div id='preview-container'></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="act_for_click" name="redirectTo" value="<?= ADMIN_URL . 'news/' ?>">
    <center style="    margin-bottom: 112px;">
        <button type="submit" id="save_close" class="btn btn-save">Save and close</button>
        <button type="submit" id="save" class="btn btn-save">Save</button>
    </center>
</form>
<?php footer(); ?>

<style>
    div.mouseover-thumbnail-holder {
        position: relative;
        display: block;
        float: left;
        margin-right: 10px;
    }

    .large-thumbnail-style {
        display: block;
        border: 2px solid #fff;
        box-shadow: 0px 0px 5px #aaa;
    }

    div.mouseover-thumbnail-holder .large-thumbnail-style {
        position: absolute;
        top: 0;
        left: -9999px;
        z-index: 1;
        opacity: 0;
        transition: opacity .5s ease-in-out;
        -moz-transition: opacity .5s ease-in-out;
        -webkit-transition: opacity .5s ease-in-out;
    }

    div.mouseover-thumbnail-holder:hover .large-thumbnail-style {
        width: 100% !important;
        top: 0;
        left: 105%;
        z-index: 1;
        opacity: 1;

    }
</style>
<script>


    $(function () {
        $('.time').datepicker( {
            format: "DD MM dd,yyyy",
            viewMode: "days",
            minViewMode: "days",
            autoclose:true,
        });

        $('#save_close').mouseover(function () {
            $("#act_for_click").val("<?= ADMIN_URL . 'news/' ?>");

        });

        $('#save').mouseover(function () {
            var id = $("input[name=id]").val();
            $("#act_for_click").val("<?=ADMIN_URL . 'news/update/'?>" + id);

        });


    });
    $("#phone-us").inputmask("+1(999)999-9999");
    $(document).ready(function () {

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '200');
                    img.attr('height', '150');
                    $("#preview-container").html(img);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
            $('#previewupload').show();
        });


    });

</script>






























