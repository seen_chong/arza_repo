<!--<div class="row">-->
<!--    <div class="col-md-8">-->
<!--        <div class="box">-->
<!--            <div class="input-group">-->
<!--                <input id="search" type="text" name="search" class="form-control" placeholder="Search by Id or Name"/>-->
<!--            <span class="input-group-addon">-->
<!--                <i class="icon-search"></i>-->
<!--            </span>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="col-md-8">-->
<!--        <div class="box">-->
<!--            Show on page:-->
<!--            <select class="how_many" name="how_many" style="cursor:pointer">-->
<!---->
<!--                <option --><?// if (isset($_GET['how_many'])) {
//                    if ($_GET['how_many'] == 10) {
//                        echo "selected";
//                    }
//                } ?><!-- selected value="10">10-->
<!--                </option>-->
<!--                <option --><?// if (isset($_GET['how_many'])) {
//                    if ($_GET['how_many'] == 50) {
//                        echo "selected";
//                    }
//                } ?><!-- value="50">50-->
<!--                </option>-->
<!--                <option --><?// if (isset($_GET['how_many'])) {
//                    if ($_GET['how_many'] == 100) {
//                        echo "selected";
//                    }
//                } ?><!-- value="100">100-->
<!--                </option>-->
<!--                <option --><?// if (isset($_GET['how_many'])) {
//                    if ($_GET['how_many'] == 500) {
//                        echo "selected";
//                    }
//                } ?><!-- value="500">500-->
<!--                </option>-->
<!--                <option --><?// if (isset($_GET['how_many'])) {
//                    if ($_GET['how_many'] == 1000) {
//                        echo "selected";
//                    }
//                } ?><!-- value="1000">1000-->
<!--                </option>-->
<!--            </select>-->
<!---->
<!--        </div>-->
<!---->
<!---->
<!--    </div>-->
<!--    <script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
<!--</div>-->
<!--<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>-->
<!---->
<!--<script>-->
<!--    $(document).ready(function () {-->
<!--        $('body').on('change', '.how_many', function () {-->
<!--            var how_many = $(this).val();-->
<!--            window.location.href = '/admin/donations?how_many=' + how_many;-->
<!--        });-->
<!--    });-->
<!--</script>-->
<?php if (count($model->news) > 0) { ?>
    <div class="box box-table">
        <table id="data-list" class="table">
            <thead>
            <tr>
<!--                <th width="5%"></th>-->
                <th>Title</th>
                <th>Date</th>
                <th>Author</th>
                <th>Edit</th>
                <th>Delete</th>

            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->news as $obj) { ?>
                <tr class="originalProducts">
<!--                    <td>-->
<!--                        --><?// if (is_null($obj->viewed) || !$obj->viewed) { ?>
<!--                            <button type="button" class="btn btn-success" disabled="disabled">New</button>-->
<!--                        --><?// } ?>
<!--                    </td>-->
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>news/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->title; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>news/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->time ; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>news/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->author; ?>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>news/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>news/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>


                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>

<?php footer(); ?>

<script>
    var site_url = '<?= ADMIN_URL.'donations';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script type="text/javascript">
    $(function () {
        $("#search").keyup(function () {
            var url = "<?php echo ADMIN_URL; ?>donations/search";
            var keywords = $(this).val();
            if (keywords.length > 0) {
                $.get(url, {keywords: keywords}, function (data) {
                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').hide();
                    $('.originalProducts').hide();
                    var list = JSON.parse(data);
                    for (key in list) {
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html(list[key].id);
                        $('<td />').appendTo(tr).html(list[key].insert_time);
                        $('<td />').appendTo(tr).html(list[key].amount);
                        $('<td />').appendTo(tr).html(list[key].name);
                        $('<td />').appendTo(tr).html(list[key].email);
                        $('<td />').appendTo(tr).html(list[key].phone);
                        $('<td />').appendTo(tr).html(list[key].type);
//                        var editTd = $('<td />').addClass('text-center').appendTo(tr);
//                        var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?//= ADMIN_URL;?>//orders/update/' + list[key].id);
//                        var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                        tr.appendTo($("#data-list tbody"));
                    }

                });
            } else {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').show();
                $('.originalProducts').show();
            }
        });
    })
</script>