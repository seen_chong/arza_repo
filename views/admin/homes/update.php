<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->home->id ?>"/>
    <input type="hidden" name="display_order" value="<?php echo $model->home->display_order ?>"/>


    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <h4>General</h4>
                <div class="form-group">
                    <label>Section Name</label>
                    <input type="text" value="<?= $model->home->section_name ?>" name="section_name" readonly/>
                </div>
                <div class="form-group">
                    <label>Title</label>
                    <?php echo $model->form->editorFor("title"); ?>
                </div>
                <?if($model->home->section_name!="Splash"){?>
                <div class="form-group">
                    <label>Description</label>
                    <?php echo $model->form->textAreaFor("description", ["class" => "ckeditor"]); ?>
                </div>
                <?}else{?>
                    <div class="form-group">
                        <label>Subtitle</label>
                        <?php echo $model->form->editorFor("subtitle"); ?>
                    </div>
                    <div class="form-group">
                        <label>Link</label>
                        <?php echo $model->form->editorFor("link"); ?>
                    </div>
                 <?}?>
                <div class="form-group">
                    <label>Display Order</label>
                    <?php echo $model->form->editorFor("display_order"); ?>
                </div>
                <div class="form-group">
                    <label>
                        <?php echo $model->form->checkBoxFor("display", 1); ?>  Display?
                    </label>
                </div>

            </div>
        </div>
<?if($model->home->section_name!="Splash"){?>
        <div class="col-md-12">
            <div class="box">
                <div class="form-group">
                    <label>Link</label>
                    <?php echo $model->form->editorFor("link"); ?>
                </div>
                <?if($model->home->section_name=='Section 1'){?>
                    <div class="form-group">
                        <label>Button</label>
                        <?php echo $model->form->editorFor("button"); ?>
                    </div>
                <?}?>
                <div class="form-group">
                    <label>Subtitle</label>
                    <?php echo $model->form->editorFor("subtitle"); ?>
                </div>
                <div class="form-group">
                    <?if($model->home->section_name == "Section 3"){?>
                        <label>Button</label>
                        <?php echo $model->form->editorFor("sub_description"); ?>
                    <?}else{?>
                        <label>SubDescription</label>
                        <?php echo $model->form->textAreaFor("sub_description", ["class" => "ckeditor"]); ?>
                    <?}?>

                </div>
            </div>
            <?if($model->home->section_name == "Section 3"){?>
            <div class="box">
                <h4>About Part</h4>
                <div class="form-group">
                    <label>Title</label>
                    <?php echo $model->form->editorFor("about"); ?>
                </div>
                <div class="form-group">
                    <label>Sub Title</label>
                    <?php echo $model->form->editorFor("sub_about"); ?>
                </div>
                <div class="form-group">
                    <label>Link</label>
                    <?php echo $model->form->editorFor("about_link"); ?>
                </div>
            </div>
            <?}?>
        </div>
    </div>
    <?}?>
    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>Meta Information</h4>

                <div class="form-group">
                    <label>Featured image</label>

                    <p>
                        <small>(ideal featured image size is 1920 x 300)</small>
                    </p>
                    <?php
                    $img_path = "";
                    if ($model->home->featured_image != "" && file_exists(UPLOAD_PATH . 'homes' . DS . $model->home->featured_image) ) {
                        $img_path = UPLOAD_URL . 'homes/' . $model->home->featured_image;
                    }
                    ?>
                    <p><input type="file" name="featured_image" class='image'/></p>
                    <?php if ($model->home->featured_image != "") { ?>
                        <div class="well well-sm pull-left">
                            <img src="<?php echo $img_path; ?>" width="1200"/>
                            <br/>
                            <a href="<?= ADMIN_URL . 'homes/delete_image/' . $model->home->id . '/?featured_image=1'; ?>"
                               onclick="return confirm('Are you sure?');"
                               class="btn btn-default btn-xs">Delete</a>
                            <input type="hidden" name="featured_image"
                                   value="<?= $model->home->featured_image ?>"/>
                        </div>
                    <?php } ?>
                    <div id='preview-container'></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <?if($model->home->section_name=="Section 2"){?>
    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <label>Other Images</label>
                <p><small>(ideal banner image size is 246 x 166)</small></p>

                <div class="dropzone" id="dropzoneForm"
                     action="<?php echo ADMIN_URL . 'homes/uploadImage/' . $model->home->id; ?>">

                </div>
                <a id="upload-dropzone" class="btn btn-danger"  href="<?= ADMIN_URL . 'homes/update/' . $model->home->id ?>">Upload</a><br />
                <br/>
            </div>
            <div>
                <table id="image-container" class="table table-sortable-container">
                    <thead>
                    <tr>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach(explode(',', $model->home->image) as $image){ ?>
                        <?php if($image){ ?>
                            <tr>
                                <td><img src="/content/uploads/homes/<?=$image?>" width="100" height="100"/></td>
                                <td><?php echo $image?></td>
                                <td><a class="btn-actions delete-product-image" href="<?= ADMIN_URL . 'homes/delete_images/' . $model->home->id . '?index='.$image;?>">
                                        <i class="icon-cancel-circled"></i></a></td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="clearfix"></div>
    </div>
    </div>
    <?}?>

        <div class="col-md-24">
            <button type="submit" class="btn btn-success btn-lg">Save</button>
        </div>
</form>


<?= footer(); ?>


<script type='text/javascript'>
    $(document).ready(function () {

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                    $("#preview-container").html(img);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
            $('#previewupload').show();
        });

        $("input[name='title']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-')
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });

    });

</script>