<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->volunteer->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div class="tab-content">
        <div role="tabpanel">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <h4>General</h4>
                        <div class="form-group">
                            <label>Name</label>
                            <?php echo $model->form->editorFor("name"); ?>
                        </div>
                        <div class="form-group">
                            <label>Link</label>
                            <?php echo $model->form->editorFor("url"); ?>
                        </div>
                        <div class="form-group">
                            <label>Display?</label>
                            <?php echo $model->form->checkBoxFor("display", 1); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="box">
                        <div class="form-group">
                            <label>Description</label>
                            <?php echo $model->form->textAreaFor("description", ["class" => "ckeditor"]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" id="act_for_click" name="redirectTo" value="<?= ADMIN_URL . 'volunteers/' ?>">
        <center style="    margin-bottom: 112px;">
            <button type="submit" id="save_close" class="btn btn-save">Save and close</button>
            <button type="submit" id="save" class="btn btn-save">Save</button>
        </center>
</form>
<?php footer(); ?>

<script type="text/javascript">

    $(document).ready(function () {

        $('#save_close').mouseover(function () {
            $("#act_for_click").val("<?=ADMIN_URL . 'volunteers/'?>");

        });

        $('#save').mouseover(function () {
            var id = $("input[name=id]").val();
            $("#act_for_click").val("<?=ADMIN_URL . 'volunteers/update/'?>" + id);

        });
    })


</script>




























