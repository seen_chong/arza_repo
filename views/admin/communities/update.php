<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->community->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div class="tab-content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <h4>General</h4>

                    <div class="form-group">
                        <label>Community Name</label>
                        <?php echo $model->form->editorFor("name"); ?>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <?php echo $model->form->textAreaFor("description", ["class" => "ckeditor"]); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="box">
                    <h4>Location</h4>
                    <div class="form-group">
                        <label>Address</label>
                        <?php echo $model->form->textBoxFor('address'); ?>
                    </div>
                    <div class="form-group">
                        <label>City</label>
                        <?php echo $model->form->textBoxFor('city'); ?>
                    </div>
                    <div class="form-group">
                        <label>State/Province/Region</label>
                        <?= $model->form->textBoxFor('state'); ?>
                    </div>
                    <div class="form-group">
                        <label>Zip Code</label>
                        <?php echo $model->form->textBoxFor('zip'); ?>
                    </div>
                    <div class="form-group">
                        <label>Country</label>
                        <?= $model->form->textBoxFor('country'); ?>
                    </div>
                    <button type="button" class="btn btn-default btn-get">Get LNG & LAT</button>
                    <div class="form-group">
                        <label>Longitude</label>
                        <div class="input-group">
                            <input type="text" name="lng"
                                   value="<?php echo ($model->community->id > 0) ? $model->community->lng : ""; ?>"/>
                        </div>

                    </div>
                    <div class="form-group">
                        <label>latitude</label>

                        <div class="input-group">
                            <input type="text" name="lat"
                                   value="<?php echo ($model->community->id > 0) ? $model->community->lat : ""; ?>"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <input type="hidden" id="act_for_click" name="redirectTo" value="<?= ADMIN_URL . 'communities/' ?>">
    <center style="    margin-bottom: 112px;">
        <button type="submit" id="save_close" class="btn btn-save">Save and close</button>
        <button type="submit" id="save" class="btn btn-save">Save</button>
    </center>
</form>
<?php footer(); ?>

<script>
    $(function () {

        $('#save_close').mouseover(function () {
            $("#act_for_click").val("<?=ADMIN_URL . 'communities/'?>");

        });

        $('#save').mouseover(function () {
            var id = $("input[name=id]").val();
            $("#act_for_click").val("<?=ADMIN_URL . 'communities/update/'?>" + id);

        });
    });

    $(document).ready(function () {
        $('.btn-get').on('click',function(){
            var country = $('input[name="country"]').val();
            var state = $('input[name="state"]').val();
            var city = $('input[name="city"]').val();
            var address = $('input[name="address"]').val();
            var addr = address+'-'+city+'-'+state+'-'+country;
            if(addr.length>3) {
                $.ajax(
                    {
                        Type: "GET",
                        url: "https://maps.googleapis.com/maps/api/geocode/json?address=" + addr + "&key=AIzaSyD40vQ5_kWKQtMkbr5x9aLnTSbsy6nTj5w",
                        success: function (msg) {
                            if(msg.status=='OK'){
                                var lat = msg.results[0].geometry['location'].lat;
                                var lng = msg.results[0].geometry['location'].lng;
                                $("input[name=lng]").val(lng);
                                $("input[name=lat]").val(lat);
                            }else{
                                alert('Invalid Address, please check and try again.');
                            }

                        }
                    });
            }
        })

    });

</script>






























