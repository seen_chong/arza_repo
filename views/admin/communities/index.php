
<div class="row">
    <div class="col-md-16">
        <div class="box">
            <div class="input-group">
                <input id="search" type="text" name="search" class="form-control"
                       placeholder="Search by Community Name | by Community Location | by Community Description"/>
                <span class="input-group-addon">
                <i class="icon-search"></i>
            </span>
            </div>
        </div>
    </div>
</div>

<?php if (count($model->communities) > 0) { ?>
    <a href="<?=ADMIN_URL.'communities/sort'?>">Manage Display Order</a>
    <div class="box box-table">
        <table id="data-list" class="table">
            <thead>
            <tr>
                <th><a href="/admin/communities?orderBy=name">Name <i class="fa fa-sort" aria-hidden="true"></i></th>
                <th>City</th>
                <th>State</th>
                <th>Country</th>
                <th><a href="/admin/communities?orderBy=display">Display <i class="fa fa-sort" aria-hidden="true"></i></a></th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>

            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->communities as $obj) { ?>
                <tr class="originalProducts">
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>communities/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->name; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>communities/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->city; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>communities/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->state; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>communities/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->country; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>communities/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->display_order; ?>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>communities/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>communities/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>

                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>

<?php footer(); ?>

<script>
    var site_url = '/admin/communities?<?if (isset($_GET['orderBy'])){echo "orderBy=";echo $_GET['orderBy'] ;echo"&";}?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script type="text/javascript">
    $(function () {
        $("#search").keyup(function () {
            var url = "<?php echo ADMIN_URL; ?>communities/search";
            var keywords = $(this).val();
            if (keywords.length > 0) {
                $.get(url, {keywords: keywords}, function (data) {
                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').hide();
                    $('.originalProducts').hide();
                    var list = JSON.parse(data);
                    for (key in list) {
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>communities/update/' + list[key].id).html(list[key].name));
                        $('<td />').appendTo(tr).html(list[key].city);
                        $('<td />').appendTo(tr).html(list[key].state);
                        $('<td />').appendTo(tr).html(list[key].country);
                        $('<td />').appendTo(tr).html(list[key].display_order);
                        var editTd = $('<td />').addClass('text-center').appendTo(tr);
                        var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>communities/update/' + list[key].id);
                        var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                        var deleteTd = $('<td />').addClass('text-center').appendTo(tr);
                        var deleteLink = $('<a />').appendTo(deleteTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>communities/delete/' + list[key].id);
                        deleteLink.click(function () {
                            return confirm('Are You Sure?');
                        });
                        var deleteIcon = $('<i />').appendTo(deleteLink).addClass('icon-cancel-circled');
                        tr.appendTo($("#data-list tbody"));
                    }

                });
            } else {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').show();
                $('.originalProducts').show();
            }
        });
    })
</script>