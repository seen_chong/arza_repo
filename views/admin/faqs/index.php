<style>
    body.dragging, body.dragging * {
        cursor: move !important;
    }

    .dragged {
        position: absolute;
        opacity: 0.5;
        z-index: 2000;
    }
</style>

<?php
if (count($model->faqs) > 0): ?>
    <div class="box box-table">
        <table class="table" id="sortables">
            <thead>
            <tr>
                <th width="30%">Title</th>
                <th width="40%">Description</th>
                <th width="5%">Display Order</th>
                <th width="15%" class="text-center">Edit</th>
                <th width="15%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->faqs as $obj) { ?>
                <tr data-id="<?=$obj->id?>">
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>faqs/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->title; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>faqs/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->description; ?>
                        </a>
                    </td>
                    <td class="display-order-td">
                        <a href="<?php echo ADMIN_URL; ?>faqs/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->display_order; ?>
                        </a>
                    </td>

                    <td class="text-center">
                        <a class="btn-actions" href="<?= ADMIN_URL ?>faqs/update/<?= $obj->id ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?= ADMIN_URL ?>faqs/delete/<?= $obj->id ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php endif; ?>

<?php echo footer(); ?>
<script>
    $(document).ready(function(){
        function sort_number_display() {
            var counter = 1;
            $('#sortables').find('>tbody>tr').each(function (i, e) {
                $(e).find('td.display-order-td').html(counter);
                counter++;
            });
        }
        var adjustment;
        $('#sortables').sortable({
         containerSelector: 'table',
         itemPath: '> tbody',
         itemSelector: 'tr',
         placeholder: '<tr class="placeholder"><td style="visibility: hidden">.</td></tr>',
         onDrop: function ($item, container, _super) {
             var ids = [];
             var tr_containers = $("#sortables > tbody > tr");
             tr_containers.each(function (i, e) {
                 ids.push($(e).data("id"));

             });
             $.post(site_url + '/sort_images', {ids: ids}, function (response) {
                 sort_number_display();
             });
         _super($item,container);
         },
         onDragStart: function ($item, container, _super) {
         var offset = $item.offset(),
         pointer = container.rootGroup.pointer;

         adjustment = {
         left: pointer.left - offset.left,
         top: pointer.top - offset.top
         };

         _super($item, container);
         },
         onDrag: function ($item, position) {
         $item.css({
         left: position.left - adjustment.left,
         top: position.top - adjustment.top
         });
         }
         });
    })
</script>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'faqs';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
