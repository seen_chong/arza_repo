<div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#account-info-tab" aria-controls="general" role="tab" data-toggle="tab">Account Information</a></li>
    </ul>
    <form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $model->faq->id; ?>"/>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="account-info-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Account Information</h4>

                            <div class="form-group">
                                <label>Title</label>
                                <? echo $model->form->editorFor("title"); ?>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <? echo $model->form->textAreaFor("description", ['rows'=>4]); ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
</div>
<div class="form-group">
    <button type="submit" class="btn btn-save">Save</button>
</div>
</form>
</div>

<?php footer(); ?>

<script>

    var services = <?php echo json_encode($model->provider->services); ?>;

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var img = $("<img />");
            reader.onload = function (e) {
                img.attr('src', e.target.result);
                img.attr('alt', 'Uploaded Image');
                img.attr("width", '100%');
                img.attr('height', '100%');
            };
            $(input).parent().parent().find('.preview-container').html(img);
            $(input).parent().parent().find('input[type="hidden"]').remove();

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(function () {
        $("select[name='provider_service[]']").val(services);

        $("select.multiselect").each(function (i, e) {
            //$(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });

        $("input.featured_image").change(function () {
            readURL(this);
        });
        $("input.slider_1").change(function () {
            readURL(this);
        });
        $("input.slider_2").change(function () {
            readURL(this);
        });
        $("input.slider_3").change(function () {
            readURL(this);
        });
    })

</script>