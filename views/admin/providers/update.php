<div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#account-info-tab" aria-controls="general" role="tab"
                                                  data-toggle="tab">Account Information</a></li>
        <li role="presentation"><a href="#profile-info-tab" aria-controls="profile-info" role="tab" data-toggle="tab">Profile
                Information</a></li>
        <li role="presentation"><a href="#description-tab" aria-controls="description" role="tab" data-toggle="tab">Description</a>
        </li>
        <li role="presentation"><a href="#packages-tab" aria-controls="packages" role="tab" data-toggle="tab">Payout</a>
        </li>
        <li role="presentation"><a href="#sessions-tab" aria-controls="sessions" role="tab"
                                   data-toggle="tab">Sessions</a></li>
    </ul>
    <form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $model->provider->id; ?>"/>
        <input type="hidden" name="step_2_completed" value="<?php echo $model->provider->step_2_completed; ?>"/>
        <input type="hidden" name="auth" value="<?php echo $model->provider->authorized; ?>"/>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="account-info-tab"><? /*dd($model)*/ ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Account Information</h4>

                            <div class="form-group">
                                <?php echo $model->form->checkBoxFor('authorized', 1); ?>
                                <label>Authorized</label>
                            </div>
                            <div class="form-group">
                                <label>Email Address</label>
                                <?php echo $model->form->editorFor('email'); ?>
                            </div>
                            <div class="form-group">
                                <label>First Name</label>
                                <?php echo $model->form->editorFor('first_name'); ?>
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <?php echo $model->form->editorFor('last_name'); ?>
                            </div>
                            <div class="form-group">
                                <label>Payout Rate($)</label>
                                <?php echo $model->form->editorFor('payout_rate'); ?>
                            </div>
                            <div class="form-group">
                                <label>Password (leave blank to keep current password)</label>
                                <input type="password" name="password"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Contact Details</h4>

                            <div class="form-group">
                                <label>Zip Code</label>
                                <?php echo $model->form->editorFor('zip'); ?>
                            </div>

                            <div class="form-group">
                                <label>Phone Number</label>
                                <?php echo $model->form->editorFor('phone'); ?>
                            </div>
                            <div class="form-group">
                                <label>Independent contractor</label>
                                <br>
                                <? if (strlen($model->provider->agreed_contractor_agreement)) { ?>
                                    <?php echo date("F d, Y \a\\t g:ia", strtotime($model->provider->agreed_contractor_agreement));
                                } ?>
                            </div>
                            <div class="form-group">
                                <label>Agreed manifesto and ethics</label>
                                <br>
                                <? if (strlen($model->provider->agreed_manifesto_ethics)) { ?>
                                    <?php echo date("F d, Y \a\\t g:ia", strtotime($model->provider->agreed_manifesto_ethics));
                                } ?>
                            </div>
                            <div class="form-group">
                                <label>Candidate id for checkr</label>
                                <br>
                                <?php echo $model->provider->candidate_id; ?>
                            </div>
                            <div class="form-group">
                                <label>Report id</label>
                                <br>
                                <?php echo $model->provider->report_id; ?>
                            </div>
                            <div class="form-group">
                                <label>E-signature</label><br>
                                <? if (strlen($model->provider->e_signature) > 0) { ?>
                                    <img src="<?=S3_URL?><?php echo $model->provider->e_signature; ?>">
                                <? } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="profile-info-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Profile Information</h4>

                            <div class="form-group">
                                <label>Profile Photo</label>

                                <p>
                                    <small>(ideal profile photo size is 850 x 850)</small>
                                </p>
                                <p><input type="file" name="photo" class='image'/></p>

                                <div style="display:inline-block">
                                    <?php
                                    $img_path = "";
                                    if ($model->provider->photo != "" && \s3Bucket\s3Handler::doesObjectExist($model->provider->photo)) {
                                        $img_path = S3_URL . $model->provider->photo;
                                        ?>
                                        <div class="well well-sm pull-left"
                                             style="max-width:106.25px;max-height:106.25px;">
                                            <img src="<?php echo $img_path; ?>"/>
                                            <br/>
                                            <a href="<?= ADMIN_URL . 'providers/delete_image/' . $model->provider->id; ?>?photo=1"
                                               onclick="return confirm('Are you sure?');"
                                               class="btn btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="photo" value="<?= $model->provider->photo ?>"/>
                                        </div>
                                    <?php } ?>
                                    <div class='preview-container'
                                         style="max-width:106.25px;max-height:106.25px;"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Birthday</label>

                                <div class="row">
                                    <div class="col-md-10">
                                        <label>Month</label>
                                        <?php echo $model->form->dropDownListFor('mob', get_month(), '', ['class' => 'form-control']); ?>
                                    </div>
                                    <div class="col-md-7">
                                        <label>Day</label>
                                        <?php echo $model->form->dropDownListFor('dob', range(1, 31), '', ['class' => 'form-control']); ?>
                                    </div>
                                    <div class="col-md-7">
                                        <label>Year</label>
                                        <?php echo $model->form->dropDownListFor('yob', range(date('Y') - 20, date('Y') - 100), '', ['class' => 'form-control']); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Experience</label>
                                <?
                                $experience = [
                                    1 => '1 year',
                                    2 => '2 years',
                                    3 => '3 years',
                                    4 => '4 to 5 years',
                                    6 => '6 to 7 years',
                                    8 => '8 to 9 years',
                                    10 => '10 or more years',
                                ];
                                ?>
                                <?php echo $model->form->dropDownListFor('experience', $experience, '', ['class' => 'form-control']); ?>
                            </div>
                        </div>
                        <div class="box">
                            <h4>Services</h4>
                            <select name="provider_service[]" class="multiselect" data-placeholder="Services"
                                    multiple="multiple">
                                <? foreach ($model->services as $service) { ?>
                                    <option value="<?= $service->id ?>"><?= $service->name ?></option>
                                <? } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Testimonials</h4>

                            <div class="form-group">
                                <?php echo $model->form->textareaFor('testimonial_1'); ?>
                            </div>
                            <div class="form-group">
                                <label>Reference</label>
                                <?php echo $model->form->editorFor('testimonial_1_reference'); ?>
                            </div>
                        </div>
                        <div class="box">
                            <div class="form-group">
                                <?php echo $model->form->textareaFor('testimonial_2'); ?>
                            </div>
                            <div class="form-group">
                                <label>Reference</label>
                                <?php echo $model->form->editorFor('testimonial_2_reference'); ?>
                            </div>
                        </div>
                        <div class="box">
                            <div class="form-group">
                                <?php echo $model->form->textareaFor('testimonial_3'); ?>
                            </div>
                            <div class="form-group">
                                <label>Reference</label>
                                <?php echo $model->form->editorFor('testimonial_3_reference'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="description-tab">
                <div class="row">
                    <div class="col-md-8">
                        <div class="box">
                            <div class="form-group">
                                <label>Bio Description</label>
                                <?php echo $model->form->textareaFor('description'); ?>
                            </div>
                            <div class="form-group">
                                <label>Teaching Style</label>
                                <?php echo $model->form->textareaFor('teaching_description'); ?>
                            </div>
                            <div class="form-group">
                                <label>Promise Statement</label>
                                <?php echo $model->form->textareaFor('promise_statement'); ?>
                            </div>
                            <div class="form-group">
                                <label>Description 1</label>
                                <?php echo $model->form->textareaFor('description_1'); ?>
                            </div>
                            <div class="form-group">
                                <label>Description 2</label>
                                <?php echo $model->form->textareaFor('description_2'); ?>
                            </div>
                            <div class="form-group">
                                <label>Description 3</label>
                                <?php echo $model->form->textareaFor('description_3'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="box">
                            <div class="form-group">
                                <label>Lifestyle</label>
                                <?php echo $model->form->textareaFor('lifestyle'); ?>
                            </div>
                            <div class="form-group">
                                <label>Extra Photo 1</label>

                                <p><input type="file" name="extra_photo_1" class='image'/></p>

                                <div style="display:inline-block">
                                    <?php
                                    $img_path = "";
                                    if ($model->provider->extra_photo_1 != "" && \s3Bucket\s3Handler::doesObjectExist($model->provider->extra_photo_1)) {
                                        $img_path = S3_URL . $model->provider->extra_photo_1;
                                        ?>
                                        <div class="well well-sm pull-left"
                                             style="max-width:106.25px;max-height:106.25px;">
                                            <img src="<?php echo $img_path; ?>"/>
                                            <br/>
                                            <a href="<?= ADMIN_URL . 'providers/delete_image/' . $model->provider->id; ?>?type=extra_photo_1"
                                               onclick="return confirm('Are you sure?');"
                                               class="btn btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="extra_photo_1"
                                                   value="<?= $model->provider->extra_photo_1 ?>"/>
                                        </div>
                                    <?php } ?>
                                    <div class='preview-container'
                                         style="max-width:106.25px;max-height:106.25px;"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Extra Photo 2</label>

                                <p><input type="file" name="extra_photo_2" class='image'/></p>

                                <div style="display:inline-block">
                                    <?php
                                    $img_path = "";
                                    if ($model->provider->extra_photo_2 != "" && \s3Bucket\s3Handler::doesObjectExist($model->provider->extra_photo_2)) {
                                        $img_path = S3_URL . $model->provider->extra_photo_2;
                                        ?>
                                        <div class="well well-sm pull-left"
                                             style="max-width:106.25px;max-height:106.25px;">
                                            <img src="<?php echo $img_path; ?>"/>
                                            <br/>
                                            <a href="<?= ADMIN_URL . 'providers/delete_image/' . $model->provider->id; ?>?type=extra_photo_2"
                                               onclick="return confirm('Are you sure?');"
                                               class="btn btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="extra_photo_2"
                                                   value="<?= $model->provider->extra_photo_2 ?>"/>
                                        </div>
                                    <?php } ?>
                                    <div class='preview-container'
                                         style="max-width:106.25px;max-height:106.25px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="box">
                            <div class="form-group">
                                <label>Certificates</label>
                                <?php echo $model->form->textareaFor('certificates'); ?>
                            </div>
                            <div class="form-group">
                                <label>License Photo</label>

                                <p><input type="file" name="license_photo" class='image'/></p>

                                <div style="display:inline-block">
                                    <?php
                                    $img_path = "";
                                    if ($model->provider->license_photo != "" && \s3Bucket\s3Handler::doesObjectExist($model->provider->license_photo)) {
                                        $img_path = S3_URL . $model->provider->license_photo;
                                        ?>
                                        <div class="well well-sm pull-left"
                                             style="max-width:106.25px;max-height:106.25px;">
                                            <img src="<?php echo $img_path; ?>"/>
                                            <br/>
                                            <a href="<?= ADMIN_URL . 'providers/delete_image/' . $model->provider->id; ?>?type=license_photo"
                                               onclick="return confirm('Are you sure?');"
                                               class="btn btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="license_photo"
                                                   value="<?= $model->provider->license_photo ?>"/>
                                        </div>
                                    <?php } ?>
                                    <div class='preview-container'
                                         style="max-width:106.25px;max-height:106.25px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="sessions-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <table id="data-list" class="table">
                                <thead>
                                <tr>
                                    <th width="20%">Name</th>
                                    <th width="20%">Service</th>
                                    <th width="20%">Location</th>
                                    <th width="20%">Credit(s)</th>
                                    <th width="20%">Date</th>
                                    <th width="15%" class="text-center">Edit</th>
                                    <th width="15%" class="text-center">Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <? foreach ($model->sessionHistory as $sessionHistory) { ?>
                                    <tr class="originalProducts">
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>session_history/update/<?php echo $sessionHistory->id; ?>">
                                                <?php echo \Model\User::getItem($sessionHistory->user_id)->full_name(); ?>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>session_history/update/<?php echo $sessionHistory->id; ?>">
                                                <?php echo \Model\Service::getItem($sessionHistory->service_id)->name; ?>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>session_history/update/<?php echo $sessionHistory->id; ?>">
                                                <?php echo $sessionHistory->getLocation(); ?>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>session_history/update/<?php echo $sessionHistory->id; ?>">
                                                <?php echo $sessionHistory->credit; ?>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>session_history/update/<?php echo $sessionHistory->id; ?>">
                                                <?php echo $sessionHistory->date; ?>
                                            </a>
                                        </td>

                                        <td class="text-center">
                                            <a class="btn-actions"
                                               href="<?= ADMIN_URL ?>session_history/update/<?= $sessionHistory->id ?>">
                                                <i class="icon-pencil"></i>
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <a class="btn-actions"
                                               href="<?= ADMIN_URL ?>session_history/delete/<?= $sessionHistory->id ?>?token_id=<?php echo get_token(); ?>"
                                               onClick="return confirm('Are You Sure?');">
                                                <i class="icon-cancel-circled"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <? } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="packages-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>Bank information</h4>
                            <div class="form-group">
                                <label>Account Number</label>
                                <?php echo $model->form->editorFor('account_number'); ?>
                            </div>
                            <div class="form-group">
                                <label>Routing Number</label>
                                <?php echo $model->form->editorFor('routing_number'); ?>
                            </div>
                            <div class="form-group">
                                <label>Bank Name</label>
                                <?php echo $model->form->editorFor('bank_name'); ?>
                            </div>
                            <div class="form-group">
                                <label>Bank Address</label>
                                <?php echo $model->form->editorFor('bank_address'); ?>
                            </div>
                            <div class="form-group">
                                <label>Bank City</label>
                                <?php echo $model->form->editorFor('bank_city'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>Withdrawal for previous month</h4>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-24">
                        <div class="box box-table">
                            <table id="data-list" class="table">
                                <thead>
                                <tr>
                                    <th width="10%">Client</th>
                                    <th width="10%">Service</th>
                                    <th width="15%">Date</th>
                                    <th width="15%">Location</th>
                                    <th width="15%">Session price</th>
                                </tr>
                                </thead>
                                <tbody>
                                <? $total = 0;
                                $start = date("Y-m-d", strtotime("first day of previous month"));
                                $over = date("Y-m-d", strtotime("last day of previous month"));
                                $withdraw = \Model\Schedule::getList(['where' => "active='1' and provider_id = '" . $model->provider->id . "' and status='1' and day>='" . $start . "' and day<='" . $over . "'"]);
                                ?>
                                <?php foreach ($withdraw as $past) { ?>
                                    <? $user = \Model\User::getItem($past->user_id); ?>
                                    <? $service = \Model\Service::getItem($past->service); ?>
                                    <? $order_products = \Model\Order_Product::getItem($past->order_product_id); ?>
                                    <? @$packages = \Model\Package::getItem($order_products->product_id) ?>
                                    <tr class="originalProducts">
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>users/update/<?php echo $user->id; ?>"><?= $user->first_name ?> <?= $user->last_name ?></a>
                                        </td>
                                        <td><?= $service->name ?> </td>
                                        <td><?= date("F d, Y", strtotime($past->day)) ?> at <?= $past->time ?></td>
                                        <td><?= $past->session_location ?></td>
                                        <td>$<?= number_format($packages->price / $packages->quantity, 2) ?></td>
                                        <? $total += $packages->price / $packages->quantity; ?>
                                        <!-- <td class="text-center">
													<a class="btn-actions" href="<?php echo ADMIN_URL; ?>packages/update/<?php echo $package->id; ?>">
														<i class="icon-pencil"></i> 
													</a>
												</td>
												<td class="text-center">
													<a class="btn-actions" href="<?php echo ADMIN_URL; ?>packages/delete/<?php echo $package->id; ?>?token_id=<?php echo get_token(); ?>" onClick="return confirm('Are You Sure?');">
														<i class="icon-cancel-circled"></i> 
													</a>
												</td> -->
                                    </tr>
                                <?php } ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <? if (!\Model\Payout::getItem(null, ["where" => "active = '1' and period = '" . $start . '-' . $over . "' and provider_id ='" . $model->provider->id . "'"]) && $total > 0){ ?>
                <div class="col-md-24">
                    <input name="provider_id" value="<?= $model->provider->id ?>" style="display:none;">

                    <input name="period" value="<?= $start ?>-<?= $over ?>" style="display:none;">

                    <div class="box box-table" style="text-align: right;"><font size="8px">Subtotal
                            $<?= number_format($total, 2) ?></font><br>
                        <font size="8px">Wellns $<?= number_format($total / 100 * 30, 2) ?></font><br>
                        <font size="8px">Total $<?= number_format($total = $total / 100 * 70, 2) ?></font><br>
                        <input name="amount" value="<? echo $total; ?>">

                        <p id="do_withdraw" class="btn btn-save">Pay</p></div>
                    <? } ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-save">Save</button>
        </div>
    </form>
</div>

<?php footer(); ?>

<script>

    var services = <?php echo json_encode($model->provider->services); ?>;

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var img = $("<img />");
            reader.onload = function (e) {
                img.attr('src', e.target.result);
                img.attr('alt', 'Uploaded Image');
                img.attr("width", '100%');
                img.attr('height', '100%');
            };
            $(input).parent().parent().find('.preview-container').html(img);
            $(input).parent().parent().find('input[type="hidden"]').remove();

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(function () {
        $("select[name='provider_service[]']").val(services);

        $("select.multiselect").each(function (i, e) {
            //$(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });

        $("input.image").change(function () {
            readURL(this);
        });
    })

</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#do_withdraw').click(function () {
            var provider_id = $("[name=provider_id]").val();
            var amount = $("[name=amount]").val();
            var period = $("[name=period]").val();
            if (amount > 0) {
                $.ajax({
                    url: "/admin/payouts/make_withdraw",
                    type: "POST",
                    data: {provider_id: provider_id, amount: amount, period: period},
                    success: function (data) {
                        window.location.href = "/admin/payouts";
                    }
                });
            } else {
                alert();
            }
        });
    });
</script>