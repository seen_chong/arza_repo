<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#test" aria-controls="general" role="tab" data-toggle="tab">General</a>
            </li>
<? if($model->content->id){?>
             <li role="presentation"><a href="#content-section" aria-controls="sections" role="tab" data-toggle="tab">Sections</a>
             </li>
            <?}?>

        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="test">
                <input type="hidden" name="id" value="<?php echo $model->content->id; ?>"/>
                <input name="token" type="hidden" value="<?php echo get_token(); ?>"/>

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>
                            <div class="form-group">
                                <label>Name</label>
                                <?php echo $model->form->editorFor("name"); ?>
                            </div>
                            <div class="form-group">
                                <label>Hebrew Name</label>
                                <?php echo $model->form->editorFor("hebrew_name"); ?>
                            </div>
                            <div class="form-group">
                                <label>Title</label>
                                <?php echo $model->form->editorFor("title"); ?>
                            </div>
<!--                            <div class="form-group">-->
<!--                                <label>Display Order</label>-->
<!--                                --><?php //echo $model->form->editorFor("display_order"); ?>
<!--                            </div>-->
                            <div class="form-group">
                                <label>Location</label>
                                <?$location = ["nav"=>"Navbar","work"=>"What We Do","mission"=>"Who We Are","join"=>"Join the Community"]; ?>
                                <select name="parent_section">
                                    <?foreach ($location as $key=>$value){
                                        $select = ($model->content->parent_section==$key)?"selected":""?>
                                        <option value="<?=$key?>" <?=$select?>><?php echo $value ?></option>
                                    <?}?>
                                    <?php foreach ($model->contents as $content) {
                                        $select = ($content->id == $model->content->parent_section) ? " selected='selected'" : "";
                                        ?>
                                        <option
                                            value="<?php echo $content->id; ?>" <?php echo $select; ?> ><?php echo $content->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <?php echo $model->form->textAreaFor("description", ["class" => "ckeditor"]); ?>
                            </div>
                            <div class="form-group">
                                <label>
                                    <?php echo $model->form->checkBoxFor("display", 1); ?>  Display?
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>SEO</h4>

                            <div class="form-group">
                                <label>Slug</label>
                                <?php echo $model->form->editorFor('slug'); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Title</label>
                                <?php echo $model->form->editorFor('meta_title'); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Keywords</label>
                                <?php echo $model->form->editorFor('meta_keywords'); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Description</label>
                                <?php echo $model->form->textAreaFor('meta_description'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <? if($model->content->id){?>
            <div role="tabpanel" class="tab-pane" id="content-section">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box box-table">
                            <a class="btn-actions" href="<?=ADMIN_URL.'sections/update?content='.$model->content->id?>">Add a Section</a>
                            <a class="btn-actions" href="<?=ADMIN_URL.'contents/sort?content='.$model->content->id?>">Sort Sections</a>
                            <table id="data-list" class="table">
                                <thead>
                                <tr>
                                    <th width="5%">Display Order</th>
                                    <th width="22%">Name</th>
                                    <th width="22%">Title</th>
                                    <th width="22%">Display?</th>
                                    <th width="15%" class="text-center">Edit</th>
                                    <th width="15%" class="text-center">Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach (\Model\Section::getList(['where'=>'content_name='.$model->content->id,'orderBy' => 'display_order ASC']) as $obj) { ?>
                                    <tr class="originalProducts">
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>sections/update/<?php echo $obj->id; ?>?content=<?=$model->content->id?>"><?php echo $obj->display_order; ?></a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>sections/update/<?php echo $obj->id; ?>?content=<?=$model->content->id?>"><?php echo $obj->name; ?></a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>sections/update/<?php echo $obj->id; ?>?content=<?=$model->content->id?>"><?php echo $obj->title; ?></a>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>sections/update/<?php echo $obj->id; ?>?content=<?=$model->content->id?>"><?php echo $obj->display==1?"Yes":"No"; ?></a>
                                        </td>
                                        <td class="text-center">
                                            <a class="btn-actions"
                                               href="<?php echo ADMIN_URL; ?>sections/update/<?php echo $obj->id; ?>?content=<?=$model->content->id?>">
                                                <i class="icon-pencil"></i>
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <a class="btn-actions"
                                               href="<?= ADMIN_URL ?>sections/delete/<?= $obj->id ?>?token_id=<?php echo get_token(); ?>&content=<?=$model->content->id?>"
                                               onClick="return confirm('Are You Sure?');">
                                                <i class="icon-cancel-circled"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            <div class="box-footer clearfix">
                                <div class='paginationContent'></div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <?}?>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
<script type='text/javascript'>
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
    });
</script>