<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<div class="row">
    <div class="col-md-8">
        <div class="box">
            <div class="input-group">
                <input id="search" type="text" name="search" class="form-control" placeholder="Search by Id or Name"/>
            <span class="input-group-addon">
                <i class="icon-search"></i>
            </span>
            </div>
        </div>
    </div>

</div>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<?php if (count($model->donate) > 0) { ?>
    <div class="box box-table">
        <table id="data-list" class="table">
            <thead>
            <tr>
                <th width="5%"></th>
                <th width="20%">Name</th>
                <th width="30%">URL</th>
                <th width="10%">Display</th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->donate as $obj) { ?>
                <tr class="originalProducts">
                    <td>
                        <? if ($obj->is_default) { ?>
                            <button type="button" class="btn btn-success" disabled="disabled">Default</button>
                        <? } ?>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>donate/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->name; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>donate/update/<?php echo $obj->id; ?>">
                            <?php echo 'arza.org/donation/'.$obj->slug; ?>
                        </a>
                    </td>
                    <td>
                        <?php if($obj->display == 1){?>
                            <span class="greencheck"><i class="fa fa-check"></i></span>
                        <?php } else { ?>
                            <span class="redcancel"><i class="fa fa-times"></i></span>
                        <?php } ?>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>donate/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>donate/delete/<?php echo $obj->id; ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="fa fa-trash-o"></i>
                        </a>
                    </td>

                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>

<?php footer(); ?>

<script>
    var site_url = '<?= ADMIN_URL.'donate';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
