<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<!--<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>-->

<div class="row">
    <div class="col-md-8">
        <div class="box">
            <a class="btn btn-default" href="<?= ADMIN_URL . 'donate/campaign'?>">Add a Donate Campaign<i class="icon-plus"></i></a>
        </div>
    </div>
</div>
<?php if (count($model->donate) > 0) { ?>
    <div class="box box-table">
        <table id="data-list" class="table">
            <thead>
            <tr>
                <th width="5%"></th>
                <th class="text-center" width="25%">Image</th>
                <th class="text-center" width="25%">Background Image</th>
                <th width="15%">Name</th>
                <th width="15%">Title</th>
                <th class="text-center">Edit</th>

            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->donate as $obj) { ?>
                <tr class="originalProducts">
                    <td>
                        <? if ($obj->is_default) { ?>
                            <button type="button" class="btn btn-success" disabled="disabled">Landing</button>
                        <? } ?>
                    </td>
                    <td class="text-center">
                        <?php
                        $img_path = "";
                        if($obj->image != "" && file_exists(UPLOAD_PATH.'donate'.DS.$obj->image)){
                            $img_path = UPLOAD_URL . 'donate/' . $obj->image;
                            ?>
                            <img src="<?php echo $img_path; ?>" width="100" />
                        <?php } ?>
                    </td>
                    <td class="text-center">
                        <?php
                        $img_path = "";
                        if($obj->background_image != "" && file_exists(UPLOAD_PATH.'donate'.DS.$obj->background_image)){
                            $img_path = UPLOAD_URL . 'donate/' . $obj->background_image;
                            ?>
                            <img src="<?php echo $img_path; ?>" width="100" />
                        <?php }else{ ?>
                            <div style="width: 100px ;height: 75px; background-color:#6dca92;margin: auto"></div>
                        <?}?>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>donate/landing_update/<?php echo $obj->id; ?>">
                            <?php echo $obj->name; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>donate/landing_update/<?php echo $obj->id; ?>">
                            <?php echo $obj->title; ?>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>donate/landing_update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
<?php } ?>

<?php footer(); ?>

