<style>
    .featured {
        border: 1px solid black;
    }

    body.dragging, body.dragging * {
        cursor: move !important;
    }

    .dragged {
        position: absolute;
        opacity: 0.5;
        z-index: 2000;
    }
</style>
<div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#general" aria-controls="general" role="tab"
                                                  data-toggle="tab">General</a>
        </li>

        <li role="presentation"><a href="#content-campaign" aria-controls="sections" role="tab" data-toggle="tab">Donate
                Campaign</a>
        </li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="general">
            <form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?php echo $model->donate->id; ?>"/>
                <input type=hidden name="token" value="<?php echo get_token(); ?>"/>


                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>
<!--                            <div class="form-group">-->
<!--                                <label>-->
<!--                                    <input type="checkbox" name="default_template"-->
<!--                                           value="1" --><?// $selected = $model->donate->default_template == 1 ? "checked=\"checked\"" : ""; ?><!-- --><?//= $selected ?><!-->
<!--                                    Using default donate page template?-->
<!--                                </label>-->
<!--                            </div>-->
                            <div class="form-group">
                                <label>Name</label>
                                <? echo $model->form->editorFor("name") ?>
                            </div>
                            <div class="form-group">
                                <label>Upload Logo Image</label>
                                <small>(Leave blank to use default Arza logo)</small>
                                <?php
                                $img_path = "";
                                if ($model->donate->logo_image != "") {
                                    $img_path = UPLOAD_URL . 'donate/' . $model->donate->logo_image;
                                }
                                ?>
                                <p><input type="file" name="logo_image" class='image'/></p>
                                <?php if ($model->donate->logo_image != "") { ?>
                                    <div class="well well-sm pull-left">
                                        <img src="<?php echo $img_path; ?>" width="100"/>
                                        <br/>
                                        <a href="<?= ADMIN_URL . 'donate/landing_delete_image/' . $model->donate->id . '/?type=logo_image'; ?>"
                                           class="btn btn-default btn-xs">Delete</a>
                                        <input type="hidden" name="logo_image"
                                               value="<?= $model->donate->logo_image ?>"/>
                                    </div>
                                <?php } ?>
                                <div class='preview-container'></div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form-group">
                                <label>Descriptions of Logo</label>
                                <? echo $model->form->textAreaFor("logo_description", ['row' => '3']) ?>
                            </div>
                            <div class="form-group">
                                <label>Title of Donation</label>
                                <? echo $model->form->textAreaFor("title", ['row' => '3']) ?>
                            </div>
                            <div class="form-group">
                                <label>Descriptions of Image</label>
                                <? echo $model->form->textAreaFor("subtitle", ['row' => '3']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Form Management</h4>
                            <? $content = json_decode($model->donate->donate_form, true);
                            $check = $content['customize'] == 1 ? 'checked=checked' : '';
                            ?>
                            <div class="form-group">
                                <input type="checkbox" name="customize" <?= $check ?>> Designate a gift</li>
                            </div>
                            <div class="form-group">
                                <label>Description of Donation</label>
                                <div class="length"></div>
                                <textarea row="3"
                                          name="customize_description"><?= $content['customize_description'] ?></textarea>
                            </div>
                            <div class="form-group">
                                <label>Instruction of Donation</label>
                                <textarea type="text" row="3"
                                          name="instruction"><?= $content['instruction'] ?></textarea>
                            </div>
                            <ul style="list-style-type: none;">
                                <? foreach (\Model\Donate::$form as $key => $value) {
                                    $check = $content[$value] == 1 ? 'checked=checked' : '' ?>
                                    <li><input type="checkbox" name="<?= $value ?>"
                                               value="1" <?= $check ?> > <?= $key ?></li>
                                <? } ?>
                            </ul>
                        <div class="form-group">
                            <label>Donate Amount<br>
                                <small>Leave blank to keep default amount(36, 50, 100, 360, 500, 1000)</small>
                            </label><br>
                            <a id="add_desc" href="#">Add Amount</a>
                            <div id="content_box">
                                <? if (($content = json_decode($model->donate->donate_amount, true)) != null) {
                                    foreach ($content as $value) {
                                        ?>
                                        <div class="form-group">
                                            <input type="text" name="donate_amount[]"
                                                   placeholder="Enter the amount,eg: 100" value="<?= $value ?>">
                                            <a href="#" class="delete_content">Delete</a>
                                        </div>
                                    <? } ?>
                                <? } ?>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <h4>Background Image</h4>
                    <div class="form-group">
                        <label>Upload</label>
                        <?php
                        $img_path = "";
                        if ($model->donate->background_image != "") {
                            $img_path = UPLOAD_URL . 'donate/' . $model->donate->background_image;
                        }
                        ?>
                        <p><input type="file" name="background_image" class='image'/></p>
                        <?php if ($model->donate->background_image != "") { ?>
                            <div class="well well-sm pull-left">
                                <img src="<?php echo $img_path; ?>" width="100"/>
                                <br/>
                                <a href="<?= ADMIN_URL . 'donate/landing_delete_image/' . $model->donate->id . '/?type=background_image'; ?>"
                                   class="btn btn-default btn-xs">Delete</a>
                                <input type="hidden" name="background_image"
                                       value="<?= $model->donate->background_image ?>"/>
                            </div>
                        <?php } ?>
                        <div class='preview-container'></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="box">
                    <h4>Meta Data</h4>
                    <div class="form-group">
                        <label>Meta title</label>
                        <?php echo $model->form->editorFor("meta_title"); ?>
                    </div>
                    <div class="form-group">
                        <label>Meta keywords</label>
                        <?php echo $model->form->textAreaFor("meta_keywords"); ?>
                    </div>
                    <div class="form-group">
                        <label>Meta description</label>
                        <?php echo $model->form->textAreaFor("meta_description", ["rows" => "3"]); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="box postcard"
                     style="<?= $model->donate->default_template == 1 ? '' : 'display: none' ?>">
                    <h4>Report Card</h4>
                    <div class="form-group">
                        <label>Upload</label>

                        <p>
                            <small>(ideal featured image size is 1920 x 300)</small>
                        </p>
                        <?php
                        $img_path = "";
                        if ($model->donate->image != "") {
                            $img_path = UPLOAD_URL . 'donate/' . $model->donate->image;
                        }
                        ?>
                        <p><input type="file" name="image" class='image'/></p>
                        <?php if ($model->donate->image != "") { ?>
                            <div class="well well-sm pull-left">
                                <img src="<?php echo $img_path; ?>" width="100"/>
                                <br/>
                                <a href="<?= ADMIN_URL . 'donate/landing_delete_image/' . $model->donate->id . '/?type=featured_image'; ?>"
                                   class="btn btn-default btn-xs">Delete</a>
                                <input type="hidden" name="image"
                                       value="<?= $model->donate->image ?>"/>
                            </div>
                        <?php } ?>
                        <div class='preview-container'></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="content-campaign">
        <div class="rox">
            <div class="box box-table">
                <table id="campaign-container" class="table">
                    <thead>
                    <tr>
                        <th width="20%">Name</th>
                        <th width="20%">Display</th>
                        <th width="20%">Type</th>
                        <th width="20%">Display Order</th>
                        <th width="20%" class="text-center">Edit</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($model->campaigns as $obj) { ?>
                        <tr class="originalProducts" data-campaign_id="<?= $obj->id ?>">
                            <td>
                                <a href="<?php echo ADMIN_URL; ?>donate/campaign_update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a>
                            </td>
                            <td>
                                <?php if ($obj->display == 1) { ?>
                                    <span class="greencheck"><i class="fa fa-check"></i></span>
                                <?php } else { ?>
                                    <span class="redcancel"><i class="fa fa-times"></i></span>
                                <?php } ?>
                            </td>
                            <td>
                                <a href="<?php echo ADMIN_URL; ?>donate/campaign_update/<?php echo $obj->id; ?>"><?php echo \Model\Donate::$type[$obj->type]; ?></a>
                            </td>
                            <td class="display-order-td">
                                <a href="<?php echo ADMIN_URL; ?>donate/campaign_update/<?php echo $obj->id; ?>"><?= $obj->display_order ?></a>
                            </td>
                            <td class="text-center">
                                <a class="btn-actions" target="_blank"
                                   href="<?php echo ADMIN_URL; ?>donate/campaign_update/<?php echo $obj->id; ?>">
                                    <i class="icon-pencil"></i>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
                <div class="box-footer clearfix">
                    <div class='paginationContent'></div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<button type="submit" class="btn btn-save">Save</button>
</form>
<?php footer(); ?>

<script type="text/javascript">

    $(document).ready(function () {

        $('#add_desc').on('click', function(){
            var content = $(
                '<div class="form-group"><input data-title-id="" type="text" name="donate_amount[]" placeholder="Enter the amount,eg: 100" value=""><a href="#" class="delete_content">Delete</a></div>'
            );
            $('#content_box').append(content);
        });
        $(document).on('click','.delete_content', function(){
            $(this).parent().remove();
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '180');
                    img.attr('height', '120');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
            $('#previewupload').show();
        });

        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-')
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        $("textarea[name='customize_description']").on('keyup', function (e) {
            var val = $(this).val();
            var name_length = val.length;
            if(name_length>51){
                $("textarea[name='customize_description']").css({
                    "border-color": "red"
                });
                $(".length").html('<p style="color: red">Notice: The length of description oversized, will cause break line. </p>');
            }else{
                $("textarea[name='customize_description']").css({
                    "border-color": ""
                });
                $(".length").html('<p></p>');
            }
        });


        $(".target").click(function () {
            var slug = $("input[name='slug']").val();
            var id = <?=$model->donate->id?>;
            var errors = new Array();
            if ($.trim(slug) == "") {
                $("input[name='slug']").css({
                    "border-color": "red"
                });
                errors.push("Incorrect slug");
            } else {

                $.ajax({
                    async: false,
                    url: '/admin/donate/check_slug',
                    enctype: 'multipart/form-data',
                    method: 'POST',
                    data: {
                        slug: slug,
                        id: id
                    },
                    success: function (data) {

                        if (data == 1) {
                            errors.push("Slug must be uniq!");

                            $("input[name='slug']").css({
                                "border-color": "red"
                            });

                        } else {
                            $("input[name='slug']").css({
                                "border-color": ""
                            });
                        }
                    }
                });
            }
            var text = "";
            for (i = 0; i < errors.length; i++) {
                text += "<li>" + errors[i] + "</li>";
            }
            if (errors.length > 0) {
                $('html, body').animate({
                    scrollTop: 0
                });
                $("#custom_notifications").html('<div class="notification"><div class="alert alert-danger"><strong>An error occurred: </strong><ul>' + text + ' </ul></div></div>');
                errors = [];
            } else {
                $(".form").submit();
            }
        });
        $("input[name='default_template']").on('change', function () {
            if (this.checked) {
                $('.postcard').show();
            } else {
                $('.postcard').hide();
            }
        })

        function sort_number_display() {
            var counter = 1;
            $('#campaign-container >tbody>tr').each(function (i, e) {
                $(e).find('td.display-order-td').html(counter);
                counter++;
            });
        }

        var adjustment;
        $('#campaign-container').sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"><td></td></tr>',
            onDrop: function ($item, container, _super, event) {


                var ids = [];
                var tr_containers = $("#campaign-container > tbody > tr");
                tr_containers.each(function (i, e) {
                    ids.push($(e).data("campaign_id"));

                });
                $.post('<?=ADMIN_URL . 'donate/orderCampaign'?>', {ids: ids}, function (response) {
                    sort_number_display();
                });
                _super($item);
            },
            onDragStart: function ($item, container, _super) {
                var offset = $item.offset(),
                    pointer = container.rootGroup.pointer;

                adjustment = {
                    left: pointer.left - offset.left,
                    top: pointer.top - offset.top
                };

                _super($item, container);
            },
            onDrag: function ($item, position) {
                $item.css({
                    left: position.left - adjustment.left,
                    top: position.top - adjustment.top
                });
            }
        });

    })

</script>




























