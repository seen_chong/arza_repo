<?php if (count($model->missions) > 0): ?>
    <a href="<?=ADMIN_URL.'missions/sort'?>">Manage Sections</a>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="5%" class="text-center">Id</th>
                <th width="20%" class="text-center">Name</th>
                <th width="20%" class="text-center">Title</th>
                <th width="10%" class="text-center">Edit</th>
            </tr>
            </thead>
            <tbody>
            <?php $missions = \Model\Mission::getList()?>
            <?php foreach ($missions as $obj) { ?>
                <tr>
                    <td class="text-center"><a
                            href="<?php echo ADMIN_URL; ?>missions/update/<?php echo $obj->id; ?>"><?php echo $obj->id; ?></a>
                    </td>
                    <td class="text-center"><a
                            href="<?php echo ADMIN_URL; ?>missions/update/<?php echo $obj->id; ?>"><?php echo $obj->section_name; ?></a>
                    </td>

                    <td>
                        <a href="<?php echo ADMIN_URL; ?>missions/update/<?php echo $obj->id; ?>"><?php echo $obj->title; ?></a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>missions/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php endif; ?>
<div class="box">
    <h4>Home Map</h4>
    <div class="home-map">
        <div class="header"><div class="container">HEADER</div></div>
        <div class="header" style="border-style:solid; background: #ccc; font-weight: bold;"><div class="container">TOP</div></div>
        <div class="main-banner" style="border-style:solid; background: #ccc; font-weight: bold;"><div class="container">MAIN BANNER</div></div>
        <div class="main-banner"><div class="container">HISTORY</div></div>
        <div class="deal-of-the-week" style="border-style:solid; background: #ccc; font-weight: bold;"  >
            <div class="container">VIDEO</div>
        </div>
        <div class="featured-banner" style="border-style:solid; background: #ccc; font-weight: bold;">
            <div class="container">
                <div class="col-md-7"><div id="f-2">BLOCK 1</div></div>
                <div class="col-md-7"><div id="f-2">BLOCK 2</div></div>
                <div class="col-md-7"><div id="f-3">BLOCK 3</div></div>
            </div>
        </div>
        <div class="footer"><div class="container">FOOTER</div></div>
    </div>
</div>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'configs';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>

