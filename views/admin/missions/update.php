<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->mission->id ?>"/>

    <?if($model->mission->section_name == "Video"){?>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <h4>General</h4>
                <div class="form-group">
                    <label>Section Name</label>
                    <input type="text" value="<?= $model->mission->section_name ?>" name="section_name" readonly/>
                </div>
    <div class="form-group">
        <label>Video Link Id</label>
        <?php echo $model->form->editorFor("video_link"); ?>
    </div>
            </div>
        </div>
        </div>

    <?}elseif($model->mission->section_name =='our partners'||$model->mission->section_name =='block 1'||$model->mission->section_name =='block 2'||$model->mission->section_name =='block 3'||$model->mission->section_name =='top'){?>
        <div class="row">
            <div class="col-md-24">
                <div class="box">
                    <h4>General</h4>
                    <div class="form-group">
                        <label>Section Name</label>
                        <input type="text" value="<?= $model->mission->section_name ?>" name="section_name" readonly/>
                    </div>
                    <div class="form-group">
                        <label>Title</label>
                        <?php echo $model->form->editorFor("title"); ?>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <?php echo $model->form->textAreaFor("description", ["class" => "ckeditor"]); ?>
                    </div>
                    <?if($model->mission->section_name =='our partners'){?>
                    <div class="form-group">
                        <label>Link</label>
                        <?php echo $model->form->editorFor("link"); ?>
                    </div>
                        <div class="form-group">
                            <label>Button</label>
                            <?php echo $model->form->editorFor("subtitle"); ?>
                        </div>
                <?}?>
                </div>
            </div>
        </div>
    <?if($model->mission->section_name =='our partners'){?>
            <div class="row">
                <div class="col-md-24">
                    <div class="box">
                        <h4>Meta Information</h4>

                        <div class="form-group">
                            <label>Featured image</label>

                            <p>
                                <small>(ideal featured image size is 1920 x 300)</small>
                            </p>
                            <?php
                            $img_path = "";
                            if ($model->mission->featured_image != "" && file_exists(UPLOAD_PATH . 'missions' . DS . $model->mission->featured_image) ) {
                                $img_path = UPLOAD_URL . 'missions/' . $model->mission->featured_image;
                            }
                            ?>
                            <p><input type="file" name="featured_image" class='image'/></p>
                            <?php if ($model->mission->featured_image != "") { ?>
                                <div class="well well-sm pull-left">
                                    <img src="<?php echo $img_path; ?>" width="1200"/>
                                    <br/>
                                    <a href="<?= ADMIN_URL . 'missions/delete_image/' . $model->mission->id . '/?featured_image=1'; ?>"
                                       onclick="return confirm('Are you sure?');"
                                       class="btn btn-default btn-xs">Delete</a>
                                    <input type="hidden" name="featured_image"
                                           value="<?= $model->mission->featured_image ?>"/>
                                </div>
                            <?php } ?>
                            <div id='preview-container'></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        <?}?>
    <?}else{?>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <h4>General</h4>
                <div class="form-group">
                    <label>Section Name</label>
                    <input type="text" value="<?= $model->mission->section_name ?>" name="section_name" readonly/>
                </div>
                <div class="form-group">
                    <label>Title</label>
                    <?php echo $model->form->editorFor("title"); ?>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <?php echo $model->form->textAreaFor("description", ["class" => "ckeditor"]); ?>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="box">

                <div class="form-group">
                    <label>Link</label>
                    <?php echo $model->form->editorFor("link"); ?>
                </div>
                <div class="form-group">
                    <label>Button</label>
                    <?php echo $model->form->editorFor("button"); ?>
                </div>
                <div class="form-group">
                    <label>Subtitle</label>
                    <?php echo $model->form->editorFor("subtitle"); ?>
                </div>
                <div class="form-group">
                    <label>SubDescription</label>
                    <?php echo $model->form->textAreaFor("sub_description", ["class" => "ckeditor"]); ?>
                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>Meta Information</h4>

                <div class="form-group">
                    <label>Featured image</label>

                    <p>
                        <small>(ideal featured image size is 1920 x 300)</small>
                    </p>
                    <?php
                    $img_path = "";
                    if ($model->mission->featured_image != "" && file_exists(UPLOAD_PATH . 'missions' . DS . $model->mission->featured_image) ) {
                        $img_path = UPLOAD_URL . 'missions/' . $model->mission->featured_image;
                    }
                    ?>
                    <p><input type="file" name="featured_image" class='image'/></p>
                    <?php if ($model->mission->featured_image != "") { ?>
                        <div class="well well-sm pull-left">
                            <img src="<?php echo $img_path; ?>" width="1200"/>
                            <br/>
                            <a href="<?= ADMIN_URL . 'missions/delete_image/' . $model->mission->id . '/?featured_image=1'; ?>"
                               onclick="return confirm('Are you sure?');"
                               class="btn btn-default btn-xs">Delete</a>
                            <input type="hidden" name="featured_image"
                                   value="<?= $model->mission->featured_image ?>"/>
                        </div>
                    <?php } ?>
                    <div id='preview-container'></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <?}?>
    <div class="col-md-24">
        <button type="submit" class="btn btn-success btn-lg">Save</button>
    </div>
</form>


<?= footer(); ?>


<script type='text/javascript'>
    $(document).ready(function () {

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                    $("#preview-container").html(img);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
            $('#previewupload').show();
        });

        $("input[name='title']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-')
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });

    });

</script>