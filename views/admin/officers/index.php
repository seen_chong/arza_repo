<?php
if (count($model->officers) > 0): ?>
    <div class="box box-table">
        <a href="<?=ADMIN_URL.'officers/sort'?>">Sort Display Order</a>
        <table class="table">
            <thead>
            <tr>
                <th width="30%">Name</th>
                <th width="30%">Title</th>
                <th width="10%">Display Order</th>
                <th width="15%" class="text-center">Edit</th>
                <th width="15%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->order_officers as $obj) { ?>
                <tr>
                    <td><a href="<?= ADMIN_URL ?>officers/update/<?= $obj->id ?>"><?=$obj->full_name()?></a></td>
                    <td><a href="<?= ADMIN_URL ?>officers/update/<?= $obj->id ?>"><?=$obj->title?></a></td>
                    <td><a href="<?= ADMIN_URL ?>officers/update/<?= $obj->id ?>"><?=$obj->display_order?></a></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?= ADMIN_URL ?>officers/update/<?= $obj->id ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?= ADMIN_URL ?>officers/delete/<?= $obj->id ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'officers';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>