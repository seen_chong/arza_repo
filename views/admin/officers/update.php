<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#test" aria-controls="general" role="tab" data-toggle="tab">General</a>
            </li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="test">
                <input type="hidden" name="id" value="<?php echo $model->officer->id; ?>"/>
                <input name="token" type="hidden" value="<?php echo get_token(); ?>"/>

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>First Name</label>
                                <?php echo $model->form->editorFor("first_name"); ?>
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <?php echo $model->form->editorFor("last_name"); ?>
                            </div>
                            <div class="form-group">
                                <label>Title</label>
                                <?php echo $model->form->editorFor("title"); ?>
                            </div>
                            <div class="form-group">
                                <label>Display Order</label>
                                <?php echo $model->form->textBoxFor("display_order"); ?>
                            </div>
                            <div class="form-group">
                                <label>
                                    <?php echo $model->form->checkBoxFor("display", 1); ?>  Display?
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <input type="hidden" id="act_for_click" name="redirectTo" value="<?= ADMIN_URL . 'officers/' ?>">
    <center style="    margin-bottom: 112px;">
        <button type="submit" id="save_close" class="btn btn-save">Save and close</button>
        <button type="submit" id="save" class="btn btn-save">Save</button>
    </center>
</form>

<?php echo footer(); ?>
<script type='text/javascript'>
    $(function () {

        $('#save_close').mouseover(function () {
            $("#act_for_click").val("<?= ADMIN_URL . 'officers/' ?>");

        });

        $('#save').mouseover(function () {
            var id = $("input[name=id]").val();
            $("#act_for_click").val("<?=ADMIN_URL . 'officers/update/'?>" + id);

        });


    });
</script>