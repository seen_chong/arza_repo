<?php if (count($model->testimonials) > 0): ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="10%">Name</th>
                <th width="5%">State</th>
                <th width="5%">Status</th>
                <th width="20%">Content</th>
                <th width="5%">Services</th>
                <th width="15%" class="text-center">Edit</th>
                <th width="15%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->testimonials as $testimonial):
                $testSer = \Model\Testimonial_Services::getList(['where'=>"testimonial_id = $testimonial->id"]);
                $arr = implode('<br>',array_map(function($item){return \Model\Service::getItem($item->service_id)->name;},$testSer))?>
                <tr>
                    <td><a href="<?php echo ADMIN_URL; ?>testimonials/update/<?php echo $testimonial->id; ?>"><?php echo $testimonial->name; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>testimonials/update/<?php echo $testimonial->id; ?>"><?php echo $testimonial->state; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>testimonials/update/<?php echo $testimonial->id; ?>"><?php echo \Model\Testimonial::$status[$testimonial->status]; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>testimonials/update/<?php echo $testimonial->id; ?>"><?php echo $testimonial->content; ?></a></td>
                    <td><a href="<?php echo ADMIN_URL; ?>testimonials/update/<?php echo $testimonial->id; ?>"><?php echo $arr ? : 'General'; ?></a></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>testimonials/update/<?php echo $testimonial->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>testimonials/delete/<?php echo $testimonial->id; ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'testimonials/index';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;

</script>

