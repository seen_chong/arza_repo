<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->testimonial->id ?>"/>
    <div class="row">
        <div class="col-md-16">
            <div class="box">
                <h4>General</h4>

                <div class="form-group">
                    <label>Name</label>
                    <?php echo $model->form->editorFor("name"); ?>
                </div>
                <div class="form-group">
                    <label>State</label>
                    <?php echo $model->form->editorFor("state"); ?>
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <?php echo $model->form->dropDownListFor("status", \Model\Testimonial::$status,'',['class'=>'form-control']); ?>
                </div>
                <div class="form-group">
                    <label>Categories</label>
                    <?php echo $model->form->dropDownListFor("testimonial_services[]", $model->services,'',['class'=>'form-control multiselect', 'multiple'=>'multiple']); ?>
                </div>
                <div class="form-group">
                    <label>Content</label>
                    <?php echo $model->form->textAreaFor("content", ['maxLength'=>150, 'rows'=>2]); ?>
                </div>
            </div>
        </div>
        <div class="col-lg-24">
            <button type="submit" class="btn btn-success btn-lg">Save</button>
        </div>
    </div>
    </div>
</form>


<?= footer(); ?>


<script type='text/javascript'>
    $(document).ready(function () {
        var services = <?php echo json_encode($model->testimonial_services); ?>;

        $("select.multiselect").each(function (i, e) {
            $(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });
        $("select[name='testimonial_services[]']").val(services);
        $("select.multiselect").multiselect("rebuild");
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                    $("#preview-container").html(img);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
            $('#previewupload').show();
        });

        $("input[name='title']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-')
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });

    });

</script>