<style>
    body.dragging, body.dragging * {
        cursor: move !important;
    }

    .dragged {
        position: absolute;
        opacity: 0.5;
        z-index: 2000;
    }
</style>
<div class="box">
    <h3>Sections</h3>
        <ol class="sortable">
            <?foreach($model->latests as $latest){
                echo $latest->buildSortable();
            }?>
        </ol>
    <div class="serialize-result"></div>
</div>
<?php echo footer(); ?>
<script>
    $(document).ready(function(){
        var adjustment;
        var serial = $('.sortable').sortable({
            group:'sortable',
            onDrop: function ($item, container, _super) {
                var data = serial.sortable("serialize").get();
                var jsonData = JSON.stringify(data,null,'');

//                $('.serialize-result').text(jsonData);
                $.post('/admin/latests/updateLatest',{data:jsonData},function(){
                    console.log('done');
                });
                _super($item,container);
            },
            onDragStart: function ($item, container, _super) {
                var offset = $item.offset(),
                    pointer = container.rootGroup.pointer;

                adjustment = {
                    left: pointer.left - offset.left,
                    top: pointer.top - offset.top
                };

                _super($item, container);
            },
            onDrag: function ($item, position) {
                $item.css({
                    left: position.left - adjustment.left,
                    top: position.top - adjustment.top
                });
            }
        });
    })
</script>