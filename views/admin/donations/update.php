<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->donation->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div class="tab-content">
        <div role="tabpanel">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <h4>General</h4>
                            <? if($model->donation->monthly == 1){?>
                                <p>This is a monthly gift.</p>
                                <? if($model->donation->status == \Model\Donation::$status[6]){?>
                                    <a class="btn btn-danger">Cancelled</a>
                                <?}else{?>
                                <a class="btn btn-action" href="/admin/donations/cancel?id=<?=$model->donation->id?>">Cancel Monthly Donate</a>
                            <?}}?>
                            <div class="form-group">
                                <label>Donation #</label>
                                <p>#<?= $model->donation->id ?> </p>
                            </div>
                            <div class="form-group">
                                <label>Date</label>

                                <p><? $insert_time = new DateTime($model->donation->insert_time);
                                    $insert_time = $insert_time->format('m-d-Y H:i:s');
                                    echo $insert_time?> </p>
                            </div>
                        <div class="form-group">
                            <label>Ref Num</label>
                            <p><?= $model->donation->ref_num ?> </p>
                        </div>
                        <div class="form-group">
                            <label>Amount</label>
                            <p>$<?= number_format($model->donation->amount,2); ?> </p>
                        </div>
                            <div class="form-group" hidden>
                                <label>Status</label>
<!--                                --><?//= $model->form->dropDownListFor('status', \Model\Order::$status, '', ['class' => 'form-control']); ?>
                                <select name="status" class="form-control">
<!--                                    --><?//foreach(\Model\Order::$status as $index=>$value){?>
<!--                                        <option value="--><?//=$index?><!--" --><?//=$index == $model->order->status?'selected':''?><!-->--><?//=$value?><!--</option>-->
<!--                                    --><?//}?>
                                    <?foreach(\Model\Order::$transactionStatus as $index=>$value){?>
                                        <option value="<?=$index?>" <?=$index == $model->order->status?'selected':''?>><?=$value?></option>
                                    <?}?>
                                </select>
                            </div>
                            <div class="form-group" hidden>
                                <label>Transaction Message</label>
                                <input type="text" name="order_message" value="<?=$model->order->order_message?>" readonly>
                            </div>
                            <div class="form-group">
                                <label>Customer</label>

                                <div class="input-group">
                                    <? if ($model->donation->user_id == 0) { ?>
                                        <div class="input-group-addon">
                                            Guest
                                        </div>
                                        <input type="text" disabled="disabled" value="<?= $model->donation->email ?>"/>
                                    <? } else { ?>
                                            <div class="input-group-btn">
                                                <a href="<?php echo ADMIN_URL; ?>users/update/<?php echo $model->donation->user_id; ?>">
                                                    <button type="button" class="btn"><i class="icon-eye"></i></button>
                                                </a>
                                            </div>
                                            <input type="text" disabled="disabled"
                                                   value="<?= \Model\User::getItem($model->donation->user_id)->email ?>"/>

                                    <? } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Phone Number</label>
                                <?php echo $model->form->textBoxFor('phone', ['id' => 'phone-us']); ?>
                            </div>

                        <div class="form-group">
                            <label>Donate Type</label>
                            <?php echo $model->donation->getType(); ?>
                        </div>
                        <? $d = \Model\Donate::getItem($model->donation->type);
                        if($d->is_default == 1){?>
                            <div class="form-group">
                                <label>Designation</label>
                                <?php echo $model->donation->designation?>
                            </div>
                        <?}?>
                        <?if($model->donation->gift_designation){?>
                            <div class="form-group">
                                <label>This gift is honor of/in memory of</label>
                                <?php echo $model->donation->gift_designation?>
                            </div>
                            <div class="form-group">
                                <label>Contact Info</label>
                                <?php echo $model->donation->gift_designation_contact?>
                            </div>

                        <?}?>
                        <div class="form-group">
                            <label>Donation Preference</label>
                            <textarea><?php echo $model->donation->preference; ?></textarea>
                        </div>
                        </div>
                    <div class="box">
                        <h4>Payment Information</h4>
                            <div class="form-group">
                                <label>Payment Method</label>
                                <p>Paypal</p>
                            </div>
                        <div class="row cc-info">
                        <div class="col-xs-10 cc_number" hidden>
                            <label>Credit Card #</label>

                                    <div class="input-group">
                                        <?php echo $model->form->textBoxFor('cc_number'); ?>
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-default">Show</button>
                                        </div>
                                    </div>
                            </div>
                                <div class="col-xs-10" hidden>
                                    <div class="col-xs-24">
                                        <label>Expiration</label>
                                    </div>
                                    <div>
                                        <div class="col-xs-16">
                                            <?= $model->form->dropDownListFor('cc_expiration_month', get_month(), '', ['class' => 'form-control']); ?>
                                        </div>
                                        <div class="col-xs-8">
                                            <?= $model->form->dropDownListFor('cc_expiration_year', range(date('Y') - 15, date('Y') + 9), '', ['class' => 'form-control']); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4" hidden>
                                    <label>CCV</label>
                                    <?php echo $model->form->textBoxFor('cc_ccv'); ?>
                                </div>
                        </div>
                        </div>
                </div>
                <div class="col-md-12">
                        <div class="box">
                            <h4>Billing Information</h4>

                            <div class="form-group">
                                <label>First Name</label>
                                <?php echo $model->form->textBoxFor('first_name'); ?>
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <?php echo $model->form->textBoxFor('last_name'); ?>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <?php echo $model->form->textBoxFor('address'); ?>
                            </div>
                            <div class="form-group">
                                <label>Address 2</label>
                                <?php echo $model->form->textBoxFor('address2'); ?>
                            </div>
                            <div class="form-group">
                                <label>City</label>
                                <?php echo $model->form->textBoxFor('city'); ?>
                            </div>
                            <div class="form-group">
                                <label>State</label>
                                <?= $model->form->dropDownListFor('state', get_states(), 'Select', ['class' => 'form-control']); ?>
                            </div>
                            <div class="form-group">
                                <label>Zip Code</label>
                                <?php echo $model->form->textBoxFor('zip'); ?>
                            </div>
                        </div>
                    </div>
            </div>
    </div>

    <input type="hidden" id="act_for_click" name="redirectTo" value="donations/">
    <center style="    margin-bottom: 112px;">
        <button type="submit" id="save_close" class="btn btn-save">Save and close</button>
        <button type="submit" id="save" class="btn btn-save">Save</button>
    </center>
</form>
<?php footer(); ?>

<style>
    div.mouseover-thumbnail-holder {
        position: relative;
        display: block;
        float: left;
        margin-right: 10px;
    }

    .large-thumbnail-style {
        display: block;
        border: 2px solid #fff;
        box-shadow: 0px 0px 5px #aaa;
    }

    div.mouseover-thumbnail-holder .large-thumbnail-style {
        position: absolute;
        top: 0;
        left: -9999px;
        z-index: 1;
        opacity: 0;
        transition: opacity .5s ease-in-out;
        -moz-transition: opacity .5s ease-in-out;
        -webkit-transition: opacity .5s ease-in-out;
    }

    div.mouseover-thumbnail-holder:hover .large-thumbnail-style {
        width: 100% !important;
        top: 0;
        left: 105%;
        z-index: 1;
        opacity: 1;

    }
</style>
<script>


    $(function () {
        $('select[name="payment_method"]').on('change', function () {
            if ($(this).val() == 1) {
                $('.cc-info').show();
            } else {
                $('.cc-info').hide();
            }
        });
        $('.cc_number button').click(function () {
            var pwd = prompt("Please enter the code");
            if (pwd != null) {
                $.ajax({
                    'type': 'POST',
                    'url': '<?=ADMIN_URL?>donations/getCcNumber',
                    'data': 'password=' + pwd + '&order_id=<?=$model->order->id?>',
                    'success': function (data) {
                        data = JSON.parse(data);
                        if (data == "false") {
                            alert('Incorrect code.');
                        } else {
                            $('.cc_number input').val(data);
                        }
                    }
                })
            }
        });


        $('#save_close').mouseover(function () {
            $("#act_for_click").val("donations/");

        });

        $('#save').mouseover(function () {
            var id = $("input[name=id]").val();
            $("#act_for_click").val("donations/update/" + id);

        });


    });
    $("#phone-us").inputmask("+1(999)999-9999");
</script>
<script>
    $(function () {
        $('#print').click(function (e) {
            e.preventDefault();
            window.print();
            return false;
        })
    })
</script>






























