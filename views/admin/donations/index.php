<div class="row">
    <div class="col-md-8">
        <div class="box">
            Show Donations:
            <select class="type" name="type" style="cursor:pointer">
                <option <? if (isset($_GET['type'])) {
                    if ($_GET['type'] == 0||!$_GET['type'] ) {
                        echo "selected";
                    }
                } ?> value="all">All
                </option>
                <? foreach ($model->donates as $obj){?>
                    <option <? if (isset($_GET['type'])) {
                        if ($_GET['type'] == $obj->id) {
                            echo "selected";
                        }
                    } ?> value="<?=$obj->id?>"><?=$obj->name?>
                    </option>
                <?}?>
            </select>

        </div>
    </div>
        <div class="col-md-8">
            <div class="box">
                <div class="input-group">
                    <input id="search" type="text" name="search" class="form-control" placeholder="Search by Id or Name"/>
                <span class="input-group-addon">
                    <i class="icon-search"></i>
                </span>
                </div>
            </div>
        </div>
    <div class="col-md-24">
        <div class="box">
<!--            <a class= "btn-actions" href="--><?//=ADMIN_URL.'donations?type=3'?><!--">Show Donations for Kehilat</a>-->
            <a class="btn-actions" href="/admin/donations/export"><i class="fa fa-download"></i> Export Donors</a>
        </div>
    </div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
</div>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script>
    $(document).ready(function () {
        $('body').on('change', '.type', function () {
            var type = $(this).val();
            window.location.href = '/admin/donations?type=' + type;
        });
    });
</script>
<?php if (count($model->donations) > 0) { ?>
    <div class="box box-table">
        <table id="data-list" class="table">
            <thead>
            <tr>
                <th width="5%"></th>
                <th>ID</th>
                <th>Date</th>
                <th>Status</th>
                <th>Amount</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Donation Type</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->donations as $obj) { ?>
                <tr class="originalProducts">
                    <td>
                        <? if (is_null($obj->viewed) || !$obj->viewed) { ?>
                            <button type="button" class="btn btn-success" disabled="disabled">New</button>
                        <? } ?>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>donations/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->id; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>donations/update/<?php echo $obj->id; ?>">
                            <?$insert_time = new DateTime($obj->insert_time);
                            $insert_time = $insert_time->format('m-d-Y H:i:s');?>
                            <?php echo $insert_time ; ?>
                        </a>
                    </td>
                    <td>
                        <?= $obj->status ?>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>donations/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->amount; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>donations/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->full_name(); ?>
                        </a>
                    </td>

                    <td>
                        <a href="<?php echo ADMIN_URL; ?>donations/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->email ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>donations/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->phone ?>
                        </a>
                    </td>

                    <td>
                        <a href="<?php echo ADMIN_URL; ?>donations/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->getType() ?>
                        </a>
                    </td>


                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php } ?>

<?php footer(); ?>

<script>
    var site_url = '/admin/donations?<?if (isset($_GET['type'])){echo "type=";echo $_GET['type'] ;echo"&";} ?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script type="text/javascript">
    $(function () {
        $("#search").keyup(function () {
            var url = "<?php echo ADMIN_URL; ?>donations/search";
            var keywords = $(this).val();
            if (keywords.length > 0) {
                $.get(url, {keywords: keywords}, function (data) {
                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').hide();
                    $('.originalProducts').hide();
                    var list = JSON.parse(data);
                    console.log(list);
                    for (key in list) {
                        var tr = $('<tr />');
                        $('<td />').appendTo(tr).html(list[key].viewed ? '':'<button type="button" class="btn btn-success" disabled="disabled">New</button>' );
                        $('<td />').appendTo(tr).html(list[key].id);
                        $('<td />').appendTo(tr).html(list[key].insert_time);
                        $('<td />').appendTo(tr).html(list[key].status);
                        $('<td />').appendTo(tr).html(list[key].amount);
                        $('<td />').appendTo(tr).html(list[key].name);
                        $('<td />').appendTo(tr).html(list[key].email);
                        $('<td />').appendTo(tr).html(list[key].phone);
                        $('<td />').appendTo(tr).html(list[key].type);
//                        var editTd = $('<td />').addClass('text-center').appendTo(tr);
//                        var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?//= ADMIN_URL;?>//orders/update/' + list[key].id);
//                        var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                        tr.appendTo($("#data-list tbody"));
                    }

                });
            } else {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').show();
                $('.originalProducts').show();
            }
        });
    })
</script>