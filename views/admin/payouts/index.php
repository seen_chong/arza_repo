<div class="row">
    <div class="col-md-24">
        <div class="box">

            <a href="/admin/payouts/export" class="btn btn-warning">CSV export</a>
        </div>
    </div>

</div>
<?php  if (count($model->payouts) > 0): ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="30%">Provider</th>
                <th width="10%">Amount</th>
                <th width="10%">Wellns fee</th>
                <th width="10%">Net amount</th>
                <th width="40%">Period</th>
                <th width="30%">Confirm</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->payouts as $payout){ ?>
                <? $provider = $payout['provider'] ?>
                <?php foreach ($payout['past'] as $date => $past){ ?>
                <tr>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>providers/update/<?php echo $provider->id; ?>"><?=$provider->first_name.' '.$provider->last_name; ?></a>
                    </td>
                    <td>$<?= number_format($payout['payment'][$date]['amount'], 2) ?></td>
                    <td>$<?= number_format($payout['payment'][$date]['amount'] * (1 - \Model\SessionHistory::WELLNS_KEEP), 2) ?></td>
                    <td>$<?= number_format($payout['payment'][$date]['amount'] * \Model\SessionHistory::WELLNS_KEEP, 2) ?></td>
                    <?php $dateArray = explode('_', $date); $carbonDate = new \Carbon\Carbon("{$dateArray[0]}-{$dateArray[1]}");?>
                    <td> <?= $carbonDate->format('F Y') ?></td>
                    <td><button type="button" class="btn btn-success confirm-button" style="position:relative;" data-provider="<?=$provider->id?>" data-period="<?=$carbonDate->toDateString()?>">Confirm</button></td>
                </tr>
                <?php } ?>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'payout';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
    $('.confirm-button').on('click', function(){
        console.log(123);
        $.post('/admin/payouts/markAsPaid', {'provider': $(this).data('provider'), 'period': $(this).data('period')})
            .done(function(res){
                window.location.href = '/admin/payouts';
            })
    })
</script>

