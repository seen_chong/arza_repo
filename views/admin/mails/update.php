<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->mail->id ?>"/>

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <h4>General</h4>

                <div class="form-group">
                    <label>Title</label>
                    <?php echo $model->form->editorFor("name"); ?>
                </div>

                <div class="form-group">
                    <label>Mail</label>
                    <?php echo $model->form->textAreaFor("email", ["rows" => "25", "rows" => "65"]); ?>
                </div>
                <a class="btn btn-warning" id="generate_preview">Show preview</a>

                <div class="form-group">
                    <label>Preview</label>

                    <div id="preview"><?php echo $model->mail->email ?></div>
                </div>

            </div>
        </div>
        <div class="col-md-12">
            <div class="box">
                <h4>Contact Details</h4>

                <div class="form-group">
                    <label>Images</label> <br>
                    <input id="sortpicture" type="file" name="sortpic"/>
                    <a herf="" id="upload" style="cursor:pointer;color:red;">Upload</a>

                    <div id='preview_img'></div>
                    <?
                    $path = 'content/uploads/mails';
                    $images = scandir($path);
                    if (false !== $images) {
                        $images = preg_grep('/\\.(?:png|gif|jpe?g)$/', $images);
                        foreach ($images as $image) {
                            echo '<div id="' . str_replace(".", "_", htmlspecialchars(urlencode($image))) . '">';
                            echo '<img src="https://s3.amazonaws.com/wellns/' . htmlspecialchars(urlencode($image)) . '" style="width:100px" /> <a herf="" class="remove_image" attr_id="' . str_replace(".", "_", htmlspecialchars(urlencode($image))) . '" style="cursor:pointer;color:red;">[X]</a><br>';
                            echo '<input type="text" value="https://wellns.com' . htmlspecialchars(urlencode($image)) . '"><br>';
                            echo "</div>";
                        }

                    }
                    ?>

                </div>


            </div>
        </div>

        <div class="col-lg-24">
            <button type="submit" class="btn btn-success btn-lg">Save</button>
        </div>
    </div>
    </div>
</form>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script type='text/javascript'>
    $(document).ready(function () {

        $("#generate_preview").click(function () {
            var val = $("textarea[name='email']").val();

            $("#preview").html(val);
        });

        $('#upload').on('click', function () {
            var file_data = $('#sortpicture').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            if ($('#sortpicture').val() == "") {
                alert("Choose image!");
            } else {
                $.ajax({
                    url: '/admin/mails/upload', // point to server-side PHP script
                    dataType: 'text',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function (php_script_response) {
                        $("#preview_img").prepend(php_script_response);
                        $('#sortpicture').val('');
                    }
                });
            }
        });


        $('body').on('click', '.remove_image', function () {
            var image = $(this).attr('attr_id');


            $.ajax({
                url: '/admin/mails/remove',
                enctype: 'multipart/form-data',
                method: 'POST',
                data: {
                    image: image
                },
                success: function (data) {
                    $("#" + image).fadeOut();
                }
            });
        });


        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                    $("#preview-container").html(img);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
            $('#previewupload').show();
        });

        $("input[name='title']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-')
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });

    });

</script> 