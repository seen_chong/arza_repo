<?php if (count($model->mails) > 0): ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="40%">Mail</th>

                <th width="15%" class="text-center">Edit</th>

            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->mails as $mail): ?>
                <tr>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>mails/update/<?php echo $mail->id; ?>"><?php echo $mail->name; ?></a>
                    </td>

                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>mails/update/<?php echo $mail->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>

                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'pages/index';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;

</script>

