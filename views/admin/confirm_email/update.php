<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#test" aria-controls="general" role="tab" data-toggle="tab">General</a>
            </li>

            <?php if ($model->confirm_email->id > 0) { ?>
                <li role="presentation"><a href="#images-tab" aria-controls="images" role="tab"
                                           data-toggle="tab">Images</a></li>
            <? } ?>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="test">

                <input type="hidden" name="id" value="<?php echo $model->confirm_email->id; ?>"/>

                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>Email Name</label>
                                <select name="name" class="form-control">
                                    <?foreach($model->name as $key=>$item){?>
                                        <option value="<?=$item?>" <?echo $model->confirm_email->name==$item? 'selected':'' ?>><?=$item?></option>
                                    <?}?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Subject</label>
                                <?php echo $model->form->editorFor("subject"); ?>
                            </div>
                            <div class="form-group">
                                <label>Email Template</label>
                                <?php echo $model->form->textAreaFor("email_template", ['class' => 'ckeditor']); ?>
                            </div>
                            <div class="form-group">
                                <label>Available Syntax</label>
                                <p><?= implode('<br/>', \Model\Confirm_Email::$dynamicElements) ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php if ($model->confirm_email->id > 0) { ?>
                <div role="tabpanel" class="tab-pane" id="images-tab">
                    <div class="col-md-24">

                        <div class="box">
                            <label>Upload Email Images</label>
                            <div class="dropzone" id="dropzoneForm"
                                 action="<?php echo ADMIN_URL . 'confirm_email/uploadImage/' . $model->confirm_email->id; ?>">

                            </div>
                            <a id="upload-dropzone" class="btn btn-danger"
                               href="<?= ADMIN_URL . 'confirm_email/update/' . $model->confirm_email->id ?>">Upload</a><br/>
                            <br/>
                        </div>
                        <div>
                            <table id="image-container" class="table table-sortable-container">
                                <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>URL</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><img src="http://beta.arza.org/content/frontend/assets/img/navyLogoText.png" width="50"
                                             height="50"/>ARZA Logo</td>
                                    <td><?php echo 'http://beta.arza.org/content/frontend/assets/img/navyLogoText.png' ?></td>
                                </tr>
                                <?php foreach (explode(',', $model->confirm_email->confirm_email_images) as $image) { ?>
                                    <?php if ($image) { ?>
                                        <tr>
                                            <td><img src="/content/uploads/confirm_email/<?= $image ?>" width="50"
                                                     height="50"/></td>
                                            <td><?php echo 'http://beta.arza.org/content/uploads/confirm_email/'. $image ?></td>
                                            <!-- TODO:Change the Url to live site-->
                                            <td><a class="btn-actions delete-product-image"
                                                   href="<?= ADMIN_URL . 'confirm_email/delete_images/' . $model->confirm_email->id . '?index=' . $image; ?>">
                                                    <i class="icon-cancel-circled"></i></a></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>
            <? } ?>
        </div>
    </div>

    <button type="submit" class="btn btn-save">Save</button>

</form>


<?php footer(); ?>
<script type="text/javascript">
    var site_url = <?php echo json_encode(ADMIN_URL . 'confirm_email/');?>;
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
    });
</script>