<div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#account-info-tab" aria-controls="general" role="tab"
                                                  data-toggle="tab">Account Information</a></li>
    </ul>
    <form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $model->banner->id; ?>"/>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="account-info-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Account Information</h4>

                            <div class="form-group">
                                <label>Title</label>
                                <? echo $model->form->editorFor("title"); ?>
                            </div>
                            <div class="form-group">
                                <label>Url</label>
                                <? echo $model->form->editorFor("url"); ?>
                            </div>
                            <div class="form-group">
                                <label>Featured?</label>
                                <?php echo $model->form->checkBoxFor("featured", 1); ?>
                            </div>
                            <div class="form-group">
                                <label>Image</label>

                                <p>
                                    <small>(ideal profile photo size is 850 x 850)</small>
                                </p>
                                <p><input type="file" name="image" class='image'/></p>

                                <div style="display:inline-block">
                                    <?php
                                    $img_path = "";
                                    if ($model->banner->image != "" && \s3Bucket\s3Handler::doesObjectExist($model->banner->image)) {
                                        $img_path = S3_URL.$model->banner->image;
                                        ?>
                                        <div class="well well-sm pull-left"
                                             style="max-width:106.25px;max-height:106.25px;">
                                            <img src="<?php echo $img_path; ?>"/>
                                            <br/>
                                            <a href="<?= ADMIN_URL . 'banners/delete_image/' . $model->banner->id; ?>?photo=1"
                                               onclick="return confirm('Are you sure?');"
                                               class="btn btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="image" value="<?= $model->banner->image ?>"/>
                                        </div>
                                    <?php } ?>
                                    <div class='preview-container'
                                         style="max-width:106.25px;max-height:106.25px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
</div>
<div class="form-group">
    <button type="submit" class="btn btn-save">Save</button>
</div>
</form>
</div>

<?php footer(); ?>

<script>

    var services = <?php echo json_encode($model->provider->services); ?>;

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var img = $("<img />");
            reader.onload = function (e) {
                img.attr('src', e.target.result);
                img.attr('alt', 'Uploaded Image');
                img.attr("width", '100%');
                img.attr('height', '100%');
            };
            $(input).parent().parent().find('.preview-container').html(img);
            $(input).parent().parent().find('input[type="hidden"]').remove();

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(function () {
        $("select[name='provider_service[]']").val(services);

        $("select.multiselect").each(function (i, e) {
            //$(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });

        $("input.image").change(function () {
            readURL(this);
        });
        $("input.slider_1").change(function () {
            readURL(this);
        });
        $("input.slider_2").change(function () {
            readURL(this);
        });
        $("input.slider_3").change(function () {
            readURL(this);
        });
    })

</script>