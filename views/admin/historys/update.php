<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->history->id ?>"/>

    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>General</h4>
                <div class="form-group">
                    <label>Time</label>
                    <input name="time" id="startDate" class="time" value="<?=($model->history->id)?date('F Y',strtotime($model->history->time)):""?>" />
                </div>
                <div class="form-group">
                    <label>Title</label>
                    <?php echo $model->form->editorFor("title"); ?>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <?php echo $model->form->textAreaFor("description", ["class" => "ckeditor"]); ?>
                </div>
            </div>
        </div>

    </div>
        <div class="col-md-24">
            <button type="submit" class="btn btn-success btn-lg">Save</button>
        </div>
</form>


<?= footer(); ?>


<script type='text/javascript'>
    $(document).ready(function () {

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                    $("#preview-container").html(img);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
            $('#previewupload').show();
        });

        $("input[name='title']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-')
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        var year = $("input[name='year']").val();
        $('.time').datepicker( {
            format: "MM yyyy",
            viewMode: "months",
            minViewMode: "months",
            autoclose:true,
        });

    });

</script>