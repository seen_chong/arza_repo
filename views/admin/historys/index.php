<?php if (count($model->historys) > 0): ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="5%" class="text-center">ID</th>
                <th width="20%" class="text-center">Time</th>
                <th width="20%" class="text-center">Title</th>
                <th width="10%" class="text-center">Edit</th>
                <th width="10%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->historys as $obj) { ?>
                <tr>
                    <td class="text-center"><a
                            href="<?php echo ADMIN_URL; ?>historys/update/<?php echo $obj->id; ?>"><?php echo $obj->id; ?></a>
                    </td>
                    <td class="text-center"><a
                            href="<?php echo ADMIN_URL; ?>historys/update/<?php echo $obj->id; ?>"><?php echo $obj->time; ?></a>
                    </td>

                    <td>
                        <a href="<?php echo ADMIN_URL; ?>historys/update/<?php echo $obj->id; ?>"><?php echo $obj->title; ?></a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>historys/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>historys/delete/<?php echo $obj->id; ?>" onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php endif; ?>
<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'configs';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>

