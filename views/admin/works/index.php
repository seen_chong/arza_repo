<?php if (count($model->works) > 0): ?>
    <a href="<?=ADMIN_URL.'works/sort'?>">Manage Sections</a>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="5%" class="text-center">Id</th>
                <th width="20%" class="text-center">Name</th>
                <th width="30%" class="text-center">Title</th>
                <th width="35%" class="text-center">Image</th>
                <th width="10%" class="text-center">Edit</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->works as $obj) { ?>
                <tr>
                    <td class="text-center"><a
                            href="<?php echo ADMIN_URL; ?>works/update/<?php echo $obj->id; ?>"><?php echo $obj->id; ?></a>
                    </td>
                    <td class="text-center"><a
                            href="<?php echo ADMIN_URL; ?>works/update/<?php echo $obj->id; ?>"><?php echo $obj->section_name; ?></a>
                    </td>
                    <td class="text-center">
                        <a href="<?php echo ADMIN_URL; ?>works/update/<?php echo $obj->id; ?>"><?php echo $obj->title; ?></a>
                    </td>
                    <td class="text-center">
                        <?php
                        $img_path = "";
                        if($obj->featured_image != "" && file_exists(UPLOAD_PATH.'works'.DS.$obj->featured_image)){
                            $img_path = UPLOAD_URL . 'works/' . $obj->featured_image;
                            ?>
                            <img src="<?php echo $img_path; ?>" width="100" />
                        <?php } ?>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>works/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'configs';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>

