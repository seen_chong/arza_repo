<style>
    body.dragging, body.dragging * {
        cursor: move !important;
    }

    .dragged {
        position: absolute;
        opacity: 0.5;
        z-index: 2000;
    }
</style>
<div class="box">
    <h3><small>Drag to Order</small></h3>
        <ol class="sortable">
            <?foreach($model->sections as $section){
                echo $section->buildSortable();
            }?>
        </ol>
    <div class="serialize-result"></div>
</div>

<div class="box">
    <h3><small>Show on Page?</small></h3>
   <?foreach (\Model\Work::getList(['orderBy'=>'display_order asc']) as $item){
       $checked = $item->display == 1? 'checked':''?>
       <input type="checkbox" <?=$checked?> data-id="<?=$item->id?>"><label><?=$item->section_name?></label><br>
    <?}?>
</div>
<?php echo footer(); ?>
<script>
    $(document).ready(function(){
        var adjustment;
        var serial = $('.sortable').sortable({
            group:'sortable',
            onDrop: function ($item, container, _super) {
                var data = serial.sortable("serialize").get();
                var jsonData = JSON.stringify(data,null,'');

//                $('.serialize-result').text(jsonData);
                $.post('/admin/works/updateSection',{data:jsonData},function(){
                    console.log('done');
                });
                _super($item,container);
            },
            onDragStart: function ($item, container, _super) {
                var offset = $item.offset(),
                    pointer = container.rootGroup.pointer;

                adjustment = {
                    left: pointer.left - offset.left,
                    top: pointer.top - offset.top
                };

                _super($item, container);
            },
            onDrag: function ($item, position) {
                $item.css({
                    left: position.left - adjustment.left,
                    top: position.top - adjustment.top
                });
            }
        });

        $('input[type="checkbox"]').on('click',function(){
            var display = $(this).is(':checked');
            var id = $(this).attr('data-id');
            $.post('/admin/works/updateDisplay',{display:display,id:id},function(obj){
                window.location.reload();
            })
        })
    })
</script>