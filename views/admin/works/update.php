<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->work->id ?>"/>

    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>General</h4>
                <div class="form-group">
                    <label>Section Name</label>
                    <input type="text" value="<?= $model->work->section_name ?>" name="section_name" readonly/>
                </div>
                <div class="form-group">
                    <label>Title</label>
                    <?php echo $model->form->textAreaFor("title", ["class" => "ckeditor"]); ?>
                </div>
                <div class="form-group">
                    <label>Subtitle</label>
                    <?php echo $model->form->textAreaFor("subtitle", ["class" => "ckeditor"]); ?>
                </div>
                <div class="form-group">
                    <label>Display Order</label>
                    <?php echo $model->form->editorFor("display_order"); ?>
                </div>
                <div class="form-group">
                    <label>
                        <?php echo $model->form->checkBoxFor("display", 1); ?>  Display?
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>Meta Information</h4>

                <div class="form-group">
                    <label>Featured image</label>

                    <p>
                        <small>(ideal featured image size is 1920 x 300)</small>
                    </p>
                    <?php
                    $img_path = "";
                    if ($model->work->featured_image != "" && file_exists(UPLOAD_PATH . 'works' . DS . $model->work->featured_image) ) {
                        $img_path = UPLOAD_URL . 'works/' . $model->work->featured_image;
                    }
                    ?>
                    <p><input type="file" name="featured_image" class='image'/></p>
                    <?php if ($model->work->featured_image != "") { ?>
                        <div class="well well-sm pull-left">
                            <img src="<?php echo $img_path; ?>" width="1200"/>
                            <br/>
                            <a href="<?= ADMIN_URL . 'works/delete_image/' . $model->work->id . '/?featured_image=1'; ?>"
                               onclick="return confirm('Are you sure?');"
                               class="btn btn-default btn-xs">Delete</a>
                            <input type="hidden" name="featured_image"
                                   value="<?= $model->work->featured_image ?>"/>
                        </div>
                    <?php } ?>
                    <div id='preview-container'></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="col-md-24">
        <button type="submit" class="btn btn-success btn-lg">Save</button>
    </div>
</form>


<?= footer(); ?>


<script type='text/javascript'>
    $(document).ready(function () {

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                    $("#preview-container").html(img);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
            $('#previewupload').show();
        });

        $("input[name='title']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-')
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });

    });

</script>