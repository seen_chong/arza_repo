<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">

    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#test" aria-controls="general" role="tab" data-toggle="tab">General</a>
            </li>

            <li role="presentation"><a href="#content-section" aria-controls="sections" role="tab" data-toggle="tab">Author</a>
            </li>
            <li role="presentation"><a href="#content-meta" aria-controls="sections" role="tab" data-toggle="tab">Meta Information</a>
            </li>

        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="test">
                <input type="hidden" name="id" value="<?php echo $model->blog->id ?>"/>
                <input hidden name="insert_time" value="<?php if (isset($model->blog->insert_time)) {
                    echo $model->blog->insert_time;
                } else {
                    echo date("Y-m-d H:i:s");
                } ?>"/>

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>Title</label>
                                <?php echo $model->form->editorFor("title"); ?>
                            </div>
                            <div class="form-group">
                                <label>Category</label>
                                <select name="category">
                                    <?php foreach (\Model\Blog_Category::getList() as $cat) {
                                        $selected = ($cat->id == $model->blog->category) ? " selected='selected'" : "";
                                        ?>
                                        <option
                                            value="<?php echo $cat->id; ?>"<?php echo $selected; ?>><?php echo $cat->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Featured?</label>
                                <?php echo $model->form->checkBoxFor("featured", 1); ?>
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <select name="status">
                                    <?foreach (\Model\Blog::$status as $key=>$value){
                                        $select = $model->blog->status == $key? " selected='selected'" : ""?>
                                        <option value="<?=$key?>" <?=$select?>><?=$value?></option>
                                    <?}?>
<!--                                    --><?// if ($model->blog->status == 0) { ?>
<!---->
<!--                                        <option value="0">Not approved</option>-->
<!--                                        <option value="1">Approved</option>-->
<!--                                    --><?// } else { ?>
<!--                                        <option value="1">Approved</option>-->
<!--                                        <option value="0">Not approved</option>-->
<!--                                    --><?// } ?>

                                </select>
                            </div>


                            <div class="form-group">
                                <label>Create Time</label>
                                <?php echo $model->form->editorFor("date_modified"); ?>
                            </div>
                            <div class="form-group" id="scheduled">
                                <label>Schedule Post Date</label>
                                <input type="text" name="post_date"
                                       value="<?php echo ($model->blog->id > 0) ? date("m/d/Y", $model->blog->post_date) : ""; ?>" <?=$model->blog->status==2? '': 'disabled'?> />
                            </div>
                            <div class="form-group">
                                <label>Preview</label>
                                <?php echo $model->form->textAreaFor("preview_description", ["class" => "ckeditor"]); ?>
                            </div>
                            <div class="form-group">
                                <label>Introduction</label>
                                <?php echo $model->form->textAreaFor("introduction", ["class" => "ckeditor"]); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">

                        <div class="box">
                            <div class="form-group">
                                <label>Blog Content</label>
                                <?php echo $model->form->textAreaFor("description", ["class" => "ckeditor"]); ?>
                            </div>

                            <label>Upload Blog Images</label>
                            <div class="dropzone" id="dropzoneForm"
                                 action="<?php echo ADMIN_URL . 'blogs/uploadImage/' . $model->blog->id; ?>">

                            </div>
                            <a id="upload-dropzone" class="btn btn-danger"
                               href="<?= ADMIN_URL . 'blogs/update/' . $model->blog->id ?>">Upload</a><br/>
                            <br/>
                        </div>
                        <div>
                            <table id="image-container" class="table table-sortable-container">
                                <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>URL</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach (explode(',', $model->blog->blog_images) as $image) { ?>
                                    <?php if ($image) { ?>
                                        <tr>
                                            <td><img src="/content/uploads/blogs/<?= $image ?>" width="50"
                                                     height="50"/></td>
                                            <td><?php echo UPLOAD_URL . 'blogs/'. $image ?></td>
                                            <td><a class="btn-actions delete-product-image"
                                                   href="<?= ADMIN_URL . 'blogs/delete_images/' . $model->blog->id . '?index=' . $image; ?>">
                                                    <i class="icon-cancel-circled"></i></a></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="content-section">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="form-group">
                                <label>Author</label>
                                <?php echo $model->form->editorFor("author"); ?>
                            </div>
                            <div class="form-group">
                                <label>Author Description</label>
                                <?php echo $model->form->editorFor("author_description"); ?>
                            </div>
                            <div class="form-group">
                                <label>Author Image</label>

                                <p>
                                    <small>(ideal profile photo size is 850 x 850)</small>
                                </p>
                                <p><input type="file" name="author_image" class='image'/></p>

                                <div style="display:inline-block">
                                    <?php
                                    $img_path = "";
                                    if ($model->blog->author_image != "" && file_exists(UPLOAD_PATH . 'blogs' . DS . $model->blog->author_image)) {
                                        $img_path = UPLOAD_URL . 'blogs/' . $model->blog->author_image;
                                        ?>
                                        <div class="well well-sm pull-left"
                                             style="max-width:106.25px;max-height:106.25px;">
                                            <img src="<?php echo $img_path; ?>"/>
                                            <br/>
                                            <a href="<?= ADMIN_URL . 'blogs/delete_image/' . $model->blog->id; ?>?type=author_image"
                                               onclick="return confirm('Are you sure?');"
                                               class="btn btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="author_image"
                                                   value="<?= $model->blog->author_image ?>"/>
                                        </div>
                                    <?php } ?>
                                    <div class='preview-container'
                                         style="max-width:106.25px;max-height:106.25px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div role="tabpanel" class="tab-pane" id="content-meta">
                <div class="col-md-12">
                    <div class="box">
                        <h4>Meta Information</h4>

                        <div class="form-group">
                            <label>Featured image</label>

                            <p>
                                <small>(ideal featured image size is 1920 x 300)</small>
                            </p>
                            <?php
                            $img_path = "";
                            if ($model->blog->featured_image != "") {
                                $img_path = UPLOAD_URL . 'blogs/' . $model->blog->featured_image;
                            }
                            ?>
                            <p><input type="file" name="featured_image" class='image'/></p>
                            <?php if ($model->blog->featured_image != "") { ?>
                                <div class="well well-sm pull-left">
                                    <img src="<?php echo $img_path; ?>" width="100"/>
                                    <br/>
                                    <a href="<?= ADMIN_URL . 'blogs/delete_image/' . $model->blog->id . '/?featured_image=1'; ?>"
                                       class="btn btn-default btn-xs">Delete</a>
                                    <input type="hidden" name="featured_image"
                                           value="<?= $model->blog->featured_image ?>"/>
                                </div>
                            <?php } ?>
                            <div class='preview-container'></div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label>Slug</label>
                            <?php echo $model->form->editorFor("slug"); ?>
                        </div>
                        <div class="form-group">
                            <label>Meta title</label>
                            <?php echo $model->form->editorFor("meta_title"); ?>
                        </div>
                        <div class="form-group">
                            <label>Meta keywords</label>
                            <?php echo $model->form->textAreaFor("meta_keywords"); ?>
                        </div>
                        <div class="form-group">
                            <label>Meta description</label>
                            <?php echo $model->form->textAreaFor("meta_description", ["rows" => "3"]); ?>
                        </div>
                        <!--div class="checkbox form-group">
					<label>
						<?php echo $model->form->checkBoxFor("active", 1); ?>  Active?
					</label>
				</div-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-24">
        <button type="submit" class="btn btn-success btn-lg">Save</button>
    </div>
</form>


<?= footer(); ?>


<script type='text/javascript'>
    $(document).ready(function () {

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '360');
                    img.attr('height', '127.5');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
            $('#previewupload').show();
        });

        $("input[name='title']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-')
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });

        $("select[name='status']").on('change',function(e){
            var val = $(this).val();
            if(val == 2){
                $("input[name='post_date']").prop('disabled',false);
            }else{
                $("input[name='post_date']").val('');
                $("input[name='post_date']").prop('disabled',true);
            }
        });

        $(function () {
            var date = $("input[name='date_modified']");
            date.datepicker({
//				onSelect: function(date){
//					alert(date);
//				}
                format: "MM d, yyyy",
//                dateFormat: "DD, MM d, yy"
            });
        });
        $(function () {
            var date = $("input[name='post_date']");
            date.datepicker({
//				onSelect: function(date){
//					alert(date);
//				}
//                format: "MM d, yyyy",
//                dateFormat: "DD, MM d, yy"
            });
        });

    });

</script>