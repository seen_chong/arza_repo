<a class="btn-link btn-lg" href="<?=ADMIN_URL.'blog_categories'?>">Manage Categories</a>
<a class="btn-link btn-lg btn-add" href="<?=ADMIN_URL.'blog_categories/update'?>">Add a Category</a>
<?php if (count($model->blogs) > 0): ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="5%"> </th>
                <th width="35%">Page</th>
                <th width="30%">Author</th>
                <th width="15%" class="text-center">Edit</th>
                <th width="15%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->blogs as $blog): ?>
                <tr>
                    <td><? if ($blog->status != 1) { ?>
                            <a target="_blank" href="<?php echo SITE_URL; ?>blog/post/<?php echo $blog->slug; ?>?draft=true">
                            <button type="button" class="btn btn-danger" disabled="disabled"><?= $blog->status==0 ? '' :'Scheduled'?> Draft</button>
                            </a>
                        <? } ?></td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>blogs/update/<?php echo $blog->id; ?>"><?php echo $blog->title; ?></a>
                    </td>
                    <td><?=$blog->author?></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>blogs/update/<?php echo $blog->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>blogs/delete/<?php echo $blog->id; ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'blogs/index';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;

</script>

