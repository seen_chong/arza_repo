<div class="box box-table">
    <table class="table">
        <thead>
        <tr>
            <th width="10%">Date and time</th>
            <th width="10%">Status</th>
            <th width="10%">Service</th>
            <th width="10%">Client name</th>
            <th width="10%">Provider name</th>
            <th width="10%">Location</th>
            <th width="10%">Package</th>
        </tr>
        </thead>
        <tbody>

        <?php foreach ($model->schedules as $obj) { ?>
            <? $provider = \Model\Provider::getItem($obj->provider_id); ?>
            <? $user = \Model\User::getItem($obj->user_id); ?>
            <? $service = \Model\Service::getItem($obj->service); ?>
            <? $order_product = \Model\Order_Product::getItem($obj->order_product_id); ?>
            <tr>

                <td>
                    <?= $obj->day ?> at <?= $obj->time ?>
                </td>
                <td>
                    <? if ($obj->status == 0) {
                        $status = "Pending";
                        $bck = "#88885D";
                    } elseif ($obj->status == 1) {
                        $status = "Confirmed";
                        $bck = "green";
                    } else {
                        $status = "Rejected";
                        $bck = "red";
                    } ?>
                    <button type="button" class="btn btn-success"
                            style="position:relative;background:<?= $bck ?>;border-color:<?= $bck ?>;"
                            disabled="disabled">
                        <?= $status ?>
                    </button>
                </td>
                <td>
                    <?= $service->name ?>
                </td>
                <td>
                    <a href="/admin/users/update/<?= $user->id ?>"><?= $user->first_name ?> <?= $user->last_name ?></a>
                </td>
                <td>
                    <a href="/admin/providers/update/<?= $provider->id ?>"><?= $provider->first_name ?> <?= $provider->last_name ?></a>
                </td>
                <td>
                    <?= $obj->session_location ?>
                </td>
                <td>
                    x<?= $order_product->max_using ?> <?= $service->name ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <div class="box-footer clearfix">
        <div class='paginationContent'></div>
    </div>
</div>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'schedules';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
    */
</script>