<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#test" aria-controls="general" role="tab" data-toggle="tab">General</a>
            </li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="test">
                <input type="hidden" name="id" value="<?php echo $model->service->id; ?>"/>
                <input name="token" type="hidden" value="<?php echo get_token(); ?>"/>

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>Name</label>
                                <?php echo $model->form->editorFor("name"); ?>
                            </div>
                            <div class="form-group">
                                <label>Slogan</label>
                                <?php echo $model->form->editorFor("slogan"); ?>
                            </div>
                            <div class="form-group">
                                <label>Display Order</label>
                                <?php echo $model->form->editorFor("display_order"); ?>
                            </div>
                            <div class="form-group">
                                <label>Sub service for</label><BR>
                                <select name="sub_service">
                                    <option value="0">Make it main service</option>
                                    <?php foreach ($model->services as $sub) {

                                        ?>
                                        <option
                                            value="<?php echo $sub->id; ?>" <? if ($model->service->sub_service == $sub->id) {
                                            echo "selected";
                                        } ?> <? if (isset($_GET['sub']) && $_GET['sub'] == $sub->id) {
                                            echo "selected";
                                        } ?>><?php echo $sub->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <?php echo $model->form->textAreaFor("description", ["class" => "ckeditor"]); ?>
                            </div>
                            <div class="form-group">
                                <label>Landing description</label>
                                <?php echo $model->form->textAreaFor("description_landing", ["class" => "ckeditor"]); ?>
                            </div>
                            <div class="form-group">
                                <label>Home page description</label>
                                <?php echo $model->form->textAreaFor("description_home", ["class" => "ckeditor"]); ?>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>SEO</h4>

                            <div class="form-group">
                                <label>Slug</label>
                                <?php echo $model->form->editorFor('slug'); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Title</label>
                                <?php echo $model->form->editorFor('meta_title'); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Keywords</label>
                                <?php echo $model->form->editorFor('meta_keywords'); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Description</label>
                                <?php echo $model->form->textAreaFor('meta_description'); ?>
                            </div>
                        </div>
                        <div class="box">
                            <div class="form-group">
                                <h4>service</h4>

                                <p>
                                    <small>(ideal image size is 720 x 255)</small>
                                </p>
                                <?php
                                $img_path = "";
                                if ($model->service->featured_image != "") {
                                    $img_path = S3_URL . $model->service->featured_image;
                                }
                                ?>
                                <p><input type="file" name="featured_image" class='image'/></p>
                                <?php if ($model->service->featured_image != "") { ?>
                                    <div class="well well-sm pull-left">
                                        <div id='image-preview'>
                                            <img src="<?php echo $img_path; ?>" width="360" height="127.5"/>
                                            <br/>
                                            <a href=<?= ADMIN_URL . 'services/delete_image/' . $model->service->id . '?featured_image=1'; ?> class="btn
                                               btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="featured_image"
                                                   value="<?= $model->service->featured_image ?>"/>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class='preview-container'></div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label>Slider 1</label>

                                <p>
                                    <small>(ideal profile photo size is 850 x 850)</small>
                                </p>
                                <p><input type="file" name="slider_1" class='image'/></p>

                                <div style="display:inline-block">
                                    <?php
                                    $img_path = "";
                                    if ($model->service->slider_1 != "" && \s3Bucket\s3Handler::doesObjectExist($model->service->slider_1)) {
                                        $img_path = S3_URL . $model->service->slider_1;
                                        ?>
                                        <div class="well well-sm pull-left"
                                             style="max-width:106.25px;max-height:106.25px;">
                                            <img src="<?php echo $img_path; ?>"/>
                                            <br/>
                                            <a href="<?= ADMIN_URL . 'services/delete_image/' . $model->service->id; ?>?photo=1"
                                               onclick="return confirm('Are you sure?');"
                                               class="btn btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="slider_1"
                                                   value="<?= $model->service->slider_1 ?>"/>
                                        </div>
                                    <?php } ?>
                                    <div class='preview-container'
                                         style="max-width:106.25px;max-height:106.25px;"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Slider 2</label>

                                <p>
                                    <small>(ideal profile photo size is 850 x 850)</small>
                                </p>
                                <p><input type="file" name="slider_2" class='image'/></p>

                                <div style="display:inline-block">
                                    <?php
                                    $img_path = "";
                                    if ($model->service->slider_2 != "" && \s3Bucket\s3Handler::doesObjectExist($model->service->slider_2)) {
                                        $img_path = S3_URL . $model->service->slider_2;
                                        ?>
                                        <div class="well well-sm pull-left"
                                             style="max-width:106.25px;max-height:106.25px;">
                                            <img src="<?php echo $img_path; ?>"/>
                                            <br/>
                                            <a href="<?= ADMIN_URL . 'services/delete_image/' . $model->service->id; ?>?photo=1"
                                               onclick="return confirm('Are you sure?');"
                                               class="btn btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="slider_2"
                                                   value="<?= $model->service->slider_2 ?>"/>
                                        </div>
                                    <?php } ?>
                                    <div class='preview-container'
                                         style="max-width:106.25px;max-height:106.25px;"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Slider 3</label>

                                <p>
                                    <small>(ideal profile photo size is 850 x 850)</small>
                                </p>
                                <p><input type="file" name="slider_3" class='image'/></p>

                                <div style="display:inline-block">
                                    <?php
                                    $img_path = "";
                                    if ($model->service->slider_3 != "" && \s3Bucket\s3Handler::doesObjectExist($model->service->slider_3)) {
                                        $img_path = S3_URL . $model->service->slider_3;
                                        ?>
                                        <div class="well well-sm pull-left"
                                             style="max-width:106.25px;max-height:106.25px;">
                                            <img src="<?php echo $img_path; ?>"/>
                                            <br/>
                                            <a href="<?= ADMIN_URL . 'services/delete_image/' . $model->service->id; ?>?photo=1"
                                               onclick="return confirm('Are you sure?');"
                                               class="btn btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="slider_3"
                                                   value="<?= $model->service->slider_3 ?>"/>
                                        </div>
                                    <?php } ?>
                                    <div class='preview-container'
                                         style="max-width:106.25px;max-height:106.25px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
<script type='text/javascript'>
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '360');
                    img.attr('height', '127.5');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
        });
    });
</script>