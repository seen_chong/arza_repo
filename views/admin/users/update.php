<?
$donations = \Model\Donation::getList(['where'=>'user_id ='.$model->user->id]);
$rsvp_events = \Model\Rsvp::getList(['where'=>'user_id = '.$model->user->id]);
?>

<div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#account-info-tab" aria-controls="general" role="tab"
                                                  data-toggle="tab">Account Information</a></li>
        <li role="presentation"><a href="#donation-list-tab" aria-controls="donation" role="tab" data-toggle="tab">Donations</a></li>
        <li role="presentation"><a href="#event-list-tab" aria-controls="event" role="tab" data-toggle="tab">Events</a>
        <li role="presentation"><a href="#order-list-tab" aria-controls="order" role="tab" data-toggle="tab">Orders</a>
        </li>
    </ul>
    <form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $model->user->id; ?>"/>
        <input type="hidden" name="insert_time" value="<?php echo $model->user->insert_time; ?>"/>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="account-info-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Account Information</h4>

                            <div class="form-group">
                                <label>First name</label>
                                <?php echo $model->form->editorFor('first_name'); ?>
                            </div>
                            <div class="form-group">
                                <label>Last name</label>
                                <?php echo $model->form->editorFor('last_name'); ?>
                            </div>
                            <div class="form-group">
                                <label>Email Address</label>
                                <?php echo $model->form->editorFor('email'); ?>
                            </div>
                            <div class="form-group">
                                <label>Congregation</label>
                                <?php echo $model->form->editorFor('company'); ?>
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <?php echo $model->form->editorFor('phone'); ?>
                            </div>
                            <div class="form-group">
                                <label>Membership Type</label>
                                <select name="subtype">
                                    <option value="0">Select Type</option>
                                    <?foreach (\Model\Member_Type::getParentList() as $parent){
                                        if(count(\Model\Member_Type::getSubList($parent->id))==0){
                                                $selected = $model->user->subtype == $parent->id? 'selected' : '' ?>
                                                <option value="<?=$parent->id?>" <?=$selected?>><?=$parent->name?></option>
                                        <?}else{
                                            foreach (\Model\Member_Type::getSubList($parent->id) as $sub){
                                            $selected = $model->user->subtype == $sub->id? 'selected' : ''?>
                                            <option value="<?=$sub->id?>" <?=$selected?>><?=$parent->name?> -- <?=$sub->name?></option>
                                    <?}}?>
                                    <?}?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Password (leave blank to keep current password)</label>
                                <input type="password" name="password"/>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div role="tabpanel" class="tab-pane" id="donation-list-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Donations</h4>
                            <table id="data-list" class="table">
                                <thead>
                                <tr>
                                    <th width="30%">Amount</th>
                                    <th width="30%">Date</th>
                                    <th width="20%">Type</th>
                                    <th width="15%" class="text-center">Edit</th>
                                    <th width="15%" class="text-center">Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <? if ($donations) {
                                    foreach ($donations as $obj) {
                                        ?>
                                        <tr class="originalProducts">
                                            <td>
                                                <a href="<?php echo ADMIN_URL; ?>donations/update/<?php echo $obj->id; ?>">
                                                    <?php echo '$'.number_format($obj->amount); ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="<?php echo ADMIN_URL; ?>donations/update/<?php echo $obj->id; ?>">
                                                    <?$insert_time = new DateTime($obj->insert_time);
                                                    $insert_time = $insert_time->format('m-d-Y H:i:s');?>
                                                    <?php echo $insert_time ; ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a href="<?php echo ADMIN_URL; ?>donations/update/<?php echo $obj->id; ?>">
                                                    <?php echo $obj->getType(); ?>
                                                </a>
                                            </td>

                                            <td class="text-center">
                                                <a class="btn-actions"
                                                   href="<?= ADMIN_URL ?>donations/update/<?= $obj->id ?>">
                                                    <i class="icon-pencil"></i>
                                                </a>
                                            </td>
                                            <td class="text-center">
                                                <a class="btn-actions"
                                                   href="<?= ADMIN_URL ?>donations/delete/<?= $obj->id ?>?token_id=<?php echo get_token(); ?>"
                                                   onClick="return confirm('Are You Sure?');">
                                                    <i class="icon-cancel-circled"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?
                                    }
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>

            <div role="tabpanel" class="tab-pane" id="event-list-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Event</h4>
                            <table id="data-list" class="table">
                                <thead>
                                <tr>
                                    <th width="30%">Name</th>
                                    <th width="35%">Start Time</th>
                                    <th width="35%">End Time</th>
                                </tr>
                                </thead>
                                <tbody>
                                <? if ($rsvp_events) {
                                    foreach ($rsvp_events as $rsvp_event) {
                                        $obj = \Model\Event::getItem($rsvp_event->event_id);
                                        ?>
                                        <tr class="originalProducts">
                                            <td>

                                                    <?php echo $obj->name; ?>

                                            </td>
                                            <td>

                                                    <?php echo date("m/d/Y g:iA", $obj->start_time); ?>

                                            </td>
                                            <td>

                                                    <?php echo date("m/d/Y g:iA", $obj->end_time); ?>

                                            </td>

                                        </tr>
                                        <?
                                    }
                                } ?>
                                </tbody>
                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
</div>
<div class="form-group">
    <button type="submit" class="btn btn-save">Save</button>
</div>
</form>
</div>

<?php footer(); ?>

<script>

    var services = <?php echo json_encode($model->provider->services); ?>;

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var img = $("<img />");
            reader.onload = function (e) {
                img.attr('src', e.target.result);
                img.attr('alt', 'Uploaded Image');
                img.attr("width", '100%');
                img.attr('height', '100%');
            };
            $(input).parent().parent().find('.preview-container').html(img);
            $(input).parent().parent().find('input[type="hidden"]').remove();

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(function () {
        $("select[name='provider_service[]']").val(services);

        $("select.multiselect").each(function (i, e) {
            //$(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });

        $("input.image").change(function () {
            readURL(this);
        });
    })

</script>