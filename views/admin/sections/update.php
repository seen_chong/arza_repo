<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <?$content_style = (is_null($model->section->content_style))?'1':$model->section->content_style; ?>
    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#test" aria-controls="general" role="tab" data-toggle="tab">General</a>
            </li>

            <li role="presentation"><a href="#content-section" aria-controls="sections" role="tab" data-toggle="tab">Multiple Images</a>
            </li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="test">
                <input type="hidden" name="id" value="<?php echo $model->section->id; ?>"/>
                <input name="token" type="hidden" value="<?php echo get_token(); ?>"/>

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>
                            <div class="form-group">
                                <label>Content Name</label>
                                <?if($model->content!=''){?>
                                    <input hidden name="content_name" value = "<?=$model->content->id?>" readonly>
                                    <input hidden name="redirectTo" value="<?=ADMIN_URL.'contents/update/'.$model->content->id?>">
                                    <input value = "<?=$model->content->name?>"readonly>
                                <?}else{?>
                                    <select name="content_name">
                                        <option value="0">Select a Content</option>
                                        <?foreach ($model->contents as $content){
                                            $select = ($content->id==$model->section->content_name)?"selected":"";?>
                                            <option value="<?=$content->id?>" <?=$select?>><?=$content->name?></option>
                                        <?}?>
                                    </select>
                                <?}?>
                            </div>
                            <label>Section Style</label>
                            <select name="content_style">
                                <option value="-1" <?php if ($content_style == -1) {echo "selected";} ?> disabled>Select</option>
                                <option value="0" <?php if ($content_style == 0) {echo "selected";} ?>>No Image</option>
                                <option value="1" <?php if ($content_style == 1) {echo "selected";} ?>>Image as Background</option>
                                <option value="2" <?php if ($content_style == 2) {echo "selected";} ?>>Regular Image</option>
                                <option value="3" <?php if ($content_style == 3) {echo "selected";} ?>>Multiple Images</option>
                                <option value="4" <?php if ($content_style == 4) {echo "selected";} ?>>Image on Left</option>
                                <option value="5" <?php if ($content_style == 5) {echo "selected";} ?>>Reformed Zionism</option>
                                <option value="6" <?php if ($content_style == 6) {echo "selected";} ?>>Partners</option>
                                <option value="7" <?php if ($content_style == 7) {echo "selected";} ?>>Arza Officers</option>
                                <option value="8" <?php if ($content_style == 8) {echo "selected";} ?>>Whole Leaderships</option>
                            </select>
                            <div class="form-group">
                                <label>Name</label>
                                <?php echo $model->form->editorFor("name"); ?>
                            </div>
                            <div class="form-group" data-id = "6" <?=$content_style==6?'hidden':''?>>
                                <label>Title</label>
                                <?php echo $model->form->editorFor("title"); ?>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <?php echo $model->form->textAreaFor("description", ["class" => "ckeditor"]); ?>
                            </div>
                            <div class="form-group" data-id = "1" <?=$content_style==2||$content_style==4||$content_style==0||$content_style==7||$content_style==5?'hidden':''?>>
                                <label>Subtitle</label>
                                <?php echo $model->form->editorFor("subtitle"); ?>
                            </div>

                            <div class="form-group" data-id = "1" <?=$content_style==2||$content_style==4||$content_style==0||$content_style==7||$content_style==5?'hidden':''?>>
                                <label>Sub Description</label>
                                <?php echo $model->form->textAreaFor("sub_description", ["class" => "ckeditor"]); ?>
                            </div>
                            <div class="form-group">
                                <label>
                                    <?php echo $model->form->checkBoxFor("display", 1); ?>  Display?
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Display Order</label>
                                <?php echo $model->form->editorFor("display_order"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Meta Information</h4>
                            <div class="form-group" data-id = "7" <?=$content_style==3||$content_style==5?'hidden':''?>>
                                <label>Add Button</label><br>
                                <a class='btn-actions' id="add_desc" href="#">Add Button</a>
                                <div id="content_box">
                                    <?if(($content = json_decode($model->section->options,true)) != null){
                                        foreach($content['buttons'] as $value){?>
                                            <div class="form-group">
                                                <input type="text" name="button_title[]" placeholder="Button Title" value="<?=$value['title']?>">
                                                <input type="text" name="button_url[]" placeholder="Button URL" value="<?=$value['url']?>"/>
                                                <input type="checkbox" name="newpage[]" value="1" <?=$value['newpage']== '1'? "checked=\"checked\"":""?>>  Open in new Page? <br>
                                                <a href="#" class="delete_content">Delete</a>
                                            </div>
                                        <?}?>
                                    <?}?>
                                </div>
                            </div>
                            <div class="form-group" data-id = "2" <?=$content_style!=0&&$content_style!=7?'':'hidden'?>>
                                <label>Featured image</label>

                                <p>
                                    <small>(ideal featured image size is 1920 x 300)</small>
                                </p>
                                <?php
                                $img_path = "";
                                if ($model->section->featured_image != "" && file_exists(UPLOAD_PATH . 'sections' . DS . $model->section->featured_image) ) {
                                    $img_path = UPLOAD_URL . 'sections/' . $model->section->featured_image;
                                }
                                ?>
                                <p><input type="file" name="featured_image" class='image'/></p>
                                <?php if ($model->section->featured_image != "") { ?>
                                    <div class="well well-sm pull-left">
                                        <img src="<?php echo $img_path; ?>" width="1200"/>
                                        <br/>
                                        <a href="<?= ADMIN_URL . 'sections/delete_image/' . $model->section->id . '/?featured_image=1'; ?>"
                                           onclick="return confirm('Are you sure?');"
                                           class="btn btn-default btn-xs">Delete</a>
                                        <input type="hidden" name="featured_image"
                                               value="<?= $model->section->featured_image ?>"/>
                                    </div>
                                <?php } ?>
                                <div id='preview-container'></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="content-section">
                <div class="contentSelection box" data-id="4"  <?=$content_style==3||$content_style==5||$content_style==6?'':'hidden'?>>
                    <div class="form-group">
                        <label>Button</label>
                        <?php echo $model->form->editorFor("button"); ?>
                    </div>
                    <div class="form-group">
                        <label>Link</label>
                        <?php echo $model->form->editorFor("link"); ?>
                    </div>
                    <div class="form-group" data-id="5"  <?=$content_style==5?'':'hidden'?>>
                        <label>Image Description</label>
                        <?php echo $model->form->editorFor("image_description"); ?>
                    </div>
<!--                    <div class="form-group" data-id="5"  --><?//=$content_style==5?'':'hidden'?><!-->
<!--                        <label>Other Text</label>-->
<!--                        --><?php //echo $model->form->editorFor("notes"); ?>
<!--                    </div>-->

                    <label>Upload Two More Images</label>
                    <div class="dropzone" id="dropzoneForm"
                         action="<?php echo ADMIN_URL . 'sections/uploadImage/' . $model->section->id; ?>">

                    </div>
                    <a id="upload-dropzone" class="btn btn-danger"
                       href="<?= ADMIN_URL . 'sections/update/' . $model->section->id ?>">Upload</a><br/>
                    <br/>
                    <div>
                        <table id="image-container" class="table table-sortable-container">
                            <thead>
                            <tr>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach (explode(',', $model->section->images) as $image) { ?>
                                <?php if ($image) { ?>
                                    <tr>
                                        <td><img src="/content/uploads/sections/<?= $image ?>" width="50"
                                                 height="50"/></td>
                                        <td><?php echo $image ?></td>
                                        <td><a class="btn-actions delete-product-image"
                                               href="<?= ADMIN_URL . 'sections/delete_images/' . $model->section->id . '?index=' . $image; ?>">
                                                <i class="icon-cancel-circled"></i></a></td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
<script type='text/javascript'>
    $(document).ready(function () {

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                    $("#preview-container").html(img);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
            $('#previewupload').show();
        });

        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        $('#add_desc').on('click', function(){
            var content = $(
                '<div class="form-group"><input data-title-id="" type="text" name="button_title[]" placeholder="Button Title" value=""><input data-desc-id="" name="button_url[]" placeholder="Button URL" type="text"/><input type="checkbox" name="newpage[]" value=""> Open in new Page?<br><a href="#" class="delete_content">Delete</a></div>'
            );
            $('#content_box').append(content);
        });
        $(document).on('click','.delete_content', function(){
            $(this).parent().remove();
        });
        $("select[name='content_style']").on('change',function(){
            var content_style = $(this).val();
            $('.form-group').each(function(){
                $(this).attr('hidden',false);
            })
            $("div[data-id='4']").attr('hidden',true);
            if(content_style==0||content_style==7){
                $("div[data-id='1']").attr('hidden',true);
                $("div[data-id='2']").attr('hidden',true);
            }else if(content_style==2){
                $("div[data-id='1']").attr('hidden',true);
            }else if(content_style==4){
                $("div[data-id='1']").attr('hidden',true);
            }else if(content_style==3){
                $("div[data-id='4']").attr('hidden',false);
                $("div[data-id='5']").attr('hidden',true);
                $("div[data-id='7']").attr('hidden',true);
            }else if(content_style==5){
                $("div[data-id='4']").attr('hidden',false);
                $("div[data-id='1']").attr('hidden',true);
                $("div[data-id='7']").attr('hidden',true);
            }else if(content_style==6){
                $("div[data-id='6']").attr('hidden',true);
                $("div[data-id='5']").attr('hidden',true);
                $("div[data-id='4']").attr('hidden',false);
            }else if(content_style==7){
                $("div[data-id='6']").attr('hidden',true);
                $("div[data-id='5']").attr('hidden',true);
                $("div[data-id='4']").attr('hidden',false);
            }
        })
    });
</script>