<?php
if (count($model->sections) > 0): ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="45%">Content</th>
                <th width="45%">Name</th>
                <th width="15%" class="text-center">Edit</th>
                <th width="15%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->sections as $obj) { ?>
                <tr>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>sections/update/<?= $obj->id ?>" style="color: <?=($obj->getContentName()==false)?'grey':'Black'?>"><?php echo ($obj->getContentName()==false)?"No Contents":$obj->getContentName() ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>sections/update/<?= $obj->id ?>"><?php echo $obj->name; ?></a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?= ADMIN_URL ?>sections/update/<?= $obj->id ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?= ADMIN_URL ?>sections/delete/<?= $obj->id ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'sections';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>