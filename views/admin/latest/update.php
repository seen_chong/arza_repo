<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#test" aria-controls="general" role="tab" data-toggle="tab">General</a>
            </li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="test">
                <input type="hidden" name="id" value="<?php echo $model->latest->id; ?>"/>
                <input name="token" type="hidden" value="<?php echo get_token(); ?>"/>

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>Name</label>
                                <?php echo $model->form->editorFor("name"); ?>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <?php echo $model->form->textAreaFor("description", ["class" => "ckeditor"]); ?>
                            </div>
                            <div class="form-group">
                                <label>Date</label>
                                <input name="time" class="time" value="<?=($model->latest->id)?($model->latest->time):""?>" />
                            </div>
                            <div class="form-group">
                                <label>Url</label>
                                <?php echo $model->form->textBoxFor("url"); ?>
                            </div>
                            <div class="form-group">
                                <label>Display Order</label>
                                <?php echo $model->form->textBoxFor("display_order"); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <input type="hidden" id="act_for_click" name="redirectTo" value="<?= ADMIN_URL . 'latest/' ?>">
    <center style="    margin-bottom: 112px;">
        <button type="submit" id="save_close" class="btn btn-save">Save and close</button>
        <button type="submit" id="save" class="btn btn-save">Save</button>
    </center>
</form>

<?php echo footer(); ?>
<script type='text/javascript'>
    $(function () {
        $('.time').datepicker( {
            format: "M dd",
            viewMode: "days",
            minViewMode: "days",
            autoclose:true,
        });

        $('#save_close').mouseover(function () {
            $("#act_for_click").val("<?= ADMIN_URL . 'latest/' ?>");

        });

        $('#save').mouseover(function () {
            var id = $("input[name=id]").val();
            $("#act_for_click").val("<?=ADMIN_URL . 'latest/update/'?>" + id);

        });


    });
</script>