<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->contact->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>
    <div class="tab-content">
        <div role="tabpanel">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <h4>General</h4>
                        <div class="form-group">
                            <label>Contact Date</label>
                            <div><?$insert_time = new DateTime($model->contact->insert_time);
                                $insert_time = $insert_time->format('m-d-Y H:i:s');?>
                                <?php echo $insert_time ; ?></div>
                        </div>
                        <div class="form-group">
                            <label>Name</label>
                            <div><?=$model->contact->name?></div>
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" disabled="disabled" value="<?=$model->contact->email?>" />
                        </div>
                        <div class="form-group">
                            <label>Message</label>
                            <textarea disabled="disabled"><?=$model->contact->message?></textarea>
                        </div>

                    </div>
                </div>
                <div class="col-md-12">
                    <div class="box">
                        <h4>Response</h4>
                        <div class="form-group">
                            <label>Status</label>
                            <?= $model->form->dropDownListFor('admin_reply', \Model\Contact::$admin_reply); ?>
                        </div>
                        <div class="form-group">
                            <label>Subject</label>
                            <input type="text" value="<?=$model->contact->subject?>" name="subject"/>
                        </div>
                        <div class="form-group">
                            <label>Response</label>
                            <textarea name="answer"><?=$model->contact->answer?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <center style="    margin-bottom: 112px;">

        <button id="save" class="btn btn-save" type="submit">Save</button>
        <button id="save_close" class="btn btn-danger btn-save">Close</button>


    </center>
<!--</form>-->
<?php footer(); ?>

<script type="text/javascript">

    $(document).ready(function () {

        $('#save_close').on('click',function () {
            window.location.replace('<?=ADMIN_URL."contacts"?>');
        });

    })


</script>




























