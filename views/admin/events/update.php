<div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#event-info-tab" aria-controls="general" role="tab"
                                                  data-toggle="tab">Account Information</a></li>
        <li role="presentation"><a href="#participant-list-tab" aria-controls="participant" role="tab"
                                   data-toggle="tab">Participants</a></li>
        </li>
    </ul>

    <form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $model->event->id; ?>"/>
        <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="event-info-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>Event Name</label>
                                <?php echo $model->form->editorFor("name"); ?>
                            </div>
                            <div class="form-group">
                                <label>Date Range</label>
                                <input type="text" name="daterange"
                                       value="<?php echo ($model->event->id > 0) ? date("m/d/Y g:iA", $model->event->start_time) . ' - ' . date("m/d/Y g:iA", $model->event->end_time) : ""; ?>"/>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <?php echo $model->form->textAreaFor("description"); ?>
                            </div>
                            <div class="form-group">
                                <label>Capacity</label>
                                <?php echo $model->form->textBoxFor("capacity"); ?>
                            </div>
                            <div class="form-group">
                                <label>Price</label>
                                <?php echo $model->form->textBoxFor("price"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Location</h4>
                            <div class="form-group">
                                <label>Address</label>
                                <?php echo $model->form->textBoxFor('address'); ?>
                            </div>
                            <div class="form-group">
                                <label>Address 2</label>
                                <?php echo $model->form->textBoxFor('address2'); ?>
                            </div>
                            <div class="form-group">
                                <label>City</label>
                                <?php echo $model->form->textBoxFor('city'); ?>
                            </div>
                            <div class="form-group">
                                <label>State</label>
                                <?= $model->form->dropDownListFor('state', get_states(), 'Select', ['class' => 'form-control']); ?>
                            </div>
                            <div class="form-group">
                                <label>Zip Code</label>
                                <?php echo $model->form->textBoxFor('zip'); ?>
                            </div>
                            <div class="form-group">
                                <label>Country</label>
                                <?= $model->form->dropDownListFor('country', get_countries(), '', ['class' => 'form-control']); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Contact</h4>
                            <div class="form-group">
                                <label>Name</label>
                                <?php echo $model->form->textBoxFor('contact_name'); ?>
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <?php echo $model->form->textBoxFor('contact_number'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <div class="form-group">
                                <label>Featured image</label>

                                <p>
                                    <small>(ideal featured image size is 1920 x 300)</small>
                                </p>
                                <?php
                                $img_path = "";
                                if ($model->event->featured_image != "" && file_exists(UPLOAD_PATH . 'events' . DS . $model->event->featured_image)) {
                                    $img_path = UPLOAD_URL . 'events/' . $model->event->featured_image;
                                }
                                ?>
                                <p><input type="file" name="featured_image" class='image'/></p>
                                <?php if ($model->event->featured_image != "") { ?>
                                    <div class="well well-sm pull-left">
                                        <img src="<?php echo $img_path; ?>" width="1200"/>
                                        <br/>
                                        <a href="<?= ADMIN_URL . 'events/delete_image/' . $model->event->id . '/?featured_image=1'; ?>"
                                           onclick="return confirm('Are you sure?');"
                                           class="btn btn-default btn-xs">Delete</a>
                                        <input type="hidden" name="featured_image"
                                               value="<?= $model->event->featured_image ?>"/>
                                    </div>
                                <?php } ?>
                                <div id='preview-container'></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="participant-list-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>Participants</h4>
                            <a class="btn-actions" href="/admin/events/export?id=<?=$model->event->id?>"><i class="fa fa-download"></i> Export Participants</a>

                            <table id="data-list" class="table">
                                <thead>
                                <tr>
                                    <th width="5%">ID</th>
                                    <th width="15">Ref</th>
                                    <th width="15">Status</th>
                                    <th width="20%">Time</th>
                                    <th width="20%">Name</th>
                                    <th width="20%">Email</th>
                                    <th width="20%">Phone</th>
                                    <th width="10%">Type</th>
<!--                                    <th width="15%" class="text-center">Edit</th>-->
<!--                                    <th width="15%" class="text-center">Delete</th>-->
                                </tr>
                                </thead>
                                <tbody>
                                <?php $rsvps = \Model\Rsvp::getList(['where'=>'event_id = '.$model->event->id]);?>
                                <? if ($rsvps) {
                                    $i = 0;
                                    foreach ($rsvps as $obj) {
                                        $i ++;
                                        ?>
                                        <tr class="originalProducts">
                                            <td>
                                                <?php echo $i; ?>

                                            </td>
                                            <td>
                                                <?php echo $obj->ref_num; ?>

                                            </td>
                                            <td>
                                                <?php echo $obj->status; ?>

                                            </td>
                                            <td>
                                                    <?$insert_time = new DateTime($obj->insert_time);
                                                    $insert_time = $insert_time->format('m-d-Y H:i:s');?>
                                                    <?php echo $insert_time ; ?>
                                            </td>
                                            <td>
                                                <?php echo $obj->full_name(); ?>
                                            </td>
                                            <td>
                                                <?php echo $obj->email; ?>
                                            </td>
                                            <td>
                                                <?php echo $obj->phone; ?>
                                            </td>
                                            <td>
                                                    <? if ($obj->user_id == 0) { ?>
                                                            Guest
                                                    <? } else { ?>
                                                        <div class="input-group-btn">
                                                            <a href="<?php echo ADMIN_URL; ?>users/update/<?php echo $obj->user_id; ?>">
                                                                <button type="button" class="btn"><i class="icon-eye"></i>Members</button>
                                                            </a>
                                                        </div>
                                                    <? } ?>
                                            </td>
                                        </tr>
                                        <?
                                    }
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

            </div>
        </div>
</div>
<input type="hidden" id="act_for_click" name="redirectTo" value="<?= ADMIN_URL . 'events/' ?>">
<center style="    margin-bottom: 112px;">
    <button type="submit" id="save_close" class="btn btn-save">Save and close</button>
    <button type="submit" id="save" class="btn btn-save">Save</button>
</center>
</form>
<?php footer(); ?>

<script>
    $(function () {
        $('input[name="daterange"]').daterangepicker({
            timePicker: true,
            format: 'MM/DD/YYYY h:mmA',
            timePickerIncrement: 30,
            timePicker12Hour: true,
            timePickerSeconds: false,
            showDropdowns: true,
            <?php if($model->event->id > 0) { ?>
            startDate: "<?php echo date("m/d/Y g:iA", $model->event->start_time);?>",
            endDate: "<?php echo date("m/d/Y g:iA", $model->event->end_time);?>",
            <?php } ?>

        });
    });
    $(function () {

        $('#save_close').mouseover(function () {
            $("#act_for_click").val("<?=ADMIN_URL . 'events/'?>");

        });

        $('#save').mouseover(function () {
            var id = $("input[name=id]").val();
            $("#act_for_click").val("<?=ADMIN_URL . 'events/update/'?>" + id);

        });
    });

    $(document).ready(function () {

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '150');
                    $("#preview-container").html(img);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
            $('#previewupload').show();
        });


    });

</script>






























