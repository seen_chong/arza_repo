<!--<form class="form" action="--><?//= $this->emagid->uri ?><!--" method="post" enctype="multipart/form-data">-->
    <input type="hidden" name="id" value="<?php echo $model->applicant->id; ?>"/>
    <input type=hidden name="token" value="<?php echo get_token(); ?>"/>

    <div class="tab-content">
        <div role="tabpanel">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <h4>General</h4>
                        <div class="form-group">
                            <label>Name</label>
                            <div><?=$model->applicant->full_name()?></div>
                        </div>
                        <div class="form-group">
                            <label>Apply Date</label>
                            <div><?=$model->applicant->insert_time?></div>
                        </div>
                        <div class="form-group">
                            <label>Apply Position</label>
                            <div><a href="<?php echo ADMIN_URL; ?>positions/update/<?php echo $model->applicant->position_id; ?>"><?=\Model\Position::getItem($model->applicant->position_id)->name?></a></div>
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <div><?=$model->applicant->email?></div>
                        </div>
                        <div class="form-group">
                            <label>Phone</label>
                            <div><?=$model->applicant->phone?></div>
                        </div>
                        <div class="form-group">
                            <label>Comment</label>
                            <div><?=$model->applicant->comment?></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="box">
                        <h4>Resume</h4>
                        <div class="form-group">
                            <?=$model->applicant->resume?>
                            <a href="<?=UPLOAD_URL.'applicants/'.$model->applicant->resume?>" download="<?=$model->applicant->full_name().'_'.$model->applicant->resume?>">Download</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <center style="    margin-bottom: 112px;">
        <button id="save_close" class="btn btn-save">Close</button>

    </center>
<!--</form>-->
<?php footer(); ?>

<script type="text/javascript">

    $(document).ready(function () {

        $('#save_close').on('click',function () {
            window.location.replace('<?=ADMIN_URL."applicants"?>');
        })
    })


</script>




























