<a class="btn-link btn-lg" href="<?=ADMIN_URL.'israel_categories'?>">Manage Categories</a>
<a class="btn-link btn-lg btn-add" href="<?=ADMIN_URL.'israel_categories/update'?>">Add a Category</a>
<?php if (count($model->israels) > 0): ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="5%"> </th>
                <th width="35%">Page</th>
                <th width="30%">Author</th>
                <th width="15%" class="text-center">Edit</th>
                <th width="15%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->israels as $israel): ?>
                <tr>
                    <td><? if ($israel->status == 0) { ?>
                            <a target="_blank" href="<?php echo SITE_URL; ?>israel/post/<?php echo $israel->slug; ?>?draft=true">
                                <button type="button" class="btn btn-danger" disabled="disabled">Draft</button>
                            </a>
                        <? } ?></td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>israels/update/<?php echo $israel->id; ?>"><?php echo $israel->title; ?></a>
                    </td>
                    <td><?=$israel->author?></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>israels/update/<?php echo $israel->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>israels/delete/<?php echo $israel->id; ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'israels/index';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;

</script>

