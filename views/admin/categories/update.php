<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#test" aria-controls="general" role="tab" data-toggle="tab">General</a>
            </li>
            <?php if (isset($model->products) && count($model->products) > 0): ?>
                <li role="presentation"><a href="#cat-products" aria-controls="products" role="tab" data-toggle="tab">Products</a>
                </li>
            <? endif; ?>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="test">
                <input type="hidden" name="id" value="<?php echo $model->category->id; ?>"/>
                <input name="token" type="hidden" value="<?php echo get_token(); ?>"/>

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>Name</label>
                                <?php echo $model->form->editorFor("name"); ?>
                            </div>
                            <div class="form-group">
                                <label>Subtitle</label>
                                <?php echo $model->form->editorFor("subtitle"); ?>
                            </div>
                            <div class="form-group">
                                <label>Display Order</label>
                                <?php echo $model->form->editorFor("display_order"); ?>
                            </div>
                            <div class="form-group">
                                <label>Parent Category</label>
                                <select name="parent_category">
                                    <option value="0">None</option>
                                    <?php foreach ($model->categories as $category) {
                                        $select = ($category->id == $model->category->parent_category) ? " selected='selected'" : "";
                                        ?>
                                        <option
                                            value="<?php echo $category->id; ?>" <?php echo $select; ?> ><?php echo $category->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <?php echo $model->form->textAreaFor("description", ["class" => "ckeditor"]); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box">
                            <h4>SEO</h4>

                            <div class="form-group">
                                <label>Slug</label>
                                <?php echo $model->form->editorFor('slug'); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Title</label>
                                <?php echo $model->form->editorFor('meta_title'); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Keywords</label>
                                <?php echo $model->form->editorFor('meta_keywords'); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Description</label>
                                <?php echo $model->form->textAreaFor('meta_description'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php if (isset($model->products) && count($model->products) > 0): ?>
                <div role="tabpanel" class="tab-pane" id="cat-products">
                    <div class="rox">
                        <div class="box box-table">
                            <table id="data-list" class="table">
                                <thead>
                                <tr>
                                    <th width="5%">Image</th>
                                    <th width="45%">Name</th>
                                    <th width="22%">MPN</th>
                                    <th width="13%">Price</th>
                                    <th width="15%" class="text-center">Edit</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($model->products as $obj) { ?>
                                    <tr class="originalProducts">
                                        <input type="hidden" name="product_category[]" value="<?= $obj->id ?>"/>
                                        <td>
                                            <?php
                                            if ($obj->image_link == "http://www.itmustbetime.com/media/catalog/product" || $obj->image_link == '') {
                                                $img_path = ADMIN_IMG . 'itmustbetime_watch.png';
                                            } else {
                                                $img_path = $obj->image_link;
                                            }
                                            ?>
                                            <img src="<?php echo $img_path; ?>" width="50"/>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_URL; ?>products/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a>
                                        </td>
                                        <td><?= $obj->mpn ?></td>
                                        <td>$<?= number_format($obj->price, 2) ?></td>
                                        <td class="text-center">
                                            <a class="btn-actions"
                                               href="<?php echo ADMIN_URL; ?>products/update/<?php echo $obj->id; ?>">
                                                <i class="icon-pencil"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            <div class="box-footer clearfix">
                                <div class='paginationContent'></div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
<script type='text/javascript'>
    $(document).ready(function () {
        $("input[name='name']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
    });
</script>