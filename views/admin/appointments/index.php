<div class="row">
    <div class="col-md-8">
        <div class="box">
            <div class="input-group">
                <input id="search" type="text" name="search" class="form-control"
                       placeholder="Search by Email | by Lastname | by Firstname"/>
            <span class="input-group-addon">
                <i class="icon-search"></i>
            </span>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="box">
            Show on page:
            <select class="how_many" name="how_many" style="cursor:pointer">

                <option <? if (isset($_GET['how_many'])) {
                    if ($_GET['how_many'] == 10) {
                        echo "selected";
                    }
                } ?> selected value="10">10
                </option>
                <option <? if (isset($_GET['how_many'])) {
                    if ($_GET['how_many'] == 50) {
                        echo "selected";
                    }
                } ?> value="50">50
                </option>
                <option <? if (isset($_GET['how_many'])) {
                    if ($_GET['how_many'] == 100) {
                        echo "selected";
                    }
                } ?> value="100">100
                </option>
                <option <? if (isset($_GET['how_many'])) {
                    if ($_GET['how_many'] == 500) {
                        echo "selected";
                    }
                } ?> value="500">500
                </option>
                <option <? if (isset($_GET['how_many'])) {
                    if ($_GET['how_many'] == 1000) {
                        echo "selected";
                    }
                } ?> value="1000">1000
                </option>
            </select>
        </div>


    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
</div>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script>
    $(document).ready(function () {
        $('body').on('change', '.how_many', function () {
            var how_many = $(this).val();
            window.location.href = '/admin/providers?how_many=' + how_many;
        });
    });
</script><?php
if (count($model->appointments) > 0): ?>
    <div class="box box-table">
        <table id="data-list" class="table">
            <thead>
            <tr>
                <th width="30%">Provider</th>
                <th width="30%">Client</th>
                <th width="10%">Service</th>
                <th width="10%">Date</th>
                <th width="15%" class="text-center">Edit</th>
                <th width="15%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->appointments as $obj) {
                $providerServices = \Model\Provider_Services::getItem($obj->provider_service_id);
                $provider = \Model\Provider::getItem($providerServices->provider_id);
                $service = \Model\Service::getItem($providerServices->service_id);
                $user = \Model\User::getItem($obj->user_id) ?>
                <tr class="originalProducts">
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>appointments/upFte/<?php echo $obj->id; ?>">
                            <?php echo $provider->first_name . ' ' . $provider->last_name; ?>
                        </a>
                    </td>

                    <td>
                        <a href="<?php echo ADMIN_URL; ?>appointments/update/<?php echo $obj->id; ?>">
                            <?php echo $user->first_name . ' ' . $user->last_name; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>appointments/update/<?php echo $obj->id; ?>">
                            <?php echo $service->name; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>appointments/update/<?php echo $obj->id; ?>">
                            <?php echo $obj->date; ?>
                        </a>
                    </td>
                    <!--      <td>-->
                    <!--        --><? //if($obj->authorized){?>
                    <!--      <span style="color:green">AUTHORIZED</span>-->
                    <!--      --><? //}?>
                    <!--      </td>-->
                    <td class="text-center">
                        <a class="btn-actions" href="<?= ADMIN_URL ?>appointments/update/<?= $obj->id ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?= ADMIN_URL ?>appointments/delete/<?= $obj->id ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'appointments';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script type="text/javascript">
    $(function () {
        $("#search").keyup(function () {
            var url = "<?php echo ADMIN_URL; ?>appointments/search";
            var keywords = $(this).val();
            if (keywords.length > 0) {
                $.get(url, {keywords: keywords}, function (data) {
                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').hide();
                    $('.originalProducts').hide();
                    var list = JSON.parse(data);
                    for (key in list) {
                        var tr = $('<tr />');

                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>appointments/update/' + list[key].id).html(list[key].email));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>appointments/update/' + list[key].id).html(list[key].name));
                        var editTd = $('<td />').addClass('text-center').appendTo(tr);
                        var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>appointments/update/' + list[key].id);
                        var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                        tr.appendTo($("#data-list tbody"));
                    }

                });
            } else {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').show();
                $('.originalProducts').show();
            }
        });
    })
</script>