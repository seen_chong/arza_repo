<div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#session-info-tab" aria-controls="general" role="tab"
                                                  data-toggle="tab">Session Information</a></li>
    </ul>
    <form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $model->sessionhistory->id; ?>"/>
        <input type="hidden" name="insert_time" value="<?php echo $model->sessionhistory->insert_time; ?>"/>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="session-info-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Session Information</h4>

                            <div class="form-group">
                                <label>Provider</label>
                                <? echo $model->form->hiddenFor('provider_id') ?>
                                <input type="text"
                                       value="<?= \Model\Provider::getItem($model->sessionhistory->provider_id)->full_name() ?>"
                                       readonly>
                            </div>
                            <div class="form-group">
                                <label>Customer</label>
                                <? echo $model->form->hiddenFor('user_id') ?>
                                <input type="text"
                                       value="<?= \Model\User::getItem($model->sessionhistory->user_id)->full_name() ?>"
                                       readonly>
                            </div>
                            <div class="form-group">
                                <label>Service</label>
                                <select name="service_id">
                                    <? $pService = \Model\Provider_Services::getList(['where' => "active = 1 and provider_id = '" . $model->sessionhistory->provider_id . "'"]);
                                    foreach ($pService as $item) {
                                        $service = \Model\Service::getItem($item->service_id) ?>
                                        <option
                                            value="<?= $service->id ?>" <? if ($service->id == $model->sessionhistory->service_id) {
                                            echo 'selected';
                                        } ?>><?= $service->name ?></option>
                                    <? } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Credit Cost</label>
                                <?php echo $model->form->editorFor("credit") ?>
                            </div>
                            <div class="form-group">
                                <label>Location</label>
                                <?php echo $model->form->editorFor("location") ?>
                            </div>
                            <div class="form-group">
                                <label>Date</label>
                                <input name="date" type="text" value="<?= $model->sessionhistory->date ?>"
                                       id="datetimepicker">
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <select name="status">
                                    <?foreach($model->sessionhistory->status() as $key=>$status){?>
                                        <option value="<?=$key?>" <?=$model->sessionhistory->status == $key?'selected':'';?>><?=$status?></option>
                                    <?}?>
                                </select>
                            </div>
                            <?php echo $model->form->hiddenFor("credit_type_id") ?>
                            <input type="hidden" value="/admin" name="redirectTo">
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-save">Save</button>
        </div>
    </form>
</div>

<?php footer(); ?>

<link rel="stylesheet" type="text/css" href="<?= ADMIN_CSS ?>jquery.datetimepicker.css"/ >
<script src="<?= ADMIN_JS ?>plugins/jquery.js"></script>
<script src="<?= ADMIN_JS ?>plugins/jquery.datetimepicker.full.min.js"></script>

<script>

    //var services = <?php //echo json_encode($model->provider->services); ?>//;

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var img = $("<img />");
            reader.onload = function (e) {
                img.attr('src', e.target.result);
                img.attr('alt', 'Uploaded Image');
                img.attr("width", '100%');
                img.attr('height', '100%');
            };
            $(input).parent().parent().find('.preview-container').html(img);
            $(input).parent().parent().find('input[type="hidden"]').remove();

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(function () {
        $("select[name='provider_service[]']").val(services);

        $("select.multiselect").each(function (i, e) {
            //$(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });

        $("input.image").change(function () {
            readURL(this);
        });
    });

    $('#datetimepicker').datetimepicker({
        format: 'Y-m-d H:i:i'
    });

</script>