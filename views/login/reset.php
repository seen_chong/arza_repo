<div class="homePageWrapper">

	<div class="donateSection" style="background-color:#6dca92;">
		<div class="donateWrapper">

			<div class="loginBlock">

				<div class="donateLogo">
					<img src="<?=FRONT_IMG?>whiteLogo.png">
				</div>

				<div class="loginForm">
					<div class="loginFormTitle">
						<h2>Reset Your Password</h2>
					</div>
					<form class="form" id="reset" method="post" action="/login/reset" enctype="multipart/form-data">
						<input name="id" value="<?=$model->id?>" hidden/>
						<div class="loginFormInputs">
							<div class="stepBubbles">
								<ul>
									<a><li id="bubble1" data-part="part1" class="active">
											<span></span>
										</li></a>
								</ul>
								<p>
									<input type="password" name="password" placeholder="New Password" required>
								</p>
								<input type="submit" value="Update Password" id="signin"/>
							</div>


						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

</div>
