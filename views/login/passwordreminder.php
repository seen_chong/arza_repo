<div class="homePageWrapper">

    <div class="donateSection" style="background-color:#6dca92;">
        <div class="donateWrapper">

            <div class="loginBlock">

                <div class="donateLogo">
                    <img src="<?=FRONT_IMG?>whiteLogo.png">
                </div>

                <div class="loginForm">
                    <div class="loginFormTitle">
                        <h2>Reset Your Password</h2>
                        <p>Enter your email address below and we will send you an email with a link to reset your password.</p>
                    </div>
                    <form class="form" id="reset" method="post" action="/login/passwordreminder" enctype="multipart/form-data">
                        <div class="loginFormInputs">
                            <div class="stepBubbles">
                                <ul>
                                    <a><li id="bubble1" data-part="part1" class="active">
                                            <span></span>
                                        </li></a>
                                </ul>
                                <p>
                                    <input type="email" name="email" placeholder="Email" required>
                                </p>
                                <div id="message" class="errorMessage"></div>
                                <input type="submit" value="Continue" id="signin">
                            </div>


                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>

  
  <script>
     $(function(){
         var form = $('#reset');
         var message = $('#message');
         $(form).submit(function(event){
             event.preventDefault();
             var formData = $(form).serialize();
             $.ajax({
                 type:'POST',
                 url:$(form).attr('action'),
                 data:formData
             }).success(function(obj){
                 $('.email').val('');
                 var data = JSON.parse(obj);
                 $(message).html(data.message);
             });
         });
     })
  </script>
</div>