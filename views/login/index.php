﻿<div class="homePageWrapper">

    <div class="donateSection" style="background-color:#6dca92;">
        <div class="donateWrapper">

            <div class="loginBlock">

                <div class="donateLogo">
                    <img src="<?=FRONT_IMG?>whiteLogo.png">
                </div>

                <div class="loginForm">
                    <div class="loginFormTitle">
                        <h2>Member Login</h2>
                    </div>
                    <form class="form" method="post" action="/login/signin" enctype="multipart/form-data">
                    <div class="loginFormInputs">
                        <div class="stepBubbles">
                            <ul>
                                <a><li id="bubble1" data-part="part1" class="active">
                                    <span></span>
                                </li></a>
                            </ul>


                            <p>
                                <input type="email" name="email" placeholder="Email" required>
                            </p>
                            <p>
                                <input type="password" name="password" placeholder="Password" required>
                            </p>
                            <a href="<?=SITE_URL.'login/passwordreminder'?>" style="color: black">Forgot your Password?</a>
                            <input type="submit" value="SIGN IN" id="signin">
                        </div>


                    </div>
</form>
                </div>
            </div>
        </div>
    </div>   

</div>

<script>
    $(document).ready(function(){
        $('#signout').on('click',function(){
            $.post('/login/logout','',function(){
                window.location.replace('/');
            })
        });
    })
</script>