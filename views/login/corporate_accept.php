<div class="pageWrapper homePageWrapper">

    <div class="ourWorkHeader">
        <div class="ourWorkWrapper">
            <h5>ARZA</h5>
            <p>Making Israel an Essential Part of Jewish Identity</p>
        </div>
    </div>

        <!-- Pursuit Section -->
    <div class="pursuitContainer" style="background-image:url('<?=FRONT_IMG?>pursuitBg.png');">
        <div class="pursuitLeft" >
            <div class="innerFloatLeft">
                <h2>01</h2>
            </div>
            

            <div class="innerFloatRight">
                <h3>Taking Back the Z</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets.</p>
            </div>

        </div>

        <div class="pursuitRight">
            <h6>Rabbi Joshua Weinberg <br> Arza President</h6>
            <p>“We must end wildlife trafficking.</p>
            <p> Wild animals cannot protect themselves from those out to poach them and traffic them illegally. But we can. And we must. Voice your support for ARZA’s mission to End Wildlife Trafficking.”</p>

            <div class="subscribeForm">
                <input type="text" name="mail" value="Email Address" class="subscribeEmail">
                <input type="submit" value="Join Us" class="emailSubmit">
            </div>
        </div>

        <div class="donateToUs">
            <a href="/"><button>Donate to Us </button></a>
        </div>
    </div>



    <!-- Responsive Timeline -->
    <div class="timelineHeader">
        <h1>The Arza Story</h1>
    </div>
    <div class="timeline">
        <h2>2013</h2>
        
        <ul>
          <li>
            <h3>Lorem Ipsum Title</h3>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
            <time>August 2013</time>
          </li>
          <li>
            <h3>Lorem Ipsum Title</h3>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
            <time>July 2013</time>
          </li>
          <li>
            <h3>Lorem Ipsum Title</h3>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
            <time>June 2013</time>
          </li>
          <li>
            <h3>Lorem Ipsum Title</h3>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
            <time>February 2013</time>
          </li>
        </ul>
        
        <h2>2012</h2>
        <ul>
          <li>
            <h3>Lorem Ipsum Title</h3>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
            <time>December 2012</time>
          </li>
          <li>
            <h3>Lorem Ipsum Title</h3>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
            <time>May 2012</time>
          </li>
        </ul>
    </div>

    <!-- Video Embed Section -->

    <div class="videoEmbedSection">
        <iframe width="100%" height="500" src="https://www.youtube.com/embed/EHH7O9wUoak" frameborder="0" allowfullscreen></iframe>
    </div>
    <style type="text/css">
        .ytp-title {
            display: none !important;
        }
    </style>




    <div class="threeBlocks">
        <div class="sectionWrapper">

            <div class="blockOne">
                <h5>Am Yisrael</h5>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
            </div>
            <div class="blockTwo">
                <h5>Eretz Yisrael</h5>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
            </div>
            <div class="blockThree">
                <h5>Torat Yisrael</h5>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
            </div>
        </div>
    </div>





   

</div>