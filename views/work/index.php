﻿<div class="pageWrapper homePageWrapper">
    <?
    $work = \Model\Work::getItem(null, ['where' => "section_name='top'" ]);
    if($work->display == 1){?>
    <div class="ourWorkHeader">
        <div class="ourWorkWrapper">
            <h5><?= $work->title ?></h5>
            <p><?= $work->description ?></p>
        </div>
    </div>
    <?}?>

    <?$works = \Model\Work::getList(['where'=>'display=1','orderBy'=>'display_order']);
    $index = 1;?>

    <?foreach ($works as $work){
        if($work->section_name != 'top'){?>

        <div class="ourWorkIntro five-pillar-section">
            <div class="ourWorkWrapper">
                <div class="introBox">
                    <div class="introHeroimage">

                    </div>
                    <div class="leftBox <?=($index % 2==0)?'':'overrideRight'?>">
                        <img src="<?=UPLOAD_URL.'works/'.$work->featured_image?>">
                    </div>
                    <div class="rightBox" id="pillar<?=$index?>">
                        <h6>-0<?=$index?></h6>
                        <?=$work->title?>
                        <?=$work->subtitle?>
                    </div>
                </div>
            </div>
        </div>


    <? $index++;}}?>

<!--            <div class="ourWorkIntro">-->
<!--                <div class="ourWorkWrapper">-->
<!--                    <div class="introBox">-->
<!--                        <div class="introHeroimage">-->
<!--                           -->
<!--                        </div>-->
<!--                        <div class="leftBox">-->
<!--                             --><?// $path = $work->featured_image == '' ? FRONT_IMG . "competition.jpg" : UPLOAD_URL . "works/" . $work->featured_image; ?>
<!--                            <img src="--><?//= $path ?><!--">-->
<!--                        </div>-->
<!--                        <div class="rightBox" id="pillar1">-->
<!--                            <h6>-0--><?//=$order->display_order?><!--</h6>-->
<!--                            <h2>Resource Development for the Reform Movement in Israel</h2>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->


<!--            <div class="ourWorkIntro five-pillar-section">-->
<!--                <div class="ourWorkWrapper">-->
<!--                    <div class="introBox">-->
<!--                        <div class="introHeroimage">-->
<!--                           -->
<!--                        </div>-->
<!--                        <div class="leftBox overrideRight">-->
<!--                             <img src="--><?//=FRONT_IMG?><!--5pillar.jpg">-->
<!--                        </div>-->
<!--                        <div class="rightBox" id="pillar2">-->
<!--                            <h6>-02</h6>-->
<!--                            <h2>Travel to Israel and Missions</h2>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->


<!--            <div class="ourWorkIntro five-pillar-section">-->
<!--                <div class="ourWorkWrapper">-->
<!--                    <div class="introBox">-->
<!--                        <div class="introHeroimage">-->
<!--                           -->
<!--                        </div>-->
<!--                        <div class="leftBox">-->
<!--                             <img src="--><?//=FRONT_IMG?><!--5pillar.jpg">-->
<!--                        </div>-->
<!--                        <div class="rightBox" id="pillar3">-->
<!--                            <h6>-03</h6>-->
<!--                            <h2>Creating Models for Congregational Israel Involvement</h2>-->
<!---->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->


<!--            <div class="ourWorkIntro five-pillar-section">-->
<!--                <div class="ourWorkWrapper">-->
<!--                    <div class="introBox">-->
<!--                        <div class="introHeroimage">-->
<!--                           -->
<!--                        </div>-->
<!---->
<!--                        <div class="leftBox">-->
<!--                             <img src="--><?//=FRONT_IMG?><!--5pillar.jpg">-->
<!--                        </div>-->
<!--                        <div class="rightBox" id="pillar4">-->
<!--                            <h6>-04</h6>-->
<!--                            <h2>Creating Lifelong Zionists</h2>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->

            <!-- FIVE PILLARS OF ARZA -->
<!--            <div class="ourWorkIntro five-pillar-section">-->
<!--                <div class="ourWorkWrapper">-->
<!--                    <div class="introBox">-->
<!--                        <div class="introHeroimage">-->
<!--                           -->
<!--                        </div>-->
<!--                        <div class="leftBox overrideRight">-->
<!--                             <img src="--><?//=FRONT_IMG?><!--5pillar.jpg">-->
<!--                        </div>-->
<!--                        <div class="rightBox" id="pillar5">-->
<!--                            <h6>-05</h6>-->
<!--                            <h2>Israel Advocacy and Activism</h2>-->
<!--                        </div>-->
<!---->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->

            </div>



</div>
