<? $donate = $model->donatePage;
$background_image =
    $donate->background_image == '' ? "background-color:#6dca92" : "background-image:url(" . UPLOAD_URL . "donate/" . $donate->background_image . ")";
$logo = $donate->logo_image == '' ? FRONT_IMG . 'whiteLogo.png' : UPLOAD_URL . 'donate/' . $donate->logo_image;
if ($donate->template == 1) {
    $class = 'donateSection';
    $wrapper = "donateLeftBlock";
} else {
    $class = 'crisisSection';
    $wrapper = "donateCenterBlock";
}
?>
<div class="homePageWrapper">
    <? if ($donate->default_template == 1) { ?>
        <div class="<?= $class ?>" style="<?= $background_image ?>">
            <div class="donateWrapper">
                <div class="donateLeftBlock">
                    <div class="donateLogo">
                        <img src="<?= $logo ?>">
                    </div>
                    <div class="donateForm">
                        <div class="donateFormTitle">
                            <h5>Thank you for your donation</h5>
                        </div>
                        <? if (isset($model->donation)) { ?>
                            <div class="donateFormInputs">
                                <h3 class="as_m lines_on_sides">Details</h3>
                                <label>Id</label>
                                <p><?= str_pad($model->donation->id, 4, 0) ?></p>
                                <label>Reference Number</label>
                                <p><?= $model->donation->ref_num ?></p>
                                <label>Amount</label>
                                <div class="order-total">$<?= number_format($model->donation->amount, 2) ?></div>
                                <h3 class="as_m lines_on_sides">Your Information</h3>
                                <label>Name</label>
                                <div class="order-name"><?= $model->donation->full_name() ?></div>
                                <label>Address</label>
                                <div class="order-addr"><?= $model->donation->getAddr() ?></div>
                                <label>Email</label>
                                <div class="order-email"><?= $model->donation->email ?></div>
                                <label>Phone</label>
                                <div class="order-phone"><?= $model->donation->phone ?></div>
                            </div>
                        <? } ?>
                    </div>
                </div>

                <div class="donateRightBlock">
                    <div class="rightBlockContent">
                        <h4><?= $donate->subtitle ?></h4>
                        <? $path = UPLOAD_URL . 'donate/' . $donate->image ?>
                        <img src="<?= $path ?>">
                    </div>
                </div>
            </div>
        </div>
    <? } else { ?>
        <div class="<?= $class ?>" style="<?= $background_image ?>">
            <div class="donateWrapper">
                <div class="donateCenterBlock">
                    <div class="crisisLogo">
                        <img src="<?= $logo ?>">
                    </div>
                    <div class="donateForm">
                        <div class="donateFormTitle">
                            <h5>Thank you for your donation</h5>
                        </div>
                        <? if (isset($model->donation)) { ?>
                            <div class="donateFormInputs">
                                <h3 class="as_m lines_on_sides">Details</h3>
                                <label>Id</label>
                                <p><?= str_pad($model->donation->id, 4, 0) ?></p>
                                <label>Reference Number</label>
                                <p><?= $model->donation->ref_num ?></p>
                                <label>Amount</label>
                                <div class="order-total">$<?= number_format($model->donation->amount, 2) ?></div>
                                <h3 class="as_m lines_on_sides">Your Information</h3>
                                <label>Name</label>
                                <div class="order-name"><?= $model->donation->full_name() ?></div>
                                <label>Address</label>
                                <div class="order-addr"><?= $model->donation->getAddr() ?></div>
                                <label>Email</label>
                                <div class="order-email"><?= $model->donation->email ?></div>
                                <label>Phone</label>
                                <div class="order-phone"><?= $model->donation->phone ?></div>
                            </div>
                        <? } ?>
                    </div>
                </div>

                <div class="crisisLogoText">
                    <p><span><?= $donate->subtitle ?></span></p>
                </div>
            </div>
        </div>
    <? } ?>
</div>
