<div class="homePageWrapper">

    <div class="donateSection" style="background-color:#6dca92;">
        <div class="donateWrapper">

            <div class="donateLeftBlock">

                <div class="donateLogo">
                    <img src="<?=FRONT_IMG?>whiteLogo.png">
                </div>

                <div class="donateForm">
                    <div class="donateFormTitle">
                        <h5><?=$model->title?></h5>
                    </div>
                    <form method="post" action="<?=$this->emagid->uri?>" enctype="multipart/form-data" id="donateForm">
                    <div class="donateFormInputs">
                        <div class="stepBubbles">
                            <ul>
                                <a><li id="bubble1" data-part="part1" class="active">
                                    <span></span>
                                </li></a>
                                <li id="bubble2" data-part="part2" >
                                    <span></span>
                                </li>
<!--                                <li id="bubble3" data-part="part3">-->
<!--                                    <span></span>-->
<!--                                </li>-->
                            </ul>
                        </div>

                        <div class="amountSelect part part1">
                            <p>Select an Amount</p>

                            <input type="button"  class="donateAmount" value="36">
                            <input type="button"  class="donateAmount" value="50">
                            <input type="button"  class="donateAmount" value="100">
                            <input type="button"  class="donateAmount" value="360">
                            <input type="button"  class="donateAmount" value="500">
                            <input type="button"  class="donateAmount" value="1000">
                            <input hidden type="number" name = "amount">
                            <input  hidden type="number" name = "monthly" value="0">

                            <input type="number" min="1.00" step="1" class="otherAmount" placeholder="Other Amount">

                           <label id="repeatDonation"><input type="checkbox"><p>I'd like to repeat this gift every month</p></label>

                            <input value="Next >" id="nextStep1" class="nextStepBtn">
                        </div>
                        <?php $states = get_states();?>
                        <div class="addyFields part part2" style="display:none;">

                            <p class="field50">First name <br>
                                <input type="text" name="first_name">
                            </p>
                            <p class="field50">Last name <br>
                                <input type="text" name="last_name">
                            </p>
                            <p class="field100">Address <br>
                                <input type="text" name="address">
                            </p>
                            <p class="field50">City <br>
                                <input type="text" name="city">
                            </p>
                            <p class="field25">State <br>
                                <select name="state" required>
                                    <?foreach(get_states() as $short=>$long){?>
                                        <option value="<?=$short?>"><?=$long?></option>
                                    <?}?>
                                </select>
                            </p>
                            <p class="field25">ZIP code <br>
                                <input type="text" name="zip">
                            </p>
                            <p class="field50">Email <br>
                                <input type="text" id="email" name="email">
                            </p>
                            <p class="field50">Phone <br>
                                <input type="text" name="phone">
                            </p>
                            <p class="field100">Donation Preference<br>
                                <textarea name="preference"></textarea>
                            </p>
                            <input type="submit" value="Donate $100" id="nextStep3" class="nextStepBtn">
<!--                             <div class="boxredirect">
                                <a class="nextStepBtn" href="#popup1">
                                    Donate
                                </a>
                            </div> -->
                            
<!-- Temporary Donate Button Redirect Popup -->
<!--                             <div id="popup1" class="overlay">
                                <div class="popup">
                                    <h2>Temporarily Out Of Service</h2>
                                    <a class="close" href="#">&times;</a>
                                    <div class="content">
                                        <p> 
                                        Please contact ARZA directly at <a href="mailto:alanger@arza.org?Subject=Donation%20inquiry">alanger@arza.org</a> to complete this transaction. We are so sorry for this inconvenience.</p> 
                                    </div>
                                </div>
                            </div> -->


<style type="text/css">

/*.overlay {
  position: fixed;
  z-index: 99;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.7);
  transition: opacity 500ms;
  visibility: hidden;
  opacity: 0;
}
.overlay:target {
  visibility: visible;
  opacity: 1;
}

.popup {
  margin: 70px auto;
  padding: 20px;
  background: #fff;
  border-radius: 5px;
  width: 30%;
  position: relative;
  transition: all 5s ease-in-out;
  text-align: center;
}

.popup h2 {
  margin-top: 45px;
  margin-bottom: 15px;
  color: #333;
  text-align: left;
}
.popup .close {
  position: absolute;
  top: 20px;
  right: 30px;
  transition: all 200ms;
  font-size: 30px;
  font-weight: bold;
  text-decoration: none;
  color: #333;
}
.popup .close:hover {
  color: #06D85F;
}
.popup .content {
  max-height: 30%;
  overflow: auto;
}

@media screen and (max-width: 700px){
  .popup{
    width: 70%;
  }
}*/
</style>
<!--                            <input value="Next >" id="nextStep2" class="nextStepBtn" readonly>-->
                        </div>
                    </div>
</form>
                </div>
            </div>

            <div class="donateRightBlock">
                <div class="rightBlockContent">
                    <h4><?=$model->subtitle?></h4>
                    <?$path = UPLOAD_URL.'donate/'.$model->report_card?>
                    <img src="<?=$path?>">
                </div>
            </div>
        </div>
    </div>   

</div>

<script type="text/javascript">
$( document ).ready(function() {
    $(document).keypress(
        function(event){
            if (event.which == '13') {
                event.preventDefault();
            }
        });
    $('input#nextStep1').click(function() {
        var value = $('input[name="amount"]').val();
        if(value=='') {
            alert('Please Select an amount.');
        }else {
            $('#bubble1').removeClass('active');
            $('#bubble2').addClass('active');
            $('.amountSelect').hide();
            $('.addyFields').show();
            if($('input[name="monthly"]').val()==1){
                var text = $('input#nextStep3').val();
                $('input#nextStep3').val(text + ' Monthly');
            }
        }
    });


    $('.stepBubbles li').on('click',function(){
        $(this).addClass('active');
        $("li").not(this).removeClass('active');
        var part = $(this).attr("data-part");
        $('div .part').each(function(){
            if(!$(this).hasClass(part)){
                $(this).hide();
            }else(
                $(this).show()
            )
        })

    })
    $('.donateAmount').on('click',function(){
        $('.donateAmount').not(this).removeClass('greenBg');
        $(this).addClass('greenBg');
        var amount = $(this).val();
        $('input[name="amount"]').val(amount);
        $('input#nextStep3').val('Donate $'+amount);
    })
    $('.otherAmount').on('change',function(){
        var amount = $(this).val();
        $('input[name="amount"]').val(amount);
        $('input#nextStep3').val('Donate $'+amount);
    })

    $('#repeatDonation input').on('click',function(){
        if($('input[name="monthly"]').val()==0){
            $('input[name="monthly"]').val(1);
        }else{
            $('input[name="monthly"]').val(0);
        }

    })


    $('input#nextStep3').on('click',function(e){

        e.preventDefault();
        var valid = true;
        var isFilled = true;
        $(".part2 p").each(function(){
            var val = $(this).find('input[type=text]').val();
            if(val==''){
                isFilled = false;
                name = $(this).text().trim();
                invalid = $(this);
                return false;
            }
        });
        var email = $("input#email").val();
        if(!isFilled){
            valid = false;
            invalid.find('input').css('border','1px solid #a72d2d');
            invalid.css('color','red');
        }else if(email.indexOf('@')<1){
            valid = false;
            $("input#email").css('border','1px solid #a72d2d');
            $("input#email").parent('.inputText').css('color','red');
        }
        if(valid) {
            $('#donateForm').submit();
        }

    })
});

</script>
<script>
    	// $("#paypalPayBtnClicker").on('click', function(e){
    		// $('#myContainer').submit();
    	// });

    window.paypalCheckoutReady = function() {
        paypal.checkout.setup('<?=PAYPAL_CLIENT_ID?>', {
            environment: 'live',
            button: 'nextStep3',
            click: function(){
                paypal.checkout.initXO();
                var action = $.post('/set-express-checkout');
                action.done (function(data){
                    paypal.checkout.startFlow(data.token);
                });
                action.fail(function(){
                    paypal.checkout.closeFlow();
                })
            },
            condition: function(){
                var data = {
                    bill_first_name:$('[name=first_name]').val(),
                    bill_last_name:$('[name=last_name]').val(),
                    bill_address:$('[name=address]').val(),
                    bill_city:$('[name=city]').val(),
                    bill_state:$('[name=state]:enabled').val(),
                    bill_zip:$('[name=zip]').val(),
                    bill_country:$('[name=email]').val(),
                    phone:$('[name=phone]').val()
                };
                var submit = true;
                $.each(data,function(key,value){
                    if(!value){
                        submit = false;
                        $('[name=' + key +']:enabled').css('border', '1px solid red');
                    } else {
                        $('[name=' + key +']:enabled').css('border', '1px solid #d6d6d6');
                    }
                });
                return submit;
            }
        });
    };
</script>
<script src="//www.paypalobjects.com/api/checkout.js" async></script>
