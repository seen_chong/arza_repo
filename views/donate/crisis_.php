<div class="homePageWrapper">

    <div class="crisisSection" style="background-image:url(<?=FRONT_IMG?>wildfire.jpg)">
        <div class="donateWrapper">

            <div class="donateCenterBlock">

                <div class="crisisLogo">
                    <img src="<?=FRONT_IMG?>whiteLogo.png">
                </div>
                <div class="crisisLogoTitle">
                    <h3>Association of Reform Zionists of America</h3>
                </div>
                <div class="crisisLogoText">
                    <p><span>I</span>srael <span>N</span>eeds <span>Y</span>our <span>H</span>elp</p>
                </div>

                <div class="donateForm">
                    <div class="donateFormTitle">
                        <h5><?=$model->title?></h5>
                    </div>
                    <form method="post" action="<?=$this->emagid->uri?>" enctype="multipart/form-data" id="donateForm">
                    <div class="donateFormInputs">
                        <div class="stepBubbles">
                            <ul>
                                <a><li id="bubble1" data-part="part1" class="active">
                                    <span></span>
                                </li></a>
                                <li id="bubble2" data-part="part2" >
                                    <span></span>
                                </li>
<!--                                <li id="bubble3" data-part="part3">-->
<!--                                    <span></span>-->
<!--                                </li>-->
                            </ul>
                        </div>

                        <div class="amountSelect part part1">
                            <p>Select an Amount</p>

                            <input type="button"  class="donateAmount" value="36">
                            <input type="button"  class="donateAmount" value="50">
                            <input type="button"  class="donateAmount" value="100">
                            <input type="button"  class="donateAmount" value="360">
                            <input type="button"  class="donateAmount" value="500">
                            <input type="button"  class="donateAmount" value="1000">
                            <input hidden type="number" name = "amount">
                            <input  hidden type="number" name = "monthly" value="0">

                            <input type="number" min="1.00" step="1" class="otherAmount" placeholder="Other Amount">

                           <label id="repeatDonation"><input type="checkbox"><p>I'd like to repeat this gift every month</p></label>

                            <input value="Next >" id="nextStep1" class="nextStepBtn">
                        </div>
                        <?php $states = get_states();?>
                        <div class="addyFields part part2" style="display:none;">

                            <p class="field50">First name <br>
                                <input type="text" name="first_name">
                            </p>
                            <p class="field50">Last name <br>
                                <input type="text" name="last_name">
                            </p>
                            <p class="field100">Address <br>
                                <input type="text" name="address">
                            </p>
                            <p class="field50">City <br>
                                <input type="text" name="city">
                            </p>
                            <p class="field25">State <br>
                                <select name="state" required>
                                    <?foreach(get_states() as $short=>$long){?>
                                        <option value="<?=$short?>"><?=$long?></option>
                                    <?}?>
                                </select>
                            </p>
                            <p class="field25">ZIP code <br>
                                <input type="text" name="zip">
                            </p>
                            <p class="field50">Email <br>
                                <input type="text" id="email" name="email">
                            </p>
                            <p class="field50">Phone <br>
                                <input type="text" name="phone">
                            </p>
                            <input type="submit" value="Donate $100" id="nextStep3" class="nextStepBtn">
<!--                            <input value="Next >" id="nextStep2" class="nextStepBtn" readonly>-->
                        </div>
                    </div>
</form>

                </div>

                <div class="crisisLogoText">
                    <p>test text</p>
                    <p><span>D</span>onate Through <span>ARZA</span> To Provide <span>Relief</span> and <span>Rebuild</span></p>
                    <h6>Join us and partner with the Israeli Reform Movement as we support Keren B’Kavod: the Humanitarian Aid arm of the IMPJ.</h6>
                </div>
            </div>
        </div>
    </div>   

</div>

<script type="text/javascript">
$( document ).ready(function() {
    $(document).keypress(
        function(event){
            if (event.which == '13') {
                event.preventDefault();
            }
        });
    $('input#nextStep1').click(function() {
        var value = $('input[name="amount"]').val();
        if(value=='') {
            alert('Please Select an amount.');
        }else {
            $('#bubble1').removeClass('active');
            $('#bubble2').addClass('active');
            $('.amountSelect').hide();
            $('.addyFields').show();
            if($('input[name="monthly"]').val()==1){
                var text = $('input#nextStep3').val();
                $('input#nextStep3').val(text + ' Monthly');
            }
        }
    });
//    $('input#nextStep2').on('click',function(e) {
//        e.preventDefault();
//        var valid = true;
//        var isFilled = true;
//        $(".part2 p").each(function(){
//            var val = $(this).find('input[type=text]').val();
//            if(val==''){
//                isFilled = false;
//                name = $(this).text().trim();
//                invalid = $(this);
//                return false;
//            }
//        });
//        var email = $("input#email").val();
//        if(!isFilled){
//            valid = false;
//            invalid.find('input').css('border','1px solid #a72d2d');
//            invalid.css('color','red');
//        }else if(email.indexOf('@')<1){
//            valid = false;
//            $("input#email").css('border','1px solid #a72d2d');
//            $("input#email").parent('.inputText').css('color','red');
//        }
//        if(valid) {
//            $('#bubble2').removeClass('active');
//            $('#bubble3').addClass('active');
//            $('.addyFields').hide();
//            $('.paymentScreen').show();
//        }
//    });

    $('.stepBubbles li').on('click',function(){
        $(this).addClass('active');
        $("li").not(this).removeClass('active');
        var part = $(this).attr("data-part");
        $('div .part').each(function(){
            if(!$(this).hasClass(part)){
                $(this).hide();
            }else(
                $(this).show()
            )
        })

    })
    $('.donateAmount').on('click',function(){
        $('.donateAmount').not(this).removeClass('greenBg');
        $(this).addClass('greenBg');
        var amount = $(this).val();
        $('input[name="amount"]').val(amount);
        $('input#nextStep3').val('Donate $'+amount);
    })
    $('.otherAmount').on('change',function(){
        var amount = $(this).val();
        $('input[name="amount"]').val(amount);
        $('input#nextStep3').val('Donate $'+amount);
    })

    $('#repeatDonation input').on('click',function(){
        if($('input[name="monthly"]').val()==0){
            $('input[name="monthly"]').val(1);
        }else{
            $('input[name="monthly"]').val(0);
        }

    })


    $('input#nextStep3').on('click',function(e){

        e.preventDefault();
        var valid = true;
        var isFilled = true;
        $(".part2 p").each(function(){
            var val = $(this).find('input[type=text]').val();
            if(val==''){
                isFilled = false;
                name = $(this).text().trim();
                invalid = $(this);
                return false;
            }
        });
        var email = $("input#email").val();
        if(!isFilled){
            valid = false;
            invalid.find('input').css('border','1px solid #a72d2d');
            invalid.css('color','red');
        }else if(email.indexOf('@')<1){
            valid = false;
            $("input#email").css('border','1px solid #a72d2d');
            $("input#email").parent('.inputText').css('color','red');
        }
        if(valid) {
            $('#donateForm').submit();
        }

    })
});

</script>
<script>
    //	$("#paypalPayBtnClicker").on('click', function(e){
    //		$('#myContainer').submit();
    //	});

    window.paypalCheckoutReady = function() {
        paypal.checkout.setup('<?=PAYPAL_CLIENT_ID?>', {
            environment: 'live',
            button: 'nextStep3'
        });
    };
</script>
<script src="//www.paypalobjects.com/api/checkout.js" async></script>
