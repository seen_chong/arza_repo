<?$splash = $model->splash;
$subtitle = 'Association of Reform Zionists of America';
$title = "SEE WHAT'S HAPPENING HERE...";
$path = FRONT_IMG . "splash-bg.jpg";
if($splash){
	$subtitle = $splash->subtitle;
	$title = $splash->title;
	$path = $splash->featured_image == '' ? FRONT_IMG . "splash-bg.jpg" : UPLOAD_URL . "homes/" . $splash->featured_image;
}?>
<?if($splash->display==1){?>
<div class="landingWrapper" style="background-image:url('<?=$path?>')">

	<div class="splash-logo">
		<img src="<?=FRONT_IMG?>splash-logo-alt.png">
		<div class="splash-title">
			<h1><span><?=$subtitle?></span></h1>
		</div>
	</div>

    <div class="landing-latest">
        <div class="landing-latest-title">
			<a href="<?=$splash->link?>"><h2><?=$title?></h2></a>
        </div>

        <div class="latest-blog-slider">
        	<a href="/blog">
			<?foreach ($model->latests as $obj){
				$date = explode(' ',$obj->time);?>
			<a href="<?=$obj->getUrl()?>">
				<div class="latest-blog-post">
					<div class="date">
						<h6><?=$date[0]?><br><?=$date[1]?></h6>
					</div>
					<div class="latest-blog-post-snippet">
						<p><?=$obj->description?></p>
					</div>
				</div>
			</a>
			<?}?>
        </div>
    </div>
            <div class="scroll-div">
            <a href="#" id="slide-down">
                <img src="<?=FRONT_IMG?>slide-downgr.png">
            </a>
        </div>

</div>

<?}?>

<div class="pageWrapper homePageWrapper" id="main-div">

	<?$homes = \Model\Home::getList(['orderBy'=>'display_order'])?>
	<?foreach ($homes as $home){?>
		<?if($home->section_name=='Main Banner'&&$home->display=="1"){?>
			<div class="homeHeroWrapper">
				<div class="homeHero" style="background-image:url('<?=UPLOAD_URL?>homes/<?=$home->featured_image?>');">
					<img class="heroOverlay" src="<?=FRONT_IMG?>heroOverlay.png">
				</div>
				<div class="heroText">
					<h1><?=$home->title?></h1>
					<p><?=$home->description?></p>
				</div>
			</div>
			<?}else if($home->section_name=='Section 1'&&$home->display=="1"){?>
			<div class="pursuitContainer" style="background-image:url('<?=FRONT_IMG?>pursuitBg.png');">
				<div class="pursuitLeft" >
					<div class="innerFloatLeft">
					</div>
					<div class="innerFloatRight">
						<h3 style="font-size:42px;"><span style="font-size:56px;">T</span>AKING <span style="font-size:56px;">B</span>ACK <span style="font-size:56px;">T</span>HE <span style="font-size:56px;">Z</span></h3>
						<p><?=$home->description?></p>
					</div>
				</div>

				<div class="pursuitRight">
					<h6><?=$home->subtitle?></h6>
					<p><?=$home->sub_description?></p>
					<div class="subscribeForm">
						<input type="text" name="mail" placeholder="Email Address" class="subscribeEmail">
						<input type="submit" value="Join Us" class="emailSubmit">
					</div>
					<div class="donateToUs">
						<a href="<?=$home->link?>"><button><?=$home->button?></button></a>
					</div>
				</div>
			</div>
			<?}else if($home->section_name=='Section 2'&&$home->display=="1"){?>
			<div class="championSection" style="background-image:url('<?=UPLOAD_URL?>homes/<?=$home->featured_image?>');">
				<h2><?=$home->title?></h2>
			</div>
			<div class="section-02" style="background-image:url('<?=FRONT_IMG?>equalityCircles.png');">
				<div class="innerSectionLeft">
					<div class="innerFloatLeft">
					</div>

					<div class="innerFloatRight">
						<h3><?=$home->subtitle?></h3>
					</div>
				</div>
				<div class="innerSectionRight">
					<p><?=$home->description==''?$home->sub_description:$home->description?></p>
				</div>
				<div class="imageDeck">
					<?php
					if($home->image!='') {
						$images = $home->image;
						$images = explode(',', $images);
						$images = $images ? $images : [];
						$index = array_search("", $images);
						unset($images[$index]);
					}
					foreach ($images as $image){?>
						<img src="<?=UPLOAD_URL?>homes/<?=$image?>">
					<?}?>
				</div>
			</div>
			<?}else if($home->section_name=='Section 3'&&$home->display=="1"){?>
			<div class="section-03">
				<div class="sectionWrapper">

					<div class="left-block">
						<img src="<?=FRONT_IMG?>whatIsZionism.png">
						<p><?=$home->description==''?$home->sub_description:$home->description?></p>
					</div>

					<div class="right-block">
						<div class="innerSectionLeft">
							<a <?=($home->about_link)?'href="'. $home->about_link.'"':''?>>
								<div class="innerTitle">
									<h5><?=$home->about?></h5>
									<p><?=$home->sub_about?></p>
								</div>
							</a>
							<div class="innerFloatRight">
								<?$titles = explode(" ",$home->title);
								$array = array_chunk($titles,ceil(count($titles) / 2));
								$y = 0;?>
								<h3>
									<?foreach ($array as $title){
										foreach ($title as $t){?>
											<?=$t?>
										<?}?>
										<?=($y==0)?'':'' ?>
										<?$y++;}?>
							</div>
							<div class="innerFloatLeft">
							</div>
						</div>
						<div class="imageBlock">
							<img src="<?=UPLOAD_URL?>homes/<?=$home->featured_image?>">
							<h3><?=$home->subtitle?></h3>
							<a href="<?=$home->link?>"><button><?=$home->sub_description?></button></a>
						</div>

					</div>
				</div>
			</div>
			<?}else if($home->section_name=='Section 4'&&$home->display=="1"){?>
			<div class="mediaSection">

				<div class="slider-container">
					<?if(isset($model->events)&&($model->events)){
						foreach( $model->events as $event){
							$imagePath = ($event->featured_image)? UPLOAD_URL.'events/'.$event->featured_image:FRONT_IMG.'homeFooterHero.png';
							?>
							<div class="featuredEvents">
								<!-- <img src="<?=UPLOAD_URL?>homes/<?=$home->featured_image?>"> -->
								<img src="<?=$imagePath?>">
								<div class="eventOverlay">
									<div class="eventDate">
										<p><?=date("M",  $event->start_time) ?></p>
										<h6><?=date("d",  $event->start_time)?></h6>
									</div>
									<button class="rsvp" data-event_id="<?=$event->id?>">RSVP</button>
									<h3><?=$event->name?></h3>
									<p><?=$event->description?></p>
								</div>
							</div>
						<?}
					}else{
						$imagePath = ($home->featured_image)? UPLOAD_URL.'homes/'.$home->featured_image:FRONT_IMG.'homeFooterHero.png';?>
						<div class="featuredEvents">
							<!-- <img src="<?=UPLOAD_URL?>homes/<?=$home->featured_image?>"> -->
							<img src="<?=$imagePath?>">
							<div class="eventOverlay">
								<h3><?=$home->title?></h3>
								<p><?=$home->description?></p>
							</div>
						</div>
					<?}?>
				</div>

				<div class="getSocial">
					<div class="followUs">
						<h2>Follow Us</h2>
						<div class="socialSquare">
							<a href="https://facebook.com/arzaus" target="_blank">
							<img src="<?=FRONT_IMG?>fb-green.png">
							</a>
						</div>
						<div class="socialSquare">
							<a href="https://twitter.com/ARZAUS" target="_blank">
							<img src="<?=FRONT_IMG?>twit-green.png">
								</a>
						</div>
						<div class="socialSquare">
							<a href="https://www.youtube.com/user/ARZAUSA" target="_blank">
							<img src="<?=FRONT_IMG?>yt-green.png">
								</a>
						</div>
					</div>

					<div class="inTheNews">
						<h2>ARZA In The News</h2>
						<div class="news-slider-container">
							<?foreach ($model->news as $obj){
								$imagePath = ($obj->featured_image)? UPLOAD_URL.'news/'.$obj->featured_image:FRONT_IMG.'jewishJournal.png'; ?>
								<div class="newsQuote">
									<a href="<?=$obj->url?>"><p><?=$obj->title?> - <?=$obj->author?></p></a>
									<img class="newsLogo" src="<?=$imagePath?>">
									<h6><?=$obj->time?></h6>
								</div>
							<?}?>
						</div>
						<div class="moreNews">
							<a href="/blog"><button>Check out our Blog </button></a>
						</div>
					</div>
				</div>

				<div class="rsvpModule" style="display:none;">
					<h3>August 21, 2016</h3>
					<h2>New York Meeting</h2>
					<p id="description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
					<button class="rsvpToggle">RSVP for $180.00</button>
					<form id="eventRsvp" style="display:none;" action="/home/rsvp" method="post">
						<div class="evCustFields">
							<input hidden name="event_id">
							<input hidden name="user_id" value="<?=($model->user)? $model->user->id:0?>">
							<p class="inputText">First name <br>
								<input type="text" name="first_name">
							</p>
							<p class="inputText">Last name <br>
								<input type="text" name="last_name">
							</p>
							<p class="inputText">Address <br>
								<input type="text" name="address">
							</p>
							<p class="inputText">City <br>
								<input type="text" name="city">
							</p>
							<p class="inputText stateSelect">State <br>
								<select name="state" required>
									<?foreach(get_states() as $short=>$long){?>
										<option value="<?=$short?>"><?=$long?></option>
									<?}?>
								</select>
							</p>
							<p class="inputText">ZIP code <br>
								<input type="text" name="zip">
							</p>
							<p class="inputText">Email <br>
								<input type="email" name="email" id="rsvpEmail">
							</p>
							<p class="inputText">Phone <br>
								<input type="text" name="phone">
							</p>
							<p class="inputText">Congregation Name <br>
								<input type="text" name="congregation_name" id="rsvpEmail">
							</p>
							<p class="inputText">Position <br>
								<input type="text" name="position">
							</p>
							<p class="inputText">Congregation City <br>
								<input type="text" name="congregation_city">
							</p>
							<p class="inputText stateSelect">Congregation State <br>
								<select name="congregation_state" required>
									<?foreach(get_states() as $short=>$long){?>
										<option value="<?=$short?>"><?=$long?></option>
									<?}?>
								</select>
							</p>

							<p class="submitForm">
								<input type="submit" value="RSVP" id="nextStep2" class="memberSignup">
								<input type="submit" value="RSVP" id="paypal" class="memberSignup" style="display:none;">
							</p>
						</div>

<!--						<div class="evPaymentFields" style="display:none;">-->
<!--							<p class="inputText">Enter your credit card information <br>-->
<!--							<div class="label-input_wrapper">-->
<!--								<input type="text" id="ccnumber" class="ccFormatMonitor" name="cc_number" maxlength="19" tabindex="4"/>-->
<!--								<div id="creditCardIcons">-->
<!--									<div class="media"></div>-->
<!--								</div>-->
<!--							</div>-->
<!--							</p>-->
<!--							<p class="inputText">Exp Month<br>-->
<!--								<input type="number" placeholder="MM" maxlength="2" name="cc_expiration_month">-->
<!--							</p>-->
<!--							<p class="inputText">Exp Year<br>-->
<!--								<input type="number" placeholder="YYYY" maxlength="4" name="cc_expiration_year">-->
<!--							</p>-->
<!---->
<!--							<p class="inputText">CVC <br>-->
<!--								<input type="number" name="cc_ccv">-->
<!--							</p>-->
<!--							<p class="submitForm">-->
<!--								<input type="submit" value="Purchase Ticket" id="nextStep3" class="memberSignup">-->
<!--							</p>-->
<!---->
<!---->
<!---->
<!--						</div>-->
					</form>
				</div>
			</div>
			<?}?>
	<?}?>
	</div>

</div>
<script>
	$(document).ready(function(){
		$(document).on('click','.emailSubmit',function (e) {
			var email = $("input[name='mail']").val();
			if(email && email.indexOf('@') > 0){
				$.post('/newsletter/add',{email:email},function(data){
					if(data.status == 'success'){
						alert(data.message);
					} else{
						alert(data.message);
					}
					$('#emailAddr').val('');
				})
			}else{
				alert('Please input a valid email address.');
			}
		})
	})
</script>

<script type="text/javascript">
$(document).ready(function(){
  $('.slider-container').slick({
    autoplay: true,
    autoplaySpeed: 5000,
    arrows: false,
    dots: true,
      responsive: [
    {
      breakpoint: 1024,
      settings: {
        infinite: true,
      }
    }
    ]
  });
	$('.news-slider-container').slick({
		autoplay: true,
		autoplaySpeed: 5000,
		arrows: false,
		dots: true,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					infinite: true,
				}
			}
		]
	});
});
</script>

<script type="text/javascript">
$(document).ready(function(){
 $('button.rsvp').click(function() {
 	var id =$(this).attr('data-event_id');
	 $.post('home/buildRSVP',{id:id},function(data){
		$('.rsvpModule').find("h3").text(data.time);
		$('.rsvpModule').find("h2").text(data.name);
		$('.rsvpModule').find("#description").text(data.description);
		 if(data.price-0==0){
			 $('.rsvpModule').find("button").text("RSVP");
			 $("#nextStep2").show();
			 $("#nextStep2").prop("disabled",false);
			 $("#paypal").prop("disabled",true);
			 $("#paypal").hide();
		 }else {
			 $('.rsvpModule').find("button").text("RSVP for $" + data.price);
			 $("#paypal").show()
			 $("#nextStep2").hide();
			 $("#nextStep2").prop("disabled",true);
			 $("#paypal").prop("disabled",false);
		 }
		$('input[name="event_id"]').val(id);
	 });

 	$('.getSocial').toggle();
 	$('.rsvpModule').toggle();

 	});
});
</script>

<script type="text/javascript">
$(document).ready(function(){
 $('button.rsvpToggle').click(function(e) {
 	e.preventDefault();
 	$(this).toggleClass('navyBg');
 	$('form#eventRsvp').toggle();
 	});

 $(document).on('click','#nextStep2, #paypal',function(e) {
 	e.preventDefault();
	 var valid = true;
	 var isFilled = true;
	 $(".evCustFields .inputText").each(function(){
		 var val = $(this).find('input[type=text]').val();
		 if(val==''){
			 isFilled = false;
			 name = $(this).text().trim();
			 invalid = $(this);
			 return false;
		 }
	 });
	 var email = $('input[id=rsvpEmail]').val();
	 if(!isFilled){
		 valid = false;
		 invalid.find('input').css('border','1px solid #a72d2d');
		 invalid.css('color','red');
	 }else if(email.indexOf('@')<1){
		 valid = false;
		 $('input[type=email]').css('border','1px solid #a72d2d');
		 $('input[type=email]').parent('.inputText').css('color','red');
	 }
	 if(valid){
		 $('#eventRsvp').submit();
	 }
 	});

//	$('#nextStep3').on('click',function (e) {
//		e.preventDefault();
//
//		var valid = true;
//		var cc_num = $('input[name=cc_number]').val();
//		var cc_month = $('input[name=cc_expiration_month]').val();
//		var cc_year = $('input[name=cc_expiration_year]').val();
//		var cc_ccv = $('input[name=cc_ccv]').val();
//		if(cc_num.length==0){
//			$('input[name=cc_number]').css('border','1px solid #a72d2d');
//			$('input[name=cc_number]').parent('.inputText').css('color','red');
//			valid = false;
//		}else if(cc_month.length==0||cc_month.length>2){
//			$('input[name=cc_expiration_month]').css('border','1px solid #a72d2d');
//			$('input[name=cc_expiration_month]').parent('.inputText').css('color','red');
//			valid = false;
//		}else if(cc_year.length==0||cc_year.length>4){
//			$('input[name=cc_expiration_year]').css('border','1px solid #a72d2d');
//			$('input[name=cc_expiration_year]').parent('.inputText').css('color','red');
//			valid = false;
//		}else if(cc_ccv.length==0){
//			$('input[name=cc_ccv]').css('border','1px solid #a72d2d');
//			$('input[name=cc_ccv]').parent('.inputText').css('color','red');
//			valid = false;
//		}
//
//
//	})
});
	

</script>

<!-- Landing Wrapper Scripts -->
 <script>
$(document).ready(function(){
    //     $('.latest-blog-slider').slick({
    //     autoplay: true,
    //     autoplaySpeed: 5000,
    //     slidesToShow: 3,
    //     arrows: false,
    //     dots: true,
    // });

    $('#slide-down').click(function(e) {
    	e.preventDefault;
    	$('.landingWrapper').fadeOut('slow');
	});
});
 </script>
<script>
	//	$("#paypalPayBtnClicker").on('click', function(e){
	//		$('#myContainer').submit();
	//	});

	window.paypalCheckoutReady = function() {
		paypal.checkout.setup('<?=PAYPAL_CLIENT_ID?>', {
			environment: 'live',
			button: 'paypal',
			click: function(){
				paypal.checkout.initXO();
				var action = $.post('/set-express-checkout');
				action.done (function(data){
					paypal.checkout.startFlow(data.token);
				});
				action.fail(function(){
					paypal.checkout.closeFlow();
				})
			},
			condition: function(){
				var data = {
					first_name:$('[name=first_name]').val(),
					last_name:$('[name=last_name]').val(),
					address:$('[name=address]').val(),
					city:$('[name=city]').val(),
					state:$('[name=state]:enabled').val(),
					zip:$('[name=zip]').val(),
					email:$('[name=email]').val(),
					phone:$('[name=phone]').val(),
					congregation_name:$('[name=congregation_name]').val(),
					position:$('[name=position]').val(),
					congregation_city:$('[name=congregation_city]').val(),
					congregation_state:$('[name=congregation_state]:enabled').val()
				};
				var submit = true;
				$.each(data,function(key,value){
					if(!value){
						submit = false;
						$('[name=' + key +']:enabled').css('border', '1px solid red');
					} else {
						$('[name=' + key +']:enabled').css('border', '1px solid #d6d6d6');
					}
				});
				return submit;
			}
		});
</script>
<script src="//www.paypalobjects.com/api/checkout.js" async></script>