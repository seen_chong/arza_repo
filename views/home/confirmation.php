<div class="homePageWrapper">
    <div class="mediaSection">
        <div class="slider-container">

            <div class="featuredEvents">
                <? $imagePath = ($model->event->featured_image) ? UPLOAD_URL . 'events/' . $model->event->featured_image : FRONT_IMG . 'homeFooterHero.png'; ?>
                <img src="<?= $imagePath ?>">
                <div class="eventOverlay">
                    <div class="eventDate">
                        <p><?= date("M", $model->event->start_time) ?></p>
                        <h6><?= date("d", $model->event->start_time) ?></h6>
                    </div>
                    <h3><?=$model->event->name ?></h3>
                    <p><?=$model->event->description ?></p>
                </div>
            </div>
        </div>

        <div class="rsvpModule" style="width: 60%;margin-top: 50px">
            <h3>Thank you for your attending!</h3>
            <h2><?=$model->event->name ?></h2>
            <p id="description"><?=$model->event->description ?></p><br>
            <p>Address: <?=$model->event->getAddr()?></p>
            <h2>                                                </h2>
            <h3>Your Information</h3>
            <label>Name : </label>
            <p><?=$model->rsvp->full_name()?></p><br>
            <label>Address : </label>
            <p><?=$model->rsvp->getAddr()?></p><br>
            <label>Email : </label>
            <p><?=$model->rsvp->email?></p><br>
            <label>Phone : </label>
            <p><?=$model->rsvp->phone?></p><br>
            <h2>                                                </h2>
            <h3>Details</h3>
            <label>Reference Number : </label>
            <p><?= $model->rsvp->ref_num ?></p><br>
            <? if ($model->event->price >0){ ?>
                <label>Price</label>
                <p>$<?= number_format($model->event->price,2) ?></p>
            <?}?>

        </div>
    </div>


</div>
<style>
    .rsvpModule p{
        display:inline;
    }
</style>