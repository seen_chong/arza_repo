<div class="blog_global_wrapper">
    <div class="col_left blog_categories_list_section">
        <h2>Browse Posts</h2>

        <div class="category_list_wrapper">
            <ul>
                <? foreach ($model->services as $s) { ?>


                    <li class="blog_category_item">
                        <a href="/blog/category/<?= $s->id ?>">
                            <div class="blog_category_image img_overflow">
                                <img src="<?=S3_URL?><?= $s->featured_image; ?>">
                            </div>
                            <div class="image_overlay">
                            </div>
                            <h4 class="category_title">
                                <?= $s->name ?>
                            </h4>
                            <h6 class="category_description">
                                <?= count(\Model\Blog::getList(["where" => "service ='" . $s->id . "'"])) ?> posts
                            </h6>
                        </a>
                    </li>
                <? } ?>
            </ul>
        </div>
    </div>
    <div class="col_right">
        <div class="blog_header">
            <h1>Welcome to the Wellns Blog <span class="category_active">Yoga</span></h1>

            <p>A collection of writings by wellns providers</p>
        </div>
        <form action="/blog/do_add" method="post">
            <select name="service">
                <?php foreach (\Model\Service::getList() as $s) { ?>
                    <option value="<?php echo $s->id; ?>"><?php echo $s->name; ?></option>
                <?php } ?>
            </select>
            <label>Title</label>

            <p>
                <input type="text" name="title" value="" size="25"/>
            </p>

            <p>
                <input type="text" name="email" value="" size="25"/>
            </p>

            <p>
                <textarea name="description" cols="48" rows="8"> </textarea>
            </p>

            <p>
                <input name="submit" type="submit" id="submit" value="Add"/>
            </p>
        </form>


    </div>
</div>