<div class="contentContainer blogContainer">
    <div class="contentWrapper blogWrapper">
        <div class="blogSlider">
            <?foreach ($model->featured_posts as $post){?>
            <div class="blogHeader">
                <div class="blogHeaderVisual">
                    <a href="<?=SITE_URL. 'blog/post/'. $post->slug?>">
                        <img src="<?=UPLOAD_URL.'blogs/'.$post->featured_image?>">
                    </a>
                </div>
                <div class="blogHeaderText">
                    <h1><?=$post->title?></h1>
                    <!-- <img src="<//?=FRONT_IMG?>darkDash.png"> -->
                    <h2><?=$post->preview_description?>
                        <a href="<?=SITE_URL. 'blog/post/'. $post->slug?>">
                            <button>Read More</button>
                        </a>   
                    </h2>

                </div>
            </div>
            <?}?>
        </div>

        <div class="blogBody">
            <div class="blogBodyWrapper">
            <div class="blogBodyTitle">
                <h3>Even More Stories From <span style="text-transform:uppercase;">ARZA</span></h3>
            </div>
                <?foreach($model->posts as $post){?>
                <div class="blogBlock">
                    <a href="<?=SITE_URL. 'blog/post/'. $post->slug?>">
                        <img src="<?=UPLOAD_URL.'blogs/'.$post->featured_image?>">
                    </a>
                    <div class="blogSpacer">
                        <h3><?=$post->title?></h3>
                        <span class="blogDate"><?=$post->date_modified?></span>
                        <div class="blogContentSpacer">
                            <p><?=$post->preview_description?></p>
                        </div>
                        <a href="<?=SITE_URL. 'blog/post/'. $post->slug?>"><button>Read More</button></a>
                    </div>
                    <div class="blogSocial">
                        <div class="blogSocialShare">
                            <a class="facebookIcon">
                                <icon class="icon phone_icon" style="background-image:url('<?=FRONT_IMG?>facebookLogo.png')"></icon>
                            </a>
                            <a href="http://twitter.com/share" target="_blank" class="twitterIcon">
                                <icon class="icon phone_icon" style="background-image:url('<?=FRONT_IMG?>twitterLogo.png')"></icon>
                            </a>
                            <a class="youtubeIcon">
                                <icon class="icon phone_icon" style="background-image:url('<?=FRONT_IMG?>youtubeLogo.png')"></icon>
                            </a>
                        </div>
                    </div>
                </div>
                <?}?>
            </div>

            <div class="blogSidebar">
                <div class="recentBar">
                    <h4>Recent Posts</h4>
                    <?foreach($model->recent_posts as $post){?>
                    <div class="recentBarPost">
                        <div class="barPostImage">
                            <a href="<?=SITE_URL. 'blog/post/'. $post->slug?>">
                                <img src="<?=UPLOAD_URL.'blogs/'.$post->featured_image?>">
                            </a>
                        </div>
                        
                        <div class="barPostText">
                            <h5><a href="<?=SITE_URL. 'blog/post/'. $post->slug?>"><?=$post->title?></a></h5>
                            <span class="recentDate"><?=$post->date_modified?></span>
                        </div>
                    </div>
                    <?}?>
                </div>
                <div class="subscribeBar">
                    <h4>Subscribe and Follow</h4>
                        <div class="subscribeShare">
                            <a class="facebookIcon">
                                <icon class="icon phone_icon" style="background-image:url('<?=FRONT_IMG?>facebookLogo.png')"></icon>
                            </a>
                            <a class="twitterIcon">
                                <icon class="icon phone_icon" style="background-image:url('<?=FRONT_IMG?>twitterLogo.png')"></icon>
                            </a>
                            <a class="youtubeIcon">
                                <icon class="icon phone_icon" style="background-image:url('<?=FRONT_IMG?>youtubeLogo.png')"></icon>
                            </a>
                        </div>
                </div>
                <div class="catBar">
                    <h4>Categories</h4>
                    <?$catgories=\Model\Blog_Category::getList();?>
                    <ul>
                        <?foreach($catgories as $cat){
                            $count = \Model\Blog::getCount(['where'=>"category='".$cat->id."'"]);;?>
                            <a href="<?=SITE_URL. 'blog/category/'. $cat->id?>"><li><?=$cat->name?> <span class="lightFont">(<?=$count?>)</span></li></a>
                        <?}?>
                    </ul>
                </div>
            </div>
             
        </div>
        
    </div>

</div>

<script type="text/javascript">
$(document).ready(function(){
  $('.blogSlider').slick({
    autoplay: true,
    autoplaySpeed: 5000,
    arrows: false,
    dots: true,
     customPaging: function(slider, i) { 
    // this example would render "tabs" with titles
    return '<button class="tab">' + $(slider.$slides[i]).find('.slide-title').text() + '</button>';
  },
  });
});
</script>