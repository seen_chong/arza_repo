<div class="pageWrapper homePageWrapper">
   <?$orders = \Model\Mission_Display::getList(['orderBy'=>'display_order']);
    $mission = \Model\Mission::getItem(null,['where' => "section_name='top'"]); ?>
    <div class="ourWorkHeader">
        <div class="ourWorkWrapper">
            <h5><?=$mission->title?></h5>
            <p><?=$mission->description?></p>
        </div>
    </div>

    <?foreach ($orders as $order){?>

    <? if($order->display == 1 && $order->section_name == 'main banner'){
            $mission = \Model\Mission::getItem(null,['where' => "section_name='main banner'"]);?>
        <?$path = $mission->featured_image==''? FRONT_IMG."pursuitBg.jpg":UPLOAD_URL."missions/".$mission->featured_image;?>
        <div class="pursuitContainer" style="background-image:url('<?=$path?>');">
            <div class="pursuitLeft" >
                <div class="innerFloatLeft">
<!--                    <h2>01</h2>-->
                </div>


                <div class="innerFloatRight">
                    <h3><?=$mission->title?></h3>
                    <p><?=$mission->description?></p>
                </div>

            </div>

            <div class="pursuitRight">
                <h6><?=$mission->subtitle?></h6>
                <p><?=$mission->sub_description?></p>
                <!--            <p> Wild animals cannot protect themselves from those out to poach them and traffic them illegally. But we can. And we must. Voice your support for ARZA’s mission to End Wildlife Trafficking.”</p>-->

                <div class="subscribeForm">
                    <input type="text" name="mail" placeholder="Email Address" class="subscribeEmail">
                    <input type="submit" value="Join Us" class="emailSubmit">
                </div>
            </div>

            <div class="donateToUs">
                <a href="<?=$mission->link?>"><button><?=$mission->button?></button></a>
            </div>
        </div>
        <?}else if($order->display == 1 && $order->section_name == 'video'){?>
            <? $mission = \Model\Mission::getItem(null,['where'=>"section_name = 'Video'"]);
            $src = "https://www.youtube.com/embed/".$mission->video_link;?>
            <div class="videoEmbedSection">
                <iframe width="100%" height="500" src="<?=$src?>" frameborder="0" allowfullscreen></iframe>
            </div>
            <style type="text/css">
                .ytp-title {
                    display: none !important;
                }
            </style>
        <?}else if($order->display == 1 && $order->section_name == 'history'){?>
            <div class="timelineHeader">
                <h1>The Arza Story</h1>
            </div>

            <div class="timeline">
                <?
                $his = \Model\History::getList();
                $years=[];
                foreach ($his as $hi){
                    $time = $hi->time;
                    $time = explode(' ',$time);
                    if(!in_array($time[1],$years)){
                        $years[] = $time[1];
                    }
                }
                rsort($years);
                foreach ($years as $h){
                    $historys = \Model\History::getList(['where'=>"time like '%{$h}%'"]);?>
                    <h2><?=$h?></h2>
                    <ul>
                        <?foreach ($historys as $history){?>
                            <li>
                                <h3><?=$history->title?></h3>
                                <p><?=$history->description?></p>
                                <time><?=$history->time?></time>
                            </li>
                        <?}?>
                    </ul>
                <?};?>
            </div>
        <?}else if($order->display == 1 && $order->section_name == 'map'){?>
            <div class="locationSection">
                <div class="sectionWrapper">

                    <div class="commBlock">
                        <h4>Communities in Israel</h4>
                        <div class="commList ScrollStyle" data-mcs-theme="dark">
                            <ul>
                                <? foreach ($model->communities as $obj) { ?>
                                    <li class="listing-item" data-marker-id="<?= $obj->id ?>" data-marker-lat="<?= $obj->lat ?>"
                                        data-marker-lng="<?= $obj->lng ?>" data-marker-name="<?= $obj->name ?>" data-marker_des="<?=$obj->description
                                    ?>"><img src="<?= FRONT_IMG ?>redLine.png">
                                        <p><?= $obj->name ?> <br><?= $obj->city . ' ' . $obj->state ?></p>
                                    </li>
                                <? } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="col commMap">
                        <div class="mapKey" id="googleMap"></div>
                    </div>
                </div>
            </div>
        <?}else if($order->display == 1 && $order->section_name == 'our partners'){
            $mission = \Model\Mission::getItem(null,['where'=>"section_name = 'our partners'"]);?>
            <div class="homeHeroWrapper" style="margin-top: auto">
                <div class="homeHero"
                     style="background-image:url('<?= UPLOAD_URL ?>missions/<?= $mission->featured_image ?>');">
                    <img class="heroOverlay" src="<?= FRONT_IMG ?>heroOverlay.png">
                </div>
                <div class="heroText">
                    <h1><?= $mission->title ?></h1>
                    <p><?= $mission->description ?></p>


                </div>
                <div class="partner">
                    <a href="<?=$mission->link?>"><button><?=$mission->subtitle?></button></a>
                </div>

            </div>

        <?}else if($order->display == 1 && $order->section_name == '3 text blocks'){?>

            <div class="threeBlocks">
                <div class="sectionWrapper">
                    <? $missions = \Model\Mission::getList(['where'=>"section_name like 'block%'"]);
                    foreach ($missions as $mission){
                        $i++;?>
                        <div class="blockOne">
                            <h5><?=$mission->title?></h5>
                            <p><?=$mission->description?> </p>
                        </div>
                    <? }?>
                </div>
            </div>
        <?}?>

    <?}?>

</div>
<script>
    $(document).ready(function(){
        $(document).on('click','.emailSubmit',function (e) {
            var email = $(".subscribeEmail").val();
            if(email && email.indexOf('@') > 0){
                $.post('/newsletter/add',{email:email},function(data){
                    if(data.status == 'success'){
                        alert(data.message);
                    } else{
                        alert(data.message);
                    }
                    $('#emailAddr').val('');
                })
            }
        })
    })
</script>
<script src="https://maps.google.com/maps/api/js?libraries=geometry&v=3.24&key=AIzaSyD40vQ5_kWKQtMkbr5x9aLnTSbsy6nTj5w">
</script>

<script>
    function makeMapPlotPoints() {

        // Set marker from results list and create empty plot point array
        var mapPlotPointDOM = $(".listing-item");
        var mapPlotPointArr = [];

        $(mapPlotPointDOM).each(function () {
            if ($(this).data("marker-lat") !== '') {
                mapPlotPointArr.push([
                    $(this).data("marker-id"),
                    $(this).data("marker-lat"),
                    $(this).data("marker-lng"),
                    $(this).data("marker-name"),
                    $(this).data("marker_des"),
                ]);
            }
        });
        setMarkers(mapPlotPointArr);
    };
    var map;
    var markers = []; // Create a marker array to hold markers
    var bounds = new google.maps.LatLngBounds();
    var infowindow = new google.maps.InfoWindow();
    //        var center = {
    //            lat: 0,
    //            lng: 0
    //        };
    //        var overlay;

    function setMarkers(locations) {
        for (var i = 0; i < locations.length; i++) {

            var mapMarkerItem = locations[i];
            var myLatLng = new google.maps.LatLng(mapMarkerItem[1], mapMarkerItem[2]);

            //to use it
//                var htmlMarker = new HTMLMarker(mapMarkerItem[1], mapMarkerItem[2], mapMarkerItem[0], mapMarkerItem[3]);
//                htmlMarker.setMap(map);
            var label = locations[i][3].toUpperCase().charAt(0);
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                icon: "http://maps.google.com/mapfiles/marker" + label + ".png"
            });

            // Set Map Bounds to Auto-center
//            bounds.extend(myLatLng);
//            map.fitBounds(bounds);

            // Push marker to markers array
            markers.push(marker);
//                markers.push(htmlMarker);

            // Marker Info Window / Tooltip
            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent('<h1 id="firstHeading" class="firstHeading">'+locations[i][3]+'</h1><div id="bodyContent">'+locations[i][4]+'</div>');
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
    }



    function initMap(loc) {
        var mapOptions = {
            zoom: 10,
            center: loc,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
        }

        map = new google.maps.Map(document.getElementById('googleMap'), mapOptions);

        makeMapPlotPoints();

    }
    $(document).ready(function () {

        var lat = <?=$model->communities[0]->lat?>;
        var lng = <?=$model->communities[0]->lng?>;
        var loc = new google.maps.LatLng(lat, lng);
        initMap(loc);

        $(".pageWrapper.homePageWrapper").on("click","ul .listing-item",function(){
            var marker_id=$(this).attr("data-marker-id");
            var lat=$(this).attr("data-marker-lat");
            var lng=$(this).attr("data-marker-lng");
            var loc = new google.maps.LatLng($(this).attr("data-marker-lat"), $(this).attr("data-marker-lng"));
            map.panTo(loc);
            $('li').find('img').attr("src",'<?= FRONT_IMG ?>redLine.png');
            $(this).find('img').attr("src",'<?= FRONT_IMG ?>redMarker.png');
            for(var m=0; m<markers.length; m++){
                markers[m].setAnimation(null);
                if(JSON.stringify(markers[m]['position']) === JSON.stringify(loc)){
                    var myMarker = markers[m];
                }
            }
            myMarker.setAnimation(google.maps.Animation.BOUNCE);

        });


    })

</script>
<style>
    #googleMap .htmlMarker > .color_block {
        position: absolute;
        left: 0;
        top: 0;
        right: 0;
        bottom: 0;
        border-radius: 18px;
    }
    #googleMap .htmlMarker > .color_block {
        position: absolute;
        left: 0;
        top: 0;
        right: 0;
        bottom: 0;
        border-radius: 18px;
    }
    .htmlMarker span.color_block_triangle {
        content: '';
        position: absolute;
        top: 100%;
        left: 50%;
        margin-left: -6px;
        width: 0;
        background-color: transparent!important;
        height: 0;
        border-style: solid;
        border-left-color: transparent!important;
        border-right-color: transparent!important;
        border-width: 5px 6px 0 6px;
        -webkit-transition: border-color .1s cubic-bezier(.645,.045,.355,1);
        transition: border-color .1s cubic-bezier(.645,.045,.355,1);
    }
    .htmlMarker span.whiten_block {
        position: absolute;
        left: 0px;
        top: 0;
        bottom: 0;
        right: 0;
        opacity: 0;
        background: #f5efeb;
        z-index: 10;
    }

    #googleMap .htmlMarker{
        background: #f2c559;
        width: 93px;
        padding-right: 22px;
        opacity: 0.9;
        text-align: center;
    }


    #googleMap .htmlMarker.markerActive span.whiten_block {
        opacity: .2;
    }
    .col.commMap .mapKey {

        width:70%;
        height:500px;
    }
    .ScrollStyle
    {
        max-height: 450px;
        overflow-y:auto;
        overflow-x:scroll;
        word-wrap:break-word;

    }

    .homeHeroWrapper button
    {
        margin: 8px;
        border: 0;
        border-radius: 3px;
        background-color: #5da87b;
        color: #f1f1f2;
        width: 200px;
        height: 40px;
        padding: 5px;
        letter-spacing: 0.1em;
        font-size: 10px;
        text-transform: uppercase;
        font-family: 'Futura';
        font-weight: 500;
    }
</style>