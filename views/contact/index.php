<div class="pageWrapper homePageWrapper">

    <div class="getInvolvedSection">
    </div>

                <div class="contactForm">
                    <div class="contact-details" style="margin-bottom:40px;">
    <h1>633 Third Avenue, New York, NY 10017</h1>
    <h2>Phone 212.650.4280</h2>
    <h3>arza@arza.org</h3>
</div>
                    <div class="loginFormTitle">
                        <h2>Contact Us</h2>
                    </div>
                    <form class="form" method="post" action="/contact" enctype="multipart/form-data">
                    <div class="contactFormInputs">
                        <div class="stepBubbles">
                            <ul>
                                <a><li id="bubble1" data-part="part1" class="active">
                                    <span></span>
                                </li></a>
                            </ul>


                            <p>NAME <br>
                                <input type="text" name="name" placeholder="Name" required>
                            </p>
                            <p>EMAIL <br>
                                <input type="email" name="email" placeholder="Email" required>
                            </p>
                            <p>SUBJECT <br>
                                <input type="text" name="subject" placeholder="Subject" required>
                            </p>
                            <p>MESSAGE <br>
                                <textarea type="text" name="message" placeholder="Message" required></textarea>
                            </p>

                            <input type="submit" value="SUBMIT" id="contactSubmit">
                        </div>


                    </div>
</form>


                </div>

    <div class="inTheLoop">
        <img src="<?=FRONT_IMG?>whiteCircles.png">
        <h2>Stay in the Loop</h2>

        <div class="loopJoin">
            <input type="text" name="mail" value="Your Email Address" class="loopEmail">
            <input type="submit" value="Join Our Newsletter" class="loopSubmit">
        </div>


    </div>
   

</div>