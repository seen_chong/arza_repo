<? $donate = $model->donate;
$background_image =
    $donate->background_image == '' ? "background-color:#6dca92" : "background-image:url(" . UPLOAD_URL . "donate/" . $donate->background_image . ")";
$logo = $donate->logo_image == ''? FRONT_IMG .'whiteLogo.png' : UPLOAD_URL.'donate/'.$donate->logo_image;
if($donate->template == 1){
    $class='donateSection';
    $wrapper = "donateLeftBlock";
}else{
    $class='crisisSection';
    $wrapper = "donateCenterBlock";
}
$content = json_decode($donate->donate_amount,true);
 $form = json_decode($donate->donate_form,true);
?>
<div class="homePageWrapper">
    <!-- index donate page -->
    <?if($donate->default_template == 1){?>
    <div class="donateSection" style="<?=$background_image?>">
        <div class="donateWrapper">
            <div class="donateLeftBlock">
                <div class="donateLogo">
                    <img src="<?=$logo?>">
                </div>
                <div class="donateForm">
                    <div class="donateFormTitle">
                        <h5><?=$donate->title?></h5>
                    </div>
                    <form method="post" action="/pay" enctype="multipart/form-data" id="donateForm">
                        <div class="donateFormInputs">
                            <div class="stepBubbles">
                                <ul>
                                    <li id="bubble3" data-part="part3">
                                        <span></span>
                                    </li>
                                    <a><li id="bubble1" data-part="part1" class="active">
                                            <span></span>
                                        </li></a>
                                    <li id="bubble2" data-part="part2" >
                                        <span></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="amountSelect part part1">
                                <p>Select an Amount</p>
                                <?
                                foreach($content as $value){?>
                                    <input type="button"  class="donateAmount" value="<?=$value?>">
                                <?}?>
                                <input type="number" min="1.00" step="1" class="otherAmount" placeholder="Other Amount">
                                <label class="inlineSelector" id="repeatDonation" style=" <?= ($form['monthly'] == 0)? 'display:none':''?> "><input type="checkbox"><p>I'd like to repeat this gift every month</p></label>
                                <label class="inlineSelector" id = "specify" style=" <?= ($form['specify'] == 0)? 'display:none':''?> "><input type="checkbox"><p>This gift is honor of/in memory of</p></label>
                                <p class="addyFields field100 notify_content" hidden>
                                    <input type="text" name="gift_designation">
                                </p>
                                <p class="addyFields field10 notify_content" hidden>Please notify <span style="text-decoration: underline"><b>(name)(address)</b></span> about my gift.</p>
                                <input value="< Back" id="goBack" class="nextStep">
                                <input value="Next >" id="nextStep2" class="nextStep">
                            </div>
                            <?php $states = get_states();?>
                            <div class="addyFields part part2" style="display:none;">
                                <input hidden type="number" name = "amount">
                                <input hidden name="type" value="<?=$donate->id?>">
                                <input hidden type="number" name = "monthly" value="0">
                                <p class="field50" style=" <?= ($form['first_name'] == 0)? 'display:none':''?> ">First name <br>
                                    <input type="text" name="first_name">
                                </p>
                                <p class="field50" style=" <?= ($form['last_name'] == 0)? 'display:none':''?> ">Last name <br>
                                    <input type="text" name="last_name">
                                </p>
                                <p class="field100" style=" <?= ($form['address'] == 0)? 'display:none':''?> ">Address <br>
                                    <input type="text" name="address">
                                </p>
                                <p class="field50" style=" <?= ($form['city'] == 0)? 'display:none':''?> ">City <br>
                                    <input type="text" name="city">
                                </p>
                                <p class="field25" style=" <?= ($form['state'] == 0)? 'display:none':''?> ">State <br>
                                    <select name="state" required>
                                        <?foreach(get_states() as $short=>$long){?>
                                            <option value="<?=$short?>"><?=$long?></option>
                                        <?}?>
                                    </select>
                                </p>
                                <p class="field25" style=" <?= ($form['zip'] == 0)? 'display:none':''?> ">ZIP code <br>
                                    <input type="text" name="zip">
                                </p>
                                <p class="field50" style=" <?= ($form['email'] == 0)? 'display:none':''?> ">Email <br>
                                    <input type="text" id="email" name="email">
                                </p>
                                <p class="field50" style=" <?= ($form['phone'] == 0)? 'display:none':''?> ">Phone <br>
                                    <input type="text" name="phone">
                                </p>
                                <p class="field100" style=" <?= ($form['preference'] == 0)? 'display:none':''?> ">Any Additional Instructions<br>
                                    <textarea name="preference"></textarea>
                                </p>
                                <input type="submit" value="Donate $100" id="nextStep3" class="nextStepBtn">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="donateRightBlock">
                <div class="rightBlockContent">
                    <h4><?=$donate->subtitle?></h4>
                    <?$path = UPLOAD_URL.'donate/'.$donate->image?>
                    <img src="<?=$path?>">
                </div>
            </div>
        </div>
    </div>
    <?}else{?>
        <div class="crisisSection" style="<?=$background_image?>">
            <div class="donateWrapper">
                <div class="donateCenterBlock">
                    <div class="crisisLogo">
                        <img src="<?=$logo?>">
                    </div>
                    <div class="crisisLogoTitle">
                        <h3><?=$donate->logo_description?></h3>
                    </div>
                    <div class="crisisLogoText">
                        <p><?=$donate->title?></p>
                    </div>
                    <div class="donateForm">
                        <div class="donateFormTitle">
<!--                            <h5>--><?//=$donate->title?><!--</h5>-->
                        </div>
                        <form method="post" action="/pay" enctype="multipart/form-data" id="donateForm">
                        <div class="donateFormInputs">
                            <div class="stepBubbles">
                                <ul>
                                    <li id="bubble3" data-part="part3">
                                        <span></span>
                                    </li>
                                    <a><li id="bubble1" data-part="part1" class="active">
                                            <span></span>
                                        </li></a>
                                    <li id="bubble2" data-part="part2" >
                                        <span></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="amountSelect part part1">
                                <p>Select an Amount</p>
                                <?
                                foreach($content as $value){?>
                                    <input type="button"  class="donateAmount" value="<?=$value?>">
                                <?}?>
                                <input type="number" min="1.00" step="1" class="otherAmount" placeholder="Other Amount">
                                <label class="inlineSelector" id="repeatDonation" style=" <?= ($form['monthly'] == 0)? 'display:none':''?> "><input type="checkbox"><p>I'd like to repeat this gift every month</p></label>
                                <label class="inlineSelector" id = "specify" style=" <?= ($form['specify'] == 0)? 'display:none':''?> "><input type="checkbox"><p>This gift is honor of/in memory of</p></label>
                                <p class="addyFields field100 notify_content" hidden>
                                    <input type="text" name="gift_designation">
                                </p>
                                <p class="addyFields field10 notify_content" hidden>Please notify <span style="text-decoration: underline"><b>(name)(address)</b></span> about my gift.</p>
                                <input value="< Back" id="goBack" class="nextStep">
                                <input value="Next >" id="nextStep2" class="nextStep">
                            </div>
                            <?php $states = get_states();?>
                            <div class="addyFields part part2" style="display:none;">
                                <input hidden type="number" name = "amount">
                                <input hidden name="type" value="<?=$donate->id?>">
                                <input hidden type="number" name = "monthly" value="0">
                                <p class="field50" style=" <?= ($form['first_name'] == 0)? 'display:none':''?> ">First name <br>
                                    <input type="text" name="first_name">
                                </p>
                                <p class="field50" style=" <?= ($form['last_name'] == 0)? 'display:none':''?> ">Last name <br>
                                    <input type="text" name="last_name">
                                </p>
                                <p class="field100" style=" <?= ($form['address'] == 0)? 'display:none':''?> ">Address <br>
                                    <input type="text" name="address">
                                </p>
                                <p class="field50" style=" <?= ($form['city'] == 0)? 'display:none':''?> ">City <br>
                                    <input type="text" name="city">
                                </p>
                                <p class="field25" style=" <?= ($form['state'] == 0)? 'display:none':''?> ">State <br>
                                    <select name="state" required>
                                        <?foreach(get_states() as $short=>$long){?>
                                            <option value="<?=$short?>"><?=$long?></option>
                                        <?}?>
                                    </select>
                                </p>
                                <p class="field25" style=" <?= ($form['zip'] == 0)? 'display:none':''?> ">ZIP code <br>
                                    <input type="text" name="zip">
                                </p>
                                <p class="field50" style=" <?= ($form['email'] == 0)? 'display:none':''?> ">Email <br>
                                    <input type="text" id="email" name="email">
                                </p>
                                <p class="field50" style=" <?= ($form['phone'] == 0)? 'display:none':''?> ">Phone <br>
                                    <input type="text" name="phone">
                                </p>
                                <p class="field100" style=" <?= ($form['preference'] == 0)? 'display:none':''?> ">Any Additional Instructions<br>
                                    <textarea name="preference"></textarea>
                                </p>
                                <input type="submit" value="Donate $100" id="nextStep3" class="nextStepBtn">
                            </div>
                        </div>
                    </form>
                        </div>
                        <div class="crisisLogoText">
                        <p><span><?=$donate->subtitle?></span></p>
                    </div>
                </div>
            </div>
        </div>
    <?}?>
</div>
<script type="text/javascript">
    $( document ).ready(function() {
        $(document).keypress(
            function(event){
                if (event.which == '13') {
                    event.preventDefault();
                }
            });
        $('input#nextStep2').click(function() {
            var value = $('input[name="amount"]').val();
            if(value=='') {
                alert('Please Select an amount.');
            }else {
                $('#bubble1').removeClass('active');
                $('#bubble2').addClass('active');
                $('.amountSelect').hide();
                $('.addyFields').show();
                if($('input[name="monthly"]').val()==1){
                    var text = $('input#nextStep3').val();
                    $('input#nextStep3').val(text + ' Monthly');
                }
            }
        });

        $('.donateAmount').on('click',function(){
            $('.donateAmount').not(this).removeClass('greenBg');
            $(this).addClass('greenBg');
            var amount = $(this).val();
            $('input[name="amount"]').val(amount);
            $('input[class="otherAmount"]').val('');
            $('input#nextStep3').val('Donate $'+amount);
        })
        $('.otherAmount').on('keypress',function () {
            $('.donateAmount').each(function () {
                $(this).removeClass('greenBg');
            })
        });
        $('.otherAmount').on('change',function(){
            var amount = $(this).val();
            $('input[name="amount"]').val(amount);
            $('input#nextStep3').val('Donate $'+amount);
        })

        $('#repeatDonation input').on('click',function(){
            if($('input[name="monthly"]').val()==0){
                $('input[name="monthly"]').val(1);
            }else{
                $('input[name="monthly"]').val(0);
            }

        });
        $('#specify input').on('click',function(){

            var isChecked = $(this).prop('checked');
            if(isChecked){
                $(".notify_content").show();
            }else {
                $(".notify_content").hide();
            }
        });
        $('input#goBack').click(function() {
            window.location.href = '<?=SITE_URL . 'donation'?>';

        })
        $(function(){
            $("#donateForm").validate({
                rules: {
                    first_name:{
                        required: true,
                    },
                    last_name:{
                        required: true,
                    },
                    address:{
                        required: true,
                    },
                    city:{
                        required: true,
                    },
                    state:{
                        required: true,
                    },
                    zip:{
                        required: true
                    },
                    email:{
                        required: true,
                        email:true
                    },
                    phone:{
                        required: true,
                        number: true
                    },
                    amount:{
                        required: true,
                        number: true
                    }
                },
                messages:{
                    first_name:{
                        required: "Please enter your first name",
                    },
                    last_name:{
                        required: "Please enter your last name",
                    },
                    address:{
                        required: "Please enter your address",
                    },
                    city:{
                        required: "Please enter your city",
                    },
                    state:{
                        required: "Please select your state",
                    },
                    zip:{
                        required: "Please enter your zip",
                    },
                    email:{
                        required: "Please enter your email",
                        email:"Invalid Email Address"
                    },
                    phone:{
                        required: "Please enter your phone number",
                        number:"Invalid phone number"
                    },
                    amount:{
                        required: "Please enter your donation amount",
                        number:"Invalid amount"
                    },
                    position:{
                        required: "Please enter your position",
                    }

                },
                errorClass: "error-float"
            });

        });
    });

</script>
<script>
    window.paypalCheckoutReady = function() {
        paypal.checkout.setup('<?=PAYPAL_CLIENT_ID?>', {
            environment: 'live',
            button: 'nextStep3',
            click: function(){
                paypal.checkout.initXO();
                var action = $.post('/set-express-checkout');
                action.done (function(data){
                    paypal.checkout.startFlow(data.token);
                });
                action.fail(function(){
                    paypal.checkout.closeFlow();
                })
            },
            condition: function(){
                var data = {
                    bill_first_name:$('[name=first_name]').val(),
                    bill_last_name:$('[name=last_name]').val(),
                    bill_address:$('[name=address]').val(),
                    bill_city:$('[name=city]').val(),
                    bill_state:$('[name=state]:enabled').val(),
                    bill_zip:$('[name=zip]').val(),
                    bill_country:$('[name=email]').val(),
                    phone:$('[name=phone]').val(),
                    preference:$('[name=preference]').val()
                };
                var submit = true;
                $.each(data,function(key,value){
                    if(!value){
                        submit = false;
                        $('[name=' + key +']:enabled').css('border', '1px solid red');
                    } else {
                        $('[name=' + key +']:enabled').css('border', '1px solid #d6d6d6');
                    }
                });
                return submit;
            }
        });
    };
</script>
<script src="//www.paypalobjects.com/api/checkout.js" async></script>
