<div class="pageWrapper categoryPageWrapper">
    <div class="inventoryUIWrapper">
        <div class="inventoryFilterBars">
            <div class="filterTriggerBar row">
                <div class="row">
                    <div class="col">
                        <div class="dropdownUI filterDropdownUI">
                            <div class="visible">
                                <div class="visible_trigger">
                                    <p class="futura futura_caps">Tumi</p><icon class="downTriangle geo"></icon>
                                </div>
                            </div>
                            <div class="hidden">
                                <ol class="dropdownVerticalList">
                                    <li>    
                                        <a>Steamline</a>
                                    </li>
                                    <li>    
                                        <a>Rimowa</a>
                                    </li>
                                    <li>    
                                        <a>Tumi</a>
                                    </li>
                                    <li>    
                                        <a>Samsonite</a>
                                    </li>
                                    <li>    
                                        <a>Eagle Creek</a>
                                    </li>
                                    <li>    
                                        <a>Travelpro</a>
                                    </li>
                                    <li>    
                                        <a>High Sierra</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="dropdownUI filterDropdownUI">
                            <div class="visible">
                                <div class="visible_trigger">
                                    <p class="futura">All Products</p><icon class="downTriangle geo"></icon>
                                </div>
                            </div>
                            <div class="hidden">
                                <ol class="dropdownVerticalList">
                                    <li>    
                                        <a>Suitcases</a>
                                    </li>
                                    <li>    
                                        <a>Carry-Ons</a>
                                    </li>
                                    <li>    
                                        <a>Trunks</a>
                                    </li>
                                    <li>    
                                        <a>Duffel Bags</a>
                                    </li>
                                    <li>    
                                        <a>Backpacks</a>
                                    </li>
                                    <li>    
                                        <a>Briefcases</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="dropdownUI filterDropdownUI">
                            <div class="visible">
                                <div class="visible_trigger">
                                    <p class="futura">Filters</p><icon class="plusSign geo"></icon>
                                </div>
                            </div>
                        </div>
                        <div class="filtersSubBar row">
                            <div class="col">
                                <div class="nestedDropdownUI filterDropdownUI filterDropdownUIFullWidth">
                                    <div class="visible">
                                        <div class="visible_trigger">
                                            <p class="futura futura_caps">All Colour</p><icon class="plusSign geo"></icon>
                                        </div>
                                    </div>
                                </div>
                                <div class="hidden nestedDropdownUIHiddenSection">
                                    <ol class="dropdownVerticalList">
                                        <li>    
                                            <a>
                                                <icon class="swatch" style="background-color:black;"></icon>
                                                <p>Black</p>
                                            </a>
                                        </li>
                                        <li>    
                                            <a>
                                                <icon class="swatch" style="background-color:blue;"></icon>
                                                <p>Blue</p>
                                            </a>
                                        </li>
                                        <li>    
                                            <a>
                                                <icon class="swatch" style="background-color:brown;"></icon>
                                                <p>Brown</p>
                                            </a>
                                        </li>
                                        <li>    
                                            <a>
                                                <icon class="swatch" style="background-color:green;"></icon>
                                                <p>Green</p>
                                            </a>
                                        </li>
                                        <li>    
                                            <a>
                                                <icon class="swatch" style="background-color:grey;"></icon>
                                                <p>Grey</p>
                                            </a>
                                        </li>
                                        <li>    
                                            <a>
                                                <icon class="swatch" style="background-color:orange;"></icon>
                                                <p>Orange</p>
                                            </a>
                                        </li>
                                        <li>    
                                            <a>
                                                <icon class="swatch" style="background-color:purple;"></icon>
                                                <p>Purple</p>
                                            </a>
                                        </li>
                                        <li>    
                                            <a>
                                                <icon class="swatch" style="background-color:red;"></icon>
                                                <p>Red</p>
                                            </a>
                                        </li>
                                        <li>    
                                            <a>
                                                <icon class="swatch" style="background-color:tan;"></icon>
                                                <p>Tan</p>
                                            </a>
                                        </li>
                                        <li>    
                                            <a>
                                                <icon class="swatch" style="background-color:white;"></icon>
                                                <p>White</p>
                                            </a>
                                        </li>
                                    </ol>
                                </div>                                
                            </div>                                
                        </div>
                    </div>
                    <div class="col right_float">
                        <div class="dropdownUI filterDropdownUI">
                            <div class="visible">
                                <div class="visible_trigger">
                                    <p class="futura futura_caps">Sort by</p><icon class="downTriangle geo"></icon>
                                </div>
                            </div>
                            <div class="hidden">
                                <ol class="dropdownVerticalList">
                                    <li>    
                                        <a>Newest</a>
                                    </li>
                                    <li>    
                                        <a>Price: Low to High</a>
                                    </li>
                                    <li>    
                                        <a>Price: High to Low</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="filterResultsBar row">
                <div class="col">
                    <h5 class="futura futuraCaps">92 Results</h5>
                </div>
            </div>
        </div>    
        <div class="inventoryViewBanner collectionInventoryViewBanner">
            <div class="mediaWrapper">
                <div class="media" style="background-image:url(<?=FRONT_IMG?>hero.jpg)"></div>
            </div>
            <div class="absVerticalCenter">
                <h1 class="futura">Travel Collection <span class="collectionCount"><h2 class="futura">11</h2></span></h1>
                <h4 class="futura">A rotating selection of luggage, fine bags, and travel cases. Taking into account the season's best destinations.</h4>
            </div>
        </div>
        <div class="inventoryGridUI">
            <div class="row row_of_4">
                <div class="col productInventoryItem">
                    <a href="<?=SITE_URL?>products">
                        <div class="featuredImgWrapper">
                            <img src='<?=FRONT_IMG?>tumi5.jpg'>
                        </div>
                        <div class="additionalImgs">
                            <img src='<?=FRONT_IMG?>addProdImg.png'>
                        </div>
                        <div class="productItemDataWrapper">
                            <h6 class="sackers brand">Tumi</h6>
                            <h3 class="caledonia name">
                                <span class="productName">Product Name Lorem Ipsum Dolor Sit Amet</span>
                                <div class="colorSwatches">
                                    <div class="swatch active">
                                        <img src='<?=FRONT_IMG?>swatch1.png'>
                                    </div>
                                    <div class="swatch">
                                        <img src='<?=FRONT_IMG?>swatch2.png'>
                                    </div>
                                    <div class="swatch">
                                        <img src='<?=FRONT_IMG?>swatch3.png'>
                                    </div>
                                </div>
                            </h3>
                            <h3 class="futura futuraCaps price"><span class="productPrice">$1,160.00</span><span class="colorCount">3 Colors</span></h3>
                        </div>
                    </a>
                </div>
                <div class="col productInventoryItem">
                    <a>
                        <div class="featuredImgWrapper">
                            <img src='<?=FRONT_IMG?>tumi4.jpg'>
                        </div>
                        <div class="additionalImgs">
                            <img src='<?=FRONT_IMG?>addProdImg.png'>
                        </div>
                        <div class="productItemDataWrapper">
                            <h6 class="sackers brand">Tumi</h6>
                            <h3 class="caledonia name">
                                <span class="productName">Product Name Lorem Ipsum Dolor Sit Amet</span>
                                <div class="colorSwatches">
                                    <div class="swatch active">
                                        <img src='<?=FRONT_IMG?>swatch1.png'>
                                    </div>
                                    <div class="swatch">
                                        <img src='<?=FRONT_IMG?>swatch2.png'>
                                    </div>
                                    <div class="swatch">
                                        <img src='<?=FRONT_IMG?>swatch3.png'>
                                    </div>
                                </div>
                            </h3>
                            <h3 class="futura futuraCaps price"><span class="productPrice">$1,160.00</span><span class="colorCount">3 Colors</span></h3>
                        </div>
                    </a>
                </div>
                <div class="col productInventoryItem">
                    <a>
                        <div class="featuredImgWrapper">
                            <img src='<?=FRONT_IMG?>tumi3.jpg'>
                        </div>
                        <div class="additionalImgs">
                            <img src='<?=FRONT_IMG?>addProdImg.png'>
                        </div>
                        <div class="productItemDataWrapper">
                            <h6 class="sackers brand">Tumi</h6>
                            <h3 class="caledonia name">
                                <span class="productName">Product Name Lorem Ipsum Dolor Sit Amet</span>
                                <div class="colorSwatches">
                                    <div class="swatch active">
                                        <img src='<?=FRONT_IMG?>swatch1.png'>
                                    </div>
                                    <div class="swatch">
                                        <img src='<?=FRONT_IMG?>swatch2.png'>
                                    </div>
                                    <div class="swatch">
                                        <img src='<?=FRONT_IMG?>swatch3.png'>
                                    </div>
                                </div>
                            </h3>
                            <h3 class="futura futuraCaps price"><span class="productPrice">$1,160.00</span><span class="colorCount">3 Colors</span></h3>
                        </div>
                    </a>
                </div>
                <div class="col productInventoryItem">
                    <a>
                        <div class="featuredImgWrapper">
                            <img src='<?=FRONT_IMG?>tumi1.jpg'>
                        </div>
                        <div class="additionalImgs">
                            <img src='<?=FRONT_IMG?>addProdImg.png'>
                        </div>
                        <div class="productItemDataWrapper">
                            <h6 class="sackers brand">Tumi</h6>
                            <h3 class="caledonia name">
                                <span class="productName">Product Name Lorem Ipsum Dolor Sit Amet</span>
                                <div class="colorSwatches">
                                    <div class="swatch active">
                                        <img src='<?=FRONT_IMG?>swatch1.png'>
                                    </div>
                                    <div class="swatch">
                                        <img src='<?=FRONT_IMG?>swatch2.png'>
                                    </div>
                                    <div class="swatch">
                                        <img src='<?=FRONT_IMG?>swatch3.png'>
                                    </div>
                                </div>
                            </h3>
                            <h3 class="futura futuraCaps price"><span class="productPrice">$1,160.00</span><span class="colorCount">3 Colors</span></h3>
                        </div>
                    </a>
                </div>
                <div class="col productInventoryItem">
                    <a>
                        <div class="featuredImgWrapper">
                            <img src='<?=FRONT_IMG?>tumi2.jpg'>
                        </div>
                        <div class="additionalImgs">
                            <img src='<?=FRONT_IMG?>addProdImg.png'>
                        </div>
                        <div class="productItemDataWrapper">
                            <h6 class="sackers brand">Tumi</h6>
                            <h3 class="caledonia name">
                                <span class="productName">Product Name Lorem Ipsum Dolor Sit Amet</span>
                                <div class="colorSwatches">
                                    <div class="swatch active">
                                        <img src='<?=FRONT_IMG?>swatch1.png'>
                                    </div>
                                    <div class="swatch">
                                        <img src='<?=FRONT_IMG?>swatch2.png'>
                                    </div>
                                    <div class="swatch">
                                        <img src='<?=FRONT_IMG?>swatch3.png'>
                                    </div>
                                </div>
                            </h3>
                            <h3 class="futura futuraCaps price"><span class="productPrice">$1,160.00</span><span class="colorCount">3 Colors</span></h3>
                        </div>
                    </a>
                </div>
                <div class="col productInventoryItem">
                    <a>
                        <div class="featuredImgWrapper">
                            <img src='<?=FRONT_IMG?>tumi3.jpg'>
                        </div>
                        <div class="additionalImgs">
                            <img src='<?=FRONT_IMG?>addProdImg.png'>
                        </div>
                        <div class="productItemDataWrapper">
                            <h6 class="sackers brand">Tumi</h6>
                            <h3 class="caledonia name">
                                <span class="productName">Product Name Lorem Ipsum Dolor Sit Amet</span>
                                <div class="colorSwatches">
                                    <div class="swatch active">
                                        <img src='<?=FRONT_IMG?>swatch1.png'>
                                    </div>
                                    <div class="swatch">
                                        <img src='<?=FRONT_IMG?>swatch2.png'>
                                    </div>
                                    <div class="swatch">
                                        <img src='<?=FRONT_IMG?>swatch3.png'>
                                    </div>
                                </div>
                            </h3>
                            <h3 class="futura futuraCaps price"><span class="productPrice">$1,160.00</span><span class="colorCount">3 Colors</span></h3>
                        </div>
                    </a>
                </div>
                <div class="col productInventoryItem">
                    <a>
                        <div class="featuredImgWrapper">
                            <img src='<?=FRONT_IMG?>tumi4.jpg'>
                        </div>
                        <div class="additionalImgs">
                            <img src='<?=FRONT_IMG?>addProdImg.png'>
                        </div>
                        <div class="productItemDataWrapper">
                            <h6 class="sackers brand">Tumi</h6>
                            <h3 class="caledonia name">
                                <span class="productName">Product Name Lorem Ipsum Dolor Sit Amet</span>
                                <div class="colorSwatches">
                                    <div class="swatch active">
                                        <img src='<?=FRONT_IMG?>swatch1.png'>
                                    </div>
                                    <div class="swatch">
                                        <img src='<?=FRONT_IMG?>swatch2.png'>
                                    </div>
                                    <div class="swatch">
                                        <img src='<?=FRONT_IMG?>swatch3.png'>
                                    </div>
                                </div>
                            </h3>
                            <h3 class="futura futuraCaps price"><span class="productPrice">$1,160.00</span><span class="colorCount">3 Colors</span></h3>
                        </div>
                    </a>
                </div>
                <div class="col productInventoryItem">
                    <a>
                        <div class="featuredImgWrapper">
                            <img src='<?=FRONT_IMG?>tumi5.jpg'>
                        </div>
                        <div class="additionalImgs">
                            <img src='<?=FRONT_IMG?>addProdImg.png'>
                        </div>
                        <div class="productItemDataWrapper">
                            <h6 class="sackers brand">Tumi</h6>
                            <h3 class="caledonia name">
                                <span class="productName">Product Name Lorem Ipsum Dolor Sit Amet</span>
                                <div class="colorSwatches">
                                    <div class="swatch active">
                                        <img src='<?=FRONT_IMG?>swatch1.png'>
                                    </div>
                                    <div class="swatch">
                                        <img src='<?=FRONT_IMG?>swatch2.png'>
                                    </div>
                                    <div class="swatch">
                                        <img src='<?=FRONT_IMG?>swatch3.png'>
                                    </div>
                                </div>
                            </h3>
                            <h3 class="futura futuraCaps price"><span class="productPrice">$1,160.00</span><span class="colorCount">3 Colors</span></h3>
                        </div>
                    </a>
                </div>                  
            </div>
        </div>
    </div>
</div> 