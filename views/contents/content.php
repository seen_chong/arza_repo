<style>
    .homeHeroWrapper button
    {
        margin: 8px;
        border: 0;
        border-radius: 3px;
        background-color: #5da87b;
        color: #f1f1f2;
        width: 200px;
        height: 40px;
        padding: 5px;
        letter-spacing: 0.1em;
        font-size: 10px;
        text-transform: uppercase;
        font-family: 'Futura';
        font-weight: 500;
    }
</style>
<div class="pageWrapper homePageWrapper">
    <div class="ourWorkHeader">
        <div class="ourWorkWrapper">
            <h5><?=$model->content->title?></h5>
            <p><?=$model->content->description?></p>
        </div>
    </div>


    <!-- Pursuit Section -->
    <?foreach ($model->sections as $section) {
        if ($section->featured_image != '' && $section->content_style == 1) {
            ?>
            <? $path = $section->featured_image == '' ? FRONT_IMG . "pursuitBg.jpg" : UPLOAD_URL . "sections/" . $section->featured_image; ?>
            <div class="pursuitContainer" style="background-image:url('<?= $path ?>');">
                <div class="pursuitLeft">
                    <div class="innerFloatLeft">

                    </div>
                    <div class="innerFloatRight boardNames">
                        <h3><strong><?= $section->title ?></strong></h3>
                        <p><?= $section->description ?></p>
                    </div>

                </div>

                <div class="pursuitRight boardNames">
                    <h6><?= $section->subtitle ?></h6>
                    <p><?= $section->sub_description ?></p>

                </div>
                <?foreach(json_decode($section->options,true)['buttons'] as $selectionSection){?>
                    <div class="donateToUs">
                        <a <?=$selectionSection['newpage'] == 1? "target=\"_blank\"" :"" ?> href="<?=$selectionSection['url']?>"><button><?=$selectionSection['title']?></button></a>
                    </div>
                <?}?>

            </div>
        <?
        } elseif ($section->featured_image != '' && $section->content_style == 2) {
            ?>
            <? $path = $section->featured_image == '' ? FRONT_IMG . "pursuitBg.jpg" : UPLOAD_URL . "sections/" . $section->featured_image; ?>
            <div class="contentWrapper" id="leaderBlock">
                <div class="contentHeader">
                    <div class="contentHeaderVisual">
                        <img src="<?=$path?>">
                            <h1><?=$section->title?></h1>
                            <h2><?=$section->description?></h2>
                        <div class="ms2Content">
                            <?foreach(json_decode($section->options,true)['buttons'] as $selectionSection){?>
                                <a <?=$selectionSection['newpage'] == 1? "target=\"_blank\"" :"" ?> href="<?=$selectionSection['url']?>"><button style="margin-left: 10%"><?=$selectionSection['title']?></button></a>
                            <?}?>
                        </div>
                        </div>

                    </div>
                </div>

            <!-- </div> -->
        <?
        } else if ($section->content_style == 0) { ?>
            <div class="contentBody">
                <div class="contentBodyTitle">
                    <h3><?= $section->title ?></h3>
                </div>

                <div class="contentBodyContent">
                    <p><?= $section->description ?></p>
                </div>
                <div class="ms2Content">
                    <?foreach(json_decode($section->options,true)['buttons'] as $selectionSection){?>
                        <a <?=$selectionSection['newpage'] == 1? "target=\"_blank\"" :"" ?> href="<?=$selectionSection['url']?>"><button style="width: 20%;margin-left: 30%;margin-bottom: 2%;"><?=$selectionSection['title']?></button></a>
                    <?}?>
                </div>


            </div>
            <?} else if ($section->content_style == 7) { ?>
            <div class="ourWorkHeader" style="padding-top: 50px">
                <div class="ourWorkWrapper">
                    <h5 class="officerTitle"><?=$section->title?></h5>
                    <p><?=$section->description?></p>
                    <?$officers = \Model\Officer::getList(['where'=>'display = 1','orderBy'=>'display_order']);
                    $arrays = array_chunk($officers,4); ?>
                    <div class="officerTable">

                    <ul>
                        <?foreach ($arrays as $officers){?>
                        
                            <?foreach ($officers as $officer){?>
                            <li><strong><?=$officer->full_name()?></strong><br><?=$officer->title?></li>
                    <?}?>
                        
                <?}?>
                    </ul>


                </div>
            </div>
        <?
        } else if ($section->content_style == 3||$section->content_style == 6) {
            $path = $section->featured_image == '' ? FRONT_IMG . "readingImg.jpg" : UPLOAD_URL . "sections/" . $section->featured_image; ?>
            <div class="middleSection" id="officerBlock" style="border-bottom:10px solid #5da87b;">
                <div class="middleSectionTwo">
                    <div class="ms2Header partnerLogo">
                        <img src="<?= $path ?>">
                        <?if($section->content_style == 3){?>
                        <div class="ms2HeaderOverlay">
                            <h2><?= $section->title ?></h2>
                        </div>
                        <?}?>
                    </div>
                    <div class="ms2Content">
                        <p><?= $section->description ?></p>
                        <?if($section->button){?>
                        <a href="<?=$section->link ?>" target="_blank">
                            <button><?= $section->button ?></button>
                        </a>
                        <?}?>
                    </div>
                </div>
                <div class="middleSectionThree">
                    <div class="ms3Header">
                        <div class="ms3Title partnerTitle">
                            <!-- <h6>-0<?=$section->display_order?></h6> -->
                            <h2><?= $section->subtitle ?></h2>
                        </div>
                        <div class="ms3Intro partnerIntro">
                            <p><?= $section->sub_description ?></p>
                        </div>
                    </div>
                    <div class="ms3Content">
                        <?php
                        if ($section->images != '') {
                            $images = $section->images;
                            $images = explode(',', $images);
                            $images = $images ? $images : [];
                            $index = array_search("", $images);
                            unset($images[$index]);
                            $index = 1;
                        }
                        foreach ($images as $image) {
                            ?>
                            <div class="partnerImg <?= 'imageHolder holder' . $index ?>">
                                <img src="<?= UPLOAD_URL ?>sections/<?= $image ?>">
                            </div>
                            <? $index++;
                        } ?>

                    </div>

                </div>
            </div>

        <?
        } else if ($section->content_style == 4) { ?>
            <div class="homeHeroWrapper" style="margin-top: auto">
                <div class="homeHero"
                     style="background-image:url('<?= UPLOAD_URL ?>sections/<?= $section->featured_image ?>');">
                    <img class="heroOverlay" src="<?= FRONT_IMG ?>heroOverlay.png">
                </div>
                <div class="heroText">
                    <h1><?= $section->title ?></h1>
                    <p><?= $section->description ?></p>
                </div>
                <?foreach(json_decode($section->options,true)['buttons'] as $selectionSection){?>
                    <div class="partner">
                        <a <?=$selectionSection['newpage'] == 1? "target=\"_blank\"" :"" ?> href="<?=$selectionSection['url']?>"><button><?=$selectionSection['title']?></button></a>
                    </div>
                <?}?>

            </div>
        <?
        }else if($section->content_style == 5){
            if ($section->images != '') {
                $images = $section->images;
                $images = explode(',', $images);
                $images = $images ? $images : [];
                $index = array_search("", $images);
                unset($images[$index]);
                $index = 1;
            }
            $path =  UPLOAD_URL . "sections/" . $images[1]; ?>
            <div class="section-03">
                <div class="sectionWrapper">

                <div class="left-block">
                    <img src="<?=$path?>">
                    <p><?=$section->description?></p>
                </div>
                <div class="right-block">
                    <div class="innerSectionLeft">
                        <div class="innerTitle">
                            <p><?=$section->sub_description?></p>
                        </div>
                        <div class="innerFloatRight">
                            <?$titles = explode(" ",$section->title);
                            $array = array_chunk($titles,ceil(count($titles) / 2));
                            $y = 0;?>
                            <h3>
                                <?foreach ($array as $title){
                                    foreach ($title as $t){?>
                                        <?=$t?>
                                    <?}?>
                                    <?=($y==0)?'<br>':'' ?>
                                    <?$y++;}?>
                        </div>
<!--                        <div class="innerFloatLeft">-->
<!--                            <h2>03</h2>-->
<!--                        </div>-->

                    </div>
                    <div class="imageBlock">
                        <img src="<?=UPLOAD_URL?>sections/<?=$section->featured_image?>">
                        <!--					<h3>Peaceful yet Powerful</h3>-->
                        <h3><?=$section->subtitle?></h3>
                        <? if($section->button){?>
                        <a href="<?=$section->link ?>">
                            <button><?= $section->button ?></button>
                        </a>
                <?}?>
                    </div>

                </div>
            </div>
</div>
     <?   }
    }?>
</div>
<script>
    $(document).ready(function(){
        $(document).on('click','.emailSubmit',function (e) {
            var email = $(".subscribeEmail").val();
            if(email && email.indexOf('@') > 0){
                $.post('/newsletter/add',{email:email},function(data){
                    if(data.status == 'success'){
                        alert(data.message);
                    } else{
                        alert(data.message);
                    }
                    $('#emailAddr').val('');
                })
            }
        })
    })
</script>