
<div class="pageWrapper homePageWrapper">
    <?if($model->user==0){?>
    <div class="getInvolvedSection" style="background-image:url('<?= FRONT_IMG ?>starsAlign.png');">
        <div class="getInvolvedWrapper">
            <h1>Do you want to get involved?</h1>
            <h3>A basic membership starts with your email.</h3>
            <?$logo=[1=>"bee-icon.png",2=>"hive-icon.png",3=>"honey-icon.png"];?>
            <div class="memberOptions">
                <?$logo_index= 1;
                foreach ($model->parentType as $type){?>
                    <div class="Member">
                        <img src="<?= FRONT_IMG . $logo[$logo_index] ?>">
                        <h2><?=$type->name?></h2>
                        <div class="spacer">
                        <p><?=$type->description?></p>
                        </div>
                        <?if(count(\Model\Member_Type::getSubList($type->id))==0){?>
                            <h6>Interested in becoming a member?</h6>
                            <a class="returnAmount">
                                <button class="memberSelection width100" data-member_type="<?=$type->id?>">Sign Up for <?=$type->name?><br><?=($type->donation_type==1? '$'.$type->amount:'')?></button>
                            </a>
                        <?}else{?>
                            <h6>Please choose one of the following:</h6>
                    <?foreach (\Model\Member_Type::getSubList($type->id) as $obj){?>
                        <a class="returnAmount">
                            <button class="memberSelection" data-member_type="<?=$obj->id?>"> <?=$obj->name?><br><?=($obj->donation_type==1? '$'.$obj->amount:'')?></button>
                        </a>
                    <?}
                        }?>
                        </div>
                <?$logo_index = $logo_index==3? 1:$logo_index+1;}?>
            </div>
            <form method="post" action="/login/register" enctype="multipart/form-data" id="involvedFrom">
                <div class="getInvolvedForm" style="display:none;">
                    <div class="involveStep1">
                        <p class="inputText">First Name <br>
                            <input type="text" name="first_name" id="memberFirstName">
                        </p>
                        <p class="inputText">Last Name <br>
                            <input type="text" name="last_name" id="memberLastName">
                        </p>
                        <p class="inputText">Congregation <br>
                            <input type="text" name="company" id="memberCompany">
                        </p>
                        <p class="inputText">Phone <br>
                            <input type="text" name="phone" id="memberPhone">
                        </p>
                        <p class="inputText">Email <br>
                            <input type="text" name="email" id="memberEmail">
                        </p>
                        <p class="inputText">ARZA Password <br>
                            <input type="password" name="password" id="memberPW">
                        </p>
                        <input hidden name="subtype"/>
                        <input hidden type="number" name="amount"/>
                        <div class="renderForm"></div>

                        <p class="submitForm"><input value="Next" id="nextStep1" class="memberSignup" readonly></p>
                    </div>

                    <div class="involveStep2" style="display:none;">
                        <p class="inputText">Address <br>
                            <input type="text" name="address">
                        </p>
                        <p class="inputText">City <br>
                            <input type="text" name="city">
                        </p>
                        <?php $states = get_states();?>
                        <p class="inputText stateSelect">State <br>
                            <select name="state" required>
                                <?foreach(get_states() as $short=>$long){?>
                                    <option value="<?=$short?>"><?=$long?></option>
                                <?}?>
                            </select>
                        </p>
                        <p class="inputText">ZIP code <br>
                            <input type="text" name="zip">
                        </p>

<!--                        <p class="inputText">Credit Card Number <br>-->
<!--                        <div class="label-input_wrapper">-->
<!--                            <input type="text" id="ccnumber" class="ccFormatMonitor" name="cc_number" maxlength="19" tabindex="4"/>-->
<!--                            <div id="creditCardIcons">-->
<!--                                <div class="media"></div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        </p>-->
<!--                        <p class="inputText monSelect">Expires Month <br>-->
<!--                            <select name="cc_expiration_month">-->
<!--                                <option class="select-option select-option-placeholder" label="Month" disabled-->
<!--                                        selected="selected" value="-1">Month-->
<!--                                </option>-->
<!--                                --><?php //foreach (get_month() as $short => $full) { ?>
<!--                                    <option class="select-option" label="--><?//= $full ?><!--"-->
<!--                                            value="--><?//= $short ?><!--">--><?//= $full ?><!--</option>-->
<!--                                --><?php //} ?>
<!--                            </select>-->
<!--                        </p>-->
<!--                        <p class="inputText yearSelect">Expires Year<br>-->
<!--                            <select name="cc_expiration_year">-->
<!--                                <option class="select-option select-option-placeholder" label="Year" disabled-->
<!--                                        selected="selected" value="-1">Year-->
<!--                                </option>-->
<!--                                --><?php //foreach (range(date('Y') - 2, date('Y') + 10) as $year) { ?>
<!--                                    <option class="select-option" label="--><?//= $year ?><!--"-->
<!--                                            value="--><?//= $year ?><!--">--><?//= $year ?><!--</option>-->
<!--                                --><?php //} ?>
<!--                            </select>-->
<!--                        </p>-->
<!---->
<!--                        <p class="inputText">CVC <br>-->
<!--                            <input type="text" name="cc_ccv">-->
<!--                        </p>-->

                        <input hidden name="redirect-url" value="<?=$emagid->uri?>">
                        <input type="submit" value="Sign Up" id="signUp" class="memberSignup">

                    </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
<?}?>


    <div class="fourCorners">
        <div class="memberCalendar calendarSlick">
            <?$event = $model->events[0];
                $Month = date("F",  $event->start_time);
                $mon = date("M",  $event->start_time);
                $day = date("d",  $event->start_time); ?>
                <div class="calendarModule">
                    <h4>Member Calendar</h4>
                    <div class="monthSelector">
                        <h3><?=$Month?></h3>
                    </div>
                    <div class="eventScroll">
                        <div class="eventDate">
                            <p><?=$mon?></p>
                            <h6><?=$day?></h6>
                        </div>
                        <div class="eventDetails">
                            <h5><?=$event->name?></h5>
                            <p><?=$event->description?></p>
                        </div>
                    </div>
                </div>
        </div>
        <div class="memberSelect">
            <div class="calendarModule">
                <div class="cornerTitle">
                    <h3>Selecting a Membership</h3>
                </div>
                <div class="eventScroll">
                    <select class="memberPreview">
                        <option value="-1" disabled selected>Select a Membership</option>
                        <?foreach ($model->parentType as $type){?>
                        <option value="<?=$type->id?>"><?=$type->name?></option>
                        <?}?>
                    </select>
                    <select id="mySelect" name="mySelect" hidden>
                    </select>
                    <p id="mem_description"></p>
                        <button id="scrollUp">Join The Community</button>

                </div>

            </div>
        </div>
        <div class="memberVolunteer">
            <div class="calendarModule">
                <div class="cornerTitle">
                    <h3>Volunteer</h3>
                </div>
                <div class="eventScroll">
                    <? $volunteers = \Model\Volunteer::getList(['where'=>'display = 1']);
                    if(count($volunteers)>0){?>
                    <div class="volunteerName">
                        <ul>
                       <? foreach ($volunteers as $volunteer){?>
                           <li><a href="<?=$volunteer->url?>" style="color: black">
                               <h3><?=$volunteer->name?></h3>
                           <p><?=$volunteer->description?></p>
                               </a></li>
                       <?}?>
                        </ul>
                        </div>
                    <?}else{?>
                        Coming Soon
                    <?}?>
                </div>
            </div>
        </div>
        <div class="memberCareer">
            <div class="calendarModule">
                <div class="cornerTitle">
                    <h3>Careers</h3>
                </div>
                <div class="eventScroll">
                    <?$positions = \Model\Position::getList(['where'=>'display = 1']);
                    if(count($positions)>0){?>
                    <div class="volunteerName">
                        <ul class="careerUl">
                            <?foreach ($positions as $position){?>
                                <li data-position_id="<?=$position->id?>">
                                        <h3><?=$position->name?></h3>
                                        <p><?=$position->description?></p>
                                </li>
                            <?}?>
                        </ul>
                    </div>
                    <div class="careerForm" hidden>
                        <form action="join/career" method="post" enctype="multipart/form-data" id="careerForm">
                       <input hidden name="insert_time" value="<?=date("Y-m-d H:i:s")?>"/>
                       <input hidden name="position_id"/>

                       <p class="inputText">
                           <input type="text" placeholder="First Name" name="first_name">
                       </p>
                       <p class="inputText">
                           <input type="text" placeholder="Last Name" name="last_name">
                       </p>
                       <p class="inputText">
                           <input type="text" placeholder="Phone Number" name="phone">
                       </p>
                       <p class="inputText">
                           <input type="text" placeholder="Email Address" name="email">
                       </p>
                       <textarea placeholder="cover letter" name="comment"
                                 style="font-family: 'Gill Sans';font-weight: normal;width: 340px;height: 100px;padding: 10px;
                                 margin-top: 8px;border: 1px solid #5da87b;border-radius: 4px;font-size: 15px;color: rgba(22, 34, 49, 0.85);"></textarea>
                       <p>Upload Resume
                           <input type="file" name="resume">
                       </p>
                   </form>
                    <a id="career">Submit</a>
                    <a id="career_back">Go Back</a>
                    </div>
                    <?}else{?>
                        No Open Positions
                    <?}?>
                </div>
            </div>
        </div>
    </div>
    <div class="inTheLoop">
        <img src="<?= FRONT_IMG ?>whiteCircles.png">
        <h2>Stay in the Loop</h2>

        <div class="loopJoin">
            <input type="text" name="mail" placeholder="Your Email Address" class="loopEmail" id="email">
            <input type="submit" value="Join Our Newsletter" class="loopSubmit">
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $(document).on('click', '.loopSubmit', function (e) {
            var email = $("#email").val();
            if (email && email.indexOf('@') > 0) {
                $.post('/newsletter/add', {email: email}, function (data) {
                    if (data.status == 'success') {
                        alert(data.message);
                    } else {
                        alert(data.message);
                    }
                    $('#email').val('');
                })
            }else{
                alert('Please input a valid email address.');
            }
        })
        $(document).on('click','.careerUl li',function(e){
            var pos_id = $(this).attr('data-position_id');
            $('.careerForm').find('input[name="position_id"]').val(pos_id);
            $('.memberCareer').find('.volunteerName').hide();
            $('.careerForm').show();
        })

        $('#career').on('click',function (e) {
            e.preventDefault();
            var submit = true;
            $('.eventScroll input').each(function () {
                if($(this).val()==''){
                    submit = false;
                    console.log(submit);
                }
            })
            if(submit){
                $('#careerForm').submit();
            }
        })

        $('#career_back').on('click',function (e) {
            $('.careerForm').hide();
            $('.memberCareer').find('.volunteerName').show();

        })

        $(".memberPreview").on('change',function (e) {
            var id = $(this).val();
            $('#mySelect').hide();
            $.post('/join/showDetails',{id:id},function(data){
                if(data.status == 'success'){
                    $('#mem_description').html(data.description);
                    if(data.showChild == true){
                        $('#mySelect').find('option').remove();
                        $('#mySelect').append('<option value="-1" disabled selected>Select</option>')
                        for(var obj in data.child){
                            $('#mySelect').append('<option value=' + data.child[obj]['id'] + '>' + data.child[obj]['name'] + '</option>');
                        }
                        $('#mySelect').show();
                    }
                }

            })
        })
        $("select[name='mySelect']").on('change',function (e) {
            var id = $(this).val();
            $.post('/join/showSubDetails',{id:id},function(data){
                if(data.status == 'success'&&data.description!=''){
                    $('#mem_description').html(data.description);
                }
            })
        })

        $("#scrollUp").on("click" ,function(){
            var scrolled= 150;

            $("body").animate({
                scrollTop:  scrolled
            });

        });

        $(".memberSelection").on('click',function(e){
            e.preventDefault();
            var id = $(this).attr("data-member_type");
            $('input[value="Next"]').show();
            $.post('/join/buildForm',{id:id},function(obj){
                if(obj.amount>=0) {
                    $('.renderForm').html(obj.joinAmount_html);
                    $('input[name="amount"]').val(obj.amount);
                    $('input[name="subtype"]').val(obj.type);
                    $('input#signUp').val('Sign up & Donate $' + obj.amount);
                    $('input#signUp').prop('disabled',true);
                }else if(obj.amount==-1){
                    $('.renderForm').html(obj.joinAmount_html);
                    $('input[name="subtype"]').val(obj.type);
                    $('input[value="Next"]').hide();
                }
            })
        })
        $(document).on('click','#freeSignup',function () {
            var isFilled = true;
            var valid = true;
            $('.involveStep1 .inputText').each(function () {
                var value = $(this).find('input').val();
                if (value.length == 0) {
                    name = $(this).text().trim();
                    invalid = $(this);
                    isFilled = false;
                    return false;
                }
            });
            var email = $("#memberEmail").val();
            var password = $("#memberPW").val();
            var subtype = $('input[name="subtype"]').val();
            if (!isFilled) {
                valid = false;
                invalid.find('input').css('border','1px solid #a72d2d');
                invalid.css('color','red');
            } else if (email.indexOf('@') < 1) {
                valid = false;
                $('input[type=email]').css('border','1px solid #a72d2d');
                $('input[type=email]').parent('.inputText').css('color','red');
            } else if (password.length < 8) {
                alert("Password too short!");
                valid = false;
            }
            if (valid) {
                $('#involvedFrom').submit();
            }

        });

        $(document).on('click','.donateAmount',function(){
            $('.donateAmount').not(this).removeClass('greenBg');
            $(this).addClass('greenBg');
            var amount = $(this).val();
            $('input[name="amount"]').val(amount);
            $('input#signUp').val('Sign up & Donate $' + amount);
        })
        $(document).on('change','.otherAmount',function(){
            var amount = $(this).val();
            $('input[name="amount"]').val(amount);
            $('input#signUp').val('Sign up & Donate $' + amount);
        })

        $('#signUp').on('click',function(event){
            event.preventDefault();
            var valid = true;
            var isFilled = true;
            $(".involveStep2 .inputText").each(function(){
                var val = $(this).find('input').val();
                if(val==''){
                    isFilled = false;
                    name = $(this).text().trim();
                    invalid = $(this);
                    return false;
                }
            })
//            var cc_num = $('input[name=cc_number]').val();
//            var cc_month = $('select[name=cc_expiration_month]').val();
//            var cc_year = $('select[name=cc_expiration_year]').val();
//            var cc_ccv = $('input[name=cc_ccv]').val();
//            if(cc_num.length==0){
//                $('input[name=cc_number]').css('border','1px solid #a72d2d');
//                $('input[name=cc_number]').parent('.inputText').css('color','red');
//                valid = false;
//            }else if(cc_month==null){
//                $('select[name=cc_expiration_month]').css('border','1px solid #a72d2d');
//                $('select[name=cc_expiration_month]').parent('.inputText').css('color','red');
//                valid = false;
//            }else if(cc_year==null){
//                $('select[name=cc_expiration_year]').css('border','1px solid #a72d2d');
//                $('select[name=cc_expiration_year]').parent('.inputText').css('color','red');
//                valid = false;
//            }else if(cc_ccv.length==0){
//                $('input[name=cc_ccv]').css('border','1px solid #a72d2d');
//                $('input[name=cc_ccv]').parent('.inputText').css('color','red');
//                valid = false;
//            }
            if(!isFilled){
                valid = false;
                invalid.find('input').css('border','1px solid #a72d2d');
                invalid.css('color','red');
            }
           if(valid){
               $('#involvedFrom').submit();
           }
        })
    })
</script>

<!-- Toggle Sign Up Form -->
<script type="text/javascript">
    $(document).ready(function () {
        $('.memberOptions > div a button').click(function (event) {
            event.preventDefault();
            $('.memberOptions > div a button.peachBg').not(this).removeClass('peachBg');
            $(this).addClass('peachBg');
            if($('.involveStep2').css('display')!='none'){
                $('.involveStep2').hide();
                $('.involveStep1 .inputText').css('color','black');
                $('.involveStep1 input').css('border','1px solid #5da87b');
                $('.involveStep1').show();
            }
            $('.getInvolvedForm').show();
            $('html,body').animate({
                scrollTop: $(".getInvolvedForm").offset().top - 55},
                'slow');
        });

        $('.getInvolvedForm input').click(function (event) {
            event.preventDefault();
            $('.getInvolvedForm input.greenBg').not(this).removeClass('greenBg');
            $(this).addClass('greenBg');
        });

        $('input#nextStep1').click(function () {
            var isFilled = true;
            var valid = true;
            $('.involveStep1 .inputText').each(function () {
                var value = $(this).find('input').val();
                if (value.length == 0) {
                    name = $(this).text().trim();
                    invalid = $(this);
                    isFilled = false;
                    return false;
                }
            });
            var email = $("#memberEmail").val();
            var password = $("#memberPW").val();
            var subtype = $('input[name="subtype"]').val();
            var $amount = $('input[name="amount"]');
            if (!isFilled) {
                valid = false;
                invalid.find('input').css('border','1px solid #a72d2d');
                invalid.css('color','red');
            } else if (email.indexOf('@') < 1) {
                valid = false;
                $('input[type=email]').css('border','1px solid #a72d2d');
                $('input[type=email]').parent('.inputText').css('color','red');
            } else if (password.length < 8) {
                alert("Password too short!");
                valid = false;
            } else if ($amount.val() == '') {
                valid = false;
                alert("Please Select an Amount");
            }
            if (valid) {
                $('.involveStep1').hide();
                $('.involveStep2').show();
                $('input#signUp').prop('disabled',false);

            }

        });
    });

</script>
<script>
    //	$("#paypalPayBtnClicker").on('click', function(e){
    //		$('#myContainer').submit();
    //	});

    window.paypalCheckoutReady = function() {
        paypal.checkout.setup('<?=PAYPAL_CLIENT_ID?>', {
            environment: 'live',
            button: 'signUp'
        });
    };
</script>
<script src="//www.paypalobjects.com/api/checkout.js" async></script>