<?$event = $model->event;
$imagePath = ($event->featured_image)? UPLOAD_URL.'events/'.$event->featured_image:FRONT_IMG.'homeFooterHero.png';
$rsvp = ($event->price==0)?'RSVP':'RSVP for $'.number_format($event->price,0);
?>
<div class="homePageWrapper" style="margin-top: 55px">
    <div class="mediaSection">
        <div class="slider-container" style="width: 40%">

            <div class="featuredEvents">
                <? $imagePath = ($model->event->featured_image) ? UPLOAD_URL . 'events/' . $model->event->featured_image : FRONT_IMG . 'homeFooterHero.png'; ?>
                <img src="<?= $imagePath ?>">
                <div class="eventOverlay">
                    <div class="eventDate">
                        <p><?= date("M", $model->event->start_time) ?></p>
                        <h6><?= date("d", $model->event->start_time) ?></h6>
                    </div>
                    <h3><?=$model->event->name ?></h3>
                    <p><?=$model->event->description ?></p>
                </div>
            </div>
        </div>

        <div class="rsvpModule"  style="width: 60%">
            <h3><?=$model->time?></h3>
            <h2><?=$event->name?></h2>
            <p id="description"><?=$event->description?></p>
            <form id="eventRsvp" action="/home/rsvp" method="post">
                <div class="evCustFields">
                    <input hidden name="event_id" value="<?=$event->id?>">
                    <input hidden name="user_id" value="<?=($model->user)? $model->user->id:0?>">
                    <p class="inputText">First name <br>
                        <input type="text" name="first_name">
                    </p>
                    <p class="inputText">Last name <br>
                        <input type="text" name="last_name">
                    </p>
                    <p class="inputText">Address <br>
                        <input type="text" name="address">
                    </p>
                    <p class="inputText">City <br>
                        <input type="text" name="city">
                    </p>
                    <p class="inputText stateSelect">State <br>
                        <select name="state" required>
                            <?foreach(get_states() as $short=>$long){?>
                                <option value="<?=$short?>"><?=$long?></option>
                            <?}?>
                        </select>
                    </p>
                    <p class="inputText">ZIP code <br>
                        <input type="text" name="zip">
                    </p>
                    <p class="inputText">Email <br>
                        <input type="email" name="email" id="rsvpEmail">
                    </p>
                    <p class="inputText">Phone <br>
                        <input type="text" name="phone">
                    </p>
                    <p class="inputText">Congregation Name <br>
                        <input type="text" name="congregation_name" id="rsvpEmail">
                    </p>
                    <p class="inputText">Position <br>
                        <input type="text" name="position">
                    </p>
                    <p class="inputText">Congregation City <br>
                        <input type="text" name="congregation_city">
                    </p>
                    <p class="inputText stateSelect">Congregation State <br>
                        <select name="congregation_state" required>
                            <?foreach(get_states() as $short=>$long){?>
                                <option value="<?=$short?>"><?=$long?></option>
                            <?}?>
                        </select>
                    </p>

                    <p class="submitForm">
                        <?if($event->price ==0){?>
                            <input type="submit" value="<?=$rsvp?>" id="nextStep2" class="memberSignup">
                        <?}else{?>
                            <input type="submit" value="<?=$rsvp?>" id="paypal" class="memberSignup">
                        <?}?>
                    </p>
                </div>
            </form>
        </div>
    </div>


</div>
<script type="text/javascript">
    $(function(){
        $("#eventRsvp").validate({
            rules: {
                first_name:{
                    required: true,
                },
                last_name:{
                    required: true,
                },
                address:{
                    required: true,
                },
                city:{
                    required: true,
                },
                state:{
                    required: true,
                },
                zip:{
                    required: true
                },
                email:{
                    required: true,
                    email:true
                },
                phone:{
                    required: true,
                    number: true
                },
                congregation_name:{
                    required: true,
                },
                position:{
                    required: true,
                },
                congregation_city:{
                    required: true,
                },
                congregation_state:{
                    required: true,
                }

            },
            messages:{
                first_name:{
                    required: "Please enter your first name",
                },
                last_name:{
                    required: "Please enter your last name",
                },
                address:{
                    required: "Please enter your address",
                },
                city:{
                    required: "Please enter your city",
                },
                state:{
                    required: "Please select your state",
                },
                zip:{
                    required: "Please enter your zip",
                },
                last_name:{
                    required: "Please enter your last_name",
                },
                email:{
                    required: "Please enter your email",
                    email:"Invalid Email Address"
                },
                phone:{
                    required: "Please enter your phone number",
                    number:"Invalid phone number"
                },
                congregation_name:{
                    required: "Please enter your congregation name",
                },
                position:{
                    required: "Please enter your position",
                },
                congregation_city:{
                    required: "Please enter your congregation city",
                },
                congregation_state:{
                    required: "Please select your congregation state",
                }

            },
            errorClass: "error-float"
        });

        //		$('#login input[name="email"]').focus();
    });
 </script>
<script>
    //	$("#paypalPayBtnClicker").on('click', function(e){
    //		$('#myContainer').submit();
    //	});

    window.paypalCheckoutReady = function() {
        paypal.checkout.setup('<?=PAYPAL_CLIENT_ID?>', {
            environment: 'live',
            button: 'paypal',
            click: function(){
                paypal.checkout.initXO();
                var action = $.post('/set-express-checkout');
                action.done (function(data){
                    paypal.checkout.startFlow(data.token);
                });
                action.fail(function(){
                    paypal.checkout.closeFlow();
                })
            },
            condition: function(){
                var data = {
                    first_name:$('[name=first_name]').val(),
                    last_name:$('[name=last_name]').val(),
                    address:$('[name=address]').val(),
                    city:$('[name=city]').val(),
                    state:$('[name=state]:enabled').val(),
                    zip:$('[name=zip]').val(),
                    email:$('[name=email]').val(),
                    phone:$('[name=phone]').val(),
                    congregation_name:$('[name=congregation_name]').val(),
                    position:$('[name=position]').val(),
                    congregation_city:$('[name=congregation_city]').val(),
                    congregation_state:$('[name=congregation_state]:enabled').val()
                };
                var submit = true;
                $.each(data,function(key,value){
                    if(!value){
                        submit = false;
                        $('[name=' + key +']:enabled').css('border', '1px solid red');
                    } else {
                        $('[name=' + key +']:enabled').css('border', '1px solid #d6d6d6');
                    }
                });
                return submit;
            }
        });
    };
</script>
<script src="//www.paypalobjects.com/api/checkout.js" async></script>