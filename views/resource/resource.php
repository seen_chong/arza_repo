<? $resource = $model->resource?>
<div class="contentContainer blogContainer">
    <div class="contentWrapper blogWrapper">
        <div class="blogSlider">

            <div class="blogHeader">
                <div class="blogHeaderVisual">
                    <img src="<?=UPLOAD_URL.'ebooks/'. $resource->featured_image?>">
                </div>
                <div class="blogHeaderText">
                    <h1><?=$resource->name?></h1>
                    <img src="<?=FRONT_IMG?>darkDash.png">
                    <h2><?if($resource->author){?>
                            Author : <?=$resource->author?><br>
                        <?}?>
                        <?if($resource->publish_date){?>
                            Publish Date : <?=$resource->publish_date?><br><br>
                        <?}?>
                        <?=$resource->description?></h2>
                    <a download="<?=$resource->name?>" href="<?=UPLOAD_URL. 'ebooks/'. $resource->file_name?>"><button>Download</button></a>
                </div>
            </div>

        </div>
</div>