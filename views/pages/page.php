<div class="contentContainer">
    <div class="contentWrapper">
        <div class="contentHeader templateHeader">
            <img class="eqCircleLeft" src="<?=FRONT_IMG?>equalityCircles.png">
            <div class="contentHeaderText">
                <h1><?=$model->page->title?></h1>
                <!-- <img src="<//?=FRONT_IMG?>darkDash.png"> -->
                <h2><?=$model->page->subtitle?></h2>
            </div>
<!--             <div class="contentHeaderVisual templateHeaderVisual">
                <//?$path = $model->page->featured_image==''? FRONT_IMG.'competition.png':UPLOAD_URL.'pages/'.$model->page->featured_image?>
                <img src="<//?=$path?>">
            </div> -->
        </div>

        <div class="contentBody">
<!--             <div class="contentBodyTitle">
                <h3><//?=$model->page->sub_description?></h3>
            </div> -->

            <div class="contentBodyContent templateBodycontent">
                <p><?=$model->page->description?></p>
            </div>
            
            
        </div>
        
    </div>

</div>