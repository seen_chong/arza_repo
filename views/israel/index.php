<div class="contentContainer blogContainer">
    <div class="contentWrapper blogWrapper">
        <div class="blogSlider">
            <?foreach ($model->featured_posts as $post){?>
            <div class="blogHeader">
                <div class="blogHeaderVisual">
                    <img src="<?=UPLOAD_URL.'israels/'.$post->featured_image?>">
                </div>
                <div class="blogHeaderText">
                    <h1><?=$post->title?></h1>
                    <img src="<?=FRONT_IMG?>darkDash.png">
                    <h2><?=$post->preview_description?></h2>
                    <a href="<?=SITE_URL. 'israel/post/'. $post->slug?>"><button>Read More</button></a>
                </div>
            </div>
            <?}?>
        </div>

        <div class="blogBody">
            <div class="blogBodyWrapper">
            <div class="blogBodyTitle">
                <h3>Israel in the Parasha</h3>
            </div>
                <?foreach($model->posts as $post){?>
                <div class="blogBlock">
                    <a href="<?=SITE_URL. 'israel/post/'. $post->slug?>">
                        <img src="<?=UPLOAD_URL.'israels/'.$post->featured_image?>">
                    </a>
                    <div class="blogSpacer">
                        <h3><?=$post->title?></h3>
                        <span class="blogDate"><?=date("M d, Y", $post->date_modified)?></span>
                        <div class="blogContentSpacer">
                            <p><?=$post->preview_description?></p>
                        </div>
                        <a href="<?=SITE_URL. 'israel/post/'. $post->slug?>"><button>Read More</button></a>
                    </div>
                    <div class="blogSocial">
                        <div class="blogSocialShare">
                            <a class="facebookIcon">
                                <icon class="icon phone_icon" style="background-image:url('<?=FRONT_IMG?>facebookLogo.png')"></icon>
                            </a>
                            <a class="instaIcon">
                                <icon class="icon phone_icon" style="background-image:url('<?=FRONT_IMG?>instagramLogo.png')"></icon>
                            </a>
                            <a class="twitterIcon">
                                <icon class="icon phone_icon" style="background-image:url('<?=FRONT_IMG?>twitterLogo.png')"></icon>
                            </a>
                            <a class="youtubeIcon">
                                <icon class="icon phone_icon" style="background-image:url('<?=FRONT_IMG?>youtubeLogo.png')"></icon>
                            </a>
                        </div>
                    </div>
                </div>
                <?}?>
            </div>

            <div class="blogSidebar">
                <div class="recentBar">
                    <h4>Recent Posts</h4>
                    <?foreach($model->recent_posts as $post){?>
                    <div class="recentBarPost">
                        <div class="barPostImage">
                            <img src="<?=UPLOAD_URL.'israels/'.$post->featured_image?>">
                        </div>
                        
                        <div class="barPostText">
                            <h5><a href="<?=SITE_URL. 'israel/post/'. $post->slug?>"><?=$post->title?></a></h5>
                            <span class="recentDate"><?=date("M d, Y", $post->date_modified)?></span>
                        </div>
                    </div>
                    <?}?>
                </div>
                <div class="subscribeBar">
                    <h4>Subscribe and Follow</h4>
                        <div class="subscribeShare">
                            <a class="facebookIcon">
                                <icon class="icon phone_icon" style="background-image:url('<?=FRONT_IMG?>facebookLogo.png')"></icon>
                            </a>
                            <a class="instaIcon">
                                <icon class="icon phone_icon" style="background-image:url('<?=FRONT_IMG?>instagramLogo.png')"></icon>
                            </a>
                            <a class="twitterIcon">
                                <icon class="icon phone_icon" style="background-image:url('<?=FRONT_IMG?>twitterLogo.png')"></icon>
                            </a>
                            <a class="youtubeIcon">
                                <icon class="icon phone_icon" style="background-image:url('<?=FRONT_IMG?>youtubeLogo.png')"></icon>
                            </a>
                        </div>
                </div>
        </div>
        
    </div>

</div>

<script type="text/javascript">
$(document).ready(function(){
  $('.blogSlider').slick({
    autoplay: true,
    autoplaySpeed: 5000,
    arrows: false,
    dots: true,
     customPaging: function(slider, i) { 
    // this example would render "tabs" with titles
    return '<button class="tab">' + $(slider.$slides[i]).find('.slide-title').text() + '</button>';
  },
  });
});
</script>