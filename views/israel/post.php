<div class="contentContainer blogContainer">
    <div class="contentWrapper blogWrapper">
            <div class="blogTitle">
            	<h1><?=$model->post->title?></h1>
                <a class="backbtn" href="/israel"><button>Back to Weekly Israel</button></a>
            </div>
            <div class="blogHero" style="background-image:url('<?=UPLOAD_URL.'israels/'.$model->post->featured_image?>')">
                
            </div>

        <div class="blogBody">
            <div class="blogSidebar">
            	<div class="recentBar">
                    <h4>About The Author</h4>
                    <div class="recentBarPost aboutAuthor">
                        <div class="barPostImage aboutAuthorImage">
                            <?if(($model->post->author_image)!=''){?>
                            <img src="<?=UPLOAD_URL.'israels/'.$model->post->author_image?>">
                            <?}?>
                        </div>
                        
                        <div class="barPostText aboutAuthorText">
                            <h5><?=$model->post->author?></h5>
                            <p><?=$model->post->author_description?></p>
                        </div>
                    </div>
                </div>
                <div class="subscribeBar">
                    <h4>Subscribe and Follow</h4>
                        <div class="subscribeShare">
                            <a class="facebookIcon">
                                <icon class="icon phone_icon" style="background-image:url('<?=FRONT_IMG?>facebookLogo.png')"></icon>
                            </a>
                            <a class="instaIcon">
                                <icon class="icon phone_icon" style="background-image:url('<?=FRONT_IMG?>instagramLogo.png')"></icon>
                            </a>
                            <a class="twitterIcon">
                                <icon class="icon phone_icon" style="background-image:url('<?=FRONT_IMG?>twitterLogo.png')"></icon>
                            </a>
                            <a class="youtubeIcon">
                                <icon class="icon phone_icon" style="background-image:url('<?=FRONT_IMG?>youtubeLogo.png')"></icon>
                            </a>
                        </div>
                </div>
            </div>

            <div class="innerBlogContent">
            	<div class="innerBlogIntro"> 
            		<h4><?=$model->post->introduction?></h4>
            	</div>
            	<div class="innerBlogText">
            		<p><?=$model->post->description?></p>
            	</div>
            </div>
        
    </div>

</div>