<?php
/*
 *
 * This script should run every hour every day
 * Used to remind providers of upcoming session & complete sessions
 *
 * */
include(__DIR__.'/../index.php');

//send email 24 hours before session
function twentyFourHourNotification(){
    $carbon = \Carbon\Carbon::now()->minute(0)->second(0);
//    $carbon = \Carbon\Carbon::create(2016,04,01,9); //swap for testing '2016-01-20 09:00:00'
    $carbon->addHours(24);
    $sessionHistory = \Model\SessionHistory::getList(['where'=>"date = '{$carbon}' and status = 1"]);
    if($sessionHistory){
        foreach($sessionHistory as $sesh){
            $sesh->sendSessionWarningEmail(24);
        }
    }
}

//send email 3 hours before session
function threeHourNotification(){
    $carbon = \Carbon\Carbon::now()->minute(0)->second(0);
//    $carbon = \Carbon\Carbon::create(2016,01,20,6); //swap for testing '2016-01-20 09:00:00'
    $carbon->addHours(3);
    $sessionHistory = \Model\SessionHistory::getList(['where'=>"date = '{$carbon}' and status = 1"]);
    if($sessionHistory){
        foreach($sessionHistory as $sesh){
            $sesh->sendSessionWarningEmail(3);
        }
    }
}

//send email 1 hour after session
function oneHourNotification(){
    $carbon = \Carbon\Carbon::now()->minute(0)->second(0);
//    $carbon = \Carbon\Carbon::create(2016,01,20,10); //swap for testing '2016-01-20 09:00:00'
    $carbon->subHour();
    $sessionHistory = \Model\SessionHistory::getList(['where'=>"date = '{$carbon}' and status = 1"]);
    if($sessionHistory){
        foreach($sessionHistory as $sesh){
            $sesh->sendAfterSessionWarningEmail();
        }
    }
}

twentyFourHourNotification();
threeHourNotification();
oneHourNotification();
