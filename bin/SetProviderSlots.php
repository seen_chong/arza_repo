<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 1/5/16
 * Time: 10:59 AM
 */

include(__DIR__.'/../index.php');

use Model\Provider;
use Model\Calendar;
use Carbon\Carbon;

$now = Carbon::now();

$providerLists = Provider::getList();
// for next 30 days, we update calendar to enable full day calendar
for($i = 1; $i <= 30; $i++ ){
    $nextDay = $now->addDay()->toDateString();
    foreach($providerLists as $provider){
        if($calendar = Calendar::getItem(null, ['where' => ['provider_id' => $provider->id, 'date_of_session' => $nextDay]])){
            $calendar->full_day_av = true;
            $calendar->save();

            echo "UPDATE: Provider ID: {$provider->id} for Date: {$nextDay} set full day available \n";
        } else {
            $calendar = new Calendar();
            $calendar->provider_id = $provider->id;
            $calendar->date_of_session = $nextDay;
            $calendar->full_day_av = true;
            $calendar->save();

            echo "CREATE: Provider ID: {$provider->id} for Date: {$nextDay} set full day available \n";
        }
    }
}

