<?php

include(__DIR__.'/../index.php');

use Model\Provider_Services;
use Model\Service;

$providerServices = Provider_Services::getList(['where'=>"active = 1"]);

foreach($providerServices as $providerService){                 //loop every provider_service
    $service = $providerService->getService();                  //get service
    if($service->sub_service != 0){                             //if service is a sub-service
//        echo 'Sub-service id:'. $service->id."\n";
        $mainService = getMainService($service->sub_service);   //find main \Service
        $validatePS = getProviderMainService($providerService->provider_id , $mainService->id);
//        echo 'Provider_Service main service id:'.$validatePS->id."\n";
        if(isset($validatePS) && $validatePS->active != '1'){     //if parent provider_service exists and is inactive
            $validatePS->active = 1;
            $validatePS->save();
            echo "Update Provider_Service ID: $validatePS->id for Child Service id: $providerService->id\n";
        } else if(!isset($validatePS)){                         //else if parent provider_service does not exist
            $newPS = new Provider_Services();
            $newPS->provider_id = $providerService->provider_id;
            $newPS->service_id = $mainService->id;
            $newPS->save();
            echo 'Insert main service for provider_service id: '.$providerService->id."\n";
        } else {
            echo "Provider_Service ID: $providerService->id is good\n";
        }
    }
}

function getMainService($serviceId){
    return Service::getItem($serviceId);
}

function getProviderMainService($providerId, $serviceId){
    return Provider_Services::getItem(null,['where'=>"provider_id = $providerId and service_id = $serviceId"]);
}