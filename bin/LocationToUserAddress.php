<?php

//Run this function once to swap Session_History 'location' column from string to User_Address id ref

include(__DIR__.'/../index.php');

$sessionHistory = \Model\SessionHistory::getList();
$userAddr = \Model\User_Address::getList();

foreach($sessionHistory as $sesh){
    $add = true;
    foreach($userAddr as $ua){
        $temp = [];
        array_push($temp,$ua->street,$ua->apt,$ua->zip);
        $implodedUserAddr = implode(' ', $temp);
        if($implodedUserAddr == $sesh->location && $ua->user_id == $sesh->user_id){
            $sesh->location = $ua->id;
            $add = false;
            $sesh->save();
        }
    }
    if($add){
        $newAddr = new \Model\User_Address();
        $newAddr->name = 'Dev change';
        $newAddr->street = $sesh->location;
        $newAddr->user_id = $sesh->user_id;
        $newAddr->save();

        $sesh->location = $newAddr->id;
        $sesh->save();
    }
}