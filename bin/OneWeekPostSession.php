<?php
//run everyday
include(__DIR__.'/../index.php');

function weekAfterSession(){
    $carbon = \Carbon\Carbon::now();
    $carbon->subWeek(1)->toDateString();

    $sessions = \Model\SessionHistory::getList(['where'=>"date::text like '{$carbon}%' and status = 4"]);
    foreach($sessions as $session){
        $provider = \Model\Provider::getItem($session->provider_id);
        $email = new \Email\MailMaster();
        $mergeFields = [
            'PROVIDER_NAME' => $provider->full_name()
        ];
        $email->setTo(['email' => $provider->email, 'name' => $provider->full_name(),  'type' => 'to'])->setTemplate('post-session-communication')->setMergeTags($mergeFields)->send();
    }
}

weekAfterSession();