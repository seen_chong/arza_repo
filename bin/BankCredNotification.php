<?php
//run on the 15th every month
include(__DIR__.'/../index.php');

function bankCredReminder(){
    $providers = \Model\Provider::getList(['where'=>"account_number is null or account_number = '' or routing_number is null or routing_number = ''"]);
    foreach($providers as $provider){
        $email = new Email\MailMaster();
        $mergeFields = [
            'PROVIDER_NAME'=>$provider->full_name()
        ];
        $email->setTo(['email' => $provider->email, 'name' => $provider->full_name(),  'type' => 'to'])->setTemplate('bank-credential-reminder')->setMergeTags($mergeFields)->send();
    }
}

bankCredReminder();