<?php
//run biweekly
include(__DIR__.'/../index.php');

function userCreditWarning(){
    $users = \Model\User::getList(['where'=>"credit > 0"]);
    foreach($users as $user){
        $email = new \Email\MailMaster();
        $mergeFields = [
            'USER_NAME' => $user->full_name()
        ];
        $email->setTo(['email' => $user->email, 'name' => $user->full_name(),  'type' => 'to'])->setTemplate('remind-users-to-use-the-purchased-credits')->setMergeTags($mergeFields)->send();
    }
}

function corporateCreditWarning(){
    $users = \Model\User::getList(['where'=>"corporate_credit > 0 and (credit = 0 or credit is null)"]);
    foreach($users as $user){
        $email = new \Email\MailMaster();
        $mergeFields = [
            'USER_NAME' => $user->full_name()
        ];
        $email->setTo(['email' => $user->email, 'name' => $user->full_name(),  'type' => 'to'])->setTemplate('remind-corporate-users-to-use-the-purchased-credits')->setMergeTags($mergeFields)->send();
    }
}

userCreditWarning();
corporateCreditWarning();