<?php
//run everyday
include(__DIR__.'/../index.php');

function inactiveProvider(){
    $carbon = \Carbon\Carbon::now()->minute(0)->second(0);
    $carbon->subMonth();
    $providers = \Model\Provider::getList(['where'=>"last_login::text like '{$carbon->toDateString()}%'"]);
    foreach($providers as $provider){
        $email = new \Email\MailMaster();
        $mergeFields = [
            'PROVIDER_NAME'=>$provider->full_name()
        ];
        $email->setTo(['email' => $provider->email, 'name' => $provider->full_name(), 'type' => 'to'])->setTemplate('inactive-provider-notification')->setMergeTags($mergeFields)->send();
    }
}

function inactiveUser(){
    $carbon = \Carbon\Carbon::now()->minute(0)->second(0);
    $carbon->subMonth();
    $users = \Model\User::getList(['where'=>"last_login::text like '{$carbon->toDateString()}%'"]);
    foreach($users as $user){
        $email = new \Email\MailMaster();
        $mergeFields = [
            'USER_NAME'=>$user->full_name()
        ];
        $email->setTo(['email' => $user->email, 'name' => $user->full_name(), 'type' => 'to'])->setTemplate('inactive-user-notification')->setMergeTags($mergeFields)->send();
    }
}

inactiveProvider();
inactiveUser();