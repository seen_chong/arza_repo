-- March 9th
ALTER TABLE provider ADD COLUMN account_number CHARACTER VARYING;

-- March 11th
ALTER TABLE public.order add COLUMN order_message CHARACTER VARYING;

-- March 30th
ALTER TABLE public.appointment ADD COLUMN send_email INTEGER DEFAULT 0;

-- April 1st
CREATE TABLE status_log (id serial PRIMARY KEY, active SMALLINT DEFAULT 1,insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),provider_id INTEGER NOT NULL, session_id INTEGER,start_status INTEGER ,end_status INTEGER, start_time TIMESTAMP, end_time TIMESTAMP, summary INTEGER);

ALTER TABLE public.provider ADD COLUMN avg_response_time INTEGER;

ALTER TABLE public.session_history ADD COLUMN status_updated_at TIMESTAMP WITHOUT TIME ZONE DEFAULT now();

-- TODO st-dev Add when merging pull request
alter table provider add column last_login timestamp without time zone;
alter table public.user add column last_login timestamp without time zone;

-- May 19th
ALTER TABLE calendar ADD COLUMN time INTEGER NOT NULL;
ALTER TABLE calendar ADD COLUMN status INTEGER DEFAULT 0;
ALTER TABLE calendar ADD COLUMN date CHARACTER VARYING NOT NULL;
ALTER TABLE calendar DROP COLUMN time_start;
ALTER TABLE calendar DROP COLUMN time_end;
ALTER TABLE calendar DROP COLUMN full_day_av;
ALTER TABLE calendar DROP COLUMN date_of_session;

alter table provider add column equipment CHARACTER VARYING;

-- Jun 3

alter table provider add column expect CHARACTER VARYING;

-- Jun 13
ALTER TABLE public.user add column cellphone CHARACTER VARYING;
ALTER TABLE public.provider add column cellphone CHARACTER VARYING;

-- Jun 15
ALTER TABLE public.provider add column payout_rate FLOAT DEFAULT 100;

-- Jun 17
ALTER TABLE public.user DROP column cellphone;
ALTER TABLE public.provider DROP column cellphone;

ALTER TABLE public.user add column phone CHARACTER VARYING;

-- July 13th
create table testimonial(
id serial PRIMARY KEY,
active SMALLINT DEFAULT 1,
insert_time TIMESTAMP WITHOUT TIME ZONE not null DEFAULT now(),
name CHARACTER VARYING,
content CHARACTER VARYING,
state CHARACTER VARYING,
status SMALLINT
);

create table testimonial_services(
id serial PRIMARY KEY,
active SMALLINT DEFAULT 1,
insert_time TIMESTAMP WITHOUT TIME ZONE not null DEFAULT now(),
testimonial_id INTEGER,
service_id INTEGER
);

create table external_brand(
id serial PRIMARY KEY,
active SMALLINT DEFAULT 1,
insert_time TIMESTAMP WITHOUT TIME ZONE not null DEFAULT now(),
name CHARACTER VARYING,
featured_image CHARACTER VARYING
);

-- July 14th
create table popular_service(
id serial PRIMARY KEY,
active SMALLINT DEFAULT 1,
insert_time TIMESTAMP WITHOUT TIME ZONE not null DEFAULT now(),
service_id INTEGER,
title CHARACTER VARYING,
description CHARACTER VARYING,
featured_image CHARACTER VARYING,
url CHARACTER VARYING
);

-- July 15th
create table faq(
id serial PRIMARY KEY,
active SMALLINT DEFAULT 1,
insert_time TIMESTAMP WITHOUT TIME ZONE not null DEFAULT now(),
title CHARACTER VARYING,
description CHARACTER VARYING,
display_order SMALLINT
);

alter table popular_service add column display_state SMALLINT;

-- aug 18
CREATE TABLE home (
  	id SERIAL PRIMARY KEY,
  	active SMALLINT DEFAULT 1,
  	insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
  	section_name VARCHAR,
	  title VARCHAR,
	  subtitle VARCHAR,
	  description VARCHAR,
	  sub_description VARCHAR,
	  featured_image VARCHAR,
	  image VARCHAR,
	  link VARCHAR ,
	  display SMALLINT default 1

);

INSERT INTO home (section_name)
values
('Main Banner'),
('Section 1'),
('Section 2'),
('Section 3'),
('Section 4');


CREATE TABLE work (
  	id SERIAL PRIMARY KEY,
  	active SMALLINT DEFAULT 1,
  	insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
  	section_name VARCHAR,
	  title VARCHAR,
	  subtitle VARCHAR,
	  description VARCHAR,
	  featured_image VARCHAR,
	  image VARCHAR,
	  link VARCHAR
);
INSERT INTO work (section_name)
values
('TOP'),
('main banner'),
('block 1'),
('block 2'),
('block 3'),
('section 1'),
('section 2'),
('section 3_A'),
('section 3_B'),
('section 3_C');

CREATE TABLE mission (
  	id SERIAL PRIMARY KEY,
  	active SMALLINT DEFAULT 1,
  	insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
  	section_name VARCHAR,
	  title VARCHAR,
	  subtitle VARCHAR,
	  description VARCHAR,
	  sub_description VARCHAR,
	  featured_image VARCHAR,
	  video_link VARCHAR,
	  link VARCHAR
);
INSERT INTO mission (section_name)
values
('top'),
('main banner'),
('Video'),
('block 1'),
('block 2'),
('block 3');

CREATE TABLE history (
  	id SERIAL PRIMARY KEY,
  	active SMALLINT DEFAULT 1,
  	insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
  	year VARCHAR,
  	time VARCHAR,
	  title VARCHAR,
	  description VARCHAR
);

--Aug 22
ALTER TABLE history drop COLUMN year;

create table newsletter(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
email character varying
);

update mission set active = 0 where id > 6;
--Aug 24

ALTER TABLE public.user add column member_type INTEGER ;
ALTER TABLE public.user add column subtype INTEGER ;


create table event(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
name character varying,
start_time character varying,
end_time character varying,
description character varying,
address character varying,
address2 character varying,
city character varying,
state character varying,
country character varying,
capacity character varying,
price numeric(10,2),
contact_name character varying,
contact_number character varying,
featured_image character varying
);

CREATE TABLE donation (
    id serial primary key,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    first_name character varying,
    last_name character varying,
    address character varying,
    city character varying,
    state character varying,
    zip character varying,
    cc_number character varying,
    cc_expiration_month character varying,
    cc_expiration_year character varying,
    cc_ccv character varying,
    email character varying,
    phone character varying,
    amount numeric(10,2),
    user_id Integer DEFAULT 0,
    type CHARACTER VARYING
);


ALTER TABLE public.user add column donation_amount numeric(10,2) ;

CREATE TABLE rsvp (
    id serial primary key,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    first_name character varying,
    last_name character varying,
    address character varying,
    city character varying,
    state character varying,
    zip character varying,
    cc_number character varying,
    cc_expiration_month character varying,
    cc_expiration_year character varying,
    cc_ccv character varying,
    email character varying,
    phone character varying,
    event_id Integer,
    user_id Integer DEFAULT 0
);
CREATE TABLE news (
    id serial primary key,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    title character varying,
    time character varying,
    url character varying,
    author character varying,
    featured_image character varying
);

CREATE TABLE community (
    id serial primary key,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    address character varying,
    city character varying,
    state character varying,
    country character varying,
    zip character varying,
    description character varying,
    lng character varying,
    lat character varying
);
alter table event add column zip CHARACTER VARYING ;

update  home set active = 0 where id >5;
update  work set active = 0 where id >10;

alter table blog add column category CHARACTER VARYING ;
alter table blog add column author_description CHARACTER VARYING ;
alter table blog add column author_image CHARACTER VARYING ;
alter table blog add column introduction CHARACTER VARYING ;
alter table blog alter column author type CHARACTER VARYING;



CREATE TABLE blog_category (
    id serial primary key,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    slug character varying,
    meta_title CHARACTER VARYING,
    meta_keywords CHARACTER VARYING,
    meta_description CHARACTER VARYING,
    featured_image CHARACTER VARYING,
    sub_cat INTEGER
);

--Sep 1

CREATE TABLE member_type(
    id serial primary key,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    description CHARACTER VARYING,
    parent_type INTEGER,
    donation_type INTEGER,
    amount CHARACTER VARYING,
    minimum CHARACTER VARYING,
    amount_interval CHARACTER VARYING

);
--Sep 2

CREATE TABLE content(
    id serial primary key,
    active smallint DEFAULT 1,
    insert_time timestamp without time zone DEFAULT now(),
    name character varying,
    parent_section CHARACTER VARYING,
    title CHARACTER VARYING ,
    description CHARACTER VARYING,
    slug character varying,
    meta_title CHARACTER VARYING,
    meta_keywords CHARACTER VARYING,
    meta_description CHARACTER VARYING,
    display SMALLINT DEFAULT 1

);

CREATE TABLE section (
  	id SERIAL PRIMARY KEY,
  	active SMALLINT DEFAULT 1,
  	insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
  	content_name INTEGER,
  	name VARCHAR,
	  title VARCHAR,
	  description VARCHAR,
	  subtitle VARCHAR,
	  sub_description VARCHAR,
	  featured_image VARCHAR,
	  display SMALLINT DEFAULT 1,
	  display_order INTEGER
);


alter table section add column content_style VARCHAR ;

--Sep 14th

create table latest(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone not null default now(),
name character varying,
time character varying,
description character varying ,
display_order INTEGER,
url CHARACTER VARYING
);

--Sep -15th
create table career(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone default now(),
first_name character varying,
last_name character varying,
comment character varying,
resume character varying ,
email character varying,
phone CHARACTER VARYING
);

--Sep 20th

alter table community add column display_order INTEGER ;

create table position(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone default now(),
name character varying,
description character varying,
amount character varying,
display SMALLINT
);

alter table career add column position_id INTEGER ;
ALTER TABLE career RENAME TO applicant;

-- Sep 21st
create table volunteer(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone default now(),
name character varying,
description character varying,
url character varying,
display SMALLINT
);

CREATE TABLE israel
(
  id serial NOT NULL,
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  title character varying,
  slug character varying,
  description character varying,
  featured_image character varying,
  meta_title character varying,
  meta_keywords character varying,
  meta_description character varying,
  date_modified character varying,
  author character varying,
  status integer,
  featured integer,
  preview_description character varying,
  author_description character varying,
  author_image character varying,
  introduction character varying,
  content_images CHARACTER VARYING
);
--Sep 22nd
alter table blog add column blog_images CHARACTER VARYING ;

alter table home add column display_order INTEGER ;


alter table section add column images CHARACTER VARYING ;
alter table section add column notes CHARACTER VARYING ;
alter table section add column link CHARACTER VARYING ;

INSERT INTO home (section_name)
VALUES('Splash');

CREATE TABLE work_display
(
  id serial NOT NULL,
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  section_name character varying,
  display_order CHARACTER VARYING
);

INSERT INTO work_display (section_name)
values
('Reform Movement'),
('map'),
('3 text blocks'),
('Israel'),
('Rabbinic Council');


CREATE TABLE mission_display
(
  id serial NOT NULL,
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  section_name character varying,
  display_order CHARACTER VARYING
);

INSERT INTO mission_display (section_name)
values
('main banner'),
('history'),
('3 text blocks'),
('video');

ALTER TABLE "user" ADD COLUMN token character varying;

CREATE TABLE ebook(
    id SERIAL PRIMARY KEY,
  	active SMALLINT DEFAULT 1,
  	insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
  	name VARCHAR ,
  	member_type VARCHAR ,
  	file_name VARCHAR,
  	author VARCHAR ,
  	description VARCHAR ,
  	publish_date VARCHAR,
  	featured_image VARCHAR
);

ALTER TABLE work_display ADD COLUMN display SMALLINT default 1;
ALTER TABLE mission_display ADD COLUMN display SMALLINT default 1;

--Sep, 26th

ALTER TABLE page ADD COLUMN subtitle CHARACTER VARYING ;
ALTER TABLE page ADD COLUMN sub_description CHARACTER VARYING ;
alter table section add column button CHARACTER VARYING ;
alter table section add column image_description CHARACTER VARYING ;
INSERT INTO mission_display (section_name)
values
('map'),
('our partners');

update work_display set active = 0 where section_name = 'map';

INSERT INTO mission (section_name)
values('our partners');

CREATE TABLE officer
(
  id serial NOT NULL,
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  first_name character varying,
  last_name character varying,
  title character varying,
  display_order CHARACTER VARYING,
  display SMALLINT default 1
);
create table contact(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone default now(),
name character varying,
email character varying,
message character varying
);

--Sep 28th

create table donate(
id serial primary key,
active smallint default 1,
insert_time timestamp without time zone default now(),
name character varying,
title character varying,
subtitle character varying,
image character varying
);
INSERT INTO donate (name)
values('donate page');


ALTER TABLE donation ADD COLUMN viewed BOOLEAN;
ALTER TABLE donation ADD COLUMN status CHARACTER VARYING;
ALTER TABLE donation ADD COLUMN paypal_id CHARACTER VARYING;
ALTER TABLE donation ADD COLUMN ref_num CHARACTER VARYING;
ALTER TABLE home ADD COLUMN button CHARACTER VARYING;
ALTER TABLE work ADD COLUMN button CHARACTER VARYING;
ALTER TABLE mission ADD COLUMN button CHARACTER VARYING;
ALTER TABLE rsvp ADD COLUMN ref_num CHARACTER VARYING;
ALTER TABLE rsvp ADD COLUMN paypal_id CHARACTER VARYING;

CREATE TABLE work_page_section (
  	id SERIAL PRIMARY KEY,
  	active SMALLINT DEFAULT 1,
  	insert_time TIMESTAMP WITHOUT TIME ZONE DEFAULT now(),
  	section_name VARCHAR,
	  title VARCHAR,
	  subtitle VARCHAR,
	  description VARCHAR,
	  featured_image VARCHAR,
	  button VARCHAR,
	  link VARCHAR,
	  display SMALLINT default 1,
	  display_order VARCHAR
);
INSERT INTO work_page_section (section_name)
values
('top'),
('pillar 1 '),
('pillar 2 '),
('pillar 3 '),
('pillar 4 '),
('pillar 5 ');

ALTER TABLE donation ADD COLUMN monthly SMALLINT ;

--Oct 6th
ALTER TABLE home ADD COLUMN about CHARACTER VARYING;
ALTER TABLE home ADD COLUMN sub_about CHARACTER VARYING;
ALTER TABLE home ADD COLUMN about_link CHARACTER VARYING;

-- Oct 11st
create table confirm_email(
  id serial primary key,
  active SMALLINT default 1,
  insert_time TIMESTAMP without time zone not null default now(),
  name CHARACTER VARYING,
  email_template text,
  subject CHARACTER VARYING,
  confirm_email_images CHARACTER VARYING
);

-- Dec 8

ALTER TABLE donation ADD COLUMN preference CHARACTER VARYING;

-- Jan 9th
ALTER TABLE rsvp ADD COLUMN congregation_name CHARACTER VARYING;
ALTER TABLE rsvp ADD COLUMN congregation_city CHARACTER VARYING;
ALTER TABLE rsvp ADD COLUMN congregation_state CHARACTER VARYING;
ALTER TABLE rsvp ADD COLUMN position CHARACTER VARYING;
ALTER TABLE rsvp ADD COLUMN status CHARACTER VARYING;

-- Jan 13th
ALTER TABLE donate ADD COLUMN logo_image CHARACTER VARYING;
ALTER TABLE donate ADD COLUMN logo_description CHARACTER VARYING;
ALTER TABLE donate ADD COLUMN donate_amount text;
ALTER TABLE donate ADD COLUMN background_image CHARACTER VARYING;
ALTER TABLE donate ADD COLUMN is_default SMALLINT;
ALTER TABLE donate ADD COLUMN default_template SMALLINT;
ALTER TABLE donate ADD COLUMN display SMALLINT;
ALTER TABLE donate ADD COLUMN slug CHARACTER VARYING;
ALTER TABLE donate ADD COLUMN meta_title CHARACTER VARYING;
ALTER TABLE donate ADD COLUMN meta_keywords CHARACTER VARYING;
ALTER TABLE donate ADD COLUMN meta_description CHARACTER VARYING;


ALTER TABLE contact ADD COLUMN admin_reply integer;
ALTER TABLE contact ADD COLUMN viewed BOOLEAN;
ALTER TABLE contact ADD COLUMN answer text;
ALTER TABLE contact ADD COLUMN status character varying;
ALTER TABLE contact ADD COLUMN subject character varying;


-- ALTER TABLE content ADD COLUMN leadership integer;
ALTER TABLE section ADD COLUMN options text;


ALTER TABLE donation ADD COLUMN agreement_id character varying;

ALTER TABLE blog ADD COLUMN post_date character varying;
-- ALTER TABLE officer ADD COLUMN type INTEGER;


-- Jan 23rd
CREATE TABLE israel_category (
  id serial primary key,
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  name character varying,
  slug character varying,
  meta_title CHARACTER VARYING,
  meta_keywords CHARACTER VARYING,
  meta_description CHARACTER VARYING
);

ALTER TABLE israel ADD COLUMN category INTEGER;
INSERT INTO config VALUES (4, 1, '2017-01-22 15:36:51.034302', 'Statement', '0');

