<?php
/*
 *
 * Daily script running at 9am
 * Reminds provider to accept/decline requested session
 * Reminds provider/user to complete session
 *
 * */
include(__DIR__.'/../index.php');

function emailTemplate($personId, $model, $sessionList, $templateName){
    $mod = "\Model\\{$model}";
    $person = $mod::getItem($personId);
    $email = new \Email\MailMaster();
    $mergeFields = [
        'SESSIONS' => $sessionList
    ];
    $email->setTo(['email' => $person->email, 'name' => $person->full_name(), 'type' => 'to'])->setTemplate($templateName)->setMergeTags($mergeFields)->send();
}

//notify provider of all requested sessions
function requestedSessionNotification(){
    $carbon = \Carbon\Carbon::now();
    $sessionHistory = \Model\SessionHistory::getList(['where'=>"date > '{$carbon}' and status = 0"]);
    $sessions = [];
    if($sessionHistory) {
        foreach ($sessionHistory as $sesh) {
            $sessions[$sesh->provider_id][] = $sesh->service()->name . ' with ' . $sesh->user()->full_name();
        }
        foreach ($sessions as $key => $session) {
            $sessions[$key] = implode('</br>', $session);
            emailTemplate($key,'Provider', $sessions[$key],'notify-provider-requested-session');
        }
    }
}

//notify provider to complete session if incomplete
function completedSessionProviderNotification(){
    $carbon = \Carbon\Carbon::now();
    $sessionHistory = \Model\SessionHistory::getList(['where'=>"date < '{$carbon}' and status = 1"]);
    $sessions = [];
    if($sessionHistory){
        foreach($sessionHistory as $sesh){
            $sessions[$sesh->provider_id][] = $sesh->service()->name.' with '. $sesh->user()->full_name();
        }
        foreach($sessions as $key=>$session){
            $sessions[$key] = implode('</br>', $session);
            emailTemplate($key,'Provider', $sessions[$key],'notify-user-complete-session');
        }
    }
}

//notify user to complete session after provider completes
function completedSessionUserNotification(){
    $carbon = \Carbon\Carbon::now();
    $sessionHistory = \Model\SessionHistory::getList(['where'=>"date < '{$carbon}' and status = 3"]);
    $sessions = [];
    if($sessionHistory){
        foreach($sessionHistory as $sesh){
            $sessions[$sesh->user_id][] = $sesh->service()->name.' with '. $sesh->provider()->full_name();
        }
        foreach($sessions as $key=>$session){
            $sessions[$key] = implode('</br>', $session);
            emailTemplate($key,'User', $sessions[$key],'notify-user-complete-session');
        }
    }
}

requestedSessionNotification();
completedSessionProviderNotification();
completedSessionUserNotification();
