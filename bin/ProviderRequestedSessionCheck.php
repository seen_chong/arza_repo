<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 1/5/16
 * Time: 5:34 PM
 */

/**
 *  Runs everyday at 6am, check if requested session still pending,
 *  if the requested session past or equal today, then auto cancel it
 */

include(__DIR__.'/../index.php');

use Model\Provider;
use Model\Calendar;
use Carbon\Carbon;
use Model\SessionHistory;

$providers = Provider::getList();
$expiredPendingSessions = SessionHistory::getExpiredPendingSession();
foreach($expiredPendingSessions as $session){
    $session->refund();
    $session->sendAutoCanceledSessionEmail();
    echo "Issued refund for Session ID: {$session->id} \n";
}