<?php

class ReviewsController extends siteController
{


    public function index(Array $params = [])
    {
        $this->viewData->who = $who_logged = $this->roleType;
        $prov = $use = $this->viewData->user;

        if (isset($params['session_id'])) {

            $this->viewData->session = \Model\Schedule::getItem($params['session_id']);
            if (!isset($this->viewData->session)) {
                redirect("/");
            }
            if ($who_logged == 1) {
                redirect("/");
            } elseif (isset($prov->id) && $prov->id !== $this->viewData->session->provider_id || isset($use->id) && $use !== $this->viewData->session->user_id) {
                redirect("/");
            } else {
                $this->viewData->session_id = $params['session_id'];
                $this->viewData->provider = \Model\Provider::getItem($this->viewData->session->provider_id);
                $this->viewData->service = \Model\Service::getItem($this->viewData->session->service);
                $this->viewData->user = \Model\User::getItem($this->viewData->session->user_id);
                $this->viewData->reviews = \Model\Review::getList(['where' => "active = '1' and session_id = '" . $params['session_id'] . "'"]);
            }

        } else {
            redirect("/");
        }
        $this->loadView($this->viewData);
    }

    public function add_review()
    {

        $this->viewData->who = $who_logged = $this->roleType;
        $prov = $use = $this->viewData->user;

        if (isset($_POST['session_id'])) {

            $this->viewData->session = \Model\Schedule::getItem($_POST['session_id']);
            if ($who_logged == 1) {
                redirect("/");
            } elseif (isset($prov->id) && $prov->id !== $this->viewData->session->provider_id || isset($use->id) && $use !== $this->viewData->session->user_id) {
                redirect("/");
            } else {
                $new_review = new \Model\Review();
                $new_review->insert_time = date("Y-m-d H:i:s");
                $new_review->review = trim(strip_tags($_POST['review']));
                if ($who_logged == 2) {
                    $new_review->author = 1; // user
                } else {
                    $new_review->author = 2; // provider
                }
                $new_review->session_id = trim($_POST['session_id']);
                //$new_review->rating =  $_POST['emo'] ;
                $new_review->save();
                if (!empty($_POST['emo'])) {
                    $sess = \Model\Schedule::getItem(trim($_POST['session_id']));
                    $sess->rating = $_POST['emo'];
                    if (!empty($_POST['problem'])) {
                        $sess->problem = $_POST['problem'];
                    }
                    $sess->save();
                }

                ?>
                <li class="session_review_item">
                    <div class="edit_this_review">
                        <p><?= $new_review->review ?></p>
                    </div>
                    <div class="edit_review_textarea_wrapper edit_wrapper">
                        <form>
                            <textarea class="edit_review_textarea"
                                      id="edit_review<?= $new_review->id ?>"><?= $new_review->review ?></textarea>

                            <div class="save_edits" data-id-review="<?= $new_review->id ?>">
                                <p>Save</p>
                            </div>
                            <div class="cancel_review_edit">
                                <p>Cancel</p>
                            </div>
                        </form>
                    </div>
                    <div class="review_author">
                        <span class="horiz_separator"></span>
                        <? if ($new_review->author == 1) {
                            ?>
                            <p class="review_author_name"><?= @$use->first_name ?> <?= @$use->last_name ?></p>
                        <?
                        } else {
                            ?>
                            <p class="review_author_name"><?= @$prov->first_name ?> <?= @$prov->last_name ?></p>
                        <?
                        } ?>
                        <span class="middot"></span>

                        <p class="review_datetime"><?= date("F d, Y \a\\t\ g:ia", strtotime($new_review->insert_time)) ?></p>
                    </div>
                </li>
                <?
            }
        } else {
            redirect("/");
        }
        $this->template = FALSE;
    }

    public function edit_review()
    {

        $this->viewData->who = $who_logged = $this->roleType;
        $prov = $use = $this->viewData->user;

        if (isset($_POST['session_id']) && isset($_POST['review_id'])) {

            $this->viewData->session = \Model\Schedule::getItem($_POST['session_id']);
            if ($who_logged == 1) {
                redirect("/");
            } elseif (isset($prov->id) && $prov->id !== $this->viewData->session->provider_id || isset($use->id) && $use !== $this->viewData->session->user_id) {
                redirect("/");
            } else {
                $review = \Model\Review::getItem($_POST['review_id']);
                $review->review = trim(strip_tags($_POST['review_text']));
                $review->save();


            }
        } else {
            redirect("/");
        }
        $this->template = FALSE;
    }

}