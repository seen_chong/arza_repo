<?php

class eventController extends siteController
{

    public function index(Array $params = [])
    {
        redirect(SITE_URL);
    }

    public function event(Array $params = [])
    {
        $id = (isset($params['id'])) ? $params['id'] : '';
        $event = \Model\Event::getItem($id);
        if($event){
            $this->viewData->event = $event;
            $time = date("m/d/Y g:iA",  $event->start_time).' - '.date("m/d/Y g:iA",  $event->end_time);
            $this->viewData->time = $time;
            $this->configs['Meta Title'] = $this->viewData->event->name;
        }else{
            redirect(SITE_URL);
        }
        $this->loadView($this->viewData);
    }
}