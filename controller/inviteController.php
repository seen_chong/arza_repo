﻿﻿<?php

class inviteController extends siteController
{


    public function index(Array $params = [])
    {
        $this->loadView($this->viewData);
    }

    /* 	public function generate(Array $params = []){
         $provider = \Model\Provider::getList(['where'=>"active = '1'"]);

     foreach($provider as $pr){
         $k = md5($pr->first_name.$pr->last_name.mt_rand(20000,999999999));
         $pr->ref_key = $k;
         $pr->save();
     }

     $user = \Model\User::getList(['where'=>"active = '1'"]);


       foreach($user as $u){
         $k = md5($u->first_name.$u->last_name.mt_rand(20000,999999999));
         $u->ref_key = $k;
         $u->save();
     }

        $this->loadView($this->viewData);
    } */

    public function send(Array $params = [])
    {
        $mails = trim($_POST['mails']);
        $arr = explode(",", $mails);
        $send = Array();

        for ($i = 0; $i < count($arr); $i++) {
            if (filter_var(trim($arr[$i]), FILTER_VALIDATE_EMAIL)) {
                $send[] = "send";
                $this->invite($arr[$i]);
            }
        }
        echo count($send);
        $this->template = FALSE;
    }

    private function invite($client_mail)
    {
        $user_full_name = $this->viewData->user->full_name();
        $email = new \Emagid\Email();
        global $emagid;
        $emagid->email->from->email = 'noreply@wellns.com';


        $email->addTo($client_mail);
        $email->addTo("john@emagid.com");
        $email->addTo("eitan@emagid.com");
        $email->subject('Your friend invited you to join Wellnes!');
        //$email->body = $text;
        $email->body = '
<html xmlns="http://www.w3.org/1999/xhtml" style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;"><head style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;">
<meta name="viewport" content="width=device-width" style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;">
<link href="http://fonts.googleapis.com/css?family=Varela" rel="stylesheet" type="text/css" style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;">
<title style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;">Session Request Rejected</title>

</head>

<body bgcolor="#FFFFFF" style="font-family:"Varela", sans-serif;margin: 0;padding: 0;font-size: 100%;line-height: 1.6;-webkit-font-smoothing: antialiased;-webkit-text-size-adjust: none;height: 100%;letter-spacing: .4px;">

<!-- body -->
<table class="body-wrap" bgcolor="#FFFFFF" style="margin: 0;padding: 20px;font-size: 100%;line-height: 1.6;width: 100%;">
	<tbody style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;"><tr style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;">
		<td style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;"></td>
		<td class="container" bgcolor="#FFFFFF" style="margin: 0 auto!important;padding: 8px 0 20px 0;font-size: 100%;line-height: 1.6;border: 1px solid transparent;display: block!important;max-width: 800px!important;width:100%;clear: both!important;">

			<!-- content -->
			<div class="content" style="margin: 0 auto;padding: 0;font-size: 100%;line-height: 1.6;max-width: 600px;display: block;">
			<table style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;width: 100%;">
				<tbody style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;"><tr style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;">
					<td style="text-align: center;margin: 0;padding: 0;font-size: 100%;line-height: 1.6;">
						<a href = "https://wellns.com">
							<img style="width: 200px;margin-bottom: 18px;padding: 0;font-size: 100%;line-height: 1.6;max-width: 100%;" src="http://wellns.com/content/frontend/assets/img/email_imgs/logo.png">
						</a>
						<p style = "border-top:1px solid transparent;margin-top:8px;margin-bottom:24px;"></p>
						<img style="width: 210px;margin-bottom: 0px;padding: 0;font-size: 100%;line-height: 1.6;max-width: 100%;margin-bottom:18px;" src="http://wellns.com/content/frontend/assets/img/email_imgs/rejected_sprite.png">
						<p style="text-align: center;font-size: 22px;margin: 0;padding: 0;line-height: 1.6;margin-bottom: 10px;font-weight: normal;">Hi ' . $client_mail . ',</p>
						<p style="font-size: 18px;line-height: 26.5px;text-align: center;padding: 0;margin-bottom: 10px;font-weight: normal;">' . $user_full_name . ' invited you. To join use link  below: NEED MORE TEXT
						</p>
						<a href = "https://wellns.com/signup?r=' . $user->ref_key . '" target="_blank" >
						<img style="margin-top: 2px;margin-bottom:8px;width: 330px;cursor: pointer;padding: 0;font-size: 100%;line-height: 1.6;max-width: 100%;" src="http://wellns.com/content/frontend/assets/img/email_imgs/view_calendar_btn.png">
						</a>
						
					</td>
				</tr>
			</tbody></table>
			</div>
			<!-- /content -->
			
		</td>
		<td style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;"></td>
	</tr>
</tbody></table>
<!-- /body -->

<!-- footer -->
<table class="footer-wrap" style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;width: 100%;clear: both!important;">
	<tbody style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;"><tr style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;">
		<td style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;"></td>
		<td class="container" style="margin: 0 auto!important;padding: 0;font-size: 100%;line-height: 1.6;display: block!important;max-width: 600px!important;clear: both!important;">
			
			<!-- content -->
			<div class="content" style="margin: 0 auto;padding: 0;font-size: 100%;line-height: 1.6;max-width: 600px;display: block;">
				<table style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;width: 100%;">
					<tbody style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;"><tr style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;">
						<td align="center" style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;">
							<p style="margin: 0;padding: 0;font-size: 12px;line-height: 1.6;margin-bottom: 10px;font-weight: normal;color: #666;">
								<a class="text-decoration:none;color:#a0c2ce;" href="https://wellns.com/" style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;color: #a0c2ce;text-decoration: none;"><unsubscribe style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;">Unsubscribe</unsubscribe></a>
								<span style="color: #000000;margin: 0;padding: 0;font-size: 100%;line-height: 1.6;">·</span>
								<a class="text-decoration:none;color:#a0c2ce;" href="https://wellns.com/" style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;color: #a0c2ce;text-decoration: none;">Contact Us</a>
								<span style="color: #000000;margin: 0;padding: 0;font-size: 100%;line-height: 1.6;">·</span>
								<a class="text-decoration:none;color:#a0c2ce;" href="https://wellns.com/" style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;color: #a0c2ce;text-decoration: none;">Privacy Policy</a>
							</p>
						</td>
					</tr>
					<tr style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;">
						<td align="center" style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;">
							<p style="color: #8b8b8b;font-size: 9px;width: 80%;max-width: 600px;margin: 0;padding: 0;line-height: 1.6;margin-bottom: 10px;font-weight: normal;">
							© 2015 Wellns. The Wellns logo is a registered trademark of Wellns LLC. All rights reserved. This email has been sent to your by Wellns, 225 West 39th Street, New York City, NY 10009. Made by <b style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;">Emagid</b>
							</p>
						</td>
					</tr>
				</tbody></table>
			</div>
			<!-- /content -->
			
		</td>
		<td style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;"></td>
	</tr>
</tbody></table>
<!-- /footer -->



</body></html>';

        $email->send();
    }

}