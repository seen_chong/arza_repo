<?php

class cartController extends siteController
{

    public function index(Array $params = [])
    {
        // $order_products = [];
        // $subtotal = 0;
        // foreach($this->viewData->cart->products as $product){
        // 	$subtotal += $product->price;
        // }
        // $this->viewData->subtotal = $subtotal;

        // if (isset($_SESSION['coupon'])){
        // 	$coupon = \Model\Coupon::getItem(null, ['where'=>"code = '".$_SESSION['coupon']."'"]);
        // 	if ((!is_null($coupon)
        // 			&& time() >= $coupon->start_time && time() < $coupon->end_time
        // 			&& $this->viewData->subtotal >= $coupon->min_amount
        // 			&& \Model\Order::getCount(['where'=>"coupon_code = '".$coupon->code."'"]) < $coupon->num_uses_all)){
        // 		if ($coupon->discount_type == 1){
        // 			$subtotal = $subtotal - $coupon->discount_amount;
        // 		} else if ($coupon->discount_type == 2) {
        // 			$subtotal = $subtotal * (1 - ($coupon->discount_amount/100));
        // 		}
        // 	}
        // }

        // $this->viewData->shipping_methods = [];
        // $shipping_methods = \Model\Shipping_Method::getList(['orderBy'=>'id asc']);
        // foreach ($shipping_methods as $shipping_method){
        // 	$costs = json_decode($shipping_method->cost);
        // 	$shipping_method->cost = 0;
        // 	$shipping_method->cart_subtotal_range_min = json_decode($shipping_method->cart_subtotal_range_min);
        // 	foreach($shipping_method->cart_subtotal_range_min as $key=>$range){
        // 		if (is_null($range) || trim($range) == ''){
        // 			$range = 0;
        // 		}
        // 		if ($subtotal >= $range && isset($costs[$key])){
        // 			$shipping_method->cost = $costs[$key];
        // 		}
        // 	}
        // 	$this->viewData->shipping_methods[] = $shipping_method;
        // }

        // if (isset($_COOKIE['shippingMethod'])){
        // 	$this->viewData->shipping_method = $_COOKIE['shippingMethod'];
        // } else {
        // 	$this->viewData->shipping_method = \Model\Shipping_Method::getItem(null, ['orderBy'=>'is_default desc']);
        // 	$this->viewData->shipping_method = $this->viewData->shipping_method->id;
        // 	setcookie('shippingMethod', $this->viewData->shipping_method, time()+60*60*24*100, "/");
        // }

        // parent::index($params);
    }

    public function add($params)
    { //cart page
        array_push($this->viewData->cart->products, $params['id']);

        $this->updateCookies();
        redirect(SITE_URL . 'cart');
    }

    private function updateCookies()
    {
        setcookie('cart', serialize($this->viewData->cart), time() + 60 * 60 * 24 * 100, "/");
    }

    public function side_cart()
    { //cart page
        $this->loadView($this->viewData);
    }

//    public function add_pack()
//    { //used on the product page (add to cart button)
//        $pack = $_POST['id'];
//        $provider_id = $_POST['provider_id'];
//        if ($_POST['case_pack'] == 1) {
//            $date = date("Y-m-d", strtotime($_POST['time_pack']));
//            $time = date("g:ia", strtotime($_POST['time_pack']));
//        } else {
//            $time = $_POST['time_pack'];
//            $date = $_POST['date_pack'];
//        }
//
//        $serviceID = $_POST['service'];
//        $session_location = $_POST['session_location'];
//
//        if (isset($pack) and isset($provider_id)) {
//            $package = \Model\Package::getItem($pack);
//            $item = (object)[
//                'key' => md5(md5(mt_rand())),
//                'id' => $package->id,
//                'name' => $package->name,
//                'quantity' => $package->quantity,
//                'price' => $package->price,
//                'expiration' => $package->expiration,
//                'provider_id' => $provider_id,
//                'service' => $serviceID,
//                'session_location' => $session_location,
//                'time' => $time,
//                'date' => $date
//            ];
//            $this->viewData->cart->packages[] = $item;
//
//
//            $this->updateCookies();
//            $this->template = false;
//            redirect(SITE_URL . 'checkout');
//        }
//    }

    public function add_credit()
    { //used on the product page (add to cart button)

        $credit = \Model\Credit::getItem($_POST['id']);
        if ($credit) {
            $item = (object)[
                'key' => md5(md5(mt_rand())),
                'id' => $credit->id,
                'name' => $credit->name,
                'price' => $credit->price,
                'value' => $credit->value,
                'discription' => $credit->discription,
                'image' => $credit->featuredImage()
            ];

            $this->viewData->cart->credits[$item->key] = $item;
            $this->updateCookies();
            $this->template = false;
            redirect(SITE_URL . 'checkout?type=credit');
        }
    }

//    public function invite_friend()
//    { //used on the product page (add to cart button)
//        $schedule = $_POST['id'];
//        $mails = $_POST['mails'];
//
//
//        if (isset($schedule) and isset($mails)) {
//
//            $sch = \Model\Schedule::getItem($schedule);
//
//
//            $item = (object)[
//                'key' => md5(md5(mt_rand())),
//                'id' => $schedule,
//                'mail' => $mails,
//
//
//                'provider_id' => $sch->provider_id,
//                'service' => $sch->service,
//                'session_location' => $sch->session_location,
//                'time' => $sch->time,
//                'date' => $sch->day
//            ];
//            $this->viewData->cart->invites[] = $item;
//
//
//            $this->updateCookies();
//            $this->template = false;
//            redirect(SITE_URL . 'checkout');
//        }
//    }

//    public function add_single_session()
//    { //used on the product page (add to cart button)
//        $pack = $_POST['id'];
//        $provider_id = $_POST['provider_id'];
//        if ($_POST['case_pack'] == 1) {
//            $date = date("Y-m-d", strtotime($_POST['time_pack']));
//            $time = date("g:ia", strtotime($_POST['time_pack']));
//        } else {
//            $time = $_POST['time_pack'];
//            $date = $_POST['date_pack'];
//        }
//        $serviceID = $_POST['service'];
//        $session_location = $_POST['session_location'];
//        if (isset($pack) and isset($provider_id)) {
//            $package = \Model\Package::getItem($pack);
//
//
//            $item = (object)[
//                'id' => $package->id,
//                'key' => md5(md5(mt_rand())),
//                'name' => $package->name,
//                'quantity' => $package->quantity,
//                'price' => $package->price,
//                'expiration' => $package->expiration,
//                'provider_id' => $provider_id,
//                'service' => $serviceID,
//                'session_location' => $session_location,
//                'time' => $time,
//                'date' => $date
//            ];
//            $this->viewData->cart->single_sessions[] = $item;
//
//
//            $this->updateCookies();
//            $this->template = false;
//            redirect(SITE_URL . 'checkout');
//        }
//    }

    // public function remove_once($params){
    // 	foreach ($this->viewData->cart->products as $key=>$product){
    // 		if ($product == $params['id']){
    // 			unset($this->viewData->cart->products[$key]);
    // 			break;
    // 		}
    // 	}
    // 	$this->updateCookies();
    // 	redirect(SITE_URL.'cart');
    // }r

    public function add_post()
    { //used on the product page (add to cart button)
        if (isset($_POST['product_id'])) {
            $product = \Model\Product::getItem($_POST['product_id']);

            if (!is_null($product)) {
                $item = (object)[
                    'id' => $product->id,
                    'key' => md5(md5(mt_rand())),
                    'name' => $product->name,
                    'slug' => $product->slug,
                    'price' => $product->price,
                    'featured_image' => $product->featured_image
                ];
                if (isset($_POST['size'])) {
                    $item->size = $_POST['size'];
                }
                if (isset($_POST['color'])) {
                    $item->color = $_POST['color'];
                }
                $this->viewData->cart->products[] = $item;
            }
        }

        $this->updateCookies();

        $this->template = false;

        $this->loadView('side_cart', $this->viewData);
    }

    public function remove_product($params)
    {

        foreach ($this->viewData->cart->products as $key => $product) {
            if ($product->key == $params['id']) {
                unset($this->viewData->cart->products[$key]);
            }
        }
        $this->updateCookies();
        redirect(SITE_URL . 'checkout');
    }

    public function remove_credit($params)
    {

        foreach ($this->viewData->cart->credits as $key => $product) {
            if ($key == $params['id']) {
                unset($this->viewData->cart->credits[$key]);
            }
        }
        $this->updateCookies();
        redirect(SITE_URL . 'checkout?type=credit');
    }

    public function remove_pack($params)
    {

        foreach ($this->viewData->cart->packages as $key => $product) {
            if ($product->key == $params['id']) {
                unset($this->viewData->cart->packages[$key]);
            }
        }
        $this->updateCookies();
        redirect(SITE_URL . 'checkout');
    }

    public function remove_session($params)
    {

        foreach ($this->viewData->cart->single_sessions as $key => $product) {
            if ($product->key == $params['id']) {
                unset($this->viewData->cart->single_sessions[$key]);
            }
        }
        $this->updateCookies();
        redirect(SITE_URL . 'checkout');
    }

    public function remove_invite($params)
    {

        foreach ($this->viewData->cart->invites as $key => $product) {
            if ($product->key == $params['id']) {
                unset($this->viewData->cart->invites[$key]);
            }
        }
        $this->updateCookies();
        redirect(SITE_URL . 'checkout');
    }

    //This function is also executed in the ordersController after a new order is inserted

    public function remove_all()
    {
        $this->viewData->cart->products = [];
        $this->updateCookies();
        redirect(SITE_URL . 'checkout?type=credit');
    }

    public function updateShipping(Array $params = [])
    {
        if (isset($_GET['id']) && is_numeric($_GET['id']) && !is_null(\Model\Shipping_Method::getItem($_GET['id']))) {
            setcookie('shippingMethod', $_GET['id'], time() + 60 * 60 * 24 * 100, "/");
        }
    }

    public function applyCoupon(Array $params = [])
    {
        if (isset($_GET['code'])) {
            $coupon = \Model\Coupon::getItem(null, ['where' => " code = '" . $_GET['code'] . "'"]);
            if (is_null($coupon)) {
                $_SESSION['coupon'] = null;
                echo json_encode(['error' => ['code' => 1, 'message' => 'Invalid coupon code.'], 'coupon' => null]);
            } else if (time() >= $coupon->start_time && time() < $coupon->end_time) {
                $cartTotal = (float)str_replace(',', '', $this->viewData->cart->total);
                if ($cartTotal >= $coupon->min_amount) {
                    $already_used = \Model\Order::getCount(['where' => "coupon_code = '" . $coupon->code . "'"]);
                    if (is_null($already_used)) {
                        $already_used = 0;
                    }

                    if (is_null($coupon->num_uses_all) || $already_used < $coupon->num_uses_all) {
                        $_SESSION['coupon'] = $coupon->code;
                        echo json_encode(['error' => ['code' => 0, 'message' => ''], 'coupon' => $coupon]);
                    } else {
                        $_SESSION['coupon'] = null;
                        echo json_encode(['error' => ['code' => 4, 'message' => 'Coupon sold out.'], 'coupon' => null]);
                    }
                } else {
                    $_SESSION['coupon'] = null;
                    echo json_encode(['error' => ['code' => 2, 'message' => 'Coupon requires a minimum amount of ' . $coupon->min_amount . '.'], 'coupon' => null]);
                }
            } else {
                $_SESSION['coupon'] = null;
                echo json_encode(['error' => ['code' => 3, 'message' => 'Coupon currently unavailable.'], 'coupon' => null]);
            }
        } else {
            $_SESSION['coupon'] = null;
            echo json_encode(['error' => ['code' => 1, 'message' => 'Invalid coupon code.'], 'coupon' => null]);
        }
    }

}




























