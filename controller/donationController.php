<?php

class donationController extends siteController
{

    public function index(Array $params = [])
    {

        $landing = \Model\Donate::getItem(null,['where'=>"is_default = 1"]);
        $this->viewData->landing = $landing;
        $donateCampaigns = \Model\Donate::getList(['where'=>'is_default = 0 and type = 2 and display = 1','orderBy'=>'display_order']);
        $this->viewData->donateCampaigns = $donateCampaigns;

        $this->loadView($this->viewData);
    }

    public function donate(Array $params = [])
    {
        $slug = (isset($params['donation_slug'])) ? $params['donation_slug'] : '';
        if(isset($_GET['draft'])&&$_GET['draft']=='true'){
            $this->viewData->donate = \Model\Donate::getItem(null,['where'=>"display is null or display <>1 and lower(slug) = '". $slug."'"]);
        }else {
            $this->viewData->donate =  \Model\Donate::getItem(null,['where'=>"display = 1 and lower(slug) = '". $slug."'"]);
        }
        if($this->viewData->donate) {
            if (!is_null($this->viewData->donate->meta_keywords) && $this->viewData->donate->meta_keywords != ''){
                $this->configs['Meta Keywords'] = $this->viewData->donate->meta_keywords;
            }
            if (!is_null($this->viewData->donate->meta_description) && $this->viewData->donate->meta_description != ''){
                $this->configs['Meta Description'] = $this->viewData->donate->meta_description;
            }
            if (!is_null($this->viewData->donate->meta_title) && $this->viewData->donate->meta_title != ''){
                $this->configs['Meta Title'] = $this->viewData->donate->meta_title;
            }
            $this->loadView($this->viewData);
        }else{
            redirect('/');
        }

    }
}