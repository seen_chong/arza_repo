<?php

class contactsController extends adminController
{

    function __construct()
    {
        parent::__construct("Contact");
    }

    function beforeLoadUpdateView()
    {
        if (is_null($this->_viewData->contact->viewed) || !$this->_viewData->contact->viewed){
            $this->_viewData->contact->viewed = true;
            $this->_viewData->contact->save();
        }
    }
    function update_post()
    {
        $notification = '';
        if (isset($_POST['id']) && is_numeric($_POST['id']) && $_POST['id'] > 0) {
            $contact = \Model\Contact::getItem($_POST['id']);
            $contact->admin_reply = $_POST['admin_reply'];
            $contact->answer = $_POST['answer'];
            if ($_POST['subject'] !== $contact->subject) {
                $contact->subject = $_POST['subject'];
            }
            if($contact->admin_reply == 2){
                $notification .='Email sent and ';
                global $emagid;
                $emagid->email->from->email = 'report@emagid.com';
                $email = new \Emagid\Email();
                $email->addTo($contact->email);
                $email->subject('RE:'.$contact->subject);
                $email->body .= '<p><a href="www.modernvice.com"><img src="http://beta.modernvice.com/content/frontend/img/logo.png" /></a></p><br />';
                $email->body = '<p><a href="https://arza.org"><img src="http://beta.arza.org/content/frontend/assets/img/navyLogoText.png" /></a></p>'
                    .'<p>You have the answer about your question:</p>';
                $email->body .= '<p>Subject:'.$contact->subject.'</p>';
                $email->body .= '<p>------------------------------------</p>';
                $email->body .= '<p>Reply:</p>';
				$email->body .= '<p>'.$contact->answer.'</p>';
                $email->send();
            }
        }
        if ($contact->save()){
            $n = new \Notification\MessageHandler($notification . 'Question saved.');
            $_SESSION["notification"] = serialize($n);
        } else {
            $n = new \Notification\ErrorHandler($contact->errors);
            $_SESSION["notification"] = serialize($n);
            redirect(ADMIN_URL.$this->_content.'/update/'.$contact->id);
        }
        redirect(ADMIN_URL.'contacts');
    }

}