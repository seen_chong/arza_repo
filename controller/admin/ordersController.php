<?php

require_once(ROOT_DIR . "libs/authorize/AuthorizeNet.php");

class ordersController extends adminController
{

    function __construct()
    {
        parent::__construct("Order");
    }

    public function index(Array $params = [])
    {
        //$order_by=$_GET['order'];
        //$limit=$_GET['limit'];
        if (!Empty($_GET['how_many'])) {
            $params['limit'] = $_GET['how_many'];

        } else {
            $params['limit'] = 10;
        }
        if (!empty($_GET['type'])) {
            $st = $_GET['type'];
            $params['queryOptions']['where'] = " type = '$st'";

        }

        parent::index($params);
    }

    public function beforeLoadIndexView()
    {
        $this->_viewData->products = [];
        foreach ($this->_viewData->orders as $order) {
            $order->insert_time = new DateTime($order->insert_time);
            $order->insert_time = $order->insert_time->format('m-d-Y H:i:s');
            $order_products = \Model\Order_Product::getList(['where' => 'order_id = ' . $order->id]);
            $this->_viewData->products[$order->id] = [];
            foreach ($order_products as $order_product) {
                $product = \Model\Product::getItem($order_product->product_id);

                $this->_viewData->products[$order->id][] = $product;
            }
        }
    }

    public function beforeLoadUpdateView()
    {
        if (is_null($this->_viewData->order->viewed) || !$this->_viewData->order->viewed) {
            $this->_viewData->order->viewed = true;
            $this->_viewData->order->save();
        }
        $this->_viewData->order_status = \Model\Order::$status;

        $this->_viewData->order_products = \Model\Order_Product::getList(['where' => "order_id=" . $this->_viewData->order->id]);
        $this->_viewData->orderCredits = \Model\CreditHistory::getList(['where' => "order_id=" . $this->_viewData->order->id]);

        if (is_null($this->_viewData->order->user_id)) {
            $this->_viewData->order->user = null;
        } else {
            $this->_viewData->order->user = \Model\User::getItem($this->_viewData->order->user_id);
        }

        if (is_null($this->_viewData->order->coupon_code)) {
            $this->_viewData->coupon = null;
            $this->_viewData->savings = '0';
        } else {
            $this->_viewData->coupon = \Model\Coupon::getItem(null, ['where' => "code = '" . $this->_viewData->order->coupon_code . "'"]);
            if ($this->_viewData->order->coupon_type == 1) {//$
                $this->_viewData->savings = $this->_viewData->order->coupon_amount;
            } else if ($this->_viewData->order->coupon_type == 2) {//%
                $this->_viewData->savings = $this->_viewData->order->subtotal * $this->_viewData->order->coupon_amount / 100;
            }
        }

        if (!is_null($this->_viewData->order->cc_number)) {
            $this->_viewData->order->cc_number = '****' . substr($this->_viewData->order->cc_number, -4);
        }
    }


    public function update_post()
    {
        if (isset($_POST['id']) && is_numeric($_POST['id']) && $_POST['id'] > 0) {

            $order = \Model\Order::getItem($_POST['id']);

            /*if ($order->status != $_POST['status'] && $_POST['status'] == \Model\Order::$status[3] && trim($_POST['tracking_number']) != ""){
                $this->sendShippedEmail($order, $_POST['tracking_number']);
            }*/


            $order->status = $_POST['status'];
            $order->tracking_number = $_POST['tracking_number'];
            if ('****' . substr($order->cc_number, -4) != $_POST['cc_number']) {
                $order->cc_number = $_POST['cc_number'];
            }
            $order->cc_expiration_month = $_POST['cc_expiration_month'];
            $order->cc_expiration_year = $_POST['cc_expiration_year'];
            $order->cc_ccv = $_POST['cc_ccv'];
            $order->payment_method = $_POST['payment_method'];

            $order->bill_first_name = $_POST['bill_first_name'];
            $order->bill_last_name = $_POST['bill_last_name'];
            $order->bill_address = $_POST['bill_address'];
            $order->bill_address2 = $_POST['bill_address2'];
            $order->bill_city = $_POST['bill_city'];
            $order->bill_state = $_POST['bill_state'];
            $order->bill_country = $_POST['bill_country'];
            $order->bill_zip = $_POST['bill_zip'];

            $order->ship_first_name = $_POST['ship_first_name'];
            $order->ship_last_name = $_POST['ship_last_name'];
            $order->ship_address = $_POST['ship_address'];
            $order->ship_address2 = $_POST['ship_address2'];
            $order->ship_city = $_POST['ship_city'];
            $order->ship_state = $_POST['ship_state'];
            $order->ship_country = $_POST['ship_country'];
            $order->ship_zip = $_POST['ship_zip'];
            $order->note = $_POST['note'];

            $order->phone = $_POST['phone'];
            $subtotal = $order->subtotal;
            if (!is_null($order->coupon_code)) {
                if ($order->coupon_type == 1) {
                    $subtotal = $subtotal - $order->coupon_amount;
                } else if ($order->coupon_type == 2) {
                    $subtotal = $subtotal * (1 - ($order->coupon_amount / 100));
                }
            }

            $logged_admin = \Model\Admin::getItem(\Emagid\Core\Membership::userId());
            $orderchange = new \Model\Orderchange();
            $orderchange->order_id = $_POST['id'];
            $orderchange->user_id = $logged_admin->id;
            $orderchange->save();

            if (!empty($_POST['mail'])) {
                $email = new \Emagid\Email();
                if (is_null($order->user_id)) {
                    $email->addTo($order->email);
                } else {
                    $email->addTo(\Model\User::getItem($order->user_id)->email);
                }
                $email->subject('Your order status has been changed to ' . $order->status);
                $email->body = $_POST['mail'];
                $email->send();
            }


            if ($order->shipping_method != $_POST['shipping_method']) {
                $order->shipping_method = \Model\Shipping_Method::getItem($_POST['shipping_method']);
                $costs = json_decode($order->shipping_method->cost);
                $order->shipping_method->cost = 0;
                $order->shipping_method->cart_subtotal_range_min = json_decode($order->shipping_method->cart_subtotal_range_min);
                foreach ($order->shipping_method->cart_subtotal_range_min as $key => $range) {
                    if (is_null($range) || trim($range) == '') {
                        $range = 0;
                    }
                    if ($subtotal >= $range && isset($costs[$key])) {
                        $order->shipping_cost = $costs[$key];
                    }
                }
                $order->shipping_method = $order->shipping_method->id;
            }

            if ($order->ship_state == 'NY') {
                $order->tax_rate = 8.875;
                $order->tax = $subtotal * ($order->tax_rate / 100);
            } else {
                $order->tax_rate = null;
                $order->tax = null;
            }

            $order->total = $subtotal + $order->shipping_cost + $order->tax;

            if ($order->save()) {
                $n = new \Notification\MessageHandler('Order saved.');
                $_SESSION["notification"] = serialize($n);
            } else {
                $n = new \Notification\ErrorHandler($order->errors);
                $_SESSION["notification"] = serialize($n);
                redirect(ADMIN_URL . $this->_content . '/update/' . $order->id);
            }

        }
        redirect(ADMIN_URL . $_POST['redirectTo']);
    }


    public function update_post_status()
    {
        $id = $_POST['id'];
        $status = $_POST['status'];
        $order = \Model\Order::getItem($_POST['id']);
        $order->status = $status;
        $order->save();
    }


    /*  private function sendShippedEmail($order, $trackingNumber){
          $email = new \Emagid\Email();
          if (is_null($order->user_id)){
              $email->addTo($order->email);
          } else {
              $email->addTo(\Model\User::getItem($order->user_id)->email);
          }
          $email->subject('Order shipped');
          $email->body = '<p><a href="www.itmustbetime.com"><img src="http://beta.itmustbetime.com/content/frontend/img/logo.png" /></a></p>'
                        .'<p>Your ItMustBeTime order was shipped!</p>'
                        .'<p>The tracking number is: <b>'.$trackingNumber.'</b></p>'
                        .'<p>Regards,<br />Itmustbetime Team</p>'
                        .'<p>Find us on <a href="https://www.facebook.com/pages/Itmustbetimecom/202088860134">Facebook</a> and <a href="https://twitter.com/itmustbetime">Twitter</a>.</p>';
          $email->send();
      }*/

    public function getCcNumber()
    {
        if (isset($_POST['password']) && $_POST['password'] == 'test5343') {
            echo json_encode(\Model\Order::getItem($_POST['order_id'])->cc_number);
        } else {
            echo json_encode("false");
        }
    }

    public function search()
    {
        $orders = \Model\Order::search($_GET['keywords']);

        echo '[';
        foreach ($orders as $key => $order) {
            $order_products = \Model\Order_Product::getList(['where' => 'order_id = ' . $order->id]);

            echo '{ "id": "' . $order->id . '",
			"status": "' . $order->status . '",
			"insert_time": "' . $order->insert_time . '",
			"tracking_number": "' . $order->tracking_number . '",
			"status": "' . $order->status . '",
			"bill_name": "' . $order->bill_first_name . ' ' . $order->bill_last_name . '",
			"products": "'; ?><? foreach ($order_products as $op) {
                $product = \Model\Product::getItem($op->product_id);
                echo $product->name;
                echo "<br>";
            } ?><? echo '",' ?><? echo '"brands": "'; ?><? foreach ($order_products as $op) {
                $product = \Model\Product::getItem($op->product_id);
                $brand = \Model\Brand::getItem($product->brand);
                echo $brand->name;
                echo "<br>";
            } ?><? echo '","total": "' . number_format($order->total, 2) . '"}';
            if ($key < (count($orders) - 1)) {
                echo ",";
            }
        }
        echo ']';
    }


    public function search_by_mpn()
    {
        $mpn = trim($_GET['keywords']);
        $mpn = str_replace('%20', ' ', $mpn);
        $mpn = str_replace('+', ' ', $mpn);
        $mpn = str_replace('%2F', '/', $mpn);
        $mpn = urldecode($mpn);

        $product = \Model\Product::getList(['where' => "mpn = '$mpn'"]);
        $a = Array();

        echo '[';
        foreach ($product as $pr) {
            $order_product = \Model\Order_Product::getList(['where' => 'product_id = ' . $pr->id]);


            foreach ($order_product as $op) {
                $orders = \Model\Order::getList(['where' => 'id = ' . $op->order_id]);


                foreach ($orders as $order) {


                    $a[] = '{ "id": "' . $order->id . '",
			"insert_time": "' . $order->insert_time . '",
			"tracking_number": "' . $order->tracking_number . '",
			"status": "' . $order->status . '",
			"bill_name": "' . $order->bill_first_name . ' ' . $order->bill_last_name . '",
			"products": "' . $pr->name . '",
			"total": "' . $order->total . '"}';


                }
            }
        }

        echo implode(',', $a);
        echo "]";

    }


    public function pay(Array $params = [])
    {
        if (isset($params['id']) && is_numeric($params['id'])) {

            $order = \Model\Order::getItem($params['id']);

            if (!is_null($order)) {

                $localTransaction = $this->emagid->db->execute('SELECT ref_trans_id FROM transaction WHERE order_id = ' . $order->id);

                if (isset($localTransaction[0]) && isset($localTransaction[0]['ref_trans_id'])) {
                    $refTransId = $localTransaction[0]['ref_trans_id'];

                    $transaction = new AuthorizeNetAIM;
                    $response = $transaction->priorAuthCapture($refTransId);

                    $n = new \Notification\MessageHandler($response->response_reason_text);
                    $_SESSION["notification"] = serialize($n);
                }

                redirect(ADMIN_URL . 'orders/update/' . $order->id);

            }

        }

        redirect(ADMIN_URL . 'orders');
    }

}

 


         
















































