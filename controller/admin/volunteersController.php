<?php

class volunteersController extends adminController
{

    function __construct()
    {
        parent::__construct("Volunteer");
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;
        $this->_viewData->page_title = "Manage Volunteer Opportunities";
        parent::index($params);
    }
 

}