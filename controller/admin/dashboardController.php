<?php

class dashboardController extends adminController
{

    function __construct()
    {
        parent::__construct('Dashboard');
    }

    public function index(Array $params = [])
    {
        $this->_viewData->page_title = 'Dashboard';
//        redirect('/admin/schedules/');
        $this->_viewData->members = \Model\User::getCount();

        $this->_viewData->recent_donations = \Model\Donation::getList(['where'=>"lower(status) = 'new'",'orderBy'=>'insert_time DESC', 'limit'=>5]);

        $this->_viewData->recent_rsvps = \Model\Rsvp::getList(['orderBy'=>'insert_time DESC', 'limit'=>5]);

        $this->_viewData->recent_users = \Model\User::getList(['orderBy'=>'insert_time DESC', 'limit'=>5]);

        $this->_viewData->recent_newsletters = \Model\Newsletter::getList(['orderBy'=>'insert_time DESC', 'limit'=>5]);

        $this->_viewData->new_contact = \Model\Contact::getCount(['where'=>"viewed is null or viewed = false"]);

        $this->_viewData->statement = \Model\Config::getItem(4);

        $allEvents = \Model\Event::getList(['orderBy'=>'insert_time DESC']);
        foreach ($allEvents as $event){
            if(time()<$event->start_time){
                $events[]= $event;
            }
        }
        $this->_viewData->events = $events;

        $this->_viewData->total_donation = 0;
        foreach(\Model\Donation::getList(['where'=>"status='New'"]) as $donation){
            $this->_viewData->total_donation += $donation->amount;
        }
        $this->_viewData->total_kehilat_donation = 0;
        foreach(\Model\Donation::getList(['where'=>"type = '3' and status='New'"]) as $donation){
            $this->_viewData->total_kehilat_donation += $donation->amount;
        }

        $this->loadView($this->_viewData);
    }
    public function updateConfig(){
        if(isset($_POST['value'])){
            $id = $_POST['id'];
            $config = \Model\Config::getItem(4);
            $config->value = $_POST['value'];
            if($config->save()){
                $this->toJson(['status'=>'success']);
            }else{
                $this->toJson(['status'=>'fail']);

            }
        }else{
            $this->toJson(['status'=>'fail']);
        }
    }
}