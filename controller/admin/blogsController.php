<?php

class blogsController extends adminController
{

    function __construct()
    {
        parent::__construct("Blog");
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;

        parent::index($params);
    }
    function uploadImage($params = []) {

        $data = [];
        $data['success'] = false;
        $data['redirect'] = false;
        $id = (isset($params['id']) && is_numeric($params['id']) && $params['id']>0) ? $params['id']  : 0;
        if((int)$id>0) {
            $blog = Model\Blog::getItem($id);
            if($blog==null) {$data['redirect']=true;}
            //dd($_FILES);
            $this->save_upload_images($_FILES['file'], $blog->id, UPLOAD_PATH.'blogs'.DS,[],$id);
            $data['success'] = true;

        }
        echo json_encode($data);exit();
    }
    public function save_upload_images($files,$obj_id,$upload_path,$sizes=[],$id) {
        $temp_file_name = $files['tmp_name'];
        $file_name = uniqid()."_".basename($files['name']);
        $file_name = str_replace(' ','_',$file_name);
        $file_name = str_replace('-', '_',$file_name);

        $allow_format = array('jpg', 'png', 'gif','JPG');
        $file_type = explode("/", $files['type']);
        $file_type = strtolower(array_pop($file_type));
        if($file_type == 'jpeg' || $file_type=="JPG"){
            $file_type = 'jpg';
        }
        if(in_array($file_type,$allow_format)){

            $img_path = compress_image($temp_file_name, $upload_path . $file_name, 60,$files['size']);
            if($img_path===false) {
                move_uploaded_file($temp_file_name, $upload_path . $file_name);
            }
            if(count($sizes)>0) {
                foreach($sizes as $key=>$val) {
                    if(is_array($val) && count($val)==2) {
                        resize_image_by_minlen($upload_path,
                            $file_name, min($val), $file_type);

                        $path = images_thumb($file_name,min($val),
                            $val[0],$val[1], file_format($file_name));
                        $image_size_str = implode('_',$val);
                        copy($path, $upload_path.$image_size_str.$file_name);
                    }
                }
            }

            $blog = \Model\Blog::getItem($id);
            $oldImage = $blog->blog_images;
            $oldImages = explode(',', $oldImage);
            $oldImages = $oldImages?$oldImages:[];
            $oldImages[] = $file_name;
            $blog->blog_images = implode(',', $oldImages);
            $blog->save();

        }
    }
    function delete_images($params=[]){
        $id = (isset($params['id']) && is_numeric($params['id']) && $params['id']>0) ? $params['id']  : 0;
        $index = $_GET['index'];
        if((int)$id>0){
            $blog = \Model\Blog::getItem($id);
            $oldImage = $blog->blog_images;
            $oldImages = explode(',', $oldImage);
            $index = array_search($index, $oldImages);
            //$oldImages = $oldImages?$oldImages:[];
            unset($oldImages[$index]);
            $blog->blog_images = implode(',', $oldImages);
            //dd($page->image);
            $blog->save();
        }
        if (isset($_POST['redirectTo'])){
            redirect($_POST['redirectTo']);
        } else {
            redirect(ADMIN_URL.'blogs/update/'.$blog->id);
        }

    }
    public function update_post() {
        $_POST['post_date'] =  strtotime($_POST['post_date']);
        $obj = new $this->_model($_POST);

        if(isset($_POST['id'])&&$_POST['id']){
            $oldObj = \Model\Blog::getItem($_POST['id']);
            $obj->blog_images = $oldObj->blog_images;
        }
        if ($obj->save()){
            foreach($_FILES as $fileType=>$file){
                if ($file['error'] == 0){
                    if ($fileType == 'image' || $fileType == 'featured_image' || $fileType == 'banner'||$fileType == 'author_image'){
                        $image = new \Emagid\Image();
                        $image->upload($_FILES[$fileType], UPLOAD_PATH.$this->_content.DS);
                        $this->afterImageUpload($image);
                        $obj->$fileType = $image->fileName;
                        $obj->save();
                    }
                }
            }
            $this->update_relationships($obj);
            $this->afterObjSave($obj);
            $content = str_replace("\Model\\", "", $this->_model);
            $content = str_replace('_', ' ', $content);
            $n = new \Notification\MessageHandler(ucwords($content).' saved.');
            $_SESSION["notification"] = serialize($n);
        } else {
            $n = new \Notification\ErrorHandler($obj->errors);
            $_SESSION["notification"] = serialize($n);
            redirect(ADMIN_URL.$this->_content.'/update/'.$obj->id);
        }

        if (isset($_POST['redirectTo'])){
            redirect($_POST['redirectTo']);
        } else {
            redirect(ADMIN_URL.$this->_content);
        }
    }

}