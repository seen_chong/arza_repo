<?php

class donateController extends adminController
{

    function __construct()
    {
        parent::__construct("Donate","donate");
    }

    function index(Array $params = []){}
    function update_post(){}
    public function landing(Array $params = []){
        $this->_viewData->page_title = 'Manage Donate Landing Page';
        $params['queryOptions'] = ['where'=>"is_default = 1"];
        parent::index($params);
    }
    public function landing_update(Array $params = []){
        $this->_viewData->page_title = 'Manage Donate Landing Page';
        $params['overrideView'] = 'landing_update';
        $dCampaign = \Model\Donate::getList(['where'=>'active = 1 and  is_default = 0','orderBy'=>'display_order']);
        $this->_viewData->campaigns = $dCampaign;
        parent::update($params);
    }
    public function landing_update_post(){
        $_POST['is_default'] = 1;
        $_POST['default_template'] = 1;
        if(isset($_POST['donate_amount'])&&count($_POST['donate_amount'])>0){
            $_POST['donate_amount'] = json_encode($_POST['donate_amount']);
        }else{
            $_POST['donate_amount'] = json_encode(['36', '50', '100', '360', '500', '1000']);
        }
        $_POST['donate_form'] = [];
        foreach (\Model\Donate::$form as $key=>$value){
            $_POST['donate_form'][$value] = 0;
            if(key_exists($value,$_POST)){
                $_POST['donate_form'][$value] = 1;
            }
        }
        if(isset($_POST['customize'])){
            $_POST['donate_form']['customize'] = 1;
            $_POST['donate_form']['customize_description'] = $_POST['customize_description'];
            $_POST['donate_form']['instruction'] = $_POST['instruction'];
        }
        $_POST['donate_form'] = json_encode($_POST['donate_form']);
        $obj = new \Model\Donate($_POST);
        if ($obj->save()){
            foreach ($_FILES as $fileType => $file) {
                if ($file['error'] == 0) {
                    if ($fileType == 'logo_image' || $fileType == 'background_image' || $fileType == 'image' ) {
                        $image = new \Emagid\Image();
                        $image->upload($_FILES[$fileType], UPLOAD_PATH . $this->_content . DS);
                        $this->afterImageUpload($image);
                        $obj->$fileType = $image->fileName;
                        $obj->save();
                    }
                }
            }
            $n = new \Notification\MessageHandler('Donate Landing Page saved.');
            $_SESSION["notification"] = serialize($n);
            redirect(ADMIN_URL.'donate/landing');
        } else {
            $n = new \Notification\ErrorHandler($obj->errors);
            $_SESSION["notification"] = serialize($n);
            redirect(ADMIN_URL.'donate/landing_update/'.$obj->id);
        }
    }
    public function landing_delete_image(Array $params = []){
        $donate = \Model\Donate::getItem($params['id']);
        if (isset($_GET['type'])) {
            $imgField = $_GET['type'];
        }
        if (file_exists(UPLOAD_PATH.'donate/'.$donate->$imgField)){
            \Emagid\Image::delete('donate/'.$donate->$imgField);
        }
        $donate->$imgField = "";
        $donate->save();

        redirect(ADMIN_URL.'donate/landing_update/'.$donate->id);
    }
    public function campaign(Array $params = []){
        $this->_viewData->hasCreateBtn = true;
        $this->_viewData->page_title = 'Manage Donate Campaign Page';
        $params['queryOptions'] = ['where'=>"is_default = 0"];
        parent::index($params);
    }
    public function campaign_update(Array $params = []){
        $params['overrideView'] = 'campaign_update';
        $this->_viewData->page_title = 'Manage Donate Campaign Page';
        parent::update($params);
    }
    public function campaign_update_post(){
        $_POST['donate_form'] = [];
        $_POST['is_default'] = 0;
        if(isset($_POST['donate_amount'])&&count($_POST['donate_amount'])>0){
            $_POST['donate_amount'] = json_encode($_POST['donate_amount']);
        }else{
            $_POST['donate_amount'] = json_encode(['36', '50', '100', '360', '500', '1000']);
        }

        foreach (\Model\Donate::$form as $key=>$value){
            $_POST['donate_form'][$value] = 0;
            if(key_exists($value,$_POST)){
                $_POST['donate_form'][$value] = 1;
            }
        }
        $_POST['donate_form'] = json_encode($_POST['donate_form']);
        $obj = new \Model\Donate($_POST);
        if ($obj->save()) {
            foreach ($_FILES as $fileType => $file) {
                if ($file['error'] == 0) {
                    if ($fileType == 'logo_image' || $fileType == 'background_image' || $fileType == 'image' ) {
                        $image = new \Emagid\Image();
                        $image->upload($_FILES[$fileType], UPLOAD_PATH . $this->_content . DS);
                        $this->afterImageUpload($image);
                        $obj->$fileType = $image->fileName;
                        $obj->save();
                    }
                }
            }
            $n = new \Notification\MessageHandler('Donate Campaign saved.');
            $_SESSION["notification"] = serialize($n);

        } else {
            $n = new \Notification\ErrorHandler($obj->errors);
            $_SESSION["notification"] = serialize($n);
//            redirect(ADMIN_URL.'donate/campaign_update/'.$obj->id);
        }
        redirect($_POST['redirectTo']);
    }
    public function campaign_delete(Array $params = []){
        $_POST['redirectTo'] = ADMIN_URL.'donate/campaign';
        parent::delete($params);
    }

    public function campaign_delete_image(Array $params = []){
        $donate = \Model\Donate::getItem($params['id']);
        if (isset($_GET['type'])) {
            $imgField = $_GET['type'];
        }
        if (file_exists(UPLOAD_PATH.'donate/'.$donate->$imgField)){
            \Emagid\Image::delete('donate/'.$donate->$imgField);
        }
        $donate->$imgField = "";
        $donate->save();

        redirect(ADMIN_URL.'donate/campaign_update/'.$donate->id);
    }

    public function orderCampaign(){
        $increment = 1;
        foreach($_POST['ids'] as $id){
            $d = \Model\Donate::getItem($id);
            $d->display_order = $increment;
            $d->save();
            $increment++;
        }
        echo 'fail';
    }
    public function check_slug(){
        $slug = $_POST['slug'];
        $id = $_POST['id'];
        if($id>0){
            $list = \Model\Donate::getItem(null,['where'=>"slug = '".$slug."' and id = '".$id."'"]);
            if (isset($list) && count($list) == 1){
                echo "0";
            }else{
                $list = \Model\Donate::getList(['where'=>"slug = '".$slug."' "]);
                if (isset($list) && count($list) > 0){
                    echo "1";
                }else{
                    echo "0";
                }
            }
        }else{
            $list = \Model\Donate::getList(['where'=>"slug = '".$slug."' "]);
            if (isset($list) && count($list) > 0){
                echo "1";
            }else{
                echo "0";
            }
        }

    }
}