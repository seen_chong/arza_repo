<?php

class pagesController extends adminController
{

    function __construct()
    {
        parent::__construct("Page");
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;

        parent::index($params);
    }

}