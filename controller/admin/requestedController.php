<?php

class requestedController extends adminController
{

    public function __construct()
    {
        parent::__construct('SessionHistory', 'requested');
    }

    public function index(Array $params = [])
    {
        $this->_viewData->requested = \Model\SessionHistory::getByStatus(0);
        $this->loadView($this->_viewData);
//        parent::index($params); // TODO: Change the autogenerated stub
    }
}