<?php

class couponsController extends adminController
{

    function __construct()
    {
        parent::__construct("Coupon");
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;

        parent::index($params);
    }

    function beforeLoadUpdateView()
    {
        /*if ($this->_viewData->coupon->id > 0){
            $already_used = \Model\Order::getCount(['where'=>"coupon_code = '".$this->_viewData->coupon->code."'"]);
            if (is_null($already_used)){
                $already_used = 0;
            }
        } else {
            $already_used = 0;
        }

        $this->_viewData->already_used = $already_used;*/
    }

    function update_post()
    {
        $n = "";
        $coupon = new \Model\Coupon($_POST);
        $coupon->min_amount = str_replace("$", "", str_replace(',', '', $coupon->min_amount));
        $coupon->discount_amount = str_replace("$", "", str_replace(',', '', $coupon->discount_amount));
        if (!is_numeric($coupon->min_amount)) {
            $coupon->min_amount = 0.00;
        }
        if (!is_numeric($coupon->discount_amount)) {
            $n = new \Notification\ErrorHandler('Invalid discount amount.');
        }

        $daterange = filter_input(INPUT_POST, 'daterange');
        if ($daterange !== null && $daterange !== false) {
            $date_arr = explode(" - ", $daterange);
            if (count($date_arr) == 2) {
                $start_time = strtotime($date_arr[0]);
                $end_time = strtotime($date_arr[1]);

                if ($start_time > $end_time) {
                    $n = new \Notification\ErrorHandler('Invalid date range.');
                } else {
                    $coupon->start_time = $start_time;
                    $coupon->end_time = $end_time;
                }
            }
        } else {
            $n = new \Notification\ErrorHandler('Invalid date range.');
        }
        if ($n == "") {
            if ($coupon->save()) {
                $n = new \Notification\MessageHandler('Coupon saved.');
                $_SESSION["notification"] = serialize($n);
                redirect(ADMIN_URL . 'coupons');
            } else {
                $n = new \Notification\ErrorHandler($coupon->errors);
                $_SESSION["notification"] = serialize($n);
                redirect(ADMIN_URL . 'coupons/update/' . $coupon->id);
            };
        } else {
            $_SESSION["notification"] = serialize($n);
            redirect(ADMIN_URL . 'coupons/update/' . $coupon->id);
        };
    }

    function validate_code()
    {
        header("Content-type:application/json");
        $data = [];
        $data['success'] = false;
        $code = filter_input(INPUT_GET, 'val');
        $id = filter_input(INPUT_GET, 'id');
        if ($code !== null && $code !== false && $code !== "") {
            $where = "code='$code'";
            if ($id !== null && $id !== false && $id > 0) {
                $where .= " AND id<>{$id}";
            }
            $coupon = Coupon::getCountStatic(['where' => $where]);
            if ($coupon <= 0) {
                $data['success'] = true;
            }
        }
        echo json_encode($data);
        exit();
    }

    function validate_daterange()
    {
        header("Content-type:application/json");
        $data = [];
        $data['success'] = false;
        $daterange = filter_input(INPUT_GET, 'val');

        $range_arr = explode("-", $daterange);
        //print_r($range_arr);
        if (count($range_arr) == 2) {
            $start_date = strtotime($range_arr[0]);
            $end_date = strtotime($range_arr[1]);
            $curr_time = time();
            /*print_r(date("m/d/Y g m a",$start_date));
            print_r(date("m/d/Y g m a",$end_date));
            print_r(date("m/d/Y g m a",$curr_time));*/
            if ($start_date > $end_date) {
                /*$data['curr_time'] = $curr_time;
                $data['start_time'] = $start_date;
                $data['end_time'] = $end_date;*/
            } else {
                $data['success'] = true;
            }
        }
        echo json_encode($data);
        exit();
    }

    function validate_money()
    {
        header("Content-type:application/json");
        $data = [];
        $data['success'] = false;
        $money = filter_input(INPUT_GET, 'val');
        $gtzero = filter_input(INPUT_GET, 'gtzero');
        if ($money !== null && $money !== false && $money !== "") {
            $money = str_replace("$", "", $money);
            $money = str_replace(",", "", $money);
            if (is_numeric($money) && (($gtzero == 1 && (float)$money > 0) || $gtzero == 0)) {
                $data['success'] = true;
            }

        }
        echo json_encode($data);
        exit();
    }
}
