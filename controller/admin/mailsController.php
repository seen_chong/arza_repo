<?php

class mailsController extends adminController
{

    function __construct()
    {
        parent::__construct("Mail");
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = false;

        parent::index($params);
    }

    function upload(Array $params = [])
    {

        if (0 < $_FILES['file']['error']) {
            echo 'Error: ' . $_FILES['file']['error'] . '<br>';
        } else {
            $explode = explode('.', $_FILES['file']['name']); // разрезаем имя по точкам
            $i = count($explode) - 1; // извращаемся на случай, если в названии файла были еще точки
            $extension = $explode[$i]; // расширение файла нашли
            $name = time();
            $new_name = $name . '.' . $extension;
            move_uploaded_file($_FILES['file']['tmp_name'], 'content/uploads/mails/' . $new_name);


            echo '<div id="' . str_replace(".", "_", htmlspecialchars(urlencode($new_name))) . '">';
            echo '<img src="/content/uploads/mails/' . htmlspecialchars(urlencode($new_name)) . '" style="width:100px" /> <a herf="" class="remove_image" attr_id="' . str_replace(".", "_", htmlspecialchars(urlencode($new_name))) . '" style="cursor:pointer;color:red;">[X]</a><br>';
            echo '<input type="text" value="https://wellns.com/content/uploads/mails/' . htmlspecialchars(urlencode($new_name)) . '"><br>';
            echo "</div>";

        }


    }

    function remove(Array $params = [])
    {


        $image_name = str_replace("_", ".", htmlspecialchars(urlencode($_POST['image'])));
        unlink('content/uploads/mails/' . $image_name);


    }


}