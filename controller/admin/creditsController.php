<?php

class creditsController extends adminController
{

    function __construct()
    {
        parent::__construct("Credit", "credits");
    }

    function index(Array $params = [])
    {
        $this->_viewData->credits = \Model\Credit::getList(['where' => " active = 1", 'orderBy' => "price"]);
        $this->_viewData->hasCreateBtn = true;
        $this->loadView($this->_viewData);
//		parent::index($params);
    }

    function update(Array $arr = [])
    {
        $cat = new $this->_model(isset($arr['id']) ? $arr['id'] : null);
        $this->_viewData->credits = \Model\Credit::getList(['where' => " active = 1"]);


        parent::update($arr);
    }

}