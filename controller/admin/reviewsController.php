<?php

class ReviewsController extends adminController
{

    function __construct()
    {
        parent::__construct("Review");
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = false;


        parent::index($params);
    }

    function update(Array $arr = [])
    {
        $ser = new $this->_model(isset($arr['id']) ? $arr['id'] : null);
        //$this->_viewData->services = \Model\Service::getList(['where sub_service = 0']);
        //$this->_viewData->services = \Model\Service::getList(['where'=>' sub_service = 0', 'orderBy'=>' name DESC ']);
        parent::update($arr);
    }

    public function update_post(Array $arr = [])
    {

        //$this->_viewData->services = \Model\Service::getList(['where sub_service = 0']);
        if (isset($_POST['review'])) {
            $review = \Model\Review::getItem($_POST["id"]);
            $review->review = $_POST["review"];
            $review->save();
        }

        if (isset($_POST['problem'])) {
            @$problem = \Model\Schedule::getItem($_POST["session_id"]);
            $problem->problem = $_POST["problem"];
            $problem->save();
        }

        parent::update($arr);
    }

}