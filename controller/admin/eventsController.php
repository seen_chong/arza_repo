<?php


class eventsController extends adminController
{

    function __construct()
    {
        parent::__construct("Event");
    }
    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;
        parent::index($params);
    }

    function update_post() {

        $daterange = filter_input(INPUT_POST,'daterange');
        if($daterange!==null && $daterange!==false) {
            $date_arr = explode(" - ",$daterange);
            if(count($date_arr)==2) {
                $start_time = strtotime($date_arr[0]);
                $end_time   = strtotime($date_arr[1]);

                $_POST['start_time'] = $start_time;
                $_POST['end_time']   = $end_time;

            }
        } else {
            $n = new \Notification\ErrorHandler('Invalid date range.');
        }
        parent::update_post();

    }
    
    function export(){
        $event = \Model\Event::getItem($_GET['id']);
        $participants = \Model\Rsvp::getList(['where'=>'event_id = '.$_GET['id'] . "and (status ='".\Model\Rsvp::$status[0] ."' or status = '" . \Model\Rsvp::$status[1] ."')"]);
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition:attachment; filename='.$event->name.'.csv');
        $output = fopen('php://output', 'w');
        $first = ['ID','DateTime','Status' ,'First Name','Last Name','Email','Phone','Address','City','State','ZipCode','Congregation Name','Congregation City','Congregation State','Position'];
        fputcsv($output,$first);

        foreach($participants as $item){
            $arr = [$item->id,date('F, M d h:ia',strtotime($item->insert_time)), $item->status, $item->first_name,$item->last_name,$item->email,
                $item->phone,$item->address,$item->city,$item->state,$item->zip,$item->congregation_name,$item->congregation_city,$item->congregation_state,$item->position];
            fputcsv($output,$arr);
        }
    }

}

 


         
















































