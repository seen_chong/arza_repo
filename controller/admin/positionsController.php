<?php

class positionsController extends adminController
{

    function __construct()
    {
        parent::__construct("Position");
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;

        parent::index($params);
    }

}