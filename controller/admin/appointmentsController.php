<?php

class appointmentsController extends adminController
{

    public function __construct()
    {
        parent::__construct('Appointment', 'appointments');
    }

    public function index(Array $params = [])
    {
        parent::index($params);
    }

}