<?php

require_once(ROOT_DIR . "libs/authorize/AuthorizeNet.php");

class donationsController extends adminController
{

    function __construct()
    {
        parent::__construct("Donation");
    }

    public function index(Array $params = [])
    {
        //$order_by=$_GET['order'];
        //$limit=$_GET['limit'];
//        if (!Empty($_GET['show'])) {
//            $params['limit'] = $_GET['how_many'];
//
//        } else {
//            $params['limit'] = 10;
//        }
        if (!empty($_GET['type'])) {
            $st = $_GET['type'];
            if($st == 'all'){
                redirect(ADMIN_URL.'donations');
            }else{
                $params['queryOptions']['where'] = " type = '$st'";
            }

        }
        $this->_viewData->donates = \Model\Donate::getList();

        parent::index($params);
    }


    public function update_post()
    {
        if (isset($_POST['id']) && is_numeric($_POST['id']) && $_POST['id'] > 0) {

            $donation = \Model\Donation::getItem($_POST['id']);

            /*if ($order->status != $_POST['status'] && $_POST['status'] == \Model\Order::$status[3] && trim($_POST['tracking_number']) != ""){
                $this->sendShippedEmail($order, $_POST['tracking_number']);
            }*/


//            $order->status = $_POST['status'];
//            $order->tracking_number = $_POST['tracking_number'];
            if ('****' . substr($donation->cc_number, -4) != $_POST['cc_number']) {
                $donation->cc_number = $_POST['cc_number'];
            }
            $donation->cc_expiration_month = $_POST['cc_expiration_month'];
            $donation->cc_expiration_year = $_POST['cc_expiration_year'];
            $donation->cc_ccv = $_POST['cc_ccv'];
//            $order->payment_method = $_POST['payment_method'];

            $donation->first_name = $_POST['first_name'];
            $donation->last_name = $_POST['last_name'];
            $donation->address = $_POST['address'];
            $donation->city = $_POST['city'];
            $donation->state = $_POST['state'];
//            $donation->bill_country = $_POST['bill_country'];
            $donation->zip = $_POST['zip'];


//            $order->note = $_POST['note'];

            $donation->phone = $_POST['phone'];
//            $subtotal = $order->subtotal;


//            $logged_admin = \Model\Admin::getItem(\Emagid\Core\Membership::userId());
//            $orderchange = new \Model\Orderchange();
//            $orderchange->order_id = $_POST['id'];
//            $orderchange->user_id = $logged_admin->id;
//            $orderchange->save();

//            if (!empty($_POST['mail'])) {
//                $email = new \Emagid\Email();
//                if (is_null($order->user_id)) {
//                    $email->addTo($order->email);
//                } else {
//                    $email->addTo(\Model\User::getItem($order->user_id)->email);
//                }
//                $email->subject('Your order status has been changed to ' . $order->status);
//                $email->body = $_POST['mail'];
//                $email->send();
//            }


            if ($donation->save()) {
                $n = new \Notification\MessageHandler('Order saved.');
                $_SESSION["notification"] = serialize($n);
            } else {
                $n = new \Notification\ErrorHandler($donation->errors);
                $_SESSION["notification"] = serialize($n);
                redirect(ADMIN_URL . $this->_content . '/update/' . $donation->id);
            }

        }
        redirect(ADMIN_URL . $_POST['redirectTo']);
    }


    public function update_post_status()
    {
        $id = $_POST['id'];
        $status = $_POST['status'];
        $order = \Model\Order::getItem($_POST['id']);
        $order->status = $status;
        $order->save();
    }


    /*  private function sendShippedEmail($order, $trackingNumber){
          $email = new \Emagid\Email();
          if (is_null($order->user_id)){
              $email->addTo($order->email);
          } else {
              $email->addTo(\Model\User::getItem($order->user_id)->email);
          }
          $email->subject('Order shipped');
          $email->body = '<p><a href="www.itmustbetime.com"><img src="http://beta.itmustbetime.com/content/frontend/img/logo.png" /></a></p>'
                        .'<p>Your ItMustBeTime order was shipped!</p>'
                        .'<p>The tracking number is: <b>'.$trackingNumber.'</b></p>'
                        .'<p>Regards,<br />Itmustbetime Team</p>'
                        .'<p>Find us on <a href="https://www.facebook.com/pages/Itmustbetimecom/202088860134">Facebook</a> and <a href="https://twitter.com/itmustbetime">Twitter</a>.</p>';
          $email->send();
      }*/

    public function getCcNumber()
    {
        if (isset($_POST['password']) && $_POST['password'] == 'test5343') {
            echo json_encode(\Model\Donation::getItem($_POST['order_id'])->cc_number);
        } else {
            echo json_encode("false");
        }
    }

    public function search()
    {
        $donations = \Model\Donation::search($_GET['keywords'],20);
        $arr = ['id','insert_time','viewed','status','amount','name','email','type','phone'];
        $jsonEncode = [];
        foreach($donations as $key=>$donation){
            $json = [];
            foreach($arr as $value){
                if($value == 'name'){
                    $json[$value] = $donation->full_name();
                }else if($value == 'type'){
                    $json[$value] = $donation->getType();
                }else{
                    $json[$value] = $donation->$value;
                }
            }
            $jsonEncode[] = $json;
        }
        echo json_encode($jsonEncode);

//        echo '[';
//        foreach ($donations as $key => $donation) {
////            $order_products = \Model\Order_Product::getList(['where' => 'order_id = ' . $order->id]);
////            if($donation->user_id==0){
////                $type='Guset';
////            }else{
////                $type = \Model\User::getItem($donation->user_id)->getMemberType();
////            }
//
//            echo '{ "id": "' . $donation->id . '",
//			"insert_time": "' . $donation->insert_time . '",
//			"amount": "' . $donation->amount . '",
//			"name": "' . $donation->full_name().'",
//			"email": "'.$donation->email.'",
//			"type": "'.$donation->getType().'",
//			"phone": "' . $donation->phone. '"}';
//        }
//        echo ']';
    }


    public function search_by_mpn()
    {
        $mpn = trim($_GET['keywords']);
        $mpn = str_replace('%20', ' ', $mpn);
        $mpn = str_replace('+', ' ', $mpn);
        $mpn = str_replace('%2F', '/', $mpn);
        $mpn = urldecode($mpn);

        $product = \Model\Product::getList(['where' => "mpn = '$mpn'"]);
        $a = Array();

        echo '[';
        foreach ($product as $pr) {
            $order_product = \Model\Order_Product::getList(['where' => 'product_id = ' . $pr->id]);


            foreach ($order_product as $op) {
                $orders = \Model\Order::getList(['where' => 'id = ' . $op->order_id]);


                foreach ($orders as $order) {


                    $a[] = '{ "id": "' . $order->id . '",
			"insert_time": "' . $order->insert_time . '",
			"tracking_number": "' . $order->tracking_number . '",
			"status": "' . $order->status . '",
			"bill_name": "' . $order->bill_first_name . ' ' . $order->bill_last_name . '",
			"products": "' . $pr->name . '",
			"total": "' . $order->total . '"}';


                }
            }
        }

        echo implode(',', $a);
        echo "]";

    }


    public function pay(Array $params = [])
    {
        if (isset($params['id']) && is_numeric($params['id'])) {

            $order = \Model\Order::getItem($params['id']);

            if (!is_null($order)) {

                $localTransaction = $this->emagid->db->execute('SELECT ref_trans_id FROM transaction WHERE order_id = ' . $order->id);

                if (isset($localTransaction[0]) && isset($localTransaction[0]['ref_trans_id'])) {
                    $refTransId = $localTransaction[0]['ref_trans_id'];

                    $transaction = new AuthorizeNetAIM;
                    $response = $transaction->priorAuthCapture($refTransId);

                    $n = new \Notification\MessageHandler($response->response_reason_text);
                    $_SESSION["notification"] = serialize($n);
                }

                redirect(ADMIN_URL . 'orders/update/' . $order->id);

            }

        }

        redirect(ADMIN_URL . 'orders');
    }
    public function beforeLoadUpdateView()
    {
        if (is_null($this->_viewData->donation->viewed) || !$this->_viewData->donation->viewed) {
            $this->_viewData->donation->viewed = true;
            $this->_viewData->donation->save();
        }
    }
    function export(){
        $donations = \Model\Donation::getList(['orderBy'=>'id']);
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition:attachment; filename=donation.csv');
        $output = fopen('php://output', 'w');
        $first = ['ID','DateTime','Status' ,'First Name','Last Name','Email','Phone','Address','City','State','ZipCode','Amount','Donation Type'];
        fputcsv($output,$first);

        foreach($donations as $item){
            $arr = [$item->id,date('F, M d h:ia',strtotime($item->insert_time)), $item->status, $item->first_name,$item->last_name,$item->email,
                $item->phone,$item->address,$item->city,$item->state,$item->zip,$item->amount,$item->getType()];
            fputcsv($output,$arr);
        }
    }
    function cancel(){

        if (isset($_GET['id']) && is_numeric($_GET['id'])) {
            $donation = \Model\Donation::getItem($_GET['id']);
            if($donation){
                $apiContext = new \PayPal\Rest\ApiContext(new \PayPal\Auth\OAuthTokenCredential(PAYPAL_CLIENT_ID, PAYPAL_CLIENT_SECRET));
                $agreement = \PayPal\Api\Agreement::get($donation->agreement_id,$apiContext);
                if(strtolower($agreement->getState()) == 'active') {
                    $agreementStateDescriptor = new PayPal\Api\AgreementStateDescriptor();
                    $agreementStateDescriptor->setNote('Cancel Monthly Donation');
                    if($agreement->cancel($agreementStateDescriptor,$apiContext)){
                        $donation->status = \Model\Donation::$status[6];
                        if($donation->save()){
                            $n = new \Notification\MessageHandler('Payment Cancelled.');
                            $_SESSION["notification"] = serialize($n);
                        }else{
                            $n = new \Notification\ErrorHandler($donation->errors);
                            $_SESSION["notification"] = serialize($n);
                        }
                    }
                }
            }else{
                $n = new \Notification\ErrorHandler('Cannot cancel the monthly payment');
                $_SESSION["notification"] = serialize($n);

            }
            redirect(ADMIN_URL . 'donations/update/' . $donation->id);
        }

    }

}

 


         
















































