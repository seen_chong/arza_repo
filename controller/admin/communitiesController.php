<?php


class communitiesController extends adminController
{

    function __construct()
    {
        parent::__construct("Community","communities");
    }
    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;
        $this->_viewData->page_title = 'Manage Communities in Israel';
        $params['queryOptions']['orderBy'] = "id ASC";

        if (isset($_GET['orderBy'])&&$_GET['orderBy']) {
            if($_GET['orderBy']=='display'){
                $params['queryOptions']['orderBy'] = "display_order ASC";
            }else if($_GET['orderBy']=='name'){
                $params['queryOptions']['orderBy'] = "name ASC";
            }
        }

        parent::index($params);
    }
    function sort(){

        $this->_viewData->communities = \Model\Community::getList(['orderBy'=>'display_order']);
        $this->loadView($this->_viewData);
    }
    function updateCommunity(){
        $data = json_decode($_POST['data'])[0];
        $display = 1;
        foreach($data as $da){

            $community = \Model\Community::getItem($da->id);

            $community->display_order = $display;
            $community->save();

            $display++;
        }
    }
    public function search()
    {
        $communities = \Model\Community::search($_GET['keywords']);

        $arr = ['id','name','city','state','country','display_order'];
        $jsonEncode = [];
        foreach($communities as $key=>$product){
            $json = [];
            foreach($arr as $value){
                $json[$value] = $product->$value;
            }
            $jsonEncode[] = $json;
        }
        echo json_encode($jsonEncode);
    }


}

 


         
















































