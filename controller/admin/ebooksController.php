<?php

class ebooksController extends adminController
{

    function __construct()
    {
        parent::__construct("Ebook");
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;

        parent::index($params);
    }
    function update(Array $arr = [])
    {

        parent::update($arr);
    }
    public function update_post(){
        $jsonList = ['member_type'];
        foreach($jsonList as $post) {
            if (isset($_POST[$post]) && count($_POST[$post]) > 0) {
                $_POST[$post] = json_encode($_POST[$post]);
            }
        }
        parent::update_post();
    }
    public function search(){
        $ebooks = \Model\Ebook::search($_GET['keywords']);
        $arr = ['id','name','author','publish_date'];
        $jsonEncode = [];
        foreach($ebooks as $key=>$ebook){
            $json = [];
            foreach($arr as $value){
                $json[$value] =  $ebook->$value;
            }
            $jsonEncode[] = $json;
        }
        echo json_encode($jsonEncode);

//		echo '[';
//		foreach($products as $key=>$product){
//			echo '{ "id": "'.$product->id.'", "name": "'.$product->name.'", "featured_image": "'.$product->featured_image.'", "price":"'.$product->price.'", "msrp":"'.$product->msrp.'", "availability":"'.$product->getAvailability().'" }';
//			if ($key < (count($products)-1)){
//				echo ",";
//			}
//		}
//		echo ']';
    }
}

