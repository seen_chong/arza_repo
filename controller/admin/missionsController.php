<?php

class missionsController extends adminController
{

    function __construct()
    {
        parent::__construct("Mission");
    }

    function index(Array $params = [])
    {
        $this->_viewData->page_title = "Manage Who We Are Page";
        parent::index($params);
    }
    function update(Array $arr = [])
    {

        parent::update($arr);
    }

    function sort(){

        $this->_viewData->sections = \Model\Mission_Display::getList(['where'=>'display = 1','orderBy'=>'display_order']);
        $this->loadView($this->_viewData);
    }
    function updateSection(){
        $data = json_decode($_POST['data'])[0];
        $display = 1;
        foreach($data as $da){

            $section = \Model\Mission_Display::getItem($da->id);

            $section->display_order = $display;
            $section->save();

            $display++;
        }
    }
    function updateDisplay(){
        $id = $_POST['id'];
        $item = \Model\Mission_Display::getItem($id);
        if($item->display == 1){
            $item->display =0;
        }else{
            $item->display = 1;
        }
        $item->save();
    }
}

