<?php

class officersController extends adminController
{

    function __construct()
    {
        parent::__construct("Officer");
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;
        $this->_viewData->order_officers = \Model\Officer::getList(['orderBy' => 'display_order ASC']);
        parent::index($params);
    }

    function update(Array $arr = [])
    {

        parent::update($arr);
    }
    function sort($arr = []){

        $this->_viewData->officers = \Model\Officer::getList(['orderBy' => 'display_order ASC']);
        $this->loadView($this->_viewData);
//		parent::index($arr);
    }
    function updateOfficer(){
        $data = json_decode($_POST['data'])[0];
        $display = 1;
        foreach($data as $da){

            $latest = \Model\Officer::getItem($da->id);

            $latest->display_order = $display;
            $latest->save();

            $display++;
        }
    }

}