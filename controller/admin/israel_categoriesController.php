<?php

class israel_categoriesController extends adminController
{

    function __construct()
    {
        parent::__construct("Israel_Category", "statement_categories");
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;


        parent::index($params);
    }

    public function delete(Array $arr = [])
    {
        $class = new $this->_model();

        $class::delete($arr['id']);

        $content = str_replace("\Model\\", "", $this->_model);
        $content = str_replace('_', ' ', $content);
        $n = new \Notification\MessageHandler(ucwords($content) . ' deleted.');
        $_SESSION["notification"] = serialize($n);

        redirect(ADMIN_URL . 'israel_categories');

    }
}