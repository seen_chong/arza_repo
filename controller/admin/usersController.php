<?php
use Model\User;

class usersController extends adminController
{

    function __construct()
    {
        parent::__construct("User", "users");
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;
        if (!Empty($_GET['how_many'])) {
            $params['limit'] = $_GET['how_many'];

        } else {
            $params['limit'] = 10;
        }

        //remove fb users
        $params['queryOptions']['where'] = "fb_id is null";
        parent::index($params);
    }

    public function search()
    {
        $users = \Model\User::Search($_GET['keywords']);

        echo '[';
        foreach ($users as $key => $user) {
            $name = $user->first_name . ' ' . $user->last_name;
            echo '{ "id": "' . $user->id . '", "name": "' . $name . '", "email": "' . $user->email . '"}';
            if ($key < (count($users) - 1)) {
                echo ",";
            }
        }
        echo ']';
    }

    public function update(Array $arr = [])
    {
        $user = new $this->_model(isset($params['id']) ? $params['id'] : null);
        //$this->_viewData->services = \Model\Service::getList(['where sub_service = 0']);
        //$this->_viewData->services = \Model\User::getList();
        $hash_password = true;
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        $password = "";
        if ($id > 0) {
            $id = $_POST['id'];
            $user_exist = User::getItem($id);
            if ($_POST['password'] == "") {
                $password = $user_exist->password;
                $hash_password = false;
            }
        } else {
            $hash_password = true;
        }

//        $user = User::loadFromPost();

        if ($hash_password) {
            $hash = \Emagid\Core\Membership::hash($user->password);

            $user->password = $hash['password'];
            $user->hash = $hash['salt'];
        } else {
            $user->password = $password;
        }

            parent::update($arr);
    }

    public function update_post()
    {
        $hash_password = true;
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        $password = "";
        if ($id > 0) {
            $id = $_POST['id'];
            $user_exist = User::getItem($id);
            if ($_POST['password'] == "") {
                $password = $user_exist->password;
                $hash = $user_exist->hash;
                $hash_password = false;
            }
        } else {
            $hash_password = true;
        }

        $user = User::loadFromPost();

        if ($hash_password) {
            $hash = \Emagid\Core\Membership::hash($user->password);

            $user->password = $hash['password'];
            $user->hash = $hash['salt'];
        } else {
            $user->password = $password;
            $user->hash = $hash;
        }

        if ($user->save()) {
            $n = new \Notification\MessageHandler('Customer Account saved.');
            $_SESSION["notification"] = serialize($n);
            redirect(ADMIN_URL . 'users');
        } else {
            $n = new \Notification\ErrorHandler($user->errors);
            $_SESSION["notification"] = serialize($n);
            redirect(ADMIN_URL . 'users/update/' . $id);
        };
    }
    function export(){
        $users = \Model\User::getList(['orderBy'=>'id']);
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition:attachment; filename=member.csv');
        $output = fopen('php://output', 'w');
        $first = ['ID','Register Time','First Name','Last Name','Email','Phone','Congregation','Membership Group','Membership Level'];
        fputcsv($output,$first);

        foreach($users as $item){
            $group = \Model\Member_Type::getParent($item->subtype)->name;
            $level = \Model\Member_Type::getItem($item->subtype)->name;
            $arr = [$item->id,date('F, M d h:ia',strtotime($item->insert_time)), $item->first_name,$item->last_name,$item->email,
                $item->phone,$item->company,$group,$level];
            fputcsv($output,$arr);
        }
    }
}