<?php

class ReferralsController extends adminController
{

    function __construct()
    {
        parent::__construct("Referral");
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = false;


        parent::index($params);
    }


}