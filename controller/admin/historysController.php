<?php

class historysController extends adminController
{

    function __construct()
    {
        parent::__construct("History");
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;
        $this->_viewData->page_title = "Manage Timeline";
        parent::index($params);
    }
    function update(Array $arr = [])
    {

        parent::update($arr);
    }
}

