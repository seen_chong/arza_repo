<?php

class contentsController extends adminController
{

    function __construct()
    {
        parent::__construct("Content");
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;

        parent::index($params);
    }

    function update(Array $arr = [])
    {
        $this->_viewData->contents = \Model\Content::getList(['where'=>"parent_section='nav'"]);
        parent::update($arr);
    }

    function sort($arr = []){

        $content = (isset($_GET['content'])&&$_GET['content'])?\Model\Content::getItem($_GET['content']):'';
        $this->_viewData->sections = \Model\Section::getList(['where'=>'content_name='.$content->id,'orderBy' => 'display_order ASC']);
        $this->_viewData->content = $content;
        $this->loadView($this->_viewData);
    }
    function updateSection(){
        $data = json_decode($_POST['data'])[0];
        $display = 1;
        foreach($data as $da){

            $section = \Model\Section::getItem($da->id);

            $section->display_order = $display;
            $section->save();

            $display++;
        }
    }


}