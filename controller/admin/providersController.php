<?php

use s3Bucket\s3Handler;

class providersController extends adminController
{

    function __construct()
    {
        parent::__construct("Provider");
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;
        if (!Empty($_GET['how_many'])) {
            $params['limit'] = $_GET['how_many'];

        } else {
            $params['limit'] = 10;
        }
        parent::index($params);
    }

    function beforeLoadUpdateView()
    {
        $this->_viewData->services = \Model\Service::getList();

        $provider_services = \Model\Provider_Services::getList(['where' => "provider_id = " . $this->_viewData->provider->id]);
        $services = [];

        foreach ($provider_services as $provider_service) {
            $services[] = $provider_service->service_id;
        }
        $this->_viewData->provider->services = $services;

        $this->_viewData->provider->mob = date('m', strtotime($this->_viewData->provider->dob));
        $this->_viewData->provider->yob = date('Y', strtotime($this->_viewData->provider->dob));
        $this->_viewData->provider->dob = date('d', strtotime($this->_viewData->provider->dob));

        $this->_viewData->sessionHistory = \Model\SessionHistory::getList(['where' => "provider_id = '" . $this->_viewData->provider->id . "'"]);

        $this->_viewData->packages = \Model\Package::getList(['where' => 'provider_id = ' . $this->_viewData->provider->id]);
    }


    function update_post()
    {
        $hash_password = true;
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);

        if ($id > 0) {
            $provider_exist = \Model\Provider::getItem($id);

            if ($_POST['password'] == "") {
                $hash_password = false;
            }
        }

        $provider = \Model\Provider::loadFromPost();


        if (isset($provider_exist)) {
            $existFields = ['agreed_manifesto_ethics', 'agreed_contractor_agreement', 'agreed_terms', 'agreed_privacy', 'key'];
            foreach ($existFields as $field) {
                $provider->$field = $provider_exist->$field;
            }
            $provider->candidate_id = $provider_exist->candidate_id;
            $provider->report_id = $provider_exist->report_id;
            $provider->ref_key = $provider_exist->ref_key;
            $provider->back_check = $provider_exist->back_check;
            $provider->sign = $provider_exist->sign;
            $provider->oklahoma_check = $provider_exist->oklahoma_check;
            $provider->minnesota_check = $provider_exist->minnesota_check;
            $provider->disclosure_check = $provider_exist->disclosure_check;
            $provider->remind_password = $provider_exist->remind_password;
            $provider->bank_name = $provider_exist->bank_name;
            $provider->bank_address = $provider_exist->bank_address;
            $provider->bank_city = $provider_exist->bank_city;
            $provider->routing_number = $provider_exist->routing_number;
            $provider->photo_position = $provider_exist->photo_position;
            $provider->extra_photo_position = $provider_exist->extra_photo_position;
            $provider->extra_photo_position_2 = $provider_exist->extra_photo_position_2;
            $provider->e_signature = $provider_exist->e_signature;
        }

        if ($hash_password) {
            $hash = \Emagid\Core\Membership::hash($provider->password);

            $provider->password = $hash['password'];
            $provider->hash = $hash['salt'];
        } else {
            $provider->password = $provider_exist->password;
            $provider->hash = $provider_exist->hash;
        }

        if (isset($_POST['dob']) && isset($_POST['mob']) && isset($_POST['yob']) && checkdate($_POST['mob'], $_POST['dob'], $_POST['yob'])) {
            $provider->dob = $_POST['yob'] . '-' . $_POST['mob'] . '-' . $_POST['dob'];
        } else {
            $provider->dob = null;
        }

        if ($provider->save()) {
            $providerRoles = \Model\Provider_Roles::getList(['where' => 'active = 1 and role_id = 3 and provider_id = ' . $provider->id]);
            if (count($providerRoles) <= 0) {
                $providerRole = new \Model\Provider_Roles();
                $providerRole->role_id = 3;
                $providerRole->provider_id = $provider->id;
                $providerRole->save();
            }

            foreach ($_FILES as $fileType => $file) {
                if ($file['error'] == 0) {
                    $s3 = new s3Handler();
                    $provider->$fileType = $s3->upload($_FILES[$fileType]);
                    $provider->save();
                }
            }

            $this->update_relationships($provider);
            $this->afterObjSave($provider);
            $content = str_replace("\Model\\", "", $this->_model);
            $content = str_replace('_', ' ', $content);
            $n = new \Notification\MessageHandler(ucwords($content) . ' saved.');
            $_SESSION["notification"] = serialize($n);
            if (isset($_POST['auth']) and $_POST['auth'] == 1) {
            } else {
                if (isset($_POST['authorized']) and $_POST['authorized'] == 1) {
                    $this->auth_mail($provider->id);
                }
            }
            redirect(ADMIN_URL . 'providers');
        } else {
            $n = new \Notification\ErrorHandler($provider->errors);
            $_SESSION["notification"] = serialize($n);
            redirect(ADMIN_URL . $this->_content . '/update/' . $provider->id);
        }
    }

    private function auth_mail($provider)
    {
        global $emagid;
        $emagid->email->from->email = 'noreply@wellns.com';
        $provider = \Model\Provider::getItem($provider);
        $pr_name = $provider->first_name . ' ' . $provider->last_name;
        $provider_services = \Model\Provider_Services::getList(['where' => "active = '1' and  provider_id= '" . $provider->id . "'"]);

        foreach ($provider_services as $service) {
            $srevice = \Model\Service::getItem($service->service_id);
            $s[] = $srevice->name;
        }
        $ss = implode(",", $s);
        $mail = \Model\Mail::getItem(1);
        $text = $mail->email;
        $text = str_replace("{{provider_full_name}}", $pr_name, $text);
        $text = str_replace("{{provider_services_list}}", $ss, $text);
        $email = new \Emagid\Email();
        //{{provider_full_name}}
        //{{provider_services_list}}
        $email->addTo($provider->email);
        $email->addTo("john@emagid.com");
        $email->addTo("eitan@emagid.com");
        $email->subject('You are authorized!');
        $email->body = $text;
        $q = "<html xmlns='http://www.w3.org/1999/xhtml' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'><head style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
<meta name='viewport' content='width=device-width' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
<link href='http://fonts.googleapis.com/css?family=Varela' rel='stylesheet' type='text/css' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
<title style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>You have Completed Step One of the Wellns Provider Application</title>
<style style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
/* -------------------------------------
		GLOBAL
------------------------------------- */
* {
	margin: 0;
	padding: 0;
	font-size: 100%;
	line-height: 1.6;
}

img {
	max-width: 100%;
}

body {
	-webkit-font-smoothing: antialiased;
	-webkit-text-size-adjust: none;
	width: 100%!important;
	height: 100%;
	letter-spacing: .4px;
	font-family: 'Varela', sans-serif;
}


/* -------------------------------------
		ELEMENTS
------------------------------------- */
a {
	color: #348eda;
}

.btn-primary {
	text-decoration: none;
	color: #FFF;
	background-color: #348eda;
	border: solid #348eda;
	border-width: 10px 20px;
	line-height: 2;
	font-weight: bold;
	margin-right: 10px;
	text-align: center;
	cursor: pointer;
	display: inline-block;
	border-radius: 25px;
}

.btn-secondary {
	text-decoration: none;
	color: #FFF;
	background-color: #aaa;
	border: solid #aaa;
	border-width: 10px 20px;
	line-height: 2;
	font-weight: bold;
	margin-right: 10px;
	text-align: center;
	cursor: pointer;
	display: inline-block;
	border-radius: 25px;
}

.last {
	margin-bottom: 0;
}

.first {
	margin-top: 0;
}

.padding {
	padding: 10px 0;
}


/* -------------------------------------
		BODY
------------------------------------- */
table.body-wrap {
	width: 100%;
	padding: 20px;
}

table.body-wrap .container {
	border: 1px solid transparent;
}


/* -------------------------------------
		FOOTER
------------------------------------- */
table.footer-wrap {
	width: 100%;	
	clear: both!important;
}

.footer-wrap .container p {
	font-size: 12px;
	color: #666;
	
}

table.footer-wrap a {
	color: #a0c2ce;
	text-decoration: none;
}


/* -------------------------------------
		TYPOGRAPHY
------------------------------------- */
h1, h2, h3 {
	color: #000;
	margin: 40px 0 10px;
	line-height: 1.2;
	font-weight: 200;
}

h1 {
	font-size: 36px;
}
h2 {
	font-size: 28px;
}
h3 {
	font-size: 22px;
}

p, ul, ol {
	margin-bottom: 10px;
	font-weight: normal;
	font-size: 14px;
}

ul li, ol li {
	margin-left: 5px;
	list-style-position: inside;
}

/* ---------------------------------------------------
		RESPONSIVENESS
		Nuke it from orbit. It's the only way to be sure.
------------------------------------------------------ */

/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
.container {
	display: block!important;
	max-width: 600px!important;
	margin: 0 auto!important; /* makes it centered */
	clear: both!important;
}

/* Set the padding on the td rather than the div for Outlook compatibility */
.body-wrap .container {
	padding: 20px;
}

/* This should also be a block element, so that it will fill 100% of the .container */
.content {
	max-width: 600px;
	margin: 0 auto;
	display: block;
}

/* Let's make sure tables in the content area are 100% wide */

</style>
</head>

<body bgcolor='#FFFFFF' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;-webkit-font-smoothing: antialiased;-webkit-text-size-adjust: none;height: 100%;letter-spacing: .4px;font-family: 'Varela', sans-serif;width: 100%!important;'>

<!-- body -->
<table class='body-wrap' bgcolor='#FFFFFF' style='margin: 0;padding: 20px;font-size: 100%;line-height: 1.6;width: 100%;'>
	<tbody style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'><tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
		<td style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'></td>
		<td class='container' bgcolor='#FFFFFF' style='margin: 0 auto!important;padding: 8px 0 20px 0;font-size: 100%;line-height: 1.6;border: 1px solid transparent;display: block!important;max-width: 800px!important;width:100%;clear: both!important;'>

			<!-- content -->
			<div class='content' style='margin: 0 auto;padding: 0;font-size: 100%;line-height: 1.6;max-width: 600px;display: block;'>
			<table style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;width: 100%;'>
				<tbody style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'><tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
					<td style='text-align: center;margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
						<a href = 'http://wellns.com'>
							<img style='width: 200px;margin-bottom: 18px;padding: 0;font-size: 100%;line-height: 1.6;max-width: 100%;' src='http://wellns.com/content/frontend/assets/img/email_imgs/logo.png'>
						</a>
						<p style = 'border-top:1px solid #000000;margin-top:8px;margin-bottom:24px;'></p>
						<img style='width: 150px;margin-bottom: 0px;padding: 0;font-size: 100%;line-height: 1.6;max-width: 100%;margin-bottom:18px;' src='http://wellns.com/content/frontend/assets/img/email_imgs/provider_accepted_sprite.png'>
						<p style='text-align: center;font-size: 22px;margin: 0;padding: 0;line-height: 1.6;margin-bottom: 10px;font-weight: normal;'>Hi $pr_name,</p>
						<p style='font-size: 19px;line-height: 29.5px;text-align: center;padding: 0;margin-bottom: 10px;font-weight: normal;'>Welcome to Arza. We are happy to accept you into our community of $ss providers. 
						</p>
						<a href='http://wellns.com/provider/dashboard/'>
						<img style='margin-top: 2px;margin-bottom:8px;width: 330px;cursor: pointer;padding: 0;font-size: 100%;line-height: 1.6;max-width: 100%;' src='http://wellns.com/content/frontend/assets/img/email_imgs/go_to_dashboard_btn.png'></a>
						<p style='color:#8b8b8b;font-size: 14px;line-height: 22.5px;text-align: center;padding: 0;margin-bottom: 10px;font-weight: normal;'>This is the start of your journey spreading wellness among our passionate client base in <span>New York City</span> and beyond. We look forward to hearing about your experience on our service, and we will strive to make Wellns a better place for you and your clients as time goes on. 
						</p>
						
						<img src='http://wellns.com/content/frontend/assets/img/email_imgs/accepted_email_provider_steps.png' style='width: 85%;min-width: 303px;margin: 0;padding: 0;font-size: 100%;line-height: 1.6;max-width: 100%;margin-top:12px;'>

						
					</td>
				</tr>
			</tbody></table>
			</div>
			<!-- /content -->
			
		</td>
		<td style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'></td>
	</tr>
</tbody></table>
<!-- /body -->

<!-- footer -->
<table class='footer-wrap' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;width: 100%;clear: both!important;'>
	<tbody style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'><tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
		<td style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'></td>
		<td class='container' style='margin: 0 auto!important;padding: 0;font-size: 100%;line-height: 1.6;display: block!important;max-width: 600px!important;clear: both!important;'>
			
			<!-- content -->
			<div class='content' style='margin: 0 auto;padding: 0;font-size: 100%;line-height: 1.6;max-width: 600px;display: block;'>
				<table style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;width: 100%;'>
					<tbody style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'><tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
						<td align='center' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
							<p style='margin: 0;padding: 0;font-size: 12px;line-height: 1.6;margin-bottom: 10px;font-weight: normal;color: #666;'>
								<a class='text-decoration:none;color:#a0c2ce;' href='#' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;color: #a0c2ce;text-decoration: none;'><unsubscribe style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>Unsubscribe</unsubscribe></a>
								<span style='color: #000000;margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>·</span>
								<a class='text-decoration:none;color:#a0c2ce;' href='#' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;color: #a0c2ce;text-decoration: none;'>Contact Us</a>
								<span style='color: #000000;margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>·</span>
								<a class='text-decoration:none;color:#a0c2ce;' href='#' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;color: #a0c2ce;text-decoration: none;'>Privacy Policy</a>
							</p>
						</td>
					</tr>
					<tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
						<td align='center' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
							<p style='color: #8b8b8b;font-size: 9px;width: 80%;max-width: 600px;margin: 0;padding: 0;line-height: 1.6;margin-bottom: 10px;font-weight: normal;'>
							© 2015 Wellns. The Wellns logo is a registered trademark of Wellns LLC. All rights reserved. This email has been sent to your by Wellns, 225 West 39th Street, New York City, NY 10009. Made by <b style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>Emagid</b>
							</p>
						</td>
					</tr>
				</tbody></table>
			</div>
			<!-- /content -->
			
		</td>
		<td style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'></td>
	</tr>
</tbody></table>
<!-- /footer -->



</body></html>";

        $email->send(); //sends to the customer

    }

    public function search()
    {
        $providers = \Model\Provider::search($_GET['keywords']);

        echo '[';
        foreach ($providers as $key => $provider) {
            $name = $provider->first_name . ' ' . $provider->last_name;
            echo '{ "id": "' . $provider->id . '", "name": "' . $name . '", "email": "' . $provider->email . '"}';
            if ($key < (count($providers) - 1)) {
                echo ",";
            }
        }
        echo ']';
    }


}
