<?php

class newsletterController extends adminController {
	
	function __construct(){
		parent::__construct("Newsletter");
	}
  	
	function index(Array $params = []){
		$this->_pageSize = 20;
		$this->_viewData->page_title = "Subscribers";
		$params['queryOptions'] = ['orderBy'=>'newsletter.email'];
		parent::index($params);
	}
    function delete(Array $arr = []) {

        \Model\Newsletter::delete($arr['id']);
        $content = str_replace("\Model\\", "", $this->_model);
        $content = str_replace('_', ' ', $content);
        $n = new \Notification\MessageHandler(ucwords($content).' deleted.');
        $_SESSION["notification"] = serialize($n);
        redirect(ADMIN_URL.'newsletter');

    }
    function export(){
        $newsletter = \Model\Newsletter::getList();
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition:attachment; filename=newsletter.csv');
        $output = fopen('php://output', 'w');
        $first = ['DateTime', 'Email'];
        fputcsv($output,$first);

        foreach($newsletter as $item){
            $arr = [date('F, M d h:ia',strtotime($item->create_date)), $item->email];
            fputcsv($output,$arr);
        }
    }


}