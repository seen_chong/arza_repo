<?php

class blog_categoriesController extends adminController
{

    function __construct()
    {
        parent::__construct("Blog_Category", "blog_categories");
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;


        parent::index($params);
    }


}