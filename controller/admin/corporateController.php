<?php
use Model\User;

class corporateController extends adminController
{

    function __construct()
    {
        parent::__construct('Corporate', 'corporate');
    }

    function index(Array $params = [])
    {
        $corporate = new \Model\Corporate();

        $this->_viewData->hasCreateBtn = true;
        $this->_viewData->corporate = $corporate->getCorporateList();

        $this->loadView($this->_viewData);
    }

    function update(Array $arr = [])
    {
        $user = new $this->_model(isset($params['id']) ? $params['id'] : null);
        //$this->_viewData->services = \Model\Service::getList(['where sub_service = 0']);
        //$this->_viewData->services = \Model\User::getList();
        $hash_password = true;
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        $password = "";
        if ($id > 0) {
            $id = $_POST['id'];
            $user_exist = User::getItem($id);
            if ($_POST['password'] == "") {
                $password = $user_exist->password;
                $hash_password = false;
            }
        } else {
            $hash_password = true;
        }

        $user = User::loadFromPost();

        if ($hash_password) {
            $hash = \Emagid\Core\Membership::hash($user->password);

            $user->password = $hash['password'];
            $user->hash = $hash['salt'];
        } else {
            $user->password = $password;
        }

        $cEmployee = \Model\Corporate_Employee::getList(['where' => "employer_id = " . $arr['id'] . ""]);
        $this->_viewData->users = $cEmployee;
        $cCredit = [];
        foreach ($cEmployee as $ce) {
            $cCredit[] = \Model\Corporate_Credit::getList(['where' => "corporate_employee_id = $ce->id"]);
        }
        $this->_viewData->credits = $cCredit;

        parent::update($arr);
    }
}