<?php

class latestController extends adminController
{

    function __construct()
    {
        parent::__construct("Latest",'latest');
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;
        $this->_viewData->order_latest = \Model\Latest::getList(['orderBy' => 'display_order ASC']);
        parent::index($params);
    }

    function update(Array $arr = [])
    {

        parent::update($arr);
    }
    function sort($arr = []){

        $this->_viewData->latests = \Model\Latest::getList(['orderBy' => 'display_order ASC']);
        $this->loadView($this->_viewData);
//		parent::index($arr);
    }
    function updateLatest(){
        $data = json_decode($_POST['data'])[0];
        $display = 1;
        foreach($data as $da){

            $latest = \Model\Latest::getItem($da->id);

            $latest->display_order = $display;
            $latest->save();

            $display++;
        }
    }

}