<?php

class PayoutsController extends adminController
{

    function __construct()
    {
        parent::__construct('SessionHistory', 'Payouts');
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = false;
        $this->_viewData->payouts = \Model\SessionHistory::getAllHistoryUnpaidSessions();
        parent::index($params);
    }

    public function make_withdraw()
    {
        $new_withdraw = new \Model\Payout();
        $new_withdraw->provider_id = $_POST['provider_id'];
        $new_withdraw->status = 1;
        $new_withdraw->period = $_POST['period'];
        $new_withdraw->amount = $_POST['amount'];
        $new_withdraw->save();/*	$n = new \Notification\MessageHandler("Your withdrawal is pending!");
        $_SESSION["notification"] = serialize($n);
        redirect(SITE_URL.'admin/withdraws/')*/;
        $this->loadView($this->viewData);
    }

    public function export(Array $params = [])
    {

        $payouts = \Model\Payout::getList();
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=payouts.csv');
        $output = fopen('php://output', 'w');
        $t = array("Provider", "Amount", "Status", "Peroid");

        fputcsv($output, $t);
        $row = '';
        foreach ($payouts as $b) {
            /* number_format($withdraw->amount*100/70,2);
                number_format(($withdraw->amount*100/70)/100*30,2);*/

            $provider = \Model\Provider::getItem($b->provider_id);
            $name = $provider->first_name . ' ' . $provider->last_name;

            $row = array($name, number_format($b->amount, 2), "Confirmed", $b->period);
            fputcsv($output, $row);
        }
        @$this->template = FALSE;

    }

    public function markAsPaid()
    {
        if(!isset($_POST['provider']) || !$_POST['period']){
            $n = new \Notification\MessageHandler("Something wrong, please try again");
            $_SESSION["notification"] = serialize($n);
            $this->toJson(['success'=>false]);
        }

        $provider = \Model\Provider::getItem($_POST['provider']);

        if($provider->markPaidForPeriodSessions($_POST['period'])){
            $n = new \Notification\MessageHandler("Payment confirm success");
            $_SESSION["notification"] = serialize($n);
            $this->toJson(['success'=>true]);
        }
    }

}