<?php

class email_listController extends adminController
{

    function __construct()
    {
        parent::__construct("Email_List", "emails");
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;

        parent::index($params);
    }

    function redir()
    {
        redirect(ADMIN_URL . 'email_list');
    }

}