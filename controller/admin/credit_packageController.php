<?php

class credit_packageController extends adminController
{

    function __construct()
    {
        parent::__construct("CreditPackage", "credit_package");
    }

    function index(Array $params = [])
    {
        $this->_viewData->hasCreateBtn = true;
        parent::index($params);
    }

    function update(Array $arr = [])
    {
        parent::update($arr);
    }
}