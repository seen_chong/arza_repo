<?php
use Model\Provider;
use PHP_JWT\JWT;

/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 2/11/16
 * Time: 11:49 AM
 */

class apiController extends siteController
{
    private $token;
    public function __construct()
    {
        parent::__construct();
        // we don't check login and signup header token
        if($this->emagid->route['action'] == 'login' || $this->emagid->route['action'] == 'signup'){
            // DO NOTHING HERE
        } else {
            if(!isset($_SERVER['HTTP_X_ACCESS_TOKEN'])){
                return $this->toJson(['success' => false, 'message' => 'token required'], 401);
            } else {
                $token = $_SERVER['HTTP_X_ACCESS_TOKEN'];

                // start JWT token decryption
                if ($token) {
                    try {
                        $this->token = JWT::decode($token, JWT_SECRET, array('HS256'));
                    } catch (Exception $e) {
                        /*
                         * the token was not able to be decoded.
                         * this is likely because the signature was not able to be verified (tampered token)
                         */
                        return $this->toJson(['success' => false, 'message' => 'token invalid'], 401);
                    }
                } else {
                    /*
                     * No token was able to be extracted from the authorization header
                     */
                    return $this->toJson(['success' => false, 'message' => 'token required'], 400);
                }
            }
        }
    }

    /**
     * POST /api/login [user_email, user_password]
     * JWT encode example
     * $jwt = JWT::encode($data, JWT_SECRET, 'HS512');
     */
    public function login()
    {
        $providerModel = new Provider();
        $userModel = new \Model\User();

        if(!$this->validatePostData(['user_email', 'user_password'])){
            echo $this->toJson(['success' => false, 'message' => 'email and password is required'], 400);
            return null;
        }

        $tokenId = base64_encode(mcrypt_create_iv(32));
        $issuedAt = time();
        $notBefore = $issuedAt;
        $expire = $notBefore + 60*60; // Add 1 hour expiration
        $data = [
            'iat'  => $issuedAt,         // Issued at: time when the token was generated
            'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
            'nbf'  => $notBefore,        // Not before
            'exp'  => $expire,           // Expire
        ];

        if ($providerModel->login($_POST['user_email'], $_POST['user_password'])) {

            $provider = Provider::getItem(null, ['where' => ['email' => $_POST['user_email']]]);
            $data['data'] = [
                'id' => $provider->ide,
                'type' => 'provider'
            ];
            $token = JWT::encode($data, JWT_SECRET);
            return $this->toJson(['success'=>true, 'token' => $token, 'type' => 'provider','id' => $provider->id, 'ide' => $provider->ide]);
        } elseif ($userModel->login($_POST['user_email'], $_POST['user_password'])) {
            $user = \Model\User::getItem(null, ['where' => ['email' => $_POST['user_email']]]);
            //TODO: Add corporate type
            $data['data'] = [
                'id' => $user->ide,
                'type' => 'user'
            ];
            $token = JWT::encode($data, JWT_SECRET);
            return $this->toJson(['success'=>true, 'token' => $token, 'type' => 'user', 'ide' => $user->ide]);
        } else {
            // No user or provider found
            // TODO: probably we want do admin later, but not right now
            return $this->toJson(['success' => false, 'message' => 'email and password does not correct'], 400);
        }
    }

    public function services(){
        $services = \Model\Service::getList();
        if($services){
            $arr = [];
            foreach($services as $service){
                $parent = $service->sub_service != 0 ? \Model\Service::getItem($service->sub_service)->ide:0;
                $arr[$service->ide] = ['name'=>$service->name, 'image'=>S3_URL.$service->featured_image,'description'=>strip_tags($service->description), 'parent'=>$parent];
            }
            $this->toJson(['success'=>true,'data'=>$arr]);
        } else {
            $this->toJson(['success'=>false,'message'=>'There are no services available'], 400);
        }
    }

    public function providersDetails($params)
    {
        $requestMethod = $_SERVER['REQUEST_METHOD'];

        $ide = $params['ide'];

        if(!$ide){
            return $this->toJson(['success' => false, 'message' => 'ide is required'], 400);
        }

        if(!($provider = Provider::getItem(Provider::getId($ide)))){
            return $this->toJson(['success' => false, 'message' => 'invalid ide'], 400);
        }

        if($requestMethod == 'GET'){
            // Start pulling provider information
            $data = $provider->getApiDetails();
            return $this->toJson(['success' => true, 'data' => $data]);
        } elseif($requestMethod == 'POST'){
            // Start updating provider information

            // API allowed update fields
            $allowedUpdateFields = ['first_name', 'last_name', 'email', 'phone'];
            foreach($_POST as $field => $value){
                if(!in_array($field, $allowedUpdateFields)){
                    // The updates is unauthorized, not allowed update
                    return $this->toJson(['success' => false, 'message' => "$field is not allowed update"], 400);
                }
            }

            foreach($_POST as $field => $value){
                $provider->$field = $value;
            }

            if($provider->save()){
                return $this->toJson(['success' => true]);
            } else {
                return $this->toJson(['success' => false, 'message' => 'update failed'], 400);
            }

        } else {
            // We do not support request method
            return $this->toJson(['success' => false, 'message' => "method $requestMethod is not implement yet."], 400);
        }
    }

    // GET /api/sessions/providers/{provider-ide}
    public function providerSessions($param)
    {
        $ide = $param['ide'];
        $sessionList = \Model\SessionHistory::getList(['where'=>['provider_id'=>Provider::getId($ide)]]);
        if(!$sessionList){
            return $this->toJson(['success'=>false,'message'=>'There are no recorded sessions here'], 400);
        } else {
            $data = [];
            foreach($sessionList as $session){
                $data[] = $session->providerApiDetails();
            }

            return $this->toJson(['success'=>true, 'data'=>$data],200);
        }
    }

    // POST /api/sessions/{sesssion-ide}/providers/{provider-ide}/
    public function providerSessions_post($params)
    {
        $ide = $params['prov-ide'];
        $session = $params['sesh-ide'];
        $requestMethod = $_SERVER['REQUEST_METHOD'];

        $sessionHistory = \Model\SessionHistory::getItem(\Model\SessionHistory::getId($session));
        if(!$sessionHistory){
            return $this->toJson(['success'=>false,'message'=>'There is no session here'],404);
        } elseif($sessionHistory->provider_id != Provider::getId($ide)) {
            return $this->toJson(['success'=>false,'message'=>'This is not your session'],400);
        }

        if($requestMethod == 'POST') {
            foreach($_POST as $field=>$value){
                $allowedUpdateFields = ['status'];
                if(!in_array($field, $allowedUpdateFields)){
                    return $this->toJson(['success'=>false,'message'=>"$field cannot be changed"], 400);
                }
            }

            foreach($_POST as $field=>$value){
                $sessionHistory->$field = $value;
            }
            if($sessionHistory->save()){
                return $this->toJson(['success'=>true]);
            } else {
                return $this->toJson(['success'=>false,'message'=>'Session has failed to save'], 400);
            }
        } else {
            return $this->toJson(['success'=>false,'message'=>"$requestMethod is not supported yet"], 400);
        }
    }

    /**
     * POST /api/services/providers/{ide}
     * @param $param
     * @return string|void
     */
    public function providerServices($param)
    {
        $requestMethod = $_SERVER['REQUEST_METHOD'];

        if($requestMethod == 'POST'){
            if($ide = $param['ide']){
                $ideServices = $_POST['services'];
                $providerId = Provider::getId($ide);
                $provider = \Model\Provider_Services::getList(['where'=>"provider_id = ".$providerId]);

                foreach($provider as $prov){
                    $prov->active = 0;
                    $prov->save();
                }
                $services = [];
                foreach($ideServices as $ideserv){
                    $services[] = \Model\Service::getId($ideserv);
                }
                foreach($services as $service){
                    $serv = \Model\Service::getItem($service);
                    if($serv->sub_service != 0 && !in_array($serv->sub_service,$services)){
                        $parent = \Model\Service::getItem($serv->sub_service);
                        $services[] = $parent->id;
                    }
                    $sql = "select * from provider_services where provider_id = {$providerId} and service_id = {$service}";
                    if($provServ = \Model\Provider_Services::getItem(null,['sql'=>$sql])){
                        $provServ->active = 1;
                        $provServ->save();
                    } else {
                        $provServ = new \Model\Provider_Services();
                        $provServ->provider_id = $providerId;
                        $provServ->service_id = $service;
                        $provServ->save();
                    }
                }

//                $sqlService = implode(',',$services);
//                $sql = "select * from provider_services where provider_id = ".Provider::getId($ide)." and service_id in ({$sqlService})";
//                $provider_services = \Model\Provider_Services::getList(['sql'=>$sql]);
//
//                foreach($provider_services as $provider_service){
//                    $provider_service->active = 1;
//                    $provider_service->save();
//                }

                return $this->toJson(['success'=>true, 'message'=>'Services updated']);
            } else {
                return $this->toJson(['success'=>false,'message'=>'update failed'], 400);
            }
        } else {
            return $this->toJson(['success'=>false,'message'=>"$requestMethod is not supported yet"], 400);
        }
    }

    private function validatePostData($fields)
    {
        foreach($fields as $field){
            if(!isset($_POST[$field])){
                return false;
            }
        }

        return true;
    }

    public function toJson($array, $statusCode = 200)
    {
        if($statusCode){
            switch ($statusCode) {
                case 100: $text = 'Continue'; break;
                case 101: $text = 'Switching Protocols'; break;
                case 200: $text = 'OK'; break;
                case 201: $text = 'Created'; break;
                case 202: $text = 'Accepted'; break;
                case 203: $text = 'Non-Authoritative Information'; break;
                case 204: $text = 'No Content'; break;
                case 205: $text = 'Reset Content'; break;
                case 206: $text = 'Partial Content'; break;
                case 300: $text = 'Multiple Choices'; break;
                case 301: $text = 'Moved Permanently'; break;
                case 302: $text = 'Moved Temporarily'; break;
                case 303: $text = 'See Other'; break;
                case 304: $text = 'Not Modified'; break;
                case 305: $text = 'Use Proxy'; break;
                case 400: $text = 'Bad Request'; break;
                case 401: $text = 'Unauthorized'; break;
                case 402: $text = 'Payment Required'; break;
                case 403: $text = 'Forbidden'; break;
                case 404: $text = 'Not Found'; break;
                case 405: $text = 'Method Not Allowed'; break;
                case 406: $text = 'Not Acceptable'; break;
                case 407: $text = 'Proxy Authentication Required'; break;
                case 408: $text = 'Request Time-out'; break;
                case 409: $text = 'Conflict'; break;
                case 410: $text = 'Gone'; break;
                case 411: $text = 'Length Required'; break;
                case 412: $text = 'Precondition Failed'; break;
                case 413: $text = 'Request Entity Too Large'; break;
                case 414: $text = 'Request-URI Too Large'; break;
                case 415: $text = 'Unsupported Media Type'; break;
                case 500: $text = 'Internal Server Error'; break;
                case 501: $text = 'Not Implemented'; break;
                case 502: $text = 'Bad Gateway'; break;
                case 503: $text = 'Service Unavailable'; break;
                case 504: $text = 'Gateway Time-out'; break;
                case 505: $text = 'HTTP Version not supported'; break;
                default:
                    exit('Unknown http status code "' . htmlentities($code) . '"');
                    break;
            }

            $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');

            header($protocol . ' ' . $statusCode . ' ' . $text);
        }

        header('Content-Type: application/json');
        echo json_encode($array);
        exit();
    }
}