<?

class calendarController extends siteController
{
    public function getTimes()
    {
        $provider = \Model\Provider::getItem($_POST['providerId']);
        $date = new \Carbon\Carbon($_POST['date']);

        $times = \Model\Calendar::getTimes($provider, $date);
        echo $this->toJson($times);
    }

    //service -> calendar
    public function setSession(){
        $providerId = $_POST['providerId'];
        $serviceId = $_POST['serviceId'];
        $date = new \Carbon\Carbon($_POST['date']);
        $time = $_POST['time'];

        if($providerId && $date && $time){
            $session = isset($_SESSION['bookSession']) ? json_decode($_SESSION['bookSession'],true): [];
            $session[$providerId] = ['date'=>$date->toDateTimeString(), 'time'=>$time,'serviceId'=>$serviceId];
            $_SESSION['bookSession'] = json_encode($session);
            echo $this->toJson(['status'=>'success']);
        } else {
            echo $this->toJson(['status'=>'failed','message'=>'Missing data']);
        }
    }

    //calendar -> checkout
    public function addSession(){
        if(!isset($this->viewData->user) || $this->viewData->user == null){
            echo $this->toJson(['status'=>'login_fail']);
        } else {
            $service = $_POST['activity'];
            $credits = $_POST['size'];
            $location = $_POST['location'];
            $date = $_POST['date'];
            $time = $_POST['time'];
            $user = $this->viewData->user->id;
            $provider = $_POST['provider'];

            // TODO: Maybe we want multiple complete session instead of overwriting it?
            $_SESSION['completeSession'] = json_encode(['user'=>$user,'service'=>$service,'credits'=>$credits,'location'=>$location,'date'=>$date,'time'=>$time,'provider'=>$provider]);
            echo $this->toJson(['status'=>'success']);
        }
    }
}

?>