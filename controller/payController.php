<?php

class payController extends siteController
{

    public function index_post(){
        $donate = \Model\Donation::loadFromPost();
        if($this->viewData->user){
            $donate->user_id = $this->viewData->user->id;
        }
        $d = \Model\Donate::getItem($donate->type);
        if($d->is_default == 1){
            $description = $donate->designation;
        }else{
            $description = $d->name;
        }
        $donate->status = \Model\Donation::$status[3];
        $donate->ref_num = generateToken();
        if($donate->save()){
            if($donate->monthly == 1){
                $des = $description . ' Monthly';
                $myPlan = new \PayPal\Api\Plan();
                $value = $donate->amount;
                $myPlan->setName('Donate to'.$description)->setDescription($des)->setType('fixed');
                $paymentDefinition = new \PayPal\Api\PaymentDefinition();
                $paymentDefinition->setName('Monthly Donation')->setType('REGULAR')->setFrequency('Month')->setFrequencyInterval("1")->setCycles("12")->setAmount(new \PayPal\Api\Currency(array('value' => $value, 'currency' => 'USD')));
                $merchantPreferences = new \PayPal\Api\MerchantPreferences();
                $apiContext = new \PayPal\Rest\ApiContext(new \PayPal\Auth\OAuthTokenCredential(PAYPAL_CLIENT_ID,PAYPAL_CLIENT_SECRET));
                $apiContext->setConfig(array('mode' => 'live'));

//                $merchantPreferences->setReturnUrl("http://arzafront.app/donate/confirmation/$donate->ref_num?success=true")
//                    ->setCancelUrl("http://arzafront.app/donate/confirmation/$donate->ref_num?success=false")
//                    ->setAutoBillAmount("yes")
//                    ->setInitialFailAmountAction("CONTINUE")
//                    ->setMaxFailAttempts("0")
//                    ->setSetupFee(new \PayPal\Api\Currency(array('value' => 1, 'currency' => 'USD')));
                //TODO: Change the Url to live site
                $merchantPreferences->setReturnUrl("http://arza.org/donate/confirmation/$donate->ref_num?success=true")
                    ->setCancelUrl("http://arza.org/donate/confirmation/$donate->ref_num?success=false")
                    ->setAutoBillAmount("yes")
                    ->setInitialFailAmountAction("CONTINUE")
                    ->setMaxFailAttempts("0")
                    ->setSetupFee(new \PayPal\Api\Currency(array('value' => 1, 'currency' => 'USD')));

                $myPlan->setPaymentDefinitions(array($paymentDefinition));
                $myPlan->setMerchantPreferences($merchantPreferences)->create($apiContext);
                if (strtolower($myPlan->getState()) == 'created') {
                    $patch = new \PayPal\Api\Patch();
                    $value = new \PayPal\Common\PayPalModel('{
                    "state":"ACTIVE"
                    }');
                    $patch->setOp('replace')->setPath('/')->setValue($value);
                    $patchRequest = new \PayPal\Api\PatchRequest();
                    $patchRequest->addPatch($patch);
                    $myPlan->update($patchRequest, $apiContext);
                    $myPlan = \PayPal\Api\Plan::get($myPlan->getId(), $apiContext);
                    if (strtolower($myPlan->getState()) == 'active') {
                        $agreement = new \PayPal\Api\Agreement();
                        $tomorrow = strtotime('+1 day');
                        $date = date("Y-m-d", $tomorrow).'T'. date("H:i:s", $tomorrow).'Z';
                        $agreement->setName('Monthly Donate Agreement')->setDescription($des)->setStartDate($date);
                        $plan = new \PayPal\Api\Plan();
                        $plan->setId($myPlan->getId());
                        $agreement->setPlan($plan);
                        $payer = new \PayPal\Api\Payer();
                        $payer->setPaymentMethod('paypal');
                        $agreement->setPayer($payer)->create($apiContext);

                        if($agreement->getApprovalLink()!=''){
                            redirect($agreement->getApprovalLink());
                        }else{
                            $n = new \Notification\ErrorHandler('Failed to donate');
                            $_SESSION["notification"] = serialize($n);
                            redirect(SITE_URL . 'donation/' .\Model\Donate::getItem($donate->type)->slug);
                        }

                    }else{
                        $n = new \Notification\ErrorHandler('Failed to donate');
                        $_SESSION["notification"] = serialize($n);
                        redirect(SITE_URL . 'donation/' .\Model\Donate::getItem($donate->type)->slug);
                    }

                } else {
                    $n = new \Notification\ErrorHandler('Failed to donate');
                    $_SESSION["notification"] = serialize($n);
                    redirect(SITE_URL . 'donation/' .\Model\Donate::getItem($donate->type)->slug);
                }

            }else {
                $PItems = new \PayPal\Api\ItemList();
                $PItem = new \PayPal\Api\Item();
                $PItem->setName('Donate to'.$description)->setPrice($donate->amount)->setQuantity(1)->setCurrency('USD');
                $PItems->setItems(array($PItem));
                $apiContext = new \PayPal\Rest\ApiContext(new \PayPal\Auth\OAuthTokenCredential(PAYPAL_CLIENT_ID, PAYPAL_CLIENT_SECRET));

                $apiContext->setConfig(array('mode' => 'live'));
                $payer = new \PayPal\Api\Payer();
                $payer->setPaymentMethod('paypal');
                $PAmount = new \PayPal\Api\Amount();
                $PAmount->setTotal($donate->amount)->setCurrency('USD');
                $PTransaction = new \PayPal\Api\Transaction();
                $PTransaction->setAmount($PAmount)->setDescription($description);
                $PTransArr[] = $PTransaction;

                $pPRedirectUrl = new \PayPal\Api\RedirectUrls();
//                $pPRedirectUrl->setReturnUrl("http://arzafront.app/donate/confirmation/$donate->ref_num")->setCancelUrl('http://arzafront.app/donate');//TODO:TODO: Change the Url to live site
                $pPRedirectUrl->setReturnUrl("http://arza.org/donate/confirmation/$donate->ref_num")->setCancelUrl('http://arza.org/donate');

                $payment = new \PayPal\Api\Payment();

                $payment = $payment->setIntent('sale')->setPayer($payer)->setTransactions($PTransArr)->setRedirectUrls($pPRedirectUrl)->create($apiContext);

                if (strtolower($payment->getState()) == 'created') {
                    $redirect = $payment->getLink('approval_url');
                    redirect($redirect);
                } else {
                    $donate->status = \Model\Donation::$status[2];
                    $donate->save();
                    $n = new \Notification\ErrorHandler($payment->getFailureReason());
                    $_SESSION['notification'] = serialize($n);
                    redirect(SITE_URL . 'donation/' .\Model\Donate::getItem($donate->type)->slug);
                }

            }
        }else {
            $n = new \Notification\ErrorHandler('Failed to donate');
            $_SESSION["notification"] = serialize($n);
            redirect(SITE_URL . 'donation/' .\Model\Donate::getItem($donate->type)->slug);

        }
    }


}

