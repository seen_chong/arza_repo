<?php

class joinController extends siteController
{

    public function index(Array $params = [])
    {
        $this->viewData->memberType= \Model\Member_Type::getList();
        $this->viewData->parentType = \Model\Member_Type::getParentList();
//        $this->viewData->ebooks = \Model\Ebook::getList();
        $this->viewData->events = \Model\Event::getLiveEvents();
        $this->loadView($this->viewData);
    }
    public function buildForm(){
        $id = $_POST['id'];
        $member = \Model\Member_Type::getItem($id);
        $joinAmount_html='';
        $amount = 0;
        if($member->donation_type == 1){
            $amount = $member->amount;
            $joinAmount_html.='<div class="joinNewRabbi"><p>'.$member->name.':</p><br>'.
                '<input type="button" class="donateNew" value="'.$member->amount.'"></div>';
        }elseif ($member->donation_type == 2){
            $joinAmount_html.='<div class="joinAmountSelect"><p>Select an Amount</p><br>';
            $value = $member->minimum;
            $interval = $member->amount_interval;
            $joinAmount_html.= '<input type="button" class="donateAmount" value="'.$value.'">';
            for($i=0;$i<6;$i++){
                $value = $value+$interval;
                $joinAmount_html.='<input type="button" class="donateAmount" value="'.$value.'">';
            }

            $joinAmount_html.='<input type="number" min="1.00" step="1" class="otherAmount" name="manualAmount"
                                   placeholder="Other Amount"></div>';
        }elseif($member->donation_type == 3){
            $amount = -1;
            $joinAmount_html.='<p class="submitForm"><input value="Sign up" id="freeSignup" class="memberSignup" readonly></p>';
        }
        $data = [

            'joinAmount_html'=>$joinAmount_html,
            'amount'=>$amount,
            'type'=>$member->id

        ];
        $this->toJson($data);
    }
    public function career_post(){
        $obj = new \Model\Applicant($_POST);
        $allowed =  array('doc','docx' ,'pdf');
        $filename = $_FILES['resume']['name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!in_array($ext,$allowed) ) {
            $n = new \Notification\ErrorHandler('Invalid Resume Format.');
            $_SESSION["notification"] = serialize($n);
        }else {
            foreach ($_FILES as $fileType => $file) {
                if ($file['error'] == 0) {
                    $image = new \Emagid\Image();
                    $image->upload($_FILES[$fileType], UPLOAD_PATH . 'applicants' . DS);
                    $obj->$fileType = $image->fileName;
                }
            }
            if ($obj->save()) {
                $n = new \Notification\MessageHandler('We received your application.');
                $_SESSION["notification"] = serialize($n);
            } else {
                $n = new \Notification\ErrorHandler('You already applied.');
                $_SESSION["notification"] = serialize($n);
            }
        }
        redirect(SITE_URL.'join');
    }
    public function showDetails(){
        $id = $_POST['id'];
        $type = \Model\Member_Type::getItem($id);
        if($type){
            $description = $type->description;
            $child = \Model\Member_Type::getSubList($id);
            $showChild = empty($child)? false:true;
            $this->toJson(['status'=>'success','description'=>$description,'child'=>$child,'showChild'=>$showChild]);
        }else{
            $this->toJson(['status'=>'fail']);
        }


    }
    public function showSubDetails(){
        $id = $_POST['id'];
        $type = \Model\Member_Type::getItem($id);
        if($type){
            $description = $type->description;

            $this->toJson(['status'=>'success','description'=>$description]);
        }else{
            $this->toJson(['status'=>'fail']);
        }


    }

}