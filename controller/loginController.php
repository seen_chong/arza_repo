<?php

class loginController extends siteController {

    public function signin_post(Array $params = []){
//        $uri = isset($_POST['uri'])&&($_POST['uri'])? $_POST['uri']:SITE_URL;
        if(\Model\User::login($_POST['email'], $_POST['password'])){
            $user = \Model\User::getItem(null,['where'=>"email = '".$_POST['email']."'"]);
            $member_type = \Model\Member_Type::getParent($user->subtype)->name;
            $url = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $member_type)));
            redirect(SITE_URL.'contents/'.$url);
        }else{
            redirect(SITE_URL);
        }

    }

    public function register_post(Array $params = [])
    {
        $user = \Model\User::loadFromPost();
        $hash = \Emagid\Core\Membership::hash($user->password);
        $user->password = $hash['password'];
        $user->hash = $hash['salt'];
        global $emagid;
        $emagid->email->from->email = 'report@emagid.com';

        if ($user->save()) {
            $customerRole = new \Model\User_Roles();
            $customerRole->role_id = 2;
            $customerRole->user_id = $user->id;
            $customerRole->save();


            //donating needed member
            if(isset($_POST['amount'])&&$_POST['amount']) {

                $donation = \Model\Donation::loadFromPost();
                $donation->status = \Model\Donation::$status[3];
                $donation->user_id = $user->id;
                $donation->ref_num = generateToken();
                $donation->type = 0;
                if ($donation->save()) {
                    //start Paypal transaction
                    $PItems = new \PayPal\Api\ItemList();
                    $PItem = new \PayPal\Api\Item();
                    $PItem->setName('Donate to Arza')->setPrice($donation->amount)->setQuantity(1)->setCurrency('USD');
                    $PItems->setItems(array($PItem));
                    $apiContext = new \PayPal\Rest\ApiContext(new \PayPal\Auth\OAuthTokenCredential(PAYPAL_CLIENT_ID,PAYPAL_CLIENT_SECRET));
                    $apiContext->setConfig(array('mode' => 'live'));


                    $payer = new \PayPal\Api\Payer(); $payer->setPaymentMethod('paypal');
                    $PAmount = new \PayPal\Api\Amount(); $PAmount->setTotal(number_format($donation->amount,2))->setCurrency('USD');
                    $PTransaction = new \PayPal\Api\Transaction(); $PTransaction->setAmount($PAmount)->setDescription("Donations");
                    $PTransArr[] = $PTransaction;

//                    //Live
//                    $pPRedirectUrl = new \PayPal\Api\RedirectUrls(); $pPRedirectUrl->setReturnUrl("https://beta.arza.org/donate/confirmation/$donation->ref_num")->setCancelUrl('https://beta.arza.org/donate');
                    //TODO: Change the Url to live site
                $pPRedirectUrl = new \PayPal\Api\RedirectUrls(); $pPRedirectUrl->setReturnUrl("http://arza.org/donate/confirmation/$donation->ref_num")->setCancelUrl('http://arza.org/donate');

                    $payment = new \PayPal\Api\Payment();

                    $payment = $payment->setIntent('sale')->setPayer($payer)->setTransactions($PTransArr)->setRedirectUrls($pPRedirectUrl)->create($apiContext);

                    if(strtolower($payment->getState()) == 'created'){
                        $redirect = $payment->getLink('approval_url');
                        $user->donation_amount = $donation->amount;
                        $user->save();
                        redirect($redirect);
                    }else {
                        $donation->status = \Model\Order::$status[2];
                        $donation->save();
                        \Model\User::delete($user->id);
                        $n = new \Notification\ErrorHandler($payment->getFailureReason());
                        $_SESSION['notification'] = serialize($n);
                        redirect(SITE_URL.'join');
                    }

                } else {
                    \Model\User::delete($user->id);
                    $n = new \Notification\ErrorHandler($donation->errors);
                    $_SESSION['notification'] = serialize($n);
                    redirect(SITE_URL.'join');
                }
            }else{
                $date = \Carbon\Carbon::now();
                $date = $date->format('F d, Y');
                global $emagid;
                $emagid->email->from->email = 'report@emagid.com';
                $email = new \Emagid\Email();
                $email->addTo($user->email);
                $confirm_email = \Model\Confirm_Email::getItem(null,[['where'=>"name = 'Register Confirmation'"]]);
                if($confirm_email){
                    $temp = \Model\Confirm_Email::$dynamicElements;
                    $real = [$date, $user->full_name(), 0];
                    $emailTemplate = str_replace($temp,$real,$confirm_email->email_template);
                    $subject = str_replace($temp,$real,$confirm_email->subject);
                    $email->subject(trim($subject));
                    $email->body = trim($emailTemplate);

                }else{
                    $email->subject('Welcome, ' . $user->full_name() . '!');
                    $email->body = '<p><a href="https://arza.org"><img src="http://beta.arza.org/content/frontend/assets/img/navyLogoText.png"/></a></p>'
                        . '<p><b>Dear ' . $user->full_name() . '</b></p>'
                        . '<p>Welcome to <a href="arza.org">Arza.org</a></p>'
                        . '<p>If you have any questions about your account or any other matter, please feel free to contact us by phone at 212.650.4289.</p>'
                        . '<p>Thanks again!</p>';
                }
//                $email->send();
                $n = new \Notification\MessageHandler('Your account was successfully created.');
                $_SESSION["notification"] = serialize($n);
                $this->signin_post($params);
            }
        } else {
            $n = new \Notification\ErrorHandler($user->errors);
            $_SESSION["notification"] = serialize($n);
            redirect(SITE_URL.'join');
        };


    }
    function getMemberType($subType){
        if(in_array($subType,["1","2"])){
            return  1;
        }else if(in_array($subType,["3","4"])){
            return  2;
        }else{
            return  3;
        }
    }
    public function logout(){
        \Emagid\Core\Membership::destroyAuthenticationSession();
        redirect(SITE_URL);
    }

    public function passwordreminder(Array $params = []){
        $this->loadView($this->viewData);
    }
    public function passwordreminder_post(Array $params = []){
        $email_origin = isset($_POST['email'])&&($_POST['email'])? $_POST['email']:'';
        $email = strtolower($email_origin);
        $user = \Model\User::getList(['where'=>"lower(email) ='".$email."'",'limit'=>1]);

        if(count($user)>=1){
            $user = $user[0];
            $message = "Your password reset link send to ".$email_origin.'.';

            $token = md5(uniqid(rand(), true));
            $user->token = $token;
            $user->save();
            $link = 'arza.org/login/reset?token='.$token;

//            mandrill way
//            $email = new \Email\MailMaster();
//            $mergeFields = ['RESET_LINK'=>$link];
//            $email->setTo(['email' => $user->email, 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('arza-reset-password')->send();

            global $emagid;
            $emagid->email->from->email = 'report@emagid.com';
            $email = new \Emagid\Email();
            $email->addTo($user->email);
            $email->subject('Forget password');
            $email->body = '<p><b>Dear ' . $user->full_name() . '</b></p>'
                .'<p>We received a request to change your password on arza.org.</p>'
                .'<p>Click the link below to set your new password</p>'
                .'<p><a href="'.$link.'">'.$link.'</a></p>'
                .'<p>If you do not want to change your password, please ignore this email</p>'
                .'<p>Thanks!</p>';
//            dd($email);
            $email->send();

        }else{
            $message = "Account not found.";
        }

        echo json_encode(['message'=>$message]);
    }
    public function reset(Array $params = []){
        $token=$_GET['token'];
        $user = \Model\User::getList(["where"=>"token='".$token."'"]);
        if(count($user)>=1){
            $user = $user[0];
            $this->viewData->id = $user->id;
            $user->token = md5(uniqid(rand(), true));
            $user->save();
            $this->loadView($this->viewData);
        }else {
            $n = new \Notification\ErrorHandler('Link expired');
            $_SESSION["notification"] = serialize($n);
            redirect(SITE_URL.'login');
        }
    }
    public function reset_post(Array $params = []){
        $user = \Model\User::getItem($_POST['id']);
        $hash = \Emagid\Core\Membership::hash($_POST['password']);
        $user->password = $hash['password'];
        $user->hash = $hash['salt'];
        $user->token = md5(uniqid(rand(), true));
        if($user->save()){
            $n = new \Notification\ErrorHandler('Change success');
            $_SESSION["notification"] = serialize($n);
            redirect(SITE_URL.'login');
        }else{
            $n = new \Notification\ErrorHandler('Something went wrong, please try again');
            $_SESSION["notification"] = serialize($n);
            redirect(SITE_URL.'login/passwordreminder');
        }

    }

}