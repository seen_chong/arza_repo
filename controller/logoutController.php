<?php

class logoutController extends siteController
{

    public function index(Array $params = [])
    {
        \Emagid\Core\Membership::destroyAuthenticationSession();
        unset($_SESSION['coupon']);
        redirect(SITE_URL);
    }

}