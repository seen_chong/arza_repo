<?php

class signupController extends siteController
{

    function __construct()
    {
        parent::__construct();

    }

    public function index(Array $params = [])
    {
        if (isset($_GET['r'])) {
            setcookie('ref_u', $_GET['r'], time() + 60 * 60 * 24 * 100, "/");
        }
        $this->loadView($this->viewData);
    }

}