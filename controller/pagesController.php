<?php

class pagesController extends siteController
{

    public function index(Array $params = [])
    {
        redirect(SITE_URL);
    }

    public function page(Array $params = [])
    {
        $slug = (isset($params['page_slug'])) ? $params['page_slug'] : '';
        $this->viewData->otherPages = \Model\Page::getList(['where' => "page.slug not like '".$slug."'"]);
        $this->viewData->page = \Model\Page::getItem(null, ['where' => 'page.slug like \'' . $slug . '\'']);
        $this->loadView($this->viewData);
    }

}