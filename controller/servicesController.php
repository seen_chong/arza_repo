<?php

class servicesController extends siteController
{

    public function service(Array $params = [])
    {
        $this->viewData->service = \Model\Service::getItem(null, ['where' => "slug = '" . $params['service_slug'] . "'"]);
        $this->viewData->posts = \Model\Blog::getList(["where" => "status='1' and featured='1' and service='" . $this->viewData->service->id . "'", "limit" => "2"]);
        $id = $this->viewData->service->id;

        $sql_providers = "SELECT DISTINCT ON (provider_id) * FROM provider_services WHERE active='1' and service_id = '$id'";
        $providers_list = \Model\Provider_Services::getList(['sql' => $sql_providers]);
        foreach ($providers_list as $provider_id) {
            if (\Model\Provider::getItem(NULL, ['where' => "id = '" . $provider_id->provider_id . "' and authorized='TRUE'"])) {
                $this->viewData->provider_list[] = \Model\Provider::getItem(NULL, ['where' => "id = '" . $provider_id->provider_id . "' and authorized='TRUE'"]);
            }
        }

        shuffle($this->viewData->provider_list);

        foreach($this->viewData->provider_list as $index => $provider){
            if($provider->doesAvailable()){
                unset($this->viewData->provider_list[$index]);
                $this->viewData->featuredProvider = $provider;
                break;
            }
        }

        /**
         * If not any provider setup calendar, we default show the first random provider
         */
        if(!isset($this->viewData->featuredProvider)){
            $this->viewData->featuredProvider = $this->viewData->provider_list[0];
            array_shift($this->viewData->provider_list);
        }

        if ($this->viewData->service->sub_service == 0) {
            $sql_sub = "SELECT * FROM service WHERE active='1' and sub_service = '$id' ORDER BY RANDOM() LIMIT 3";
            $this->viewData->sub_services = \Model\Service::getList(['sql' => $sql_sub]);
        }

        $this->configs['Meta Title'] = $this->viewData->service->name . " with Wellns";
        $this->loadView($this->viewData);
    }

    public function browse(Array $params = [])
    {           
        $this->loadView($this->viewData);
   
    }

    public function switchProviders(){
        $usedProviders = $_POST['list'];
        $services = \Model\Provider_Services::getServicesByServiceSlug($_POST['service']);
        $providerList = \Model\Provider_Services::getAvailableProvidersByServices($services);
        $jsonProviderList = [];
        foreach($providerList as $provider){
            if(!in_array($provider->id, $usedProviders)) {
                $jsonProviderList[] = ['id' => $provider->id, 'name' => $provider->full_name(), 'url' => "/provider/profile/$provider->id", 'image' => $provider->photo];
            }
        }
//        shuffle($jsonProviderList);
        echo $this->toJson($jsonProviderList);
    }

}