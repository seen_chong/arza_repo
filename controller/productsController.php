<?php

class productsController extends siteController
{

    public function product(Array $params = [])
    {
        $sql = 'SELECT DISTINCT(category.*) FROM category INNER JOIN product_categories pc ON (category.id = pc.category_id) ';
        $sql .= 'INNER JOIN product p ON (pc.product_id = p.id) ';
        $sql .= 'WHERE category.active = 1 AND pc.active = 1 AND p.active = 1 AND category.parent_category = 0 ';
        $sql .= 'ORDER BY category.display_order ASC';

        $this->viewData->categories = \Model\Category::getList(['sql' => $sql]);

        foreach ($this->viewData->categories as $category) {
            $sql = 'SELECT DISTINCT(category.*) FROM category INNER JOIN product_categories pc ON (category.id = pc.category_id) ';
            $sql .= 'INNER JOIN product p ON (pc.product_id = p.id) ';
            $sql .= 'WHERE category.parent_category = ' . $category->id . ' AND category.active = 1 AND pc.active = 1 AND p.active = 1 ';
            $sql .= 'ORDER BY category.display_order ASC';

            $category->children = \Model\Category::getList(['sql' => $sql]);
        }

        $this->viewData->product = \Model\Product::getItem(null, ['where' => " slug = '" . $params['slug'] . "'"]);
        if (is_null($this->viewData->product->colors)) {
            $this->viewData->product->colors = [];
        } else {
            $this->viewData->product->colors = explode(',', $this->viewData->product->colors);
        }
        if (is_null($this->viewData->product->sizes)) {
            $this->viewData->product->sizes = [];
        } else {
            $this->viewData->product->sizes = explode(',', $this->viewData->product->sizes);
        }

        $similar_products = [];
        $categories = \Model\Product_Category::get_product_categories($this->viewData->product->id, 10);
        foreach ($categories as $category) {
            $similar_products = array_merge($similar_products, \Model\Product_Category::get_category_products($category, 10, 'and product_id <> ' . $this->viewData->product->id));
        }

        $similar_products = array_merge($similar_products, \Model\Product::getList(['where' => 'brand = ' . $this->viewData->product->brand . ' and id <> ' . $this->viewData->product->id, 'limit' => 10]));
        $similar_products = array_unique($similar_products, SORT_REGULAR);
        shuffle($similar_products);
        $similar_products = array_slice($similar_products, 0, 4);

        $this->viewData->similar_products = $similar_products;

        $this->loadView('index', $this->viewData);
    }

}