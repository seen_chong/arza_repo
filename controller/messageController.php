<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 1/5/16
 * Time: 12:58 PM
 */

use Model\Message;
use Model\User;

class messageController extends siteController
{


    /**
     * AJAX call to send inbox message
     * from always be logged in user
     * to should have to_type and to_id
     * required subject and text too.
     */
    public function send()
    {
        $from = $this->viewData->user;

        if (!$_POST['subject'] || !$_POST['message'] || !$_POST['to_type'] || !$_POST['to_id']) {
            echo $this->tojson(['status' => 'error', 'message' => 'All fields required']);
            return null;
        }
        $toType = $_POST['to_type'];
        $toId = $_POST['to_id'];

        if (strtoupper($toType) == 'PROVIDER') {
            $to = \Model\Provider::getItem($toId);
        } elseif (strtoupper($toType) == 'USER') {
            $to = User::getItem($toId);
        } else {
            throw new \Exception("Can't find message to type $toType");
        }

        $message = Message::send($from, $to, $_POST['subject'], $_POST['message']);

        \Email\Message::send($to->phone, \Email\Message::getMessageContent('new-inbox-message', ['NAME' => $from->full_name()]));

        echo $this->tojson(['status' => 'success', 'message_id' => $message->subject_id]);

        return null;
    }

    public function markRead()
    {
        if ($subject = \Model\Subject::getItem($_POST['subject'])) {
            $subject->markAllMessageRead($this->viewData->user);

            echo $this->tojson(['status' => 'success']);
            return null;
        }

        echo $this->tojson(['status' => 'error']);

        return null;
    }
}