<?php

class landingController extends siteController
{

    function __construct()
    {
        parent::__construct();

        $this->template = false;
    }

    public function landing_page()
    {
        $this->loadView($this->viewData);
    }

    public function index(Array $params = [])
    {
        if (isset($_SESSION['joined'])) {
            if ($_SESSION['joined']) {
                $this->viewData->registered = true;
            }
            unset($_SESSION['joined']);
        }

        $this->loadView($this->viewData);
    }

    public function email_list_post(Array $params = [])
    {
        $email_list = \Model\Email_List::loadFromPost();
        $email_list->save();

        $_SESSION['joined'] = true;

        redirect(SITE_URL . 'landing');
    }

}