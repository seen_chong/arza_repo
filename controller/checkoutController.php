<?php

use Email\MailMaster;
define("APPROVED", 1);
define("DECLINED", 2);
define("ERROR", 3);

class gwapi {

// Initial Setting Functions

    function setLogin($username, $password) {
        $this->login['username'] = $username;
        $this->login['password'] = $password;
    }

    function setOrder($orderid,
                      $orderdescription,
                      $tax,
                      $shipping,
                      $ponumber,
                      $ipaddress) {
        $this->order['orderid']          = $orderid;
        $this->order['orderdescription'] = $orderdescription;
        $this->order['tax']              = $tax;
        $this->order['shipping']         = $shipping;
        $this->order['ponumber']         = $ponumber;
        $this->order['ipaddress']        = $ipaddress;
    }

    function setBilling($firstname,
                        $lastname,
                        $company,
                        $address1,
                        $address2,
                        $city,
                        $state,
                        $zip,
                        $country,
                        $phone,
                        $fax,
                        $email,
                        $website) {
        $this->billing['firstname'] = $firstname;
        $this->billing['lastname']  = $lastname;
        $this->billing['company']   = $company;
        $this->billing['address1']  = $address1;
        $this->billing['address2']  = $address2;
        $this->billing['city']      = $city;
        $this->billing['state']     = $state;
        $this->billing['zip']       = $zip;
        $this->billing['country']   = $country;
        $this->billing['phone']     = $phone;
        $this->billing['fax']       = $fax;
        $this->billing['email']     = $email;
        $this->billing['website']   = $website;
    }

    function setShipping($firstname,
                         $lastname,
                         $company,
                         $address1,
                         $address2,
                         $city,
                         $state,
                         $zip,
                         $country,
                         $email) {
        $this->shipping['firstname'] = $firstname;
        $this->shipping['lastname']  = $lastname;
        $this->shipping['company']   = $company;
        $this->shipping['address1']  = $address1;
        $this->shipping['address2']  = $address2;
        $this->shipping['city']      = $city;
        $this->shipping['state']     = $state;
        $this->shipping['zip']       = $zip;
        $this->shipping['country']   = $country;
        $this->shipping['email']     = $email;
    }

    // Transaction Functions

    function doSale($amount, $ccnumber, $ccexp, $cvv="") {

        $query  = "";
        // Login Information
        $query .= "username=" . urlencode($this->login['username']) . "&";
        $query .= "password=" . urlencode($this->login['password']) . "&";
        // Sales Information
        $query .= "ccnumber=" . urlencode($ccnumber) . "&";
        $query .= "ccexp=" . urlencode($ccexp) . "&";
        $query .= "amount=" . urlencode(number_format($amount,2,".","")) . "&";
        $query .= "cvv=" . urlencode($cvv) . "&";
        // Order Information
        $query .= "ipaddress=" . urlencode($this->order['ipaddress']) . "&";
        $query .= "orderid=" . urlencode($this->order['orderid']) . "&";
        $query .= "orderdescription=" . urlencode($this->order['orderdescription']) . "&";
        $query .= "tax=" . urlencode(number_format($this->order['tax'],2,".","")) . "&";
        $query .= "shipping=" . urlencode(number_format($this->order['shipping'],2,".","")) . "&";
        $query .= "ponumber=" . urlencode($this->order['ponumber']) . "&";
        // Billing Information
        $query .= "firstname=" . urlencode($this->billing['firstname']) . "&";
        $query .= "lastname=" . urlencode($this->billing['lastname']) . "&";
        $query .= "company=" . urlencode($this->billing['company']) . "&";
        $query .= "address1=" . urlencode($this->billing['address1']) . "&";
        $query .= "address2=" . urlencode($this->billing['address2']) . "&";
        $query .= "city=" . urlencode($this->billing['city']) . "&";
        $query .= "state=" . urlencode($this->billing['state']) . "&";
        $query .= "zip=" . urlencode($this->billing['zip']) . "&";
        $query .= "country=" . urlencode($this->billing['country']) . "&";
        $query .= "phone=" . urlencode($this->billing['phone']) . "&";
        $query .= "fax=" . urlencode($this->billing['fax']) . "&";
        $query .= "email=" . urlencode($this->billing['email']) . "&";
        $query .= "website=" . urlencode($this->billing['website']) . "&";
        // Shipping Information
        $query .= "shipping_firstname=" . urlencode($this->shipping['firstname']) . "&";
        $query .= "shipping_lastname=" . urlencode($this->shipping['lastname']) . "&";
        $query .= "shipping_company=" . urlencode($this->shipping['company']) . "&";
        $query .= "shipping_address1=" . urlencode($this->shipping['address1']) . "&";
        $query .= "shipping_address2=" . urlencode($this->shipping['address2']) . "&";
        $query .= "shipping_city=" . urlencode($this->shipping['city']) . "&";
        $query .= "shipping_state=" . urlencode($this->shipping['state']) . "&";
        $query .= "shipping_zip=" . urlencode($this->shipping['zip']) . "&";
        $query .= "shipping_country=" . urlencode($this->shipping['country']) . "&";
        $query .= "shipping_email=" . urlencode($this->shipping['email']) . "&";
        $query .= "type=sale";
        return $this->_doPost($query);
    }

    function doAuth($amount, $ccnumber, $ccexp, $cvv="") {

        $query  = "";
        // Login Information
        $query .= "username=" . urlencode($this->login['username']) . "&";
        $query .= "password=" . urlencode($this->login['password']) . "&";
        // Sales Information
        $query .= "ccnumber=" . urlencode($ccnumber) . "&";
        $query .= "ccexp=" . urlencode($ccexp) . "&";
        $query .= "amount=" . urlencode(number_format($amount,2,".","")) . "&";
        $query .= "cvv=" . urlencode($cvv) . "&";
        // Order Information
        $query .= "ipaddress=" . urlencode($this->order['ipaddress']) . "&";
        $query .= "orderid=" . urlencode($this->order['orderid']) . "&";
        $query .= "orderdescription=" . urlencode($this->order['orderdescription']) . "&";
        $query .= "tax=" . urlencode(number_format($this->order['tax'],2,".","")) . "&";
        $query .= "shipping=" . urlencode(number_format($this->order['shipping'],2,".","")) . "&";
        $query .= "ponumber=" . urlencode($this->order['ponumber']) . "&";
        // Billing Information
        $query .= "firstname=" . urlencode($this->billing['firstname']) . "&";
        $query .= "lastname=" . urlencode($this->billing['lastname']) . "&";
        $query .= "company=" . urlencode($this->billing['company']) . "&";
        $query .= "address1=" . urlencode($this->billing['address1']) . "&";
        $query .= "address2=" . urlencode($this->billing['address2']) . "&";
        $query .= "city=" . urlencode($this->billing['city']) . "&";
        $query .= "state=" . urlencode($this->billing['state']) . "&";
        $query .= "zip=" . urlencode($this->billing['zip']) . "&";
        $query .= "country=" . urlencode($this->billing['country']) . "&";
        $query .= "phone=" . urlencode($this->billing['phone']) . "&";
        $query .= "fax=" . urlencode($this->billing['fax']) . "&";
        $query .= "email=" . urlencode($this->billing['email']) . "&";
        $query .= "website=" . urlencode($this->billing['website']) . "&";
        // Shipping Information
        $query .= "shipping_firstname=" . urlencode($this->shipping['firstname']) . "&";
        $query .= "shipping_lastname=" . urlencode($this->shipping['lastname']) . "&";
        $query .= "shipping_company=" . urlencode($this->shipping['company']) . "&";
        $query .= "shipping_address1=" . urlencode($this->shipping['address1']) . "&";
        $query .= "shipping_address2=" . urlencode($this->shipping['address2']) . "&";
        $query .= "shipping_city=" . urlencode($this->shipping['city']) . "&";
        $query .= "shipping_state=" . urlencode($this->shipping['state']) . "&";
        $query .= "shipping_zip=" . urlencode($this->shipping['zip']) . "&";
        $query .= "shipping_country=" . urlencode($this->shipping['country']) . "&";
        $query .= "shipping_email=" . urlencode($this->shipping['email']) . "&";
        $query .= "type=auth";
        return $this->_doPost($query);
    }

    function doCredit($amount, $ccnumber, $ccexp) {

        $query  = "";
        // Login Information
        $query .= "username=" . urlencode($this->login['username']) . "&";
        $query .= "password=" . urlencode($this->login['password']) . "&";
        // Sales Information
        $query .= "ccnumber=" . urlencode($ccnumber) . "&";
        $query .= "ccexp=" . urlencode($ccexp) . "&";
        $query .= "amount=" . urlencode(number_format($amount,2,".","")) . "&";
        // Order Information
        $query .= "ipaddress=" . urlencode($this->order['ipaddress']) . "&";
        $query .= "orderid=" . urlencode($this->order['orderid']) . "&";
        $query .= "orderdescription=" . urlencode($this->order['orderdescription']) . "&";
        $query .= "tax=" . urlencode(number_format($this->order['tax'],2,".","")) . "&";
        $query .= "shipping=" . urlencode(number_format($this->order['shipping'],2,".","")) . "&";
        $query .= "ponumber=" . urlencode($this->order['ponumber']) . "&";
        // Billing Information
        $query .= "firstname=" . urlencode($this->billing['firstname']) . "&";
        $query .= "lastname=" . urlencode($this->billing['lastname']) . "&";
        $query .= "company=" . urlencode($this->billing['company']) . "&";
        $query .= "address1=" . urlencode($this->billing['address1']) . "&";
        $query .= "address2=" . urlencode($this->billing['address2']) . "&";
        $query .= "city=" . urlencode($this->billing['city']) . "&";
        $query .= "state=" . urlencode($this->billing['state']) . "&";
        $query .= "zip=" . urlencode($this->billing['zip']) . "&";
        $query .= "country=" . urlencode($this->billing['country']) . "&";
        $query .= "phone=" . urlencode($this->billing['phone']) . "&";
        $query .= "fax=" . urlencode($this->billing['fax']) . "&";
        $query .= "email=" . urlencode($this->billing['email']) . "&";
        $query .= "website=" . urlencode($this->billing['website']) . "&";
        $query .= "type=credit";
        return $this->_doPost($query);
    }

    function doOffline($authorizationcode, $amount, $ccnumber, $ccexp) {

        $query  = "";
        // Login Information
        $query .= "username=" . urlencode($this->login['username']) . "&";
        $query .= "password=" . urlencode($this->login['password']) . "&";
        // Sales Information
        $query .= "ccnumber=" . urlencode($ccnumber) . "&";
        $query .= "ccexp=" . urlencode($ccexp) . "&";
        $query .= "amount=" . urlencode(number_format($amount,2,".","")) . "&";
        $query .= "authorizationcode=" . urlencode($authorizationcode) . "&";
        // Order Information
        $query .= "ipaddress=" . urlencode($this->order['ipaddress']) . "&";
        $query .= "orderid=" . urlencode($this->order['orderid']) . "&";
        $query .= "orderdescription=" . urlencode($this->order['orderdescription']) . "&";
        $query .= "tax=" . urlencode(number_format($this->order['tax'],2,".","")) . "&";
        $query .= "shipping=" . urlencode(number_format($this->order['shipping'],2,".","")) . "&";
        $query .= "ponumber=" . urlencode($this->order['ponumber']) . "&";
        // Billing Information
        $query .= "firstname=" . urlencode($this->billing['firstname']) . "&";
        $query .= "lastname=" . urlencode($this->billing['lastname']) . "&";
        $query .= "company=" . urlencode($this->billing['company']) . "&";
        $query .= "address1=" . urlencode($this->billing['address1']) . "&";
        $query .= "address2=" . urlencode($this->billing['address2']) . "&";
        $query .= "city=" . urlencode($this->billing['city']) . "&";
        $query .= "state=" . urlencode($this->billing['state']) . "&";
        $query .= "zip=" . urlencode($this->billing['zip']) . "&";
        $query .= "country=" . urlencode($this->billing['country']) . "&";
        $query .= "phone=" . urlencode($this->billing['phone']) . "&";
        $query .= "fax=" . urlencode($this->billing['fax']) . "&";
        $query .= "email=" . urlencode($this->billing['email']) . "&";
        $query .= "website=" . urlencode($this->billing['website']) . "&";
        // Shipping Information
        $query .= "shipping_firstname=" . urlencode($this->shipping['firstname']) . "&";
        $query .= "shipping_lastname=" . urlencode($this->shipping['lastname']) . "&";
        $query .= "shipping_company=" . urlencode($this->shipping['company']) . "&";
        $query .= "shipping_address1=" . urlencode($this->shipping['address1']) . "&";
        $query .= "shipping_address2=" . urlencode($this->shipping['address2']) . "&";
        $query .= "shipping_city=" . urlencode($this->shipping['city']) . "&";
        $query .= "shipping_state=" . urlencode($this->shipping['state']) . "&";
        $query .= "shipping_zip=" . urlencode($this->shipping['zip']) . "&";
        $query .= "shipping_country=" . urlencode($this->shipping['country']) . "&";
        $query .= "shipping_email=" . urlencode($this->shipping['email']) . "&";
        $query .= "type=offline";
        return $this->_doPost($query);
    }

    function doCapture($transactionid, $amount =0) {

        $query  = "";
        // Login Information
        $query .= "username=" . urlencode($this->login['username']) . "&";
        $query .= "password=" . urlencode($this->login['password']) . "&";
        // Transaction Information
        $query .= "transactionid=" . urlencode($transactionid) . "&";
        if ($amount>0) {
            $query .= "amount=" . urlencode(number_format($amount,2,".","")) . "&";
        }
        $query .= "type=capture";
        return $this->_doPost($query);
    }

    function doVoid($transactionid) {

        $query  = "";
        // Login Information
        $query .= "username=" . urlencode($this->login['username']) . "&";
        $query .= "password=" . urlencode($this->login['password']) . "&";
        // Transaction Information
        $query .= "transactionid=" . urlencode($transactionid) . "&";
        $query .= "type=void";
        return $this->_doPost($query);
    }

    function doRefund($transactionid, $amount = 0) {

        $query  = "";
        // Login Information
        $query .= "username=" . urlencode($this->login['username']) . "&";
        $query .= "password=" . urlencode($this->login['password']) . "&";
        // Transaction Information
        $query .= "transactionid=" . urlencode($transactionid) . "&";
        if ($amount>0) {
            $query .= "amount=" . urlencode(number_format($amount,2,".","")) . "&";
        }
        $query .= "type=refund";
        return $this->_doPost($query);
    }

    function _doPost($query) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://secure.americanvolumegateway.com/api/transact.php");
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        curl_setopt($ch, CURLOPT_POST, 1);

        if (!($data = curl_exec($ch))) {
            return ERROR;
        }
        curl_close($ch);
        unset($ch);
        print "\n$data\n";
        $data = explode("&",$data);
        for($i=0;$i<count($data);$i++) {
            $rdata = explode("=",$data[$i]);
            $this->responses[$rdata[0]] = $rdata[1];
        }
        return $this->responses['response'];
    }
}

class checkoutController extends siteController
{


    public function applyCoupon(Array $params = [])
    {
        $subtotal = 0;
        $total = 0;
        $tax = 0;
        foreach ($this->viewData->cart->single_sessions as $session) {
            $subtotal += $session->price;
        }

        foreach ($this->viewData->cart->products as $product) {
            $subtotal += $product->price;

            $tax += $product->price / 100 * 8.875;
        }

        foreach ($this->viewData->cart->packages as $pack) {
            $subtotal += $pack->price;
        }

        foreach ($this->viewData->cart->credits as $credit) {
            $subtotal += $credit->price;
        }


        $this->viewData->subtotal = $subtotal;

        $this->viewData->tax = round($tax, 2);

        $this->viewData->total = $subtotal + round($tax, 2);

        if (isset($_GET['code'])) {
            $coupon = \Model\Coupon::getItem(null, ['where' => " code = '" . $_GET['code'] . "'"]);
            if (is_null($coupon)) {
                $_SESSION['coupon'] = null;
                echo json_encode(['error' => ['code' => 1, 'message' => 'Invalid coupon code.'], 'coupon' => null]);
            } else if (time() >= $coupon->start_time && time() < $coupon->end_time) {
                $cartTotal = (float)str_replace(',', '', $this->viewData->total);
                if ($cartTotal >= $coupon->min_amount) {
                    $already_used = $coupon->uses_per_user;
                    if (is_null($already_used)) {
                        $already_used = 0;
                    }

                    if (is_null($coupon->num_uses_all) || $already_used < $coupon->num_uses_all) {
                        $_SESSION['coupon'] = $coupon->code;
                        echo json_encode(['error' => ['code' => 0, 'message' => ''], 'coupon' => $coupon]);
                    } else {
                        $_SESSION['coupon'] = null;
                        echo json_encode(['error' => ['code' => 4, 'message' => 'We are sorry, but the coupon you have entered is sold out.'], 'coupon' => null]);
                    }
                } else {
                    $_SESSION['coupon'] = null;
                    echo json_encode(['error' => ['code' => 2, 'message' => 'This coupon requires a minimum subtotal of ' . $coupon->min_amount . '.'], 'coupon' => null]);
                }
            } else {
                $_SESSION['coupon'] = null;
                echo json_encode(['error' => ['code' => 3, 'message' => 'This coupon is currently unavailable.'], 'coupon' => null]);
            }
        } else {
            $_SESSION['coupon'] = null;
            echo json_encode(['error' => ['code' => 1, 'message' => 'We are sorry, but you have entered an invalid coupon code.'], 'coupon' => null]);
        }
        $this->template = FALSE;
    }

    public function bookSession()
    {
        // start book session
        $user = $this->viewData->user;
        $service = \Model\Service::getItem(null, ['where' => ['id' => $_POST['service_id']]]);
        if(($service->sub_service)==0){
            $parent_service = $service->slug;
        }else{
            $parent_service  = \Model\Service::getItem($service->sub_service)->slug;
        }
        $provider = \Model\Provider::getItem(null, ['where' => ['id' => $_POST['provider_id']]]);
        $date = $_POST['date'];
        $date = date('Y-m-d H:i:s', strtotime($date));
        $dayformat = date('l, n/j/y', strtotime($date));
        $timeformat = date('g:i A', strtotime($date));
        $location = $_POST['location'];
        $creditValue = $_POST['creditValue'];
        if($user->credit == 0 && $user->corporate_credit == 0){
//            $arr = ['cc_holder','cc_number','cc_expiration_month','cc_expiration_year','cc_cvv','country','address','address2','city','state','zip'];
//            $card = new \Model\Card();
//            foreach($arr as $ar) {
//                if ($_POST[$ar] == '') {
//                    $n = new \Notification\ErrorHandler('Incomplete payment info');
//                    $_SESSION["notification"] = serialize($n);
//                    redirect('/checkout');
//                } else {
//                    $card->$ar = $_POST[$ar];
//                }
//            }
//            $card->user_id = $user->id;
            if(true){
                /**
                 * Do sales
                 */
//                $gw = new gwapi();
//                $gw->setLogin("info@wellns.com", "poqwWellns2015");
//
//                $gw->setBilling($_POST['name'],$_POST['name'],"",$_POST['bill_address'],$_POST['bill_address2'], $_POST['bill_city'],
//                    $_POST['bill_state'],$_POST['bill_zip'],"US","","",$_POST['email'], "");
//
//                $gw->setOrder("4444","Big Order",1, 2, "PO1234","65.192.14.10");
//
//                $r = $gw->doSale("1","4246315215842921","0319", '061');

                $user->credit += $creditValue;
                $user->save();
            }
        }

        if ($user && $service && $provider && $date && $location) {
            $sessionHistory = new \Model\SessionHistory();
            $sessionHistory->user_id = $user->id;
            $sessionHistory->service_id = $service->id;
            $sessionHistory->provider_id = $provider->id;
            $sessionHistory->date = $date;
            $sessionHistory->location = $location; //2-12-16: changed to user_address id
            $sessionHistory->credit = $creditValue;
            $sessionHistory->credit_type_id = $user->getConsumeCreditType($sessionHistory->credit);

            if ($user->consumeCredit($sessionHistory->credit) && $sessionHistory->save()) {
                // send customer book session email
                $email = new Email\MailMaster();
                $mergeFields = [
                    'PROVIDER_NAME' => $provider->full_name(),
                    'CUSTOMER_NAME' => $user->first_name . ' ' . $user->last_name[0],
                    'CUSTOMER_IMAGE' => S3_URL.$user->photo,
                    'SERVICE_NAME' => $service->name,
                    'SERVICE_SLUG' => $parent_service,
                    'SESSION_LOCATION' => \Model\User_Address::getItem($location)->getAddress(),
                    'SESSION_DATE' => $dayformat,                
                    'SESSION_TIME' => $timeformat
                ];
                $email->setTo(['email' => $provider->email, 'name' => $provider->full_name(), 'type' => 'to'])->setTemplate('book-session-provider')->setMergeTags($mergeFields)->send();

                // send provider book session email
                $email = new Email\MailMaster();
                $mergeFields = [
                    'PROVIDER_NAME' => $provider->full_name(),
                    'PROVIDER_IMAGE' => S3_URL.$provider->photo,
                    'USER_NAME' => $user->first_name(),
                    'SERVICE_NAME' => $service->name,
                    'SERVICE_SLUG' => $parent_service,
                    'SESSION_LOCATION' => \Model\User_Address::getItem($location)->getAddress(),
                    'SESSION_DATE' => $dayformat,                
                    'SESSION_TIME' => $timeformat
                ];
                $email->setTo(['email' => $user->email, 'name' => $user->full_name(), 'type' => 'to'])->setTemplate('book-session-customer')->setMergeTags($mergeFields)->send();

                \Email\Message::send($provider->phone, \Email\Message::getMessageContent('new-session-to-provider', $mergeFields));

                unset($_SESSION['completeSession']);
                redirect('/confirmation/'.$sessionHistory->id);
//                echo $this->toJson(['status' => 'success', 'id'=>$sessionHistory->id]);
            } else {
                $n = new \Notification\ErrorHandler('You have no session credits remaining on your account');
                $_SESSION["notification"] = serialize($n);
                redirect(SITE_URL.'checkout');
//                echo $this->toJson(['status' => 'failed', 'message' => 'You have no session credits remaining on your account', 'error_class' => '#book-session']);
            }
        } else {
            if (!$location) {
                $n = new \Notification\ErrorHandler('Please choose a location for this session');
                $_SESSION["notification"] = serialize($n);
                redirect(SITE_URL.'checkout');
//                echo $this->toJson(['status' => 'failed', 'message' => 'Please choose a location for this session', 'error_class' => '#session_location']);
            } else {
                $n = new \Notification\ErrorHandler('An error occurred, please try again');
                $_SESSION["notification"] = serialize($n);
                redirect(SITE_URL.'checkout');
//                echo $this->toJson(['status' => 'failed', 'message' => 'An error happened, please try again.']);
            }

        }
    }

    public function pay()
    {
        $user = $this->viewData->user;
        $subtotal = 0;
        $tax = 0;
        foreach ($this->viewData->cart->products as $product) {
            $subtotal += $product->price;
            $tax += $product->price / 100 * 8.875;
        }

        foreach ($this->viewData->cart->credits as $credit) {
            $subtotal += $credit->price;
        }

        $this->viewData->subtotal = $subtotal;
        $this->viewData->tax = round($tax, 2);
        $this->viewData->saving = 0;
        $this->viewData->saving2 = 0;
        $total = $subtotal + round($tax, 2);
        $this->viewData->card = $this->viewData->user->getDefaultCard();

        if (count($this->viewData->cart->products) > 0) {
            @$this->viewData->total += $total + 6 + ((count($this->viewData->cart->products) - 1) * 1.5);
            @$this->viewData->total2 += $total + 10 + ((count($this->viewData->cart->products) - 1) * 3);

            $shipping_1 = 6 + ((count($this->viewData->cart->products) - 1) * 1.5);
            $shipping_2 = 10 + ((count($this->viewData->cart->products) - 1) * 3);
        } else {
            $this->viewData->total = $total;

        }
        if (isset($_SESSION['coupon'])) {
            $coupon = \Model\Coupon::getItem(null, ['where' => " code = '" . $_SESSION['coupon'] . "'"]);
            $this->viewData->coupon = $coupon;
            if (time() >= $coupon->start_time && time() < $coupon->end_time) {
                $cartTotal = (float)str_replace(',', '', $this->viewData->total);
                if ($cartTotal >= $coupon->min_amount) {
                    $already_used = $coupon->uses_per_user;
                    if (is_null($already_used)) {
                        $already_used = 0;
                    }

                    if (is_null($coupon->num_uses_all) || $already_used < $coupon->num_uses_all) {
                        $_SESSION['coupon'] = $coupon->code;

                        if ($coupon->discount_type == 1) {
                            $this->viewData->saving = $coupon->discount_amount;
                            $this->viewData->total = $this->viewData->total - $coupon->discount_amount;


                            $total = $this->viewData->total - $coupon->discount_amount;
                            $this->viewData->saving2 = $coupon->discount_amount;
                            @$this->viewData->total2 = $this->viewData->total2 - $coupon->discount_amount;
                        } elseif ($coupon->discount_type == 2) {
                            $this->viewData->saving = ($this->viewData->total / 100) * $coupon->discount_amount;
                            $this->viewData->total = $this->viewData->total - $this->viewData->saving;
                            $total = $this->viewData->total - $this->viewData->saving;


                            @$this->viewData->saving2 = ($this->viewData->total2 / 100) * $coupon->discount_amount;
                            @$this->viewData->total2 = $this->viewData->total2 - $this->viewData->saving2;
                        } else {
                            $this->viewData->saving = 0;
                            $this->viewData->saving2 = 0;
                        }


                    } else {
                        $_SESSION['coupon'] = null;

                    }
                } else {
                    $_SESSION['coupon'] = null;

                }
            } else {
                $_SESSION['coupon'] = null;

            }


        }

        $order = new \Model\Order();
        $order->ship_first_name = $_POST['name'];
        $order->ship_last_name = $_POST['name'];
        $order->ship_address = $_POST['bill_address'];
        $order->ship_city = $_POST['bill_city'];
        $order->ship_state = $_POST['bill_state'];
        $order->email = $_POST['email'];
        $order->ship_zip = $_POST['bill_zip'];

        if($_POST['same'] == 'on') {
            $order->bill_first_name = $order->ship_first_name;
            $order->bill_last_name = $order->ship_last_name;
            $order->bill_address = $order->ship_address;
            $order->bill_city = $order->ship_city;
            $order->bill_state = $order->ship_state;
            $order->bill_zip = $order->ship_zip;
        } else {
            $order->bill_first_name = $_POST['name'];
            $order->bill_last_name = $_POST['name'];
            $order->bill_address = $_POST['bill_address'];
            $order->bill_city = $_POST['bill_city'];
            $order->bill_state = $_POST['bill_state'];
            $order->bill_zip = $_POST['bill_zip'];
        }
        $order->user_id = isset($user)?$user->id:0;
        $order->user_role = '';
        $order->status = 0;
        $order->savings = $this->viewData->saving;
        $order->subtotal = $subtotal;
        $order->tax = round($tax, 2);
        $order->type = 1; //products and packages
        $order->total = round($total, 2);
        $order->save();

        $gw = new gwapi;
//        $gw->setLogin("info@wellns.com", "poqwWellns2015");
        // overwrite by demo information
        $gw->setLogin("demo", "password");

        $gw->setBilling($_POST['name'],$_POST['name'],"",$_POST['bill_address'],$_POST['bill_address2'], $_POST['bill_city'],
            $_POST['bill_state'],$_POST['bill_zip'],"US","","",$_POST['email'], "");

        $gw->setOrder("4444","Big Order",0, 0, "PO1234","65.192.14.10");

        $r = $gw->doSale($order->total,$_POST['cc_number'],$_POST['cc_expiration_month'].$_POST['cc_expiration_year'], $_POST['cc_cvv']);

        $order->status = $gw->responses['response'];
        $order->order_message = $gw->responses['responsetext'];
        $order->save();

        if($gw->responses['response'] == 1){
            $totalCredits = 0;
            if (isset($this->viewData->cart->credits)) {
                foreach ($this->viewData->cart->credits as $credit) {
                    $creditHistory = new \Model\CreditHistory();
                    $creditHistory->user_id = $user->id;
                    $creditHistory->credit_id = $credit->id;
                    $creditHistory->order_id = $order->id;
                    $creditHistory->save();
                    $totalCredits += $credit->value;
                }

                $user->credit += $totalCredits;
                $user->save();

                $this->creditEmail($totalCredits);
            }


            $this->remove_all();
            unset($_SESSION['coupon']);
            $tracking = new stdClass();
            $tracking->label = 'FGnPCInxrmMQ4vjWvgM';
            $_SESSION["googleAdConversionTracking"] = serialize($tracking); // tracking label

            if($user->getLoginType() == 'fb'){
                $fbTracking = new stdClass();
                $fbTracking->label = '6036632482510';
                $_SESSION['facebookAdConversionTracking'] = serialize($fbTracking);
            }
            $n = new \Notification\ErrorHandler("Thanks, you have success purchased $totalCredits credits");
            $_SESSION["notification"] = serialize($n);
            redirect(SITE_URL);
        } else {
            $n = new \Notification\ErrorHandler("There was an error during checkout, please reenter your credit card information");
            $_SESSION["notification"] = serialize($n);
            redirect(SITE_URL . 'checkout?type=credit');
        }
    }


    public function index(Array $params = [])
    {
//        if(!isset($_SESSION['completeSession'])){
//            $n = new \Notification\ErrorHandler('Book a session to progress');
//            $_SESSION["notification"] = serialize($n);
//            redirect(SITE_URL);
//        }
        if (\Emagid\Core\Membership::userId()) {
            $user_role = \Emagid\Core\Membership::getAuthenticationSession();
            if ($user_role->roles[0] == "customer" || $user_role->roles[0] == "corporate") {
                $user = \Model\User::getItem(\Emagid\Core\Membership::userId());
            } elseif ($user_role->roles[0] == "provider") {
                $user = \Model\Provider::getItem(\Emagid\Core\Membership::userId());
            } else {
                $n = new \Notification\ErrorHandler('You have to login');
                $_SESSION["notification"] = serialize($n);
                redirect(SITE_URL . 'login');
            }
        } else {
            $n = new \Notification\ErrorHandler('Please login to checkout');
            $_SESSION["notification"] = serialize($n);
            redirect(SITE_URL . 'login');
        }
        $this->viewData->states = array(
            'AL' => 'Alabama',
            'AK' => 'Alaska',
            'AZ' => 'Arizona',
            'AR' => 'Arkansas',
            'CA' => 'California',
            'CO' => 'Colorado',
            'CT' => 'Connecticut',
            'DE' => 'Delaware',
            'DC' => 'District of Columbia',
            'FL' => 'Florida',
            'GA' => 'Georgia',
            'HI' => 'Hawaii',
            'ID' => 'Idaho',
            'IL' => 'Illinois',
            'IN' => 'Indiana',
            'IA' => 'Iowa',
            'KS' => 'Kansas',
            'KY' => 'Kentucky',
            'LA' => 'Louisiana',
            'ME' => 'Maine',
            'MD' => 'Maryland',
            'MA' => 'Massachusetts',
            'MI' => 'Michigan',
            'MN' => 'Minnesota',
            'MS' => 'Mississippi',
            'MO' => 'Missouri',
            'MT' => 'Montana',
            'NE' => 'Nebraska',
            'NV' => 'Nevada',
            'NH' => 'New Hampshire',
            'NJ' => 'New Jersey',
            'NM' => 'New Mexico',
            'NY' => 'New York',
            'NC' => 'North Carolina',
            'ND' => 'North Dakota',
            'OH' => 'Ohio',
            'OK' => 'Oklahoma',
            'OR' => 'Oregon',
            'PA' => 'Pennsylvania',
            'RI' => 'Rhode Island',
            'SC' => 'South Carolina',
            'SD' => 'South Dakota',
            'TN' => 'Tennessee',
            'TX' => 'Texas',
            'UT' => 'Utah',
            'VT' => 'Vermont',
            'VA' => 'Virginia',
            'WA' => 'Washington',
            'WV' => 'West Virginia',
            'WI' => 'Wisconsin',
            'WY' => 'Wyoming');

        //unset($_SESSION['coupon']);
//        if ((count($this->viewData->cart->products) <= 0)
//            && (count($this->viewData->cart->invites) <= 0)
//            && (count($this->viewData->cart->packages) <= 0)
//            && (count($this->viewData->cart->single_sessions) <= 0)
//            && (count($this->viewData->cart->credits) <= 0)
//        ) {
//            redirect(SITE_URL . 'credits');
//        }
        $subtotal = 0;
        $tax = 0;

        foreach ($this->viewData->cart->products as $product) {
            $subtotal += $product->price;
            $tax += $product->price / 100 * 8.875;
        }

        foreach ($this->viewData->cart->credits as $credit) {
            $subtotal += $credit->price;
        }

        $this->viewData->subtotal = $subtotal;
        $this->viewData->tax = round($tax, 2);
        $this->viewData->saving = 0;
        $this->viewData->saving2 = 0;
        $total = $subtotal + round($tax, 2);
        $this->viewData->card = $this->viewData->user->getDefaultCard();

        if (count($this->viewData->cart->products) > 0) {
            @$this->viewData->total += $total + 6 + ((count($this->viewData->cart->products) - 1) * 1.5);
            @$this->viewData->total2 += $total + 10 + ((count($this->viewData->cart->products) - 1) * 3);

            $shipping_1 = 6 + ((count($this->viewData->cart->products) - 1) * 1.5);
            $shipping_2 = 10 + ((count($this->viewData->cart->products) - 1) * 3);
        } else {
            $this->viewData->total = $total;

        }
        $this->viewData->coupon = null;
        if (isset($_SESSION['coupon'])) {
            $coupon = \Model\Coupon::getItem(null, ['where' => " code = '" . $_SESSION['coupon'] . "'"]);
            $this->viewData->coupon = $coupon;
            if (time() >= $coupon->start_time && time() < $coupon->end_time) {
                $cartTotal = (float)str_replace(',', '', $this->viewData->total);
                if ($cartTotal >= $coupon->min_amount) {
                    $already_used = $coupon->uses_per_user;
                    if (is_null($already_used)) {
                        $already_used = 0;
                    }

                    if (is_null($coupon->num_uses_all) || $already_used < $coupon->num_uses_all) {
                        $_SESSION['coupon'] = $coupon->code;

                        if ($coupon->discount_type == 1) {
                            $this->viewData->saving = $coupon->discount_amount;
                            $this->viewData->total = $this->viewData->total - $coupon->discount_amount;


                            $total = $this->viewData->total - $coupon->discount_amount;
                            $this->viewData->saving2 = $coupon->discount_amount;
                            @$this->viewData->total2 = $this->viewData->total2 - $coupon->discount_amount;
                        } elseif ($coupon->discount_type == 2) {
                            $this->viewData->saving = ($this->viewData->total / 100) * $coupon->discount_amount;
                            $this->viewData->total = $this->viewData->total - $this->viewData->saving;
                            $total = $this->viewData->total - $this->viewData->saving;


                            @$this->viewData->saving2 = ($this->viewData->total2 / 100) * $coupon->discount_amount;
                            @$this->viewData->total2 = $this->viewData->total2 - $this->viewData->saving2;
                        } else {
                            $this->viewData->saving = 0;
                            $this->viewData->saving2 = 0;
                        }


                    } else {
                        $_SESSION['coupon'] = null;

                    }
                } else {
                    $_SESSION['coupon'] = null;

                }
            } else {
                $_SESSION['coupon'] = null;

            }


        }


        $gatewayURL = 'https://secure.americanvolumegateway.com/api/v2/three-step';
        $APIKey = 'WA8EtRNG7XDh4x2aR53W8ku6cRzZn4mK';

        // Testing info
        $gatewayURL = PAYMENT_GATEWAY;
        $APIKey = PAYMENT_APIKEY;

        // visa: 4111111111111111
        // expiration 10/25
        // cvv: 999

        if (isset($_POST['step'])) {
            $_SESSION['user_info'] = $_POST;

//            if (empty($_POST['ship_first_name']) ||
//                empty($_POST['ship_last_name']) ||
//                empty($_POST['ship_address']) ||
//                empty($_POST['ship_city']) ||
//                empty($_POST['ship_state']) ||
//                empty($_POST['email']) ||
//                empty($_POST['ship_zip']) ||
//                empty($_POST['phone'])
//            ) {
//                $ar[] = "1";
//            }
//            if (isset($_POST['same']) && $_POST['same'] == 'on') {
//                if (empty($_POST['ship_first_name']) ||
//                    empty($_POST['ship_last_name']) ||
//                    empty($_POST['ship_address']) ||
//                    empty($_POST['ship_city']) ||
//                    empty($_POST['ship_state']) ||
//                    empty($_POST['ship_zip'])
//                ) {
//                    $ar[] = "1";
//                }
//            } else {
//
//                if (empty($_POST['bill_first_name']) ||
//                    empty($_POST['bill_last_name']) ||
//                    empty($_POST['bill_address']) ||
//                    empty($_POST['bill_city']) ||
//                    empty($_POST['bill_state']) ||
//                    empty($_POST['bill_zip'])
//                ) {
//                    $ar[] = "1";
//                }
//
//            }
            if (isset($ar) && count($ar) > 0) {
                $n = new \Notification\MessageHandler("All fields required!");
                $_SESSION["notification"] = serialize($n);
                redirect(SITE_URL . 'checkout');
            }


            //echo $this->viewData->total2;
            /* 	echo '<br>';
                echo round($tax,2);
                echo '<br>';
                echo $shipping;
                echo '<br>';
                 echo round($total,2);
                 */


            $xmlRequest = new DOMDocument('1.0', 'UTF-8');

            $xmlRequest->formatOutput = true;
            $xmlSale = $xmlRequest->createElement('sale');
            if (count($this->viewData->cart->products) > 0) {
                if ($_POST['shipping'] == 1) {
                    $shipping = $shipping_1;

                    $this->appendXmlNode($xmlRequest, $xmlSale, 'merchant-defined-field-1', 1);
                    $total = $this->viewData->total;

                } else {
                    $this->appendXmlNode($xmlRequest, $xmlSale, 'merchant-defined-field-1', 2);
                    $shipping = $shipping_2;
                    $total = $this->viewData->total2;
                }
            } else {
                $total = $this->viewData->total;
                $shipping = 0;
            }

            // Amount, authentication, and Redirect-URL are typically the bare minimum.
            $this->appendXmlNode($xmlRequest, $xmlSale, 'api-key', $APIKey);
            $this->appendXmlNode($xmlRequest, $xmlSale, 'redirect-url', $_SERVER['HTTP_REFERER']);
            $this->appendXmlNode($xmlRequest, $xmlSale, 'amount', round($total, 2));
            $this->appendXmlNode($xmlRequest, $xmlSale, 'ip-address', $_SERVER["REMOTE_ADDR"]);
            //$this->appendXmlNode($xmlRequest, $xmlSale, 'processor-id' , 'processor-a');
            $this->appendXmlNode($xmlRequest, $xmlSale, 'currency', 'USD');

            // Some additonal fields may have been previously decided by user
            $this->appendXmlNode($xmlRequest, $xmlSale, 'order-id', '1234');
            $this->appendXmlNode($xmlRequest, $xmlSale, 'order-description', 'New Order');
            $this->appendXmlNode($xmlRequest, $xmlSale, 'tax-amount', round($tax, 2));
            $this->appendXmlNode($xmlRequest, $xmlSale, 'shipping-amount', $shipping);


//            $xmlShippingAddress = $xmlRequest->createElement('shipping');
//            $this->appendXmlNode($xmlRequest, $xmlShippingAddress, 'first-name', $_POST['ship_first_name']);
//            $this->appendXmlNode($xmlRequest, $xmlShippingAddress, 'last-name', $_POST['ship_last_name']);
//            $this->appendXmlNode($xmlRequest, $xmlShippingAddress, 'address1', $_POST['ship_address']);
//            $this->appendXmlNode($xmlRequest, $xmlShippingAddress, 'city', $_POST['ship_city']);
//            $this->appendXmlNode($xmlRequest, $xmlShippingAddress, 'state', $_POST['ship_state']);
//            $this->appendXmlNode($xmlRequest, $xmlShippingAddress, 'postal', $_POST['ship_zip']);
//            $this->appendXmlNode($xmlRequest, $xmlShippingAddress, 'phone', $_POST['phone']);
//            $this->appendXmlNode($xmlRequest, $xmlShippingAddress, 'email', $_POST['email']);
//            $xmlSale->appendChild($xmlShippingAddress);


//            if (isset($_POST['same']) && $_POST['same'] == 'on') {
//                $xmlBillingAddress = $xmlRequest->createElement('billing');
//                $this->appendXmlNode($xmlRequest, $xmlBillingAddress, 'first-name', $_POST['ship_first_name']);
//                $this->appendXmlNode($xmlRequest, $xmlBillingAddress, 'last-name', $_POST['ship_last_name']);
//                $this->appendXmlNode($xmlRequest, $xmlBillingAddress, 'address1', $_POST['ship_address']);
//                $this->appendXmlNode($xmlRequest, $xmlBillingAddress, 'city', $_POST['ship_city']);
//                $this->appendXmlNode($xmlRequest, $xmlBillingAddress, 'state', $_POST['ship_state']);
//                $this->appendXmlNode($xmlRequest, $xmlBillingAddress, 'postal', $_POST['ship_zip']);
//
//                $xmlSale->appendChild($xmlBillingAddress);
//            } else {
            $xmlBillingAddress = $xmlRequest->createElement('billing');
            $this->appendXmlNode($xmlRequest, $xmlBillingAddress, 'first-name', $_POST['name']);
            $this->appendXmlNode($xmlRequest, $xmlBillingAddress, 'last-name', $_POST['name']);
            $this->appendXmlNode($xmlRequest, $xmlBillingAddress, 'address1', $_POST['bill_address']);
            $this->appendXmlNode($xmlRequest, $xmlBillingAddress, 'city', $_POST['bill_city']);
            $this->appendXmlNode($xmlRequest, $xmlBillingAddress, 'state', $_POST['bill_state']);
            $this->appendXmlNode($xmlRequest, $xmlBillingAddress, 'postal', $_POST['bill_zip']);
            $xmlSale->appendChild($xmlBillingAddress);

//            }

            if (isset($this->viewData->cart->products)) {
                foreach ($this->viewData->cart->products as $productId) {
                    $product = \Model\Product::getItem($productId->id);
                    $xmlProduct = $xmlRequest->createElement('product');
                    $info = "";
                    if (isset($productId->color)) {
                        $info = 'Color:' . $productId->color;
                    }
                    if (isset($productId->size)) {
                        $info .= ' Size:' . $productId->size;
                    }


                    $this->appendXmlNode($xmlRequest, $xmlProduct, 'product-code', $product->id);
                    $this->appendXmlNode($xmlRequest, $xmlProduct, 'description', $product->name . ' ' . $info);
                    $this->appendXmlNode($xmlRequest, $xmlProduct, 'quantity', 1);
                    $this->appendXmlNode($xmlRequest, $xmlProduct, 'unit-cost', $product->price);
                    $xmlSale->appendChild($xmlProduct);
                }
            }

            if (isset($this->viewData->cart->credits)) {
                foreach ($this->viewData->cart->credits as $credit) {
                    $credit = \Model\Credit::getItem($credit->id);
                    $xmlProduct = $xmlRequest->createElement('product');
                    $this->appendXmlNode($xmlRequest, $xmlProduct, 'product-code', "Credit ID {$credit->id}");
                    $this->appendXmlNode($xmlRequest, $xmlProduct, 'description', "Paid {$credit->value} credits");
                    $this->appendXmlNode($xmlRequest, $xmlProduct, 'quantity', 1);
                    $this->appendXmlNode($xmlRequest, $xmlProduct, 'unit-cost', $credit->price);
                    $xmlSale->appendChild($xmlProduct);
                }
            }
            // Products already chosen by user


            $xmlRequest->appendChild($xmlSale);

            // Process Step One: Submit all transaction details to the Payment Gateway except the customer's sensitive payment information.
            // The Payment Gateway will return a variable form-url.
            $data = $this->sendXMLviaCurl($xmlRequest, $gatewayURL);

            // Parse Step One's XML response
            $gwResponse = @new SimpleXMLElement($data);

            $order = new \Model\Order();
            $order->ship_first_name = $_POST['name'];
            $order->ship_last_name = $_POST['name'];
            $order->ship_address = $_POST['bill_address'];
            $order->ship_city = $_POST['bill_city'];
            $order->ship_state = $_POST['bill_state'];
            $order->email = $_POST['email'];
            $order->ship_zip = $_POST['bill_zip'];

            if($_POST['same'] == 'on') {
                $order->bill_first_name = $order->ship_first_name;
                $order->bill_last_name = $order->ship_last_name;
                $order->bill_address = $order->ship_address;
                $order->bill_city = $order->ship_city;
                $order->bill_state = $order->ship_state;
                $order->bill_zip = $order->ship_zip;
            } else {
                $order->bill_first_name = $_POST['name'];
                $order->bill_last_name = $_POST['name'];
                $order->bill_address = $_POST['bill_address'];
                $order->bill_city = $_POST['bill_city'];
                $order->bill_state = $_POST['bill_state'];
                $order->bill_zip = $_POST['bill_zip'];
            }
            $order->user_id = isset($user)?$user->id:0;
            $order->user_role = isset($user)?$user_role->roles[0]:'';
            $order->status = 0;
            $order->savings = $this->viewData->saving;
            $order->subtotal = $subtotal;
            $order->tax = round($tax, 2);
            $order->type = 1; //products and packages
            $order->total = round($total, 2);
            $order->save();

            if (isset($this->viewData->cart->products)) {
                foreach ($this->viewData->cart->products as $productId) {

                    $product = \Model\Product::getItem($productId->id);

                    $order_product = new \Model\Order_Product();
                    $order_product->user_id = $user->id;
                    $order_product->order_id = $order->id;
                    $order_product->product_id = $productId->id;
                    if (isset($productId->color)) {
                        $order_product->color = $productId->color;
                    }
                    if (isset($productId->size)) {
                        $order_product->size = $productId->size;
                    }
                    $order_product->unit_price = $product->price;
                    $order_product->type_product = 1;
                    $order_product->save();
                }
            }
            $_SESSION['orderId'] = $order->id;
            $_SESSION['userType'] = $order->user_role;

            if ((string)$gwResponse->result == 1) {
                // The form url for used in Step Two below
                $formURL = $gwResponse->{'form-url'};
                $this->viewData->url = $formURL;
            } else {
                throw New Exception(print " Error, received " . $data);
            }
            //$this->viewData->url1 = $_POST['ship_state'];
            // Initiate Step Two: Create an HTML form that collects the customer's sensitive payment information
            // and use the form-url that the Payment Gateway returns as the submit action in that form.
            //print '  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';


            /*print '

                <html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                    <title>Collect sensitive Customer Info </title>
                </head>
                <body>';*/
            // Uncomment the line below if you would like to print Step One's response
            // print '<pre>' . (htmlentities($data)) . '</pre>';
            /* print '
                 <p><h2>Step Two: Collect sensitive payment information and POST directly to payment gateway<br /></h2></p>

                 <form action="'.$formURL. '" method="POST">
                 <h3> Payment Information</h3>
                     <table>
                         <tr><td>Credit Card Number</td><td><INPUT type ="text" name="billing-cc-number" value="4111111111111111"> </td></tr>
                         <tr><td>Expiration Date</td><td><INPUT type ="text" name="billing-cc-exp" value="1012"> </td></tr>
                         <tr><td>CVV</td><td><INPUT type ="text" name="cvv" > </td></tr>
                         <tr><Td colspan="2" align=center><INPUT type ="submit" value="Submit Step Two"></td> </tr>
                     </table>
                 </form>
                 </body>
                 </html>
                 ';*/
        } elseif (!empty($_GET['token-id'])) {

            // Step Three: Once the browser has been redirected, we can obtain the token-id and complete
            // the transaction through another XML HTTPS POST including the token-id which abstracts the
            // sensitive payment information that was previously collected by the Payment Gateway.
            $tokenId = $_GET['token-id'];
            $xmlRequest = new DOMDocument('1.0', 'UTF-8');
            $xmlRequest->formatOutput = true;
            $xmlCompleteTransaction = $xmlRequest->createElement('complete-action');
            $this->appendXmlNode($xmlRequest, $xmlCompleteTransaction, 'api-key', $APIKey);
            $this->appendXmlNode($xmlRequest, $xmlCompleteTransaction, 'token-id', $tokenId);
            $xmlRequest->appendChild($xmlCompleteTransaction);


            // Process Step Three
            $data = $this->sendXMLviaCurl($xmlRequest, $gatewayURL);


            $gwResponse = @new SimpleXMLElement($data);


            $order = $_SESSION['orderId'] != '' ? \Model\Order::getItem($_SESSION['orderId']): new \Model\Order();
            $order->status = $gwResponse->result;
            $order->order_message = (string)$gwResponse->{'result-text'};
            $order->save();
            if ((string)$gwResponse->result == 1) {
                /*   print " <p><h3> Transaction was Approved, XML response was:</h3></p>\n";*/
                if ($_SESSION['userType'] == "customer" || $_SESSION['userType'] == "corporate") {
                    $user = \Model\User::getItem(\Emagid\Core\Membership::userId());
                } elseif ($_SESSION['userType'] == "provider") {
                    $user = \Model\Provider::getItem(\Emagid\Core\Membership::userId());
                } else {
                    $n = new \Notification\ErrorHandler('You have to login');
                    $_SESSION["notification"] = serialize($n);
                    redirect(SITE_URL . 'checkout');
                }

                /* 	 if(count($this->viewData->cart->products)>0){
                            if($_POST['shipping'] == 1){
                                $order->shipping_method = 1;
                            }else{
                                $order->shipping_method = 2;
                            }
                        }else{
                            $order->shipping_method = 0;
                        } */
                $order->shipping_cost = $gwResponse->{'shipping-amount'};
                if (count($this->viewData->cart->products) > 0) {
                    $order->shipping_method = $gwResponse->{'merchant-defined-field-1'};
                }

                if (isset($this->viewData->cart->credits)) {
                    $totalCredits = 0;
                    foreach ($this->viewData->cart->credits as $credit) {
                        $creditHistory = new \Model\CreditHistory();
                        $creditHistory->user_id = $user->id;
                        $creditHistory->credit_id = $credit->id;
                        $creditHistory->order_id = $order->id;
                        $creditHistory->save();
                        $totalCredits += $credit->value;
                    }

                    $user->credit += $totalCredits;
                    $user->save();

                    $this->creditEmail($totalCredits);
                }

                if ($order->save()) {

                    $this->remove_all();
                    unset($_SESSION['coupon']);
                    $tracking = new stdClass();
                    $tracking->label = 'FGnPCInxrmMQ4vjWvgM';
                    $_SESSION["googleAdConversionTracking"] = serialize($tracking); // tracking label

                    if($user->getLoginType() == 'fb'){
                        $fbTracking = new stdClass();
                        $fbTracking->label = '6036632482510';
                        $_SESSION['facebookAdConversionTracking'] = serialize($fbTracking);
                    }
                    redirect(SITE_URL . 'order/');


                } else {
                    $n = new \Notification\ErrorHandler($order->errors);
                    $_SESSION["notification"] = serialize($n);
                    redirect(SITE_URL . 'checkout');
                }


            } elseif ((string)$gwResponse->result == 2) {
                /*   print " <p><h3> Transaction was Declined.</h3>\n";
                   print " Decline Description : " . (string)$gwResponse->{'result-text'} ." </p>";
                   print " <p><h3>XML response was:</h3></p>\n";
                   print '<pre>' . (htmlentities($data)) . '</pre>';*/
                $n = new \Notification\ErrorHandler("There was an error during checkout, please reenter your credit card information");
                $_SESSION["notification"] = serialize($n);
                redirect(SITE_URL . 'checkout');
            } else {
                /*      print " <p><h3> Transaction caused an Error.</h3>\n";
                        print " Error Description: " . (string)$gwResponse->{'result-text'} ." </p>";
                        print " <p><h3>XML response was:</h3></p>\n";
                        print '<pre>' . (htmlentities($data)) . '</pre>';*/
                $n = new \Notification\ErrorHandler("There was an error during checkout, please reenter your credit card information");
                $_SESSION["notification"] = serialize($n);
                redirect(SITE_URL . 'checkout');
            }
        } else {
        }

        parent::index($params);

    }

    private function  appendXmlNode($domDocument, $parentNode, $name, $value)
    {
        $childNode = $domDocument->createElement($name);
        $childNodeValue = $domDocument->createTextNode($value);
        $childNode->appendChild($childNodeValue);
        $parentNode->appendChild($childNode);
    }

    private function sendXMLviaCurl($xmlRequest, $gatewayURL)
    {
        // helper function demonstrating how to send the xml with curl


        $ch = curl_init(); // Initialize curl handle
        curl_setopt($ch, CURLOPT_URL, $gatewayURL); // Set POST URL

        $headers = array();
        $headers[] = "Content-type: text/xml";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); // Add http headers to let it know we're sending XML
        $xmlString = $xmlRequest->saveXML();
        curl_setopt($ch, CURLOPT_FAILONERROR, 1); // Fail on errors
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // Allow redirects
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Return into a variable
        curl_setopt($ch, CURLOPT_PORT, 443); // Set the port number
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); // Times out after 30s
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlString); // Add XML directly in POST


        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);


        // This should be unset in production use. With it on, it forces the ssl cert to be valid
        // before sending info.
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        if (!($data = curl_exec($ch))) {
            print  "curl error =>" . curl_error($ch) . "\n";
            throw New Exception(" CURL ERROR :" . curl_error($ch));

        }
        curl_close($ch);

        return $data;
    }

    // Helper function to make building xml dom easier

    public function remove_all()
    {
        $this->viewData->cart->products = [];
        $this->viewData->cart->single_sessions = [];
        $this->viewData->cart->packages = [];
        unset($_SESSION['orderId']);
        unset($_SESSION['userType']);
        $this->updateCookies();

    }

    private function updateCookies()
    {
        setcookie('cart', implode(',', $this->viewData->cart->products), time() + 60 * 60 * 24 * 100, "/");
    }

    public function checkout()
    {
        $this->loadView($this->viewData);
    }

    public function creditEmail($totalCredits)
    {
        $email = new MailMaster();
        $mergeFields = [
            'CUSTOMER_FIRST_NAME' => $this->viewData->user->first_name,
            'CREDITS' => $totalCredits
        ];

        $email->setTo(['email' => $this->viewData->user->email, 'name' => $this->viewData->user->full_name(),  'type' => 'to'])->setTemplate('buy-credits-success')->setMergeTags($mergeFields)->send();
    }


    private function provider_session_notification($id)
    {
        global $emagid;
        $mail = \Model\Mail::getItem(10);
        $text = $mail->email;


        $emagid->email->from->email = 'noreply@wellns.com';
        $order_products = \Model\Order_Product::getItem($id);
        $order = \Model\Order::getItem($order_products->order_id);
        $service = \Model\Service::getItem($order_products->service);
        //{{service_name}}
        $text = str_replace("{{service_name}}", $service->name, $text);

        $package = \Model\Package::getItem($order_products->product_id);
        //{{package_quantity}}
        $text = str_replace("{{package_quantity}}", $package->quantity, $text);
        $provider = \Model\Provider::getItem($order_products->provider_id);
        $user = \Model\User::getItem($order->user_id);
        $pr_name = $provider->first_name . ' ' . $provider->last_name;
        //{{provider_full_name}}
        $text = str_replace("{{provider_full_name}}", $pr_name, $text);


        $user_name = $user->first_name . ' ' . $user->last_name;

        //{{user_full_name}}
        $text = str_replace("{{user_full_name}}", $user_name, $text);

        //{{user_first_name}}
        $text = str_replace("{{user_first_name}}", $user->first_name, $text);

        $user_picture = $user->getProfileImage();
//{{$user_picture}}
        $text = str_replace("{{user_picture}}", $user_picture, $text);
        $email = new \Emagid\Email();

        $email->addTo($provider->email);
        $email->addTo("john@emagid.com");
        $email->addTo("eitan@emagid.com");
        $email->subject('New session request!');
        $email->body = $text;


        $email->send();
    }

//
}