<?php

class contactController extends siteController
{

    public function index(Array $params = [])
    {
        $this->loadView($this->viewData);
    }
    public function index_post(Array $params = [])
    {
        $obj = \Model\Contact::loadFromPost();
        $obj->admin_reply = 1;

        if($obj->save()){
            $n = new \Notification\MessageHandler('We received your message.');
            $_SESSION["notification"] = serialize($n);
        }else{
            $n = new \Notification\ErrorHandler($obj->errors);
            $_SESSION['notification'] = serialize($n);
        }
        redirect(SITE_URL);
    }


}