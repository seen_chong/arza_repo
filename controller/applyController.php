<?php

class applyController extends siteController
{

    function __construct()
    {
        parent::__construct();

    }

    public function index(Array $params = [])
    {
        $this->configs['Meta Title'] = "Apply to be a Specialist with Wellns";        
        $this->loadView($this->viewData);
    }

    public function start(Array $params = [])
    {
        $this->loadView($this->viewData);
    }

}