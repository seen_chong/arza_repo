<?php

class siteController extends \Emagid\Mvc\Controller
{

    protected $viewData;
    protected $roleType;

    function __construct()
    {
        parent::__construct();

        $this->configs = \Model\Config::getItems();

        $this->viewData = (object)[
            'pages' => \Model\Page::getList(),
//            'services' => \Model\Service::getList(['orderBy' => 'display_order asc, name asc']),
//            'mainServices'=>\Model\Service::getList(['where'=>"sub_service = 0 and active = 1", 'orderBy'=>"display_order asc, name asc"]),
            'cart' => (object)[
                'credits' => [],
                'packages' => [],
                'single_sessions' => [],
                'invites' => [],
                'products' => [] //id, name, price, size, color, slug, featured_image
            ],
            'user' => null
        ];
        if (\Emagid\Core\Membership::isAuthenticated() && \Emagid\Core\Membership::isInRoles(['customer']) ) {

            $this->viewData->user = \Model\User::getItem(\Emagid\Core\Membership::userId());

//            if(\Emagid\Core\Membership::userId()){
//                if (\Emagid\Core\Membership::isInRoles(['provider'])) {
//                    $this->viewData->user = \Model\Provider::getItem(\Emagid\Core\Membership::userId());
//                }
//
//                if (\Emagid\Core\Membership::isInRoles(['customer'])) {
//                    $this->viewData->user = \Model\User::getItem(\Emagid\Core\Membership::userId());
//                }
//
//                if (\Emagid\Core\Membership::isInRoles(['corporate'])) {
//                    $this->viewData->user = \Model\User::getItem(\Emagid\Core\Membership::userId());
//                }
//            }
        }
        if($this->emagid->route['controller'] == 'services' && isset($this->emagid->route['service_slug']) && $this->emagid->route['service_slug']){
            $this->viewData->testimonials = \Model\Testimonial::getTestimonials($this->emagid->route['service_slug']);
        } else {
            $this->viewData->testimonials = \Model\Testimonial::getTestimonials();
        }

        if (isset($_COOKIE['cart'])) {
            $this->viewData->cart = unserialize($_COOKIE['cart']);
        }

        $now = strtotime(date("m/d/Y"));
        $query = ['where'=>"status = '2' and post_date <= '".$now ."'" ];
        $posts = \Model\Blog::getList($query);
        if($posts){
            foreach($posts as $item){
                $item->status = 1;
                $item->save();
            }
        }


//        if($this->viewData->user){
//            $this->roleType = $this->viewData->user->getRole();
//        } else {
//            $this->roleType = null;
//        }
//
//        // admin won't able to login
//        if($this->roleType == 1){
//            redirect(SITE_URL);
//        }
    }

    public function index(Array $params = [])
    {
        $this->loadView($this->viewData);
    }

    public function toJson($array)
    {
        header('Content-Type: application/json');
        echo json_encode($array);
        exit();
    }

}