<?php

class providerController extends siteController
{
    function __construct()
    {


        parent::__construct();
    }

    public function background_check_disclosure(Array $params = [])
    {
        $this->check();
        $this->loadView($this->viewData);
    }

    private function check()
    {
        $provider = \Model\Provider::getItem(\Emagid\Core\Membership::userId());
        if (!$provider) {
            redirect(SITE_URL);
        }
        if (strlen($provider->key) > 1) {
            $n = new \Notification\ErrorHandler("You need to confirm your email address to continue.");
            $_SESSION["notification"] = serialize($n);
            \Emagid\Core\Membership::destroyAuthenticationSession();
            redirect(SITE_URL);
        } else {
            if ($provider->step_2_completed == FALSE) {
                $act = 1;
            } else {
                if ($provider->authorized == FALSE) {
                    if ($provider->back_check !== "1") {
                        $act = 3;
                    } elseif ($provider->disclosure_check !== 1) {
                        $act = 6;
                    } elseif ($provider->sign == "") {
                        $act = 5;
                    } elseif ($provider->candidate_id == "") {
                        $act = 4;

                    } else {
                        $act = 2;
                    }
                } else {
                }
            }
        }
        if (@$act == 1) {
            if ($this->emagid->route['controller'] == 'provider' && $this->emagid->route['action'] == 'manage') {
            } else {
                redirect(SITE_URL . 'provider/manage');
            }
        } elseif (@$act == 3) {

            redirect(SITE_URL . 'pay/background_check');

        } elseif (@$act == 4) {
            if ($this->emagid->route['controller'] == 'provider' && $this->emagid->route['action'] == 'background_check') {
            } else {
                redirect(SITE_URL . 'provider/background_check');
            }


        } elseif (@$act == 5) {
            if ($this->emagid->route['controller'] == 'provider' && $this->emagid->route['action'] == 'background_check_authorization') {
            } else {
                redirect(SITE_URL . 'provider/background_check_authorization');
            }
        } elseif (@$act == 6) {
            if ($this->emagid->route['controller'] == 'provider' && $this->emagid->route['action'] == 'background_check_disclosure') {
            } else {
                redirect(SITE_URL . 'provider/background_check_disclosure');
            }
        } elseif (@$act == 2) {
            if ($this->emagid->route['controller'] == 'provider' && $this->emagid->route['action'] == 'pending') {
            } else {
                redirect(SITE_URL . 'provider/pending');
            }
        } else {
            if ($this->emagid->route['controller'] == 'provider' && $this->emagid->route['action'] == 'manage') {
                redirect(SITE_URL);
            }
            if ($this->emagid->route['controller'] == 'provider' && $this->emagid->route['action'] == 'background_check') {
                redirect(SITE_URL);
            }
            if ($this->emagid->route['controller'] == 'provider' && $this->emagid->route['action'] == 'pending') {
                redirect(SITE_URL);
            }
        }
    }

    public function getAvailbleTime()
    {
        // $provider = 
    }

    /**
     * POST data:
     * {
        date_of_session:2016-05-27
        time_start:5:00am
        time_end:12:00am
        full_day_av:false,
        available: true/false
     * }
     */
    public function set_calendar()
    {
        if(isset($_POST['type']) && $_POST['type'] == 'bulk'){
            // bulk set calendar
            $provider = $this->viewData->user;
            $fromDate = new \Carbon\Carbon($_POST['from']);
            $toDate = new \Carbon\Carbon($_POST['to']);

            $startString = $_POST['from_time'];
            $endString = $_POST['to_time'];
            $status = $_POST['available'];

            $days = null;
            /**
             * Recurring days
             */
            if($_POST['days']){
                $days = array_keys($_POST['days']);
            }

            while($fromDate->lte($toDate)){

                $date = $fromDate->toDateString();

                $start = new \Carbon\Carbon("$date $startString");
                $end = new \Carbon\Carbon("$date $endString");

                if($days && !in_array($fromDate->dayOfWeek, $days)){
                    $fromDate->addDay();
                    continue;
                }

                $hours = [];

                while($start->hour != $end->hour){
                    $hours[] = $start->hour;
                    $start->addHour();
                }

                \Model\Calendar::set($provider->id, $date, $hours, $status);

                $fromDate->addDay();
            }

            $n = new \Notification\ErrorHandler("Availability Updated");
            $_SESSION["notification"] = serialize($n);
            redirect(SITE_URL . 'provider/calendar');

        } else {
            $provider = $this->viewData->user;
            $date = $_POST['date_of_session'];
            $start = $_POST['time_start'];
            $end = $_POST['time_end'];
            $status = $_POST['available'] == 'true'?'available':'unavailable';

            $start = new \Carbon\Carbon("$date $start");
            $end = new \Carbon\Carbon("$date $end");

            $hours = [$start->hour];

            while($start->hour != $end->hour){
                $hours[] = $start->hour;
                $start->addHour();
            }

            \Model\Calendar::set($provider->id, $date, $hours, $status);
        }

    }

    public function set_calendar1()
    {
        $arr_time = ARRAY("5:00am",
            "6:00am",
            "7:00am",
            "8:00am",
            "9:00am",
            "10:00am",
            "11:00am",
            "12:00pm",
            "1:00pm",
            "2:00pm",
            "3:00pm",
            "4:00pm",
            "5:00pm",
            "6:00pm",
            "7:00pm",
            "8:00pm",
            "9:00pm",
            "10:00pm",
            "11:00pm",
            "11:59pm",
            "12:00am");

        $provider = \Model\Provider::getItem(\Emagid\Core\Membership::userId());

        // M stand for set bulk availability
        if (isset($_POST['type']) && $_POST['type'] == "m") {
            if (@$_POST['to_time'] == "12:00am") {
                $to_time = '11:59pm';
            } else {
                $to_time = $_POST['to_time'];
            }
            if (strtotime($_POST['from']) >= strtotime(date("Y-m-d"))) {
                $interval = $this->getInterval(date("Y-m-d", strtotime($_POST['from'])), date("Y-m-d", strtotime($_POST['to'])));
                if (in_array($_POST['from_time'], $arr_time) AND in_array($to_time, $arr_time) AND (strtotime($_POST['from_time']) < strtotime($to_time))) {
                    foreach ($interval as $value) {
                        $check = \Model\Calendar::getItem(null, ['where' => "active = '1' and provider_id = '" . $provider->id . "' and date_of_session = '" . $value . "'"]);
                        $check_aval = \Model\Schedule::getItem(null, ['where' => "active = '1' and provider_id = '" . $provider->id . "' and date_of_session = '" . $value . "'"]);

                        if (!$check_aval) {
                            $this->emagid->getDb()->execute("DELETE from calendar  where provider_id= '" . $provider->id . "' and date_of_session = '" . $value . "' ");

                            $calendar = new \Model\Calendar();

                            $calendar->time_start = @$_POST['from_time'];

                            $calendar->time_end = $to_time;

                            $calendar->provider_id = $provider->id;
                            if (isset($_POST['full_day_av']) && $_POST['full_day_av'] == "on") {
                                $calendar->full_day_av = "true";
                            }
                            if ($calendar->time_start == "5:00am" && $calendar->time_end == "11:59pm") {
                                $calendar->full_day_av = "true";

                            } else {
                                $calendar->full_day_av = "false";

                            }

                            $calendar->time_end;
                            $calendar->time_start;
                            $calendar->date_of_session = $value;
                            $calendar->save();
                        }
                    }
                }
            }
            redirect(SITE_URL . 'provider/calendar');
        } else {
            if (@$_POST['time_end'] == "12:00am") {
                @$_POST['time_end'] = '11:59pm';
            } else {
                @$_POST['time_end'] = @$_POST['time_end'];
            }

            if (strtotime(@$_POST['time_start']) >= strtotime(@$_POST['time_end'])) {
            } elseif (!isset($_POST['time_start']) || !isset($_POST['time_end'])) {
            } elseif (!in_array($_POST['time_start'], $arr_time) || !in_array($_POST['time_end'], $arr_time)) {
            } else {

                $calendar = new Model\Calendar();
                $calendar->date_of_session = @$_POST['date_of_session'];
                $calendar->time_start = @$_POST['time_start'];
                $calendar->time_end = @$_POST['time_end'];
                $calendar->full_day_av = @$_POST['full_day_av'];
                $calendar->provider_id = $provider->id;
                if (@$_POST['time_start'] == "5:00am" && @$_POST['time_end'] == "11:59pm") {
                    $calendar->full_day_av = "true";
                }
                $check2 = \Model\Calendar::getList(['where' => "active = '1' and provider_id = '" . $provider->id . "' and date_of_session = '" . $calendar->date_of_session . "'"]);
                if (count($check2) > 0) {
                    foreach ($check2 as $date) {
                        if (strtotime($date->time_start) >= strtotime($calendar->time_end) || strtotime($date->time_end) <= strtotime($calendar->time_start)) {
                            $a = Model\Calendar::getItem(null, ['where' => "active = '1' and provider_id = '" . $provider->id . "' and date_of_session = '" . $calendar->date_of_session . "' and time_end= '" . $_POST['time_start'] . "'"]);
                            $b = Model\Calendar::getItem(null, ['where' => "active = '1' and provider_id = '" . $provider->id . "' and date_of_session = '" . $calendar->date_of_session . "' and time_start= '" . $_POST['time_end'] . "'"]);


                            // rebuilt the calendar
                            if ($a && $b) {

                                $this->emagid->getDb()->execute("DELETE from calendar  where  time_end= '" . $_POST['time_start'] . "' and provider_id= '" . $provider->id . "' and date_of_session = '" . $calendar->date_of_session . "' ");
                                $this->emagid->getDb()->execute("DELETE from calendar  where  time_start= '" . $_POST['time_end'] . "' and provider_id= '" . $provider->id . "' and date_of_session = '" . $calendar->date_of_session . "' ");
                                $calendar = new Model\Calendar();
                                $calendar->date_of_session = @$_POST['date_of_session'];
                                $calendar->time_start = @$a->time_start;
                                $calendar->time_end = @$b->time_end;
                                $calendar->full_day_av = @$_POST['full_day_av'];
                                $calendar->provider_id = $provider->id;
                                $calendar->save();

                            } else {
                                if ($_POST['time_end'] == $date->time_start) {
                                    $calendar = Model\Calendar::getItem($date->id);
                                    $calendar->date_of_session = @$_POST['date_of_session'];
                                    $calendar->time_start = @$_POST['time_start'];
                                    $calendar->full_day_av = "false";
                                    $calendar->provider_id = $provider->id;
                                    $calendar->id = $date->id;
                                    $calendar->save();
                                } elseif ($_POST['time_start'] == $date->time_end) {
                                    $calendar = Model\Calendar::getItem($date->id);
                                    $calendar->date_of_session = @$_POST['date_of_session'];
                                    $calendar->time_end = @$_POST['time_end'];
                                    $calendar->full_day_av = "false";
                                    $calendar->provider_id = $provider->id;
                                    $calendar->id = $date->id;
                                    $calendar->save();
                                } else {
                                    $calendar = new Model\Calendar();
                                    $calendar->date_of_session = @$_POST['date_of_session'];
                                    $calendar->time_start = @$_POST['time_start'];
                                    $calendar->time_end = @$_POST['time_end'];
                                    $calendar->full_day_av = "false";
                                    $calendar->provider_id = $provider->id;

                                    $calendar->save();
                                }

                            }
                        }


                    }/////dsds
                } else {
                    $calendar = new Model\Calendar();
                    $calendar->date_of_session = @$_POST['date_of_session'];
                    $calendar->time_start = @$_POST['time_start'];
                    $calendar->time_end = @$_POST['time_end'];
                    $calendar->full_day_av = @$_POST['full_day_av'];
                    $calendar->provider_id = $provider->id;
                    if (@$_POST['time_start'] == "5:00am" && @$_POST['time_end'] == "11:59pm") {
                        $calendar->full_day_av = "true";
                    }
                    $calendar->save();
                }


            }

        }

        // now check provider if they are available for client appointment request
        $this->viewData->user->checkAppointmentRequest();
    }














    /* if(strtotime($calendar->time_start)>=strtotime($calendar->time_end)){
        
    }elseif(!isset($_POST['time_start']) || !isset($_POST['time_end'])){
        
    }elseif(!in_array($calendar->time_end, $arr_time) || !in_array($calendar->time_start, $arr_time)){
        
    }else{
       $calendar = new Model\Calendar();
       $calendar->date_of_session = @$_POST['date_of_session'];
       $calendar->time_start = @$_POST['time_start'];
       
       $calendar->full_day_av = @$_POST['full_day_av'];
       $calendar->provider_id = $provider->id;
       if ($calendar->time_start == "5:00am" && $calendar->time_end == "11:59pm")
       {
           $calendar->full_day_av = "true";
       }
       $check2 = \Model\Calendar::getList(['where' => "active = '1' and provider_id = '" . $provider->id . "' and date_of_session = '" . $calendar->date_of_session. "'"]); 
       if(count($check2)>0){
           foreach ($check2 as $date) {
                if(strtotime($date->time_start) >= strtotime($calendar->time_end) || strtotime($date->time_end) <= strtotime($calendar->time_start)){
                    $calendar = new Model\Calendar();
       $calendar->date_of_session = @$_POST['date_of_session'];
       $calendar->time_start = @$_POST['time_start'];
       if (@$_POST['time_end'] == "12:00am")
       {
           $calendar->time_end = '11:59pm';
       }
       else
       {
           $calendar->time_end = @$_POST['time_end'];
       }
       $calendar->full_day_av = @$_POST['full_day_av'];
       $calendar->provider_id = $provider->id;
       if ($calendar->time_start == "5:00am" && $calendar->time_end == "11:59pm")
       {
           $calendar->full_day_av = "true";
       }
                    $calendar->save();
                     
                }
           }
       }else{$calendar = new Model\Calendar();
       $calendar->date_of_session = @$_POST['date_of_session'];
       $calendar->time_start = @$_POST['time_start'];
       if (@$_POST['time_end'] == "12:00am")
       {
           $calendar->time_end = '11:59pm';
       }
       else
       {
           $calendar->time_end = @$_POST['time_end'];
       }
       $calendar->full_day_av = @$_POST['full_day_av'];
       $calendar->provider_id = $provider->id;
       if ($calendar->time_start == "5:00am" && $calendar->time_end == "11:59pm")
       {
           $calendar->full_day_av = "true";
       }$calendar->save();}
    }
       /*if ($calendar->save())
       {
           echo "<font color='green'>Information has been saved!</font>";
       }
       else
       {
           echo "<font color='red'>All fileds required!</font>";
       } 
   } */
    /*public function set_calendar(){
  
            $provider = \Model\Provider::getItem(\Emagid\Core\Membership::userId());
            
                           $time_start= @$_POST['time_start'];
            $time_end= @$_POST['time_end'];
            $full_day_av= @$_POST['full_day_av']; 
           
   
            
          
          if($full_day_av=="true")
          {
              $t1 = strtotime(date("H:i", strtotime ("5:00am")));
            $t2 = strtotime(date("H:i", strtotime ("11:59pm")));
          }else{
             $t1 = strtotime(date("H:i", strtotime ($time_start)));
            $t2 = strtotime(date("H:i", strtotime ($time_end)));
          }
             
            $diff = ( $t2 - $t1 );
            $interval = 60*60; //интервал 30 минут в секундах
            $d = ceil($diff/$interval);
            for($i=0; $i<$d; $i++)
            {
                $calendar = new \Model\Calendar();
                $calendar->date_of_session = @$_POST['date_of_session'];
                $calendar->provider_id = $provider->id;
                $calendar->time = date("g:ia", strtotime (date('H:i', ($i*$interval)+$t1 )));
                $calendar->save();
            }
        */
    /*if($calendar->save()){
        
       echo "<font color='green'>Information has been saved!</font>";
    }
    else{
         
        echo "<font color='red'>All fileds required!</font>";
    }
        }*/

    private function getInterval($start, $end, $format = 'Y-m-d')
    {
        return array_map(create_function('$item', 'return date("' . $format . '", $item);'), range(strtotime($start), strtotime($end), 60 * 60 * 24));
    }

    public function provider_schedule()
    {

        $pr = \Model\Provider::getItem($_POST['provider_id']);
        $day = date("Y-m-d", strtotime($_POST['day']));

        $calendar = \Model\Calendar::getList(['where' => "active = '1' and provider_id = '" . $pr->id . "'  and date_of_session = '" . $day . "'"]);


        foreach ($calendar as $time) {


            if ($time->full_day_av == "true") {
                $t1 = strtotime(date("H:i", strtotime("05:00:00")));
                $t2 = strtotime(date("H:i", strtotime("23:59:59")));
            } else {
                $t1 = strtotime(date("H:i", strtotime($time->time_start)));
                $t2 = strtotime(date("H:i", strtotime($time->time_end)));
            }

            $diff = ($t2 - $t1);
            $interval = 60 * 60; //интервал 30 минут в секундах
            $d = ceil($diff / $interval);
            $n = 0;
            for ($t = 0; $t < $d; $t++) {
                $check_val = \Model\Schedule::getItem(null, ['where' => "day = '" . $day . "' and provider_id = '" . $pr->id . "' and  time ='" . date("g:ia", strtotime(date('H:i', ($t * $interval) + $t1))) . "' and status!='2'"]);
                if (!$check_val) {
                    ?>

                    <div class="fl_left fl_33 available_time_button time_selected time_option"
                         when="<?= $day ?> <?= date('g:ia', ($t * $interval) + $t1); ?>">
                        <span class="time_text"><?= date('g:ia', ($t * $interval) + $t1); ?></span>
                    </div>
                    <?
                }

            }


        }
    }

    public function background_check()
    {
        $pr = \Model\Provider::getItem(\Emagid\Core\Membership::userId());
        if ($pr->sign == "") {
            if (@trim($_POST['sign']) !== "") {
                $pr->sign = trim($_POST['sign']);
                if (isset($_POST['minnesota_check'])) {
                    $pr->minnesota_check = $_POST['minnesota_check'];
                }
                if (isset($_POST['oklahoma_check'])) {
                    $pr->oklahoma_check = $_POST['oklahoma_check'];
                }
            }

            $pr->save();
        }

        $this->check();


        if ($this->roleType == 4 || $this->roleType == 2 || $this->roleType == 1 || is_null($this->roleType)) {
            redirect(SITE_URL);
        }

        $this->viewData->provider = \Model\Provider::getItem(\Emagid\Core\Membership::userId());


        $this->loadView($this->viewData);
    }

    public function background_check_authorization()
    {

        if ($this->roleType == 4 || $this->roleType == 2 || $this->roleType == 1 || is_null($this->roleType)) {
            redirect(SITE_URL);
        }
        $provider = \Model\Provider::getItem(\Emagid\Core\Membership::userId());
        $this->viewData->provider = \Model\Provider::getItem(\Emagid\Core\Membership::userId());
        if ($provider->disclosure_check !== 1) {
            $provider->disclosure_check = $_POST['d'];
            $provider->save();
        }
        $this->check();

        $this->loadView($this->viewData);
    }

    public function do_background_check()
    {
        define('CHECKR_API_KEY', 'edb94d17ddaea6e0122b7b76c878ccbaa24c30cb'); //works
        //define('CHECKR_API_KEY', '314f40b3181ae5572b70b12962d6638d3094dfa0'); //test

        if ($this->roleType == 4 || $this->roleType == 2 || $this->roleType == 1 || is_null($this->roleType)) {
            redirect(SITE_URL);
        }
        $this->viewData->provider = \Model\Provider::getItem(\Emagid\Core\Membership::userId());
        $pr = \Model\Provider::getItem(\Emagid\Core\Membership::userId());
        if (trim($_POST['first_name']) == "" ||
            trim($_POST['last_name']) == "" ||
            trim($_POST['ssn1']) == "" ||
            trim($_POST['ssn2']) == "" ||
            trim($_POST['ssn3']) == "" ||
            trim($_POST['mob']) == "" ||
            trim($_POST['dob']) == "" ||
            trim($_POST['yob']) == "" ||
            trim($_POST['zipcode']) == "" ||
            trim($_POST['phone']) == ""
        ) {
            $n = new \Notification\ErrorHandler("Please, fill out all fields!");
            $_SESSION["notification"] = serialize($n);
            redirect(SITE_URL . 'provider/background_check');
        }
        $dob = $_POST['yob'] . '-' . $_POST['mob'] . '-' . $_POST['dob'];
        $ssn = $_POST['ssn1'] . '-' . $_POST['ssn2'] . '-' . $_POST['ssn3'];
///////////////////create candidate/////////////////
        $pr->zip = trim($_POST['zipcode']);
        $pr->phone = trim($_POST['phone']);
        $pr->dob = $dob;
        $pr->save();
        $candidate_params = [
            "first_name" => trim($_POST['first_name']),
            "middle_name" => trim($_POST['middle_name']),
            "last_name" => trim($_POST['last_name']),
            "dob" => $dob,
            "ssn" => $ssn,
            "zipcode" => trim($_POST['zipcode']),
            "phone" => trim($_POST['phone']),
            "email" => $this->viewData->provider->email
        ];
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://api.checkr.io/v1/candidates');
        curl_setopt($curl, CURLOPT_USERPWD, CHECKR_API_KEY . ":");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($candidate_params));
        $json = curl_exec($curl);
        $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
//echo "status:" . $http_status . "\n" . $json . "\n\n";
        $response = json_decode($json);

        if (isset($response->id)) {
            $pr->candidate_id = $response->id;
            $pr->save();
        } else {
            $n = new \Notification\ErrorHandler("Something happend with background check, please contact support");
            $_SESSION["notification"] = serialize($n);
        }
        $candidate_id = $response->id;
        $report_params = [
            "candidate_id" => $candidate_id,
            "package" => "tasker_basic",
        ];
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://api.checkr.io/v1/reports');
        curl_setopt($curl, CURLOPT_USERPWD, CHECKR_API_KEY . ":");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($report_params));
        $json = curl_exec($curl);
        $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $response = json_decode($json);
        if (isset($response->id)) {
            $pr->report_id = $response->id;
            $pr->save();
        }
        redirect(SITE_URL);
///////////////////create candidate/////////////////
        $this->template = FALSE;
    }

    public function acceptSession()
    {
        $sessionId = $_GET['session'];
        $sessionHistory = \Model\SessionHistory::getItem(null, ['where' => "id= $sessionId and provider_id = {$this->viewData->user->id}"]);
        if ($sessionHistory) {
            $sessionHistory->markStatus(1, true);

            $n = new \Notification\MessageHandler("Session accepted. Have a good time.");
            $_SESSION["notification"] = serialize($n);

             $this->viewData->user->sendAcceptSessionEmail($sessionHistory);
        }

        redirect('/provider/dashboard');
    }

    public function declineSession()
    {
        $sessionId = $_GET['session'];
        $sessionHistory = \Model\SessionHistory::getItem(null, ['where' => "id= $sessionId and provider_id = {$this->viewData->user->id}"]);
        if ($sessionHistory) {
            $sessionHistory->markStatus(2, true);

            $n = new \Notification\MessageHandler("Reported to customer, thanks heads up.");
            $_SESSION["notification"] = serialize($n);

            $this->viewData->user->sendCancelSessionEmail($sessionHistory);
        }

        redirect('/provider/dashboard');
    }

    public function cancelSession()
    {
        $sessionId = $_GET['session'];
        $messageId = $_GET['cancel-session-message'];
        $sessionHistory = \Model\SessionHistory::getItem(null, ['where' => "id= $sessionId and provider_id = {$this->viewData->user->id}"]);
        if ($sessionHistory) {
            $sessionHistory->markStatus(7, true);
            $sessionHistory->markCancelMessage($messageId);

            $n = new \Notification\MessageHandler("Session Canceled");
            $_SESSION["notification"] = serialize($n);

            $this->viewData->user->sendCancelSessionEmail($sessionHistory);
        }

        redirect('/provider/dashboard');
    }

    public function completeSession()
    {
        $sessionId = $_GET['session'];
        $sessionHistory = \Model\SessionHistory::getItem(null, ['where' => "id= $sessionId and provider_id = {$this->viewData->user->id}"]);
        if ($sessionHistory) {
            $sessionHistory->markStatus(3, true);

            $n = new \Notification\MessageHandler("Thanks confirming it, customer will confirm it soon.");
            $_SESSION["notification"] = serialize($n);

            $this->viewData->user->sendConfirmSessionEmail($sessionHistory);
        }

        redirect('/provider/dashboard');
    }

    public function incompleteSession()
    {
        $sessionId = $_GET['session'];
        $sessionHistory = \Model\SessionHistory::getItem(null, ['where' => "id= $sessionId and provider_id = {$this->viewData->user->id}"]);
        if ($sessionHistory) {
            $sessionHistory->markStatus(5, true);

            $n = new \Notification\MessageHandler("Thanks reporting it, a wellns customer representative will contact you soon.");
            $_SESSION["notification"] = serialize($n);

            $this->viewData->user->sendIncompleteSessionEmail($sessionHistory);
        }

        redirect('/provider/dashboard');
    }

    public function calendar()
    {
        $this->check();

        if ($this->roleType == 4 || $this->roleType == 2 || $this->roleType == 1 || is_null($this->roleType)) {
            redirect(SITE_URL);
        }
        $this->loadView($this->viewData);
    }

    /*        public function make_withdraw()
        {

            if (@\Model\Provider::getItem(null, ['where' => "active = '1' and email = '" . $this->viewData->user->email . "'"])) {
                $who_logged = 3;
            }    //provider
            elseif (@\Model\User::getItem(null, ['where' => "active = '1' and email = '" . $this->viewData->user->email . "'"])) {
                $who_logged = 2;
            }//user
            else {
                $who_logged = 1;
            }    //admin

            if ($who_logged == 2 || $who_logged == 1) {
                redirect(SITE_URL);
            }
            $time_m = date("Y-m",strtotime("-1 months"));
            $date =  $time_m."-15";
            $this->viewData->withdraw = \Model\Schedule::getList(['where' => "active='1' and provider_id = '" . $this->viewData->user->id . "' and status='1' and day>='" . $date . "'"]);
               $total = 0;
               foreach ($this->viewData->withdraw as $past) {
                                     $user = \Model\User::getItem($past->user_id);
                                     $service = \Model\Service::getItem($past->service);
                                     $order_products = \Model\Order_Product::getItem($past->order_product_id);
                                     $packages = \Model\Package::getItem($order_products->product_id) ;
                                     $total+=$packages->price/$packages->quantity;

                                     }
            $new_withdraw = new \Model\Withdraw();
            $new_withdraw->provider_id=$this->viewData->user->id;
            $new_withdraw->status = 0;
            $new_withdraw->amount = $total;
            if($total>0){
              $new_withdraw->save();
              $n = new \Notification\MessageHandler("Your withdrawal is pending!");
              $_SESSION["notification"] = serialize($n);
              redirect(SITE_URL.'provider/dashboard/');
            }else{
               $n = new \Notification\MessageHandler("Your balance is 0!");
              $_SESSION["notification"] = serialize($n);
              redirect(SITE_URL.'provider/dashboard/');
            }
            $this->loadView($this->viewData);
        }*/

    public function dashboard()
    {

        $this->check();

        if ($this->roleType == 4 || $this->roleType == 2 || $this->roleType == 1 || is_null($this->roleType)) {
            redirect(SITE_URL);
        }
        $subjectArray = \Model\Subject::getSubjectArray($this->viewData->user, ['provider_id' => $this->viewData->user->id]);
        $this->viewData->readSubject = $subjectArray['read'];
        $this->viewData->unreadSubject = $subjectArray['unread'];

        $this->viewData->paymentHistory = $this->viewData->user->getPaymentHistory();
        $this->viewData->upcoming = $this->viewData->user->getUpcomingSessions();
        $this->viewData->pendingConfirms = $this->viewData->user->getWaitingConfirmSessions();
        $this->viewData->history = $this->viewData->user->getHistorySessions();
        $this->viewData->requested = $this->viewData->user->getAllRequestedSessions();

        $sql = "SELECT * FROM days where  date>='NOW()' order by date limit 5";
        $this->viewData->day_list = \Model\Day::getList(['sql' => $sql]);
        $this->configs['Meta Title'] = 'Dashboard | Wellns';
        $this->loadView($this->viewData);
        
    }

    public function profile(Array $params = [])
    {
        $pro = (isset($params['id'])) ? $params['id'] : '';
        $my_id = null;
        if($this->viewData->user && $this->roleType == 3){
            $my_id = $this->viewData->user->id;
        }
        $who_logged = $this->roleType;

        if ($who_logged == 3 and (isset($params['id']))) {
            if ($params['id'] !== $my_id) {
                redirect(SITE_URL);
            }
        }
        if (!isset($params['id'])) {
            $pro = $my_id;
        }
        if ($who_logged == 2 and (isset($params['id']))) {
            $pro = $params['id'];
        }
        if ($who_logged == 2 and (!isset($params['id']))) {
            redirect(SITE_URL);
        }
        $this->viewData->pr = \Model\Provider_Services::getList(['where' => "active = '1' and provider_id = '" . $pro . "'"]);
        $this->viewData->provider = $provider = \Model\Provider::getItem(null, ['where' => "authorized = 'TRUE' and  id ='" . $pro . "'"]);
        $this->viewData->provider_services = \Model\Provider_Services::getList(['where' => "active = '1' and  provider_id= '" . $pro . "'"]);
        $this->viewData->otherProviders = $provider->getRelatedProvidersByService();
        if (!$this->viewData->provider) {
            redirect(SITE_URL);
        }
        $this->configs['Meta Title'] = $this->viewData->provider->first_name . ' ' . $this->viewData->provider->last_name[0];
        $this->viewData->fav = @\Model\User_Favorite::getItem(NULL, ['where' => 'active = 1 and user_id = ' . $this->viewData->user->id . ' and provider_id=' . $pro . '']);
        $this->loadView($this->viewData);
    }

    public function manage()
    {
        $this->check();

        if ($this->roleType == 4 || $this->roleType == 2 || $this->roleType == 1 || is_null($this->roleType)) {
            redirect(SITE_URL);
        }


        if (!is_null($this->viewData->user)) {

            $this->viewData->package = \Model\Package::getItem(null, ['where' => 'provider_id = ' . $this->viewData->user->id]);
            if (is_null($this->viewData->package)) {
                $this->viewData->package = new \Model\Package();
            }

            $this->loadView($this->viewData);
        }
    }

    public function services()
    {
        $id = $_POST['cur_user_id'];

        $pr = \Model\Provider_Services::getList(['where' => "active = '1' and provider_id = '" . $id . "'"]);

        foreach ($pr as $value) {
            if ($value->sub_service == 0) {
                echo '<option value="' . $value->service_id . '">' . \Model\Service::getItem($value->service_id)->name . '</option>';
            }

        }
        $this->template = FALSE;

    }

    public function manage_post()
    {
        $response = (object)[
            'status' => 'error'
        ];
        if (!is_null($this->viewData->user) && \Emagid\Core\Membership::isInRoles(['provider'])) {
            foreach ($_POST as $property => $value) {
                $this->viewData->user->$property = $value;
            }

            $this->viewData->user->save();
            $this->viewData->user->updateSteps();
            if (count($_FILES) > 0) {
                foreach ($_FILES as $fileType => $file) {
                    if ($file['error'] == 0) {
                        $imageKey = $this->viewData->user->uploadPicture($_FILES[$fileType]);
                        $this->viewData->user->$fileType = $imageKey;
                        $this->viewData->user->save();
                    }
                }
            };
            $this->viewData->user->login();
            $response->status = 'success';
        }
        echo json_encode($response);
    }

    public function edit_profile()
    {
        foreach ($_POST as $key => $value) {
            $pr = \Model\Provider::getItem(\Emagid\Core\Membership::userId());
            if ($key == "e_signature") {
                $img = $this->sigJsonToImage($value);
                $name = md5(mt_rand(1000, 999999) . time());
                imagepng($img, $name . '.png');
                rename($name . ".png", "content/uploads/sign/" . $name . ".png");
                $file['name'] = $name.".png";
                $file['tmp_name'] = "content/uploads/sign/" . $name . ".png";
                $s3 = new \s3Bucket\s3Handler();
                $s3Key = $s3->upload($file);
                imagedestroy($img);
                $pr->$key = $s3Key;



            } elseif($key == 'equipment'){
                $pr->$key = $_POST['equipment'];
            } else {
                $pr->$key = trim($value);
            }


            $pr->save();
        }
    }

    private function sigJsonToImage($json, $options = array())
    {
        $defaultOptions = array(
            'imageSize' => array(198, 55)
        , 'bgColour' => array(0xff, 0xff, 0xff)
        , 'penWidth' => 2
        , 'penColour' => array(0x14, 0x53, 0x94)
        , 'drawMultiplier' => 12
        );
        $options = array_merge($defaultOptions, $options);
        $img = imagecreatetruecolor($options['imageSize'][0] * $options['drawMultiplier'], $options['imageSize'][1] * $options['drawMultiplier']);
        if ($options['bgColour'] == 'transparent') {
            imagesavealpha($img, true);
            $bg = imagecolorallocatealpha($img, 0, 0, 0, 127);
        } else {
            $bg = imagecolorallocate($img, $options['bgColour'][0], $options['bgColour'][1], $options['bgColour'][2]);
        }
        $pen = imagecolorallocate($img, $options['penColour'][0], $options['penColour'][1], $options['penColour'][2]);
        imagefill($img, 0, 0, $bg);
        if (is_string($json))
            $json = json_decode(stripslashes($json));
        foreach ($json as $v)
            $this->drawThickLine($img, $v->lx * $options['drawMultiplier'], $v->ly * $options['drawMultiplier'], $v->mx * $options['drawMultiplier'], $v->my * $options['drawMultiplier'], $pen, $options['penWidth'] * ($options['drawMultiplier'] / 2));
        $imgDest = imagecreatetruecolor($options['imageSize'][0], $options['imageSize'][1]);
        if ($options['bgColour'] == 'transparent') {
            imagealphablending($imgDest, false);
            imagesavealpha($imgDest, true);
        }
        imagecopyresampled($imgDest, $img, 0, 0, 0, 0, $options['imageSize'][0], $options['imageSize'][0], $options['imageSize'][0] * $options['drawMultiplier'], $options['imageSize'][0] * $options['drawMultiplier']);
        imagedestroy($img);
        return $imgDest;
    }

    /**
     *  Draws a thick line
     *  Changing the thickness of a line using imagesetthickness doesn't produce as nice of result
     *
     * @param object $img
     * @param int $startX
     * @param int $startY
     * @param int $endX
     * @param int $endY
     * @param object $colour
     * @param int $thickness
     *
     * @return void
     */
    private function drawThickLine($img, $startX, $startY, $endX, $endY, $colour, $thickness)
    {
        $angle = (atan2(($startY - $endY), ($endX - $startX)));
        $dist_x = $thickness * (sin($angle));
        $dist_y = $thickness * (cos($angle));
        $p1x = ceil(($startX + $dist_x));
        $p1y = ceil(($startY + $dist_y));
        $p2x = ceil(($endX + $dist_x));
        $p2y = ceil(($endY + $dist_y));
        $p3x = ceil(($endX - $dist_x));
        $p3y = ceil(($endY - $dist_y));
        $p4x = ceil(($startX - $dist_x));
        $p4y = ceil(($startY - $dist_y));
        $array = array(0 => $p1x, $p1y, $p2x, $p2y, $p3x, $p3y, $p4x, $p4y);
        imagefilledpolygon($img, $array, (count($array) / 2), $colour);
    }

    public function add_package_post()
    {
        $response = (object)['status' => 'error'];
        $pr = \Model\Provider::getItem(\Emagid\Core\Membership::userId());

        foreach ($_POST as $key => $service) {
            \Model\Provider_Services::validateProviderServices($pr->id, $key, $service);
        }
        $this->viewData->user->save();
        if (!is_null($this->viewData->user) && \Emagid\Core\Membership::isInRoles(['provider'])) {


            $this->viewData->user->updateSteps();
            $this->viewData->user->save();
            /*if(count($_FILES) > 0){
                foreach($_FILES as $fileType=>$file){
                    if ($file['error'] == 0){
                        $image = new \Emagid\Image();
                        $image->upload($_FILES[$fileType], UPLOAD_PATH.'packages'.DS);
                        $package->$fileType = $image->fileName;
                        $package->save();
                    }
                }
            };*/
            $response->status = 'success';
        }
        echo json_encode($response);
    }

    public function submit()
    {
        $response = (object)[
            'status' => 'error'
        ];
        if (!is_null($this->viewData->user) && \Emagid\Core\Membership::isInRoles(['provider'])) {
            $this->viewData->user->step_2_completed = TRUE;
            $this->viewData->user->save();

            $this->viewData->user->updateSteps();
            $response->status = 'success';
        }
        echo json_encode($response);
    }

    public function agreed_post()
    {
        $response = (object)[
            'status' => 'error'
        ];
        if (!is_null($this->viewData->user) && \Emagid\Core\Membership::isInRoles(['provider']) && isset($_POST['field'])) {
            $field = 'agreed_' . $_POST['field'];

            $this->viewData->user->$field = date('Y-m-d H:i:s');
            $this->viewData->user->save();

            $this->viewData->user->updateSteps();
            $response->status = 'success';
        }
        echo json_encode($response);
    }

    public function update_image()
    {
        $fileName = rand(0, 9999999999999999999);
        $what = $_POST['what_image'];
        foreach ($_FILES as $fileType => $file) {
//            $image = new \Emagid\Image();
//            $image->upload($_FILES[$fileType], UPLOAD_PATH . 'providers' . DS);
            if ($what == 1) {
                $imageKey = $this->viewData->user->uploadPicture($_FILES[$fileType]);
                $this->viewData->user->photo = $imageKey;
            } elseif ($what == 2) {
                $imageKey = $this->viewData->user->uploadPicture($_FILES[$fileType]);
                $this->viewData->user->extra_photo_1 = $imageKey;
            } else {
                $imageKey = $this->viewData->user->uploadPicture($_FILES[$fileType]);
                $this->viewData->user->extra_photo_2 = $imageKey;
            }
            $this->viewData->user->save();
            redirect(SITE_URL . 'provider/dashboard');

        }


    }

    public function pending()
    {
        $this->check();
        $who_logged = $this->roleType;

        if ($who_logged == 2 || $who_logged == 3) {
            redirect(SITE_URL);
        }
        /*if (!is_null($this->viewData->user)){
            if ($this->viewData->user->step_2_completed){
                if ($this->viewData->user->authorized){
                    redirect(SITE_URL.'provider/dashboard');
                } else {
                    $this->loadView($this->viewData);					
                }
            } else {
                $this->viewData->package = \Model\Package::getItem(null, ['where'=>'provider_id = '.$this->viewData->user->id]);
                if (is_null($this->viewData->package)){
                    $this->viewData->package = new \Model\Package();
                }
                $this->loadView($this->viewData);
            }
        } else {
            redirect(SITE_URL);
        }*/
        $this->loadView($this->viewData);
    }

    public function session_status()
    {
        $status = \Model\Schedule::getItem($_POST['day_id']);
        $status->status = @trim($_POST['approve']);


        $status->id = $_POST['day_id'];
        $status->save();
        if ($_POST['approve'] == 1) {
            $this->accepted_session($_POST['day_id']);
        }
        if ($_POST['approve'] == 2) {
            $product = \Model\Order_Product::getItem($status->order_product_id);
            $product->used = $product->used - 1;
            $product->save();
            $this->rejected_session($_POST['day_id']);
        }
    }

    private function accepted_session($id)
    {
        global $emagid;
        $emagid->email->from->email = 'noreply@wellns.com';
        $schedule = \Model\Schedule::getItem($id);
        $mail = \Model\Mail::getItem(5);
        $text = $mail->email;

        $time = date('F d Y  \a\t g:ia', strtotime($schedule->day . ' ' . $schedule->time));
        //{{time_and_date}}
        $text = str_replace("{{time_and_date}}", $time, $text);


        $service = \Model\Service::getItem($schedule->service);

        //{{service_name}}
        $text = str_replace("{{service_name}}", $service->name, $text);

        $day = date('d', strtotime($schedule->day));
        //{{day}}
        $text = str_replace("{{day}}", $day, $text);
        $month = date('M', strtotime($schedule->day));
        //{{month}}
        $text = str_replace("{{month}}", $month, $text);


        $provider = \Model\Provider::getItem($schedule->provider_id);
        $user = \Model\User::getItem($schedule->user_id);
        $pr_name = $provider->first_name . ' ' . $provider->last_name;
        //{{location}}
        $text = str_replace("{{location}}", $schedule->session_location, $text);

        //{{provider_full_name}}
        $text = str_replace("{{provider_full_name}}", $pr_name, $text);
        $user_name = $user->first_name . ' ' . $user->last_name;

        $user_name = $user->first_name . ' ' . $user->last_name;
        //{{user_full_name}}
        $text = str_replace("{{user_full_name}}", $user_name, $text);
        $email = new \Emagid\Email();

        $email->addTo($user->email);
        $email->addTo("john@emagid.com");
        $email->addTo("eitan@emagid.com");
        $email->subject('Your session accepted!');
        $email->body = $text;


        $email->send();
    }

    private function rejected_session($id)
    {
        global $emagid;
        $emagid->email->from->email = 'noreply@wellns.com';
        $schedule = \Model\Schedule::getItem($id);
        $mail = \Model\Mail::getItem(6);
        $text = $mail->email;

        $time = date('F d Y  \a\t g:ia', strtotime($schedule->day . ' ' . $schedule->time));
        //{{time_and_date}}
        $text = str_replace("{{time_and_date}}", $time, $text);


        $service = \Model\Service::getItem($schedule->service);

        //{{service_name}}
        $text = str_replace("{{service_name}}", $service->name, $text);

        $day = date('d', strtotime($schedule->day));
        //{{day}}
        $text = str_replace("{{day}}", $day, $text);
        $month = date('M', strtotime($schedule->day));
        //{{month}}
        $text = str_replace("{{month}}", $month, $text);


        $provider = \Model\Provider::getItem($schedule->provider_id);
        $user = \Model\User::getItem($schedule->user_id);
        $pr_name = $provider->first_name . ' ' . $provider->last_name;


        //{{provider_full_name}}
        $text = str_replace("{{provider_full_name}}", $pr_name, $text);
        $user_name = $user->first_name . ' ' . $user->last_name;

        $user_name = $user->first_name . ' ' . $user->last_name;
        //{{user_full_name}}
        $text = str_replace("{{user_full_name}}", $user_name, $text);


        $time = date('F d Y  \a\t g:ia', strtotime($schedule->day . ' ' . $schedule->time));
        //{{time_and_date}}
        $text = str_replace("{{time_and_date}}", $time, $text);

        $service = \Model\Service::getItem($schedule->service);

        //{{service_name}}
        $text = str_replace("{{service_name}}", $service->name, $text);

        $provider = \Model\Provider::getItem($schedule->provider_id);
        $user = \Model\User::getItem($schedule->user_id);
        $pr_name = $provider->first_name . ' ' . $provider->last_name;

        //{{provider_first_name}}
        $text = str_replace("{{provider_first_name}}", $provider->first_name, $text);


        //{{provider_full_name}}
        $text = str_replace("{{provider_full_name}}", $pr_name, $text);
        $user_name = $user->first_name . ' ' . $user->last_name;

        $user_name = $user->first_name . ' ' . $user->last_name;
        //{{user_full_name}}
        $text = str_replace("{{user_full_name}}", $user_name, $text);
        $email = new \Emagid\Email();

        $email->addTo($user->email);
        $email->addTo("john@emagid.com");
        $email->addTo("eitan@emagid.com");
        $email->subject('Your session rejected!');
        $email->body = $text;
        $q = "
<html xmlns='http://www.w3.org/1999/xhtml' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'><head style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
<meta name='viewport' content='width=device-width' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
<link href='http://fonts.googleapis.com/css?family=Varela' rel='stylesheet' type='text/css' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
<title style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>Session Request Rejected</title>

</head>

<body bgcolor='#FFFFFF' style='font-family:'Varela', sans-serif;margin: 0;padding: 0;font-size: 100%;line-height: 1.6;-webkit-font-smoothing: antialiased;-webkit-text-size-adjust: none;height: 100%;letter-spacing: .4px;'>

<!-- body -->
<table class='body-wrap' bgcolor='#FFFFFF' style='margin: 0;padding: 20px;font-size: 100%;line-height: 1.6;width: 100%;'>
	<tbody style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'><tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
		<td style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'></td>
		<td class='container' bgcolor='#FFFFFF' style='margin: 0 auto!important;padding: 8px 0 20px 0;font-size: 100%;line-height: 1.6;border: 1px solid transparent;display: block!important;max-width: 800px!important;width:100%;clear: both!important;'>

			<!-- content -->
			<div class='content' style='margin: 0 auto;padding: 0;font-size: 100%;line-height: 1.6;max-width: 600px;display: block;'>
			<table style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;width: 100%;'>
				<tbody style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'><tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
					<td style='text-align: center;margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
						<a href = 'https://wellns.com'>
							<img style='width: 200px;margin-bottom: 18px;padding: 0;font-size: 100%;line-height: 1.6;max-width: 100%;' src='http://wellns.com/content/frontend/assets/img/email_imgs/logo.png'>
						</a>
						<p style = 'border-top:1px solid transparent;margin-top:8px;margin-bottom:24px;'></p>
						<img style='width: 210px;margin-bottom: 0px;padding: 0;font-size: 100%;line-height: 1.6;max-width: 100%;margin-bottom:18px;' src='http://wellns.com/content/frontend/assets/img/email_imgs/rejected_sprite.png'>
						<p style='text-align: center;font-size: 22px;margin: 0;padding: 0;line-height: 1.6;margin-bottom: 10px;font-weight: normal;'>Hi $user_name,</p>
						<p style='font-size: 18px;line-height: 26.5px;text-align: center;padding: 0;margin-bottom: 10px;font-weight: normal;'>
						The session you booked for $time was not approved by $pr_name. Please visit $provider->first_name's calendar to book a session at a different date or time. We're sorry for any inconvenience this may have caused you.
						</p>
						<a href = 'https://wellns.com/user/dashboard' target='_blank' >
						<img style='margin-top: 2px;margin-bottom:8px;width: 330px;cursor: pointer;padding: 0;font-size: 100%;line-height: 1.6;max-width: 100%;' src='http://wellns.com/content/frontend/assets/img/email_imgs/view_calendar_btn.png'>
						</a>

					</td>
				</tr>
			</tbody></table>
			</div>
			<!-- /content -->

		</td>
		<td style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'></td>
	</tr>
</tbody></table>
<!-- /body -->

<!-- footer -->
<table class='footer-wrap' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;width: 100%;clear: both!important;'>
	<tbody style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'><tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
		<td style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'></td>
		<td class='container' style='margin: 0 auto!important;padding: 0;font-size: 100%;line-height: 1.6;display: block!important;max-width: 600px!important;clear: both!important;'>

			<!-- content -->
			<div class='content' style='margin: 0 auto;padding: 0;font-size: 100%;line-height: 1.6;max-width: 600px;display: block;'>
				<table style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;width: 100%;'>
					<tbody style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'><tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
						<td align='center' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
							<p style='margin: 0;padding: 0;font-size: 12px;line-height: 1.6;margin-bottom: 10px;font-weight: normal;color: #666;'>
								<a class='text-decoration:none;color:#a0c2ce;' href='https://wellns.com/' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;color: #a0c2ce;text-decoration: none;'><unsubscribe style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>Unsubscribe</unsubscribe></a>
								<span style='color: #000000;margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>·</span>
								<a class='text-decoration:none;color:#a0c2ce;' href='https://wellns.com/' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;color: #a0c2ce;text-decoration: none;'>Contact Us</a>
								<span style='color: #000000;margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>·</span>
								<a class='text-decoration:none;color:#a0c2ce;' href='https://wellns.com/' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;color: #a0c2ce;text-decoration: none;'>Privacy Policy</a>
							</p>
						</td>
					</tr>
					<tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
						<td align='center' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
							<p style='color: #8b8b8b;font-size: 9px;width: 80%;max-width: 600px;margin: 0;padding: 0;line-height: 1.6;margin-bottom: 10px;font-weight: normal;'>
							© 2015 Wellns. The Wellns logo is a registered trademark of Wellns LLC. All rights reserved. This email has been sent to your by Wellns, 225 West 39th Street, New York City, NY 10009. Made by <b style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>Emagid</b>
							</p>
						</td>
					</tr>
				</tbody></table>
			</div>
			<!-- /content -->

		</td>
		<td style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'></td>
	</tr>
</tbody></table>
<!-- /footer -->



</body></html>";

        $email->send();
    }

    public function rebuild()
    {
        $provider = \Model\Provider::getItem(\Emagid\Core\Membership::userId());

        try{
            $start = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$_POST['start']);
            $end = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$_POST['end']);
        } catch(Exception $e){
            $start = new \Carbon\Carbon($_POST['date']. ' '.$_POST['start']);
            $end = new \Carbon\Carbon($_POST['date']. ' '.$_POST['end']);
        }

        $hours = [];
        while($start->lte($end)){
            $hours[] = $start->hour;
            $start->hour += 1;
        }

        \Model\Calendar::deleteCalendar($provider->id, $end->toDateString(), $hours);

//        $this->emagid->getDb()->execute("DELETE from calendar  where  id= '" . $_POST['id'] . "' and provider_id= '" . $provider->id . "'");


    }

    public function send_message()
    {
        if ((trim($_POST['subject']) == "") || (trim($_POST['user']) == "") || (trim($_POST['text']) == "")) {
            echo "All fields required!";
        } else {
            $user = \Model\User::getItem($_POST['user']);
            $provider = $this->viewData->user;
            \Model\Message::send($user, $provider, $_POST['subject'], $_POST['text']);
        }
    }

    public function step_one_complete()
    {

        if (isset($_COOKIE["confirm"])) {
            $this->viewData->provider = \Model\Provider::getItem($_COOKIE["confirm"]);
            setcookie("confirm", "", time() - 3600);
            $this->loadView($this->viewData);
        } else {
            redirect(SITE_URL);
        }
    }

    public function register_post()
    {
        $user = \Model\User::getItem(null, ['where' => "active = '1' and email = '" . $_POST['email'] . "'"]);
        if (count($user) > 0) {
            $n = new \Notification\ErrorHandler("We're sorry, but a Wellns member already uses that email address.");
            $_SESSION["notification"] = serialize($n);

            redirect(SITE_URL);
        } else {
            $provider = \Model\Provider::loadFromPost();
            $provider->authorized = false;
            $hash = \Emagid\Core\Membership::hash($provider->password);
            $provider->password = $hash['password'];
            $provider->hash = $hash['salt'];
            $provider->ref_key = md5(time() . mt_rand(1000000, 99999999));
            /*if (isset($_POST['dob']) && isset($_POST['mob']) && isset($_POST['yob']) && checkdate($_POST['mob'], $_POST['dob'], $_POST['yob'])){
                $provider->dob = $_POST['yob'].'-'.$_POST['mob'].'-'.$_POST['dob'];
            } else {
                $provider->dob = null;
            }
            $provider_services = [];
            foreach($_POST['provider_services'] as $serviceId => $selected){
                if ($selected && !is_null(\Model\Service::getItem($serviceId))){
                    $provider_service = new \Model\Provider_Services();
                    $provider_service->service_id = $serviceId;
                    $provider_services[] = $provider_service;
                }
            }
            if (count($provider_services) <= 0 || count($provider_services) > 3){
                $n = new \Notification\ErrorHandler('You must select at least one, and no more than three Specialties.');
                $_SESSION["notification"] = serialize($n);
                redirect(SITE_URL.'landing');
            }
    */
            if ($provider->save()) {
                $providerRoles = \Model\Provider_Roles::getList(['where' => 'active = 1 and role_id = 1 and provider_id = ' . $provider->id]);
                if (count($providerRoles) <= 0) {
                    $providerRoles = new \Model\Provider_Roles();
                    $providerRoles->role_id = 3;
                    $providerRoles->provider_id = $provider->id;
                    $providerRoles->save();
                }
                /*	foreach($provider_services as $provider_service){
                        $provider_service->provider_id = $provider->id;
                        $provider_service->save();
                    }*/
                //$this->sendProviderRegisteredEmail($provider);
                //
                setcookie("confirm", $provider->id, time() + 3600);

                if (isset($_COOKIE['ref_u'])) {

                    if ($user_check = \Model\Provider::getItem(null, ['where' => "active = '1' and ref_key = '" . $_COOKIE['ref_u'] . "'"])) {
                        $user_id_from = $user_check->id;
                        $user_type_from = "provider";
                    }    //provider
                    elseif ($user_check = \Model\User::getItem(null, ['where' => "active = '1' and ref_key = '" . $_COOKIE['ref_u'] . "'"])) {
                        $user_id_from = $user_check->id;
                        $user_type_from = "user";
                    }//user

                    if (isset($user_check)) {
                        $ref = new \Model\Referral();
                        $ref->user_id_from = $user_id_from;
                        $ref->user_id_to = $provider->id;
                        $ref->user_type_from = $user_type_from;
                        $ref->user_type_to = "provider";
                        $ref->save();
                    }


                    setcookie('ref_u', "", -3600, "/");
                }
                $this->sendProviderRegisteredEmail($provider->id);
                redirect(SITE_URL . 'provider/step_one_complete');

            } else {
                $n = new \Notification\ErrorHandler($provider->errors);
                $_SESSION["notification"] = serialize($n);
                redirect(SITE_URL);
            }

        }
    }

    private function sendProviderRegisteredEmail($provider)
    {
        global $emagid;
        $emagid->email->from->email = 'noreply@wellns.com';
        $provider = \Model\Provider::getItem($provider);
        $pr_name = $provider->first_name . ' ' . $provider->last_name;
        $mail = \Model\Mail::getItem(4);
        $text = $mail->email;
        //{{provider_full_name}}
        $text = str_replace("{{provider_full_name}}", $pr_name, $text);
        $email = new \Emagid\Email();

        $email->addTo($provider->email);
        $email->addTo("john@emagid.com");
        $email->addTo("eitan@emagid.com");
        $email->subject('Please Confirm your Email');
        $provider->key = md5($provider->email . date('mY'));
        $provider->save();
        $link = $provider->key;
        //{{link}}
        $text = str_replace("{{link}}", $link, $text);
        $email->body = $text;

        $email->send();
    }

    public function confirm(Array $params = [])
    {
        $provider = \Model\Provider::getItem(null, ['where' => "key = '" . $params['id'] . "'"]);
        if (@strlen($provider->key) > 0) {
            $provider->key = null;
            $provider->save();
            $provider->login();
            redirect(SITE_URL . 'provider/manage');
        } else {
            redirect(SITE_URL);
        }
    }

    public function change_password()
    {

        $user = \Model\Provider::getItem(\Emagid\Core\Membership::userId());
        $current_pass = $user->password;
        $curr_password = @trim($_POST['curr_password']);
        $new_password = @trim($_POST['new_password']);
        $new_password_2 = @trim($_POST['new_password_2']);
        $hash = \Emagid\Core\Membership::hash($curr_password, $user->hash);

        if ($current_pass !== $hash['password']) {
            echo "<font color='red'>Current password is wrong!</font>";
        } else {
            if ($new_password !== $new_password_2) {
                echo "<font color='red'>New passwords doesn`t match!</font>";
            } elseif (empty($new_password) || empty($new_password_2)) {
                echo "<font color='red'>New passwords are empty!</font>";
            } elseif ((strlen($new_password) < 7) || (strlen($new_password_2) < 7)) {
                echo "<font color='red'>New password should be more than 7 symbols!</font>";
            } else {
                echo "<font color='green'>Password has been changed!</font>";
                $new_pass = \Emagid\Core\Membership::hash($new_password);
                $user->password = $new_pass['password'];
                $user->hash = $new_pass['salt'];
                $user->save();
            }
        }
    }

    public function change_bank()
    {

        $user = \Model\Provider::getItem(\Emagid\Core\Membership::userId());
        $user->bank_name = $_POST['bank_name'];
        $user->account_number = $_POST['account_number'];
        $user->account_number_2 = $_POST['account_number_2'];
        $user->routing_number = $_POST['routing_number'];
        $user->routing_number_2 = $_POST['routing_number_2'];
        $user->bank_address = $_POST['bank_address'];
        $user->bank_city = $_POST['bank_city'];
        if (empty($_POST['bank_name']) || empty($_POST['routing_number']) || empty($_POST['routing_number_2']) || empty($_POST['bank_address']) || empty($_POST['bank_city'])) {
            echo "<font color='red'>All fields required!</font>";
        } elseif ($user->account_number_2 !== $user->account_number) {
            echo "<font color='red'>Account numbers doesn`t match!</font>";
        } elseif ($user->routing_number_2 !== $user->routing_number) {
            echo "<font color='red'>Routing numbers doesn`t match!</font>";
        } else {
            echo "<font color='green'>Information has been saved!</font>";
            $user->save();
        }
    }

    public function get_health_questionnaire()
    {

        $user = \Model\User::getItem($_POST['id']);
        if ($user->satisfied_weight == 1) {
            $a = "satisfied_weight_yes";
        } else {
            $a = "satisfied_weight_no";
        }
        if ($user->sleep_well == 1) {
            $b = "sleep_well_yes";
        } else {
            $b = "sleep_well_no";
        }
        if ($user->wake_up_at_night == 1) {
            $c = "wake_up_at_night_yes";
        } else {
            $c = "wake_up_at_night_no";
        }

        echo json_encode(['height_feet' => $user->height_feet,
            'height_inches' => $user->height_inches,
            'weight' => $user->weight,
            'six_month_ago_weight' => $user->six_month_ago_weight,
            'one_year_ago_weight' => $user->one_year_ago_weight,
            'different_weight_value' => $user->different_weight_value,
            'health_concerns' => $user->health_concerns,
            'health_goals' => $user->health_goals,
            'sleep_well' => $user->sleep_well,
            'wake_up_at_night' => $user->wake_up_at_night,
            'explanation_sleep' => $user->explanation_sleep,
            'explanation_pain' => $user->explanation_pain,
            'explanation_pain' => $user->explanation_pain,
            'explanation_fitness' => $user->explanation_fitness,
            'important_diet_change' => $user->important_diet_change,
            'injuries_conditions' => $user->injuries_conditions,
            'satisfied_weight' => $user->satisfied_weight,
            'full_name' => $user->first_name . ' ' . $user->last_name,
            'dob' => date("Y-m-d", strtotime($user->dob)),
            'satisfied_weight' => $a,
            'sleep_well' => $b,
            'wake_up_at_night' => $c]);
    }

    public function change_zip()
    {

        $user = \Model\Provider::getItem(\Emagid\Core\Membership::userId());

        if (empty($_POST['zip_acc'])) {
            echo "<font color='red'>All fields required!</font>";
        } else {
            $user->zip = $_POST['zip_acc'];

            $user->save();
            echo "<font color='green'>ZIp code has been saved!</font>";
        }
    }

    public function image_position()
    {

        $user = \Model\Provider::getItem(\Emagid\Core\Membership::userId());

        if (empty($_POST['value'])) {

        } else {
            $what = $_POST['what_image'];
            $user->$what = trim($_POST['value']);
            $user->save();


        }
        redirect(SITE_URL . 'provider/profile/');
    }

    public function image_reposition()
    {

        $user = \Model\Provider::getItem(\Emagid\Core\Membership::userId());

        if (empty($_POST['value']) || empty($_POST['where'])) {

        } else {
            $what = $_POST['where'];
            $user->$what = trim($_POST['value']);
            $user->save();


        }
        redirect(SITE_URL . 'provider/profile/');
    }

    public function check_services()
    {


        foreach (\Model\Provider::getList() as $provider) {
            $p = $provider->id;
            $check = \Model\Provider_Services::getList(['where' => 'provider_id=' . $p . '']);

            if (count($check) > 0) {
                if ($provider->authorized) {
                    echo "<font color='green'>authorized</font> ";
                }
                echo $provider->first_name . ' ' . $provider->last_name . ' (id ' . $provider->id . '):<br>';
                echo '<ul>';
                foreach ($check as $s) {


                    echo '<li>';
                    echo \Model\Service::getItem($s->service_id)->name;
                    echo '</li>';

                }
                echo '</ul><br>';
            } else {
                echo $provider->first_name . ' ' . $provider->last_name . ' (id ' . $provider->id . '):<br> <font color="red">No services</font><br>';
            }

        }
    }

    public function sendRequestAppointmentEmail()
    {
        $appointment = new \Model\Appointment();
        $appointment->user_id = $_POST['requester'];
        $service = \Model\Service::getItem(null, ['where' => ['name' => $_POST['service']]]);
        $appointment->provider_service_id = \Model\Provider_Services::getItem(null, ['where' => "provider_id = {$_POST['provider']} and service_id={$service->id}"])->id;
        $appointment->date = date('Y-m-d H:i:s', strtotime($_POST['date']));
        $appointment->message = $_POST['message'];
        if ($appointment->save()) {
            global $emagid;
            $emagid->email->from->email = 'noreply@wellns.com';
            $provider = \Model\Provider::getItem($_POST['provider']);
            $requester = \Model\User::getItem($_POST['requester']);
            $message = $_POST['message'] ? $_POST['message'] : 'None';
            $email = new \Emagid\Email();

            $email->addTo($provider->email);
            $email->subject('New Appointment Request');
            $content = "<p>Hey {$provider->first_name},</p><p>{$requester->email} requested for {$_POST['service']} at {$_POST['date']}</p><p>Addtional message {$message}</p>";
            $email->body = $content;

            $email->send();
        }

    }










///////////////////////ttttttttttttttttttttttttttttttttt////////////////
///////////////////////ttttttttttttttttttttttttttttttttt////////////////
///////////////////////ttttttttttttttttttttttttttttttttt////////////////
///////////////////////ttttttttttttttttttttttttttttttttt////////////////
///////////////////////ttttttttttttttttttttttttttttttttt////////////////
///////////////////////ttttttttttttttttttttttttttttttttt////////////////

    private function signature()
    {

        $this->template = false;
    }

///////////////////////ttttttttttttttttttttttttttttttttt////////////////

///////////////////////ttttttttttttttttttttttttttttttttt////////////////

///////////////////////ttttttttttttttttttttttttttttttttt////////////////
///////////////////////ttttttttttttttttttttttttttttttttt////////////////
///////////////////////ttttttttttttttttttttttttttttttttt////////////////
///////////////////////ttttttttttttttttttttttttttttttttt////////////////
///////////////////////ttttttttttttttttttttttttttttttttt////////////////
///////////////////////ttttttttttttttttttttttttttttttttt////////////////
///////////////////////ttttttttttttttttttttttttttttttttt////////////////
///////////////////////ttttttttttttttttttttttttttttttttt////////////////
///////////////////////ttttttttttttttttttttttttttttttttt////////////////
///////////////////////ttttttttttttttttttttttttttttttttt////////////////
///////////////////////ttttttttttttttttttttttttttttttttt////////////////
///////////////////////ttttttttttttttttttttttttttttttttt////////////////
///////////////////////ttttttttttttttttttttttttttttttttt////////////////
///////////////////////ttttttttttttttttttttttttttttttttt////////////////
///////////////////////ttttttttttttttttttttttttttttttttt////////////////
///////////////////////ttttttttttttttttttttttttttttttttt////////////////
///////////////////////ttttttttttttttttttttttttttttttttt////////////////

    private function accepted_session212121($id)
    {
        global $emagid;
        $emagid->email->from->email = 'noreply@wellns.com';
        $schedule = \Model\Schedule::getItem($id);
        $time = date('F d Y  \a\t g:ia', strtotime($schedule->day . ' ' . $schedule->time));
        $service = \Model\Service::getItem($schedule->service);
        $day = date('d', strtotime($schedule->day));
        $month = date('f', strtotime($schedule->day));
        $provider = \Model\Provider::getItem($schedule->provider_id);
        $user = \Model\User::getItem($schedule->user_id);
        $pr_name = $provider->first_name . ' ' . $provider->last_name;
        $user_name = $user->first_name . ' ' . $user->last_name;
        $email = new \Emagid\Email();

        $email->addTo($user->email);
        $email->addTo("john@emagid.com");
        $email->addTo("eitan@emagid.com");
        $email->subject('Your session accepted!');
        $email->body = "<html xmlns='http://www.w3.org/1999/xhtml' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'><head style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
<meta name='viewport' content='width=device-width' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
<link href='http://fonts.googleapis.com/css?family=Varela' rel='stylesheet' type='text/css' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
<title style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>Your Upcoming Session</title>

</head>

<body bgcolor='#FFFFFF' style='font-family:'Varela', sans-serif;margin: 0;padding: 0;font-size: 100%;line-height: 1.6;-webkit-font-smoothing: antialiased;-webkit-text-size-adjust: none;height: 100%;letter-spacing: .4px;'>

<!-- body -->
<table class='body-wrap' bgcolor='#FFFFFF' style='margin: 0;padding: 20px 0;font-size: 100%;line-height: 1.6;width: 100%;'>
	<tbody style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'><tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
		<td style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'></td>
		<td class='container' bgcolor='#FFFFFF' style='margin: 0 auto!important;padding: 8px 0 20px 0;font-size: 100%;line-height: 1.6;border: 1px solid transparent;display: block!important;max-width: 800px!important;width:100%;clear: both!important;'>

			<!-- content -->
			<div class='content' style='margin: 0 auto;padding: 0;font-size: 100%;line-height: 1.6;max-width: 600px;display: block;'>
			<table style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;width: 100%;'>
				<tbody style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'><tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
					<td style='text-align: center;margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
						<img style='width: 200px;margin-bottom: 18px;padding: 0;font-size: 100%;line-height: 1.6;max-width: 100%;' src='http://wellns.com/content/frontend/assets/img/email_imgs/logo.png'>
						<p style = 'border-top:1px solid #000000;margin-top:8px;margin-bottom:24px;'></p>
						<img style='width: 170px;margin-bottom: -7px;padding: 0;font-size: 100%;line-height: 1.6;max-width: 100%;' src='http://wellns.com/content/frontend/assets/img/email_imgs/upcoming_session_sprite.png' alt='UPCOMING SESSION'>
						<p style='text-align: center;font-size: 22px;margin: 0;padding: 0;line-height: 1.6;margin-bottom: 10px;font-weight: normal;margin-top:18px;'>Hi $user_name</p>
						<p style='font-size: 14px;line-height: 22.5px;text-align: center;padding: 0;margin-bottom: 10px;font-weight: normal;'>$time, you have a session booked with <a style='text-decoration:none;color:#64cfdb' href='https://wellns.com'>$pr_name</a>.
						</p>
						<p style='font-size: 14px;line-height: 22.5px;text-align: center;padding: 0;margin-bottom: 10px;font-weight: normal;'>We have listed the details of this session below.</p>

						<div style='background-color:#FFFFFF;border-top:10px solid #fff;background:#fff;border-bottom: 1px solid #e5e5e5;'>
							<div style='background-color: #FFFFFF;font-weight:bold;border: 8px solid #FFFFFF;color: #000000!important;font-size:16px;border-bottom: 1px solid #e5e5e5;padding-bottom: 6px;margin-bottom: 7px;'>
								<a href='' style='color:#FFFFFF!important;text-decoration:none' target='_blank'>
									<font color='#000000' style='position: relative;top: -5px;letter-spacing: 1px; font-weight: light;'>Your Upcoming Session</font>
									<img style='width: 109px;padding: 0;font-size: 100%;max-width: 100%;margin-left: 12px;' src='http://wellns.com/content/frontend/assets/img/email_imgs/tomorrow_sprite.png' alt='TOMORROW'>
								</a>
							</div>
							<table cellpadding='0' cellspacing='0' style='width:100%;background-color: #FFFFFF;'>
							<tbody>
							<tr style=''>
								<td style='width:32px;background-color: #ffffff;vertical-align:top;border-top:1px solid #fff;padding:8px 0 8px 8px'>
									<div style='border: 1px solid #a0c2ce;border-radius: 1px;'>
										<div style='background-color: #a0c2ce;text-transform:uppercase;color: #000!important;font-weight: light;text-align:center;font-size:9px;line-height:14px;min-height:14px;padding: 0 14px;'>
											<font color='#fff' style='color: #ffffff;'>$month</font>
										</div>
										<div style='background-color:#fff;font-size: 21px;line-height:18px;min-height:18px;color: #a0c2ce;font-weight: 100!important;text-align:center;padding: 9px 4px 9px 4px;'>$day</div>
									</div>
								</td>
								<td style='vertical-align:top;padding:8px;padding-left:14px;border-top:1px solid #fff'>
									<table cellpadding='0' cellspacing='0' style='width:100%'>
										<tbody>
											<tr>
												<td style='position:relative;top:-8px;background-color: #ffffff;vertical-align:top'>
													<a href='' style='font-size:14px;color:#000;text-decoration:none' target='_blank'>1 Hour $service->name Session with $pr_name.</a>
													<br>
													<div style='padding:2px 0 4px 0;font-size:12px;line-height:16px;color: #8b8b8b;'>$schedule->session_location</div>
													<div style='padding:2px 0 4px 0;font-size:12px;line-height:16px;color: #8b8b8b;'>$time</div>
												</td>
												<td style='background-color: #FFFFFF;width:76px;vertical-align:top;padding-top:14px;'>
													<div style='display:inline-block;border: 1px solid #000000;background-color: #FFFFFF;border-radius:1px 1px 1px 1px;text-align:center'>
														<span style='background-color: #FFFFFF;display:inline-block'>
															<a href='' style='border-color: #ffffff;border-width:5px 18px;border-style:solid;white-space:nowrap;color: #000000!important;display:inline-block;text-decoration:none;text-align:center;color:#FFFFFF;text-transform:uppercase;' target='_blank'>		<font color='#000000' style='font-size: 14px;letter-spacing: 1.4px;'>
																	<u></u>Manage<u></u>
																</font>
															</a>
														</span>
													</div>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							</tbody>
							</table>
							</div>
							<p style='font-size: 14px;line-height: 22.5px;text-align: center;padding: 0;margin-bottom: 10px;font-weight: normal;margin-top:18px;'><a style='text-decoration:none;color:#64cfdb' href='https://wellns.com/user/dashboard'>View all of your upcoming sessions on your dashboard</a>.</p>




					</td>
				</tr>
			</tbody></table>
			</div>
			<!-- /content -->

		</td>
		<td style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'></td>
	</tr>
</tbody></table>
<!-- /body -->

<!-- footer -->
<table class='footer-wrap' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;width: 100%;clear: both!important;'>
	<tbody style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'><tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
		<td style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'></td>
		<td class='container' style='margin: 0 auto!important;padding: 0;font-size: 100%;line-height: 1.6;display: block!important;max-width: 600px!important;clear: both!important;'>

			<!-- content -->
			<div class='content' style='margin: 0 auto;padding: 0;font-size: 100%;line-height: 1.6;max-width: 600px;display: block;'>
				<table style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;width: 100%;'>
					<tbody style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'><tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
						<td align='center' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
							<p style='margin: 0;padding: 0;font-size: 12px;line-height: 1.6;margin-bottom: 10px;font-weight: normal;color: #666;'>
								<a class='text-decoration:none;color:#a0c2ce;' href='#' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;color: #a0c2ce;text-decoration: none;'><unsubscribe style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>Unsubscribe</unsubscribe></a>
								<span style='color: #000000;margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>·</span>
								<a class='text-decoration:none;color:#a0c2ce;' href='#' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;color: #a0c2ce;text-decoration: none;'>Contact Us</a>
								<span style='color: #000000;margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>·</span>
								<a class='text-decoration:none;color:#a0c2ce;' href='#' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;color: #a0c2ce;text-decoration: none;'>Privacy Policy</a>
							</p>
						</td>
					</tr>
					<tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
						<td align='center' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
							<p style='color: #8b8b8b;font-size: 9px;width: 80%;max-width: 600px;margin: 0;padding: 0;line-height: 1.6;margin-bottom: 10px;font-weight: normal;'>
							© 2015 Wellns. The Wellns logo is a registered trademark of Wellns LLC. All rights reserved. This email has been sent to your by Wellns, 225 West 39th Street, New York City, NY 10009. Made by <b style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>Emagid</b>
							</p>
						</td>
					</tr>
				</tbody></table>
			</div>
			<!-- /content -->

		</td>
		<td style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'></td>
	</tr>
</tbody></table>
<!-- /footer -->



</body></html>";
        echo $email->body;
        //$email->send();
    }

}