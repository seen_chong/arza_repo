<?php

class password_resetController extends siteController
{

    public function password_reset()
    {
        $this->loadView($this->viewData);
    }

    public function do_it(Array $params = [])
    {
        $pro = (isset($params['id'])) ? $params['id'] : ' ';
        $this->viewData->url = $pro;
        if (!($pr = \Model\Provider::getItem(null, ['where' => "remind_password = '" . $pro . "'"])) && !($pr = \Model\User::getItem(null, ['where' => "remind_password = '" . $pro . "'"]))) {
            redirect(SITE_URL);
        }

        if (isset($_POST['password']) && isset($_POST['password2'])) {
            if (strlen($_POST['password']) < 7 || strlen($_POST['password2']) < 7) {
                $n = new \Notification\ErrorHandler("Sorry, but password should be more than 7 symbols!");
                $_SESSION["notification"] = serialize($n);
                redirect(SITE_URL . 'password_reset/do_it/' . $pro);
            } else {
                if (trim($_POST['password']) == trim($_POST['password2'])) {
                    $new_pass = \Emagid\Core\Membership::hash($_POST['password']);
                    $pr->password = $new_pass['password'];
                    $pr->hash = $new_pass['salt'];
                    $pr->remind_password = null;
                    $pr->save();
                    $n = new \Notification\MessageHandler("Your password has been changed successfully!");
                    $_SESSION["notification"] = serialize($n);
                    redirect(SITE_URL . 'login');
                } else {
                    $n = new \Notification\ErrorHandler("Sorry, but passwords are different!");
                    $_SESSION["notification"] = serialize($n);
                    redirect(SITE_URL . 'password_reset/do_it/' . $pro);
                }
            }
        }


        $this->loadView($this->viewData);
    }

}	 