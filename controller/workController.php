<?php

class workController extends siteController
{

    public function index(Array $params = [])
    {
        $this->viewData->communities = \Model\Community::getList(['orderBy'=>'display_order ASC']);
        $this->loadView($this->viewData);
    }
}