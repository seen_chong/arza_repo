<?php

class newsletterController extends siteController {
	
	public function add(){
        $newsletter = \Model\Newsletter::getItem(null,['where'=>"active = 1 and email = '".$_POST['email']."'"]);

        if(!$newsletter){
            $newsletter = \Model\Newsletter::loadFromPost();
            if($newsletter->save()){
                $date = \Carbon\Carbon::now();
                $date = $date->format('F d, Y');
                $this->addToMailChimp($newsletter->email);
                global $emagid;
                $emagid->email->from->email = 'report@emagid.com';
                $email = new \Emagid\Email();
                $email->addTo($newsletter->email);
                $confirm_email = \Model\Confirm_Email::getItem(null,[['where'=>"name = 'Join Newsletter'"]]);
                if($confirm_email){
                    $temp = \Model\Confirm_Email::$dynamicElements;
                    $real = [$date,$newsletter->email, ''];
                    $emailTemplate = str_replace($temp,$real,$confirm_email->email_template);
                    $subject = str_replace($temp,$real,$confirm_email->subject);
                    $email->subject(trim($subject));
                    $email->body = trim($emailTemplate);
                    $email->send();
                }
//                $email = new \Email\MailMaster();
//                $product = \Model\Product::getItem($newsletter->product_id);
//                $mergeFields = ['CUSTOMER_EMAIL'=>$newsletter->email,'PRODUCT_NAME'=>$product->name,'PRODUCT_slug'=>$product->slug];
//                $email->setTo(['email' => $newsletter->email, 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('luggage-newsletter-confirmation')->send();
                $this->toJson(['status' => "success", 'message' => "Your email was included on our newsletter list."]);
            }
        }else{
            $this->toJson(['status' => "fail", 'message' => "Your email already on our newsletter list."]);
        }
	}

	private function addToMailChimp($email){
		$apikey = '7b2afa3a09b2dbdd6ecff15fd5f052e9-us8';
        $auth = base64_encode( 'user:'.$apikey );

        $data = array(
            'apikey'        => $apikey,
            'email_address' => $email,
            'status'        => 'subscribed'
        );
        $json_data = json_encode($data);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://us8.api.mailchimp.com/3.0/lists/1f0bb9ad0a/members');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.$auth));
        curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);                                                                                      

        $result = curl_exec($ch);

//        $arr = json_decode($result, true);
        curl_close($ch);
//        dd($arr);

        // var_dump($result);
        // die('Mailchimp executed');
	}

}