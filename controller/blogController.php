<?php

class blogController extends siteController
{

    public function index(Array $params = [])
    {
        $sql = "select * from blog where active = 1 and status = 1 and date_modified is not null and date_modified != '' order by to_date(date_modified, 'Month DD, YYYY') desc";
        $this->viewData->posts = \Model\Blog::getList(['sql'=>$sql]);
        //$this->viewData->posts = \Model\Blog::getList(['where' => "status = '1'"]);
        $this->viewData->featured_posts = \Model\Blog::getList(['where' => "status = 1 AND featured = 1"]);
        $this->viewData->recent_posts = \Model\Blog::getList(['where' => "status = 1",'orderBy'=>'insert_time DESC', 'limit'=>4]);

        $this->configs['Meta Title'] = "Arza Blog";
        $this->loadView($this->viewData);
    }

    public function category(Array $params = [])
    {
        $cat = (isset($params['slug'])) ? $params['slug'] : '';
        $this->viewData->category = \Model\Blog_Category::getItem($cat);
        $this->viewData->featured_posts = \Model\Blog::getList(['where' => "status = 1 AND featured = 1 and category = '" . $cat . "'"]);
        $this->viewData->recent_posts = \Model\Blog::getList(['where' => "status = 1 and category = '" . $cat . "'",'orderBy'=>'insert_time DESC', 'limit'=>4]);

        $this->viewData->posts = \Model\Blog::getList(['where' => "status = '1' and category = '" . $cat . "'"]);
        $this->loadView($this->viewData);
    }

    public function post(Array $params = [])
    {
        $slug = (isset($params['slug'])) ? $params['slug'] : '';
        if(isset($_GET['draft'])&&$_GET['draft']=='true'){
            $this->viewData->post = \Model\Blog::getItem(null, ['where' => "(status = '0' or status = '2') and slug = '" . $slug . "'"]);
        }else {
            $this->viewData->post = \Model\Blog::getItem(null, ['where' => "status = '1' and slug = '" . $slug . "'"]);
        }
//        $this->viewData->service = \Model\Service::getItem($this->viewData->post->service);
//        if ($this->viewData->post->author > 0) {
//            $provider = \Model\Provider::getItem($this->viewData->post->author);
//            if (isset($provider)) {
//                $this->viewData->author = $provider->first_name . ' ' . $provider->last_name;
//            }
//        } else {
//            $this->viewData->author = "WELLNS";
//        }
        if($this->viewData->post) {
            if (!is_null($this->viewData->post->meta_keywords) && $this->viewData->post->meta_keywords != ''){
                $this->configs['Meta Keywords'] = $this->viewData->post->meta_keywords;
            }
            if (!is_null($this->viewData->post->meta_description) && $this->viewData->post->meta_description != ''){
                $this->configs['Meta Description'] = $this->viewData->post->meta_description;
            }
            if (!is_null($this->viewData->post->meta_title) && $this->viewData->post->meta_title != ''){
                $this->configs['Meta Title'] = $this->viewData->post->meta_title;
            }else{
                $this->configs['Meta Title'] = $this->viewData->post->title . ' | Arza Blog';
            }
            $this->loadView($this->viewData);
        }else{
            redirect('/blog');
        }
    }

    public function add(Array $params = [])
    {
        $cat = (isset($params['id'])) ? $params['id'] : '';
        $this->viewData->service = \Model\Service::getItem($cat);
        $this->viewData->posts = \Model\Blog::getList(['where' => "status = '1' and service = '" . $cat . "'"]);
        $this->loadView($this->viewData);
    }

    public function do_add()
    {
        $provider = \Model\Provider::getItem(\Emagid\Core\Membership::userId());
        $post = new \Model\Blog();
        $post->time_post = date("Y-m-d H:i:s");
        $post->title = $_POST['title'];
        $post->service = $_POST['service'];
        $post->description = $_POST['description'];
        $post->status = 0;
        $post->author = $provider->id;
        if ($_POST['title'] == "" || $_POST['service'] == "" || $_POST['description'] == "") {
            echo "2";
        } else {
            if ($post->save()) {
                echo "1";
            }
        }
        $this->template = FALSE;
    }

}