<?php

class userController extends siteController
{

    function __construct()
    {
        parent::__construct();


    }

    public function index(Array $params = [])
    {
        if (is_null($this->viewData->user)) {
            redirect('/login');
        }
        //returns -4 random providers based on the date to be displayed
        if (isset($_GET['date']) && $_GET['date'] != "") {
            $date = date('Y-m-d', $_GET['date']);
        } else {
            $date = date('Y-m-d', getdate()[0]);
        }
        $subservices=[];
        foreach(\Model\Service::getList(['where'=>"sub_service = '0'"]) as $services) {
            $subservices[$services->name] = \Model\Provider_Services::getServicesByServiceSlug($services->slug);
        }
        $this->viewData->subServices = $subservices;
        $this->viewData->providersFiltered = \Model\Calendar::getProvidersBeforeSessionByDate($date);

        $this->viewData->mainServices = \Model\Service::getList(['where'=>"sub_service = '0'"]);
        parent::index($params);
    }

    public function index_post($params = [])
    {
        if (!empty($_POST['date'])) {
            echo strtotime($_POST['date']);
        } else {
            echo "";
        }
    }

    public function calendar(Array $params = [])
    {
        $pro = (isset($params['id'])) ? $params['id'] : '';
//        if ($pro != "" && is_null($this->viewData->user)) {
//            redirect('/login');
//        }
//        if (@\Model\Provider::getItem(null, ['where' => "active = '1' and id = '" . $pro . "'"])) {
//            $who_logged = 3;
//        }    //provider
//        elseif (@\Model\User::getItem(null, ['where' => "active = '1' and id = '" . $pro . "'"])) {
//            $who_logged = 2;
//        }//user
//        else {
//            redirect(SITE_URL);
//        }    //admin


        $this->viewData->provider = \Model\Provider::getItem($pro);

        $this->viewData->pr = \Model\Provider_Services::getList(['where' => "active = '1' and provider_id = '" . $pro . "'"]);

        $sql = "SELECT * FROM days where  date>='NOW()' order by date asc limit 365";
        $this->viewData->q = \Model\Day::getList(['sql' => $sql]);

        $this->configs['Meta Title'] = 'Book a Session with ' . $this->viewData->provider->first_name . ' ' . $this->viewData->provider->last_name[0];
        //if($who_logged == 3 || $who_logged == 1 ){redirect(SITE_URL);}
        $this->loadView($this->viewData);
    }

    public function send_normal_invite()
    {
        if($email = $_POST['email']){
            $emailMaster = new \Email\MailMaster();
            $mergeFields = [
                'CUSTOMER_INVITEE_EMAIL' => $email,
                'CUSTOMER_EMAIL' => $this->viewData->user->email,
                'CUSTOMER_NAME' => $this->viewData->user->full_name(),
            ];

            if(
            $this->viewData->user->getLoginType() == 'fb' &&
            $emailMaster->setTo(['email' => $email, 'name' => $email,  'type' => 'to'])->setTemplate('dashboard-invite')->setMergeTags($mergeFields)->send()){
                $fbTracking = new stdClass();
                $fbTracking->label = '6036632564510';
                $_SESSION['facebookAdConversionTracking'] = serialize($fbTracking);
            }

            $n = new \Notification\MessageHandler("An invitation email was successfully sent to $email.");
            $_SESSION["notification"] = serialize($n);
            redirect($this->viewData->user->getDashboard());
        } else {
            $n = new \Notification\MessageHandler("There is an error sending invitation to $email, please try again.");
            $_SESSION["notification"] = serialize($n);
            redirect($this->viewData->user->getDashboard());
        }
    }

    public function dashboard()
    {
        $who_logged = $this->roleType;
        if ($who_logged == 3 || $who_logged == 1 || $who_logged != 2 && $who_logged != 4) {
            $n = new \Notification\ErrorHandler("You have to login. You may have entered the wrong username or password.");
            $_SESSION["notification"] = serialize($n);
            redirect(SITE_URL);
        }


        $user = $this->viewData->user;

        $favorite = \Model\User_Favorite::getList(['where' => "active='1' and user_id = '" . $user->id . "'"]);

        $this->viewData->favorite = $favorite;

        $this->viewData->upcoming = $this->viewData->user->getUpcomingSessions();
        $this->viewData->pendingConfirms = $this->viewData->user->getWaitingConfirmSessions();
        $this->viewData->history = $this->viewData->user->getHistorySessions();
        $this->viewData->requested = $this->viewData->user->getAllRequestedSessions();

        $subjectArray = \Model\Subject::getSubjectArray($this->viewData->user, ['user_id' => $this->viewData->user->id]);
        $this->viewData->readSubject = $subjectArray['read'];
        $this->viewData->unreadSubject = $subjectArray['unread'];

        $m_arr = array();
        $arr = \Model\Service::getParentChildId();

        $yoga = implode(',',$arr['Yoga']);
        $countYoga = \Model\SessionHistory::getCountStatic(['where'=>"user_id = {$this->viewData->user->id} and service_id in ({$yoga})"]);
        $this->viewData->countYoga = $countYoga;
        $m_arr[] = " {
                name: 'Yoga',
                data: [0,$countYoga,0],
                color: '#f8e5c8'
            }";

        $meditation = implode(',',$arr['Meditation']);
        $countMeditation = \Model\SessionHistory::getCountStatic(['where' => "user_id = {$this->viewData->user->id} and service_id in ({$meditation})"]);
        $this->viewData->countMeditation = $countMeditation;
        $m_arr[] = " {
                name: 'Meditation',
                data: [0,$countMeditation,0],
                color: '#a0c2ce'
            }";

        $fitness = implode(',',$arr['Fitness']);
        $countFitness = \Model\SessionHistory::getCountStatic(['where' => "user_id = {$this->viewData->user->id} and service_id in ({$fitness})"]);
        $this->viewData->countFitness = $countFitness;
        $m_arr[] = " {
                name: 'Meditation',
                data: [0,$countFitness,0],
                color: '#a0c2ce'
            }";

        $alternativeHealing = implode(',',$arr['Alternative Healing']);
        $countAltHealing = \Model\SessionHistory::getCountStatic(['where' => "user_id = {$this->viewData->user->id} and service_id in ({$alternativeHealing})"]);
        $this->viewData->countAltHealing = $countAltHealing;
        $m_arr[] = " {
                name: 'Meditation',
                data: [0,$countAltHealing,0],
                color: '#a0c2ce'
            }";

        $massage = implode(',',$arr['Massage']);
        $countMassage = \Model\SessionHistory::getCountStatic(['where' => "user_id = {$this->viewData->user->id} and service_id in ({$massage})"]);
        $this->viewData->countMassage = $countMassage;
        $m_arr[] = " {
                name: 'Meditation',
                data: [0,$countMassage,0],
                color: '#a0c2ce'
            }";

        $specialActivities = implode(',',$arr['Special Activities']);
        $countSpcActivities = \Model\SessionHistory::getCountStatic(['where' => "user_id = {$this->viewData->user->id} and service_id in ({$specialActivities})"]);
        $this->viewData->countSpecialActivities = $countSpcActivities;
        $m_arr[] = " {
                name: 'Meditation',
                data: [0,$countSpcActivities,0],
                color: '#a0c2ce'
            }";


        /*$count_yoga = \Model\Schedule::getCount(['where' => "active = '1' and user_id = '" . $this->viewData->user->id . "' and service='1'"]);
        $this->viewData->count_yoga = $count_yoga;
        $m_arr[] = " {
                name: 'Yoga',
                data: [0,$count_yoga,0],
                color: '#f8e5c8'
            }";

        $count_meditaion = \Model\Schedule::getCountStatic(['where' => "active = '1' and user_id = '" . $this->viewData->user->id . "' and service='2'"]);
        $this->viewData->count_meditaion = $count_meditaion;
        $m_arr[] = " {
                name: 'Meditation',
                data: [0,$count_meditaion,0],
                color: '#a0c2ce'
            }";

        $count_fitness = \Model\Schedule::getCountStatic(['where' => "active = '1' and user_id = '" . $this->viewData->user->id . "' and service='3'"]);
        $this->viewData->count_fitness = $count_fitness;
        $m_arr[] = " {
                name: 'Fitness',
                data: [0,$count_fitness,0],
                color: '#33858E'
            }";

        $count_alt_healing = \Model\Schedule::getCountStatic(['where' => "active = '1' and user_id = '" . $this->viewData->user->id . "' and service='4'"]);
        $this->viewData->count_alt_healing = $count_alt_healing;

        $m_arr[] = "{

                name: 'Alternative',
                data: [0,$count_alt_healing,0],
                color: '#597992',
            }";

        $count_nutrition = \Model\Schedule::getCountStatic(['where' => "active = '1' and user_id = '" . $this->viewData->user->id . "' and service='5'"]);
        $this->viewData->count_nutrition = $count_nutrition;
        $m_arr[] = " {
                name: 'Nutrition',
                data: [0,$count_nutrition,0],
                color: '#9F7271'
            }";

        $count_special_activities = \Model\Schedule::getCountStatic(['where' => "active = '1' and user_id = '" . $this->viewData->user->id . "' and service='6'"]);
        $this->viewData->count_special_activities = $count_special_activities;*/
        $this->viewData->mountains = implode(",", $m_arr);
//        dd($this->viewData);


        /*  foreach ($this->_viewData->order_products as $order_product){

                if ($order_product->type_product ==  1){
                $p=\Model\Product::getItem($order_product->product_id);
                $update_link="products/update/";
                $type="Product (";
                $type.=$order_product->size.' & '. $order_product->color.")";
                $with = "";
                }
                elseif ($order_product->type_product ==  2) {
                  $p = \Model\Package::getItem($order_product->product_id);
                  $update_link="packages/update/";
                  $type="Sessions";
                  $provider = \Model\Provider::getItem($order_product->provider_id);
                  $with = "with <a style='color:green;' href='/admin/providers/update/$order_product->provider_id'> $provider->first_name $provider->last_name";
                }
               else {
                $p = \Model\Package::getItem($order_product->product_id);
                  $update_link="packages/update/";
                  $type="Single session";
                  $provider = \Model\Provider::getItem($order_product->provider_id);
                  $with = "with <a style='color:green;' href='/admin/providers/update/$order_product->provider_id'> $provider->first_name $provider->last_name"  ;

               }*/
        $role = \Model\User_Roles::getItem(null, ['where' => "user_id = '" . $this->viewData->user->id . "'"])->role_id;
        if ($role == 4) {
            $this->viewData->corporate_email = \Model\Corporate_Invite::getList(['where' => "user_id = '" . $this->viewData->user->id . "'"]);
            $this->viewData->corporate_employee = \Model\Corporate_Employee::getList(['where' => "employer_id = '" . $this->viewData->user->id . "'"]);
        } else if ($role == 2) {
            $this->viewData->corporate_employer = \Model\Corporate_Employee::getEmployerUser($this->viewData->user->id);
        }
        $this->configs['Meta Title'] = 'Dashboard | Wellns';
        $this->loadView($this->viewData);
    }

    public function cancelSession()
    {
        $sessionId = $_GET['session'];
        $messageId = $_GET['cancel-session-message'];
        $sessionHistory = \Model\SessionHistory::getItem(null, ['where' => "id= $sessionId and user_id = {$this->viewData->user->id}"]);
        if ($sessionHistory) {
            $sessionHistory->markStatus(7);
            $sessionHistory->markCancelMessage($messageId);

            $n = new \Notification\MessageHandler("Session Canceled");
            $_SESSION["notification"] = serialize($n);

            $this->viewData->user->sendCancelSessionEmail($sessionHistory);
        }

        redirect('/user/dashboard');
    }

    public function completeSession()
    {
        $sessionId = $_GET['session'];
        $sessionHistory = \Model\SessionHistory::getItem(null, ['where' => "id= $sessionId and user_id = {$this->viewData->user->id}"]);
        if ($sessionHistory) {
            $sessionHistory->markStatus(4);

            $n = new \Notification\MessageHandler("Thanks confirming session.");
            $_SESSION["notification"] = serialize($n);

            $this->viewData->user->sendCompleteSessionEmail($sessionHistory);
        }

        redirect('/user/dashboard');
    }

    public function incompleteSession()
    {
        $sessionId = $_GET['session'];
        $sessionHistory = \Model\SessionHistory::getItem(null, ['where' => "id= $sessionId and user_id = {$this->viewData->user->id}"]);
        if ($sessionHistory) {
            $sessionHistory->markStatus(5);

            $n = new \Notification\MessageHandler("Thanks reporting it, a wellns customer representative will contact you soon.");
            $_SESSION["notification"] = serialize($n);

            $this->viewData->user->sendIncompleteSessionEmail($sessionHistory);
        }

        redirect('/user/dashboard');
    }


    public function send_email()
    {
        if ($_POST['email']) {
            $clist = \Model\Corporate_Invite::getList();
            $list = [];
            foreach ($clist as $l) {
                $list[] = $l->token;
            }
            $invite = new \Model\Corporate_Invite();
            $invite->user_id = $this->viewData->user->id;
            $invite->email = $_POST['email'];
            if (!in_array(md5(uniqid(rand(), true)), $list)) {
                $invite->token = md5(uniqid(rand(), true));
            }
            $url = SITE_URL . 'corporate/' . $invite->user_id . '/verify/' . $invite->token;
            $invite->save();

            $email = new Email\MailMaster();
            $mergeFields = [
                'CORPORATE_NAME' => $this->viewData->user->company,
                'CORPORATE_ADMIN_NAME' => $this->viewData->user->full_name(),
                'CORPORATE_ADMIN_EMAIL' => $this->viewData->user->email,
                'CORPORATE_INVITATION_LINK' => 'wellns.com' . $url,
                'CORPORATE_INVITEE_EMAIL' => $_POST['email'],
            ];
            $email->setTo(['email' => $_POST['email'], 'name' => 'Wellns Invitation', 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('corporate-invitation-template')->send();

            $n = new \Notification\MessageHandler("An invitation email was successfully sent over to {$_POST['email']}.");
            $_SESSION["notification"] = serialize($n);
        } else {
            $n = new \Notification\MessageHandler("There is an error sending invitation, please try again.");
            $_SESSION["notification"] = serialize($n);
        }
        redirect('/user/dashboard');
    }

    // public function profile(){
    // 	$this->loadView($this->viewData);
    // }

    public function manage_profile()
    {

        $who_logged = $this->roleType;

        if ($who_logged == 3 || $who_logged == 1 || $who_logged != 2 && $who_logged != 4) {
            redirect(SITE_URL);
        }
        $this->loadView($this->viewData);
    }


    public function reg()
    {


        $this->template = false;


        $provider = \Model\Provider::getItem(null, ['where' => "active = '1' and email = '" . $_POST['email'] . "'"]);
        $takenUser = \Model\User::getItem(null, ['where' => "active = '1' and email = '" . $_POST['email'] . "'"]);

        if (count($provider) > 0 || count($takenUser) > 0) {
            $n = new \Notification\ErrorHandler("Sorry, that email is already taken!");
            $_SESSION["notification"] = serialize($n);

            redirect(SITE_URL);

        } else {
            if (isset($_POST['queryString'])) {
                $queryString = $_POST['queryString'] != "" ? explode('=', $_POST['queryString']) : null;

                $email = $_POST['email'];
                $token = $queryString[1];
                $corp_invite = \Model\Corporate_Invite::getItem(null, ['where' => "email = '$email' and token = '$token'"]);
                $corp_mass_invite = $corp_invite != null ? \Model\Corporate_Invite::getList(['where' => "email = '$email' and user_id = '$corp_invite->user_id'"]) : null;

                if ($corp_mass_invite != null) {
                    $corpVerified = [];
                    foreach ($corp_mass_invite as $corp) { //find all existing instances of corp_invite incase more than 1 was sent
                        $corpVerified[] = $corp->is_verified;
                    }
                }
            }
            $user = \Model\User::loadFromPost();
            $user->authorized = false;
            $hash = \Emagid\Core\Membership::hash($user->password);
            $user->password = $hash['password'];
            $user->hash = $hash['salt'];
            $user->ref_key = md5(time() . mt_rand(1000000, 99999999));
            $type = \Model\Role::getItem(null, ['where' => ['name' => $_POST['type']]]);
            if ($user->save()) {
                $userRoles = \Model\User_Roles::getList(['where' => 'active = 1 and role_id = 1 and user_id = ' . $user->id]);
                if (count($userRoles) <= 0) {
                    $userRoles = new \Model\User_Roles();
                    $userRoles->role_id = $type->id;
                    $userRoles->user_id = $user->id;
                    $userRoles->save();
                }

                if (isset($queryString) && !array_search(1, $corpVerified)) {
                    $corp_employee = new \Model\Corporate_Employee();
                    $corp_employee->employer_id = $corp_invite->user_id;
                    $corp_employee->employee_id = $user->id;
                    $corp_invite->is_verified = 1;
                    foreach ($corp_mass_invite as $corp) {
                        $corp->is_verified = 1;
                        $corp->save();
                    }
                    $corp_employee->save();
//                    $corp_invite ->save();
                }


                $n = new \Notification\MessageHandler('Thank you for joining Wellns - we look forward to helping you create a better self, on every dimension...');
                $_SESSION["notification"] = serialize($n);

                if (isset($_COOKIE['ref_u'])) {

                    if ($user_check = \Model\Provider::getItem(null, ['where' => "active = '1' and ref_key = '" . $_COOKIE['ref_u'] . "'"])) {
                        $user_id_from = $user_check->id;
                        $user_type_from = "provider";
                    }    //provider
                    elseif ($user_check = \Model\User::getItem(null, ['where' => "active = '1' and ref_key = '" . $_COOKIE['ref_u'] . "'"])) {
                        $user_id_from = $user_check->id;
                        $user_type_from = "user";
                    }

                    if (isset($user_check)) {
                        $ref = new \Model\Referral();
                        $ref->user_id_from = $user_id_from;
                        $ref->user_id_to = $user->id;
                        $ref->user_type_from = $user_type_from;
                        $ref->user_type_to = "user";
                        $ref->save();
                    }

                    setcookie('ref_u', "", -3600, "/");
                }
                $this->sign_up_mail($user->email, $user->first_name);
                $tracking = new stdClass();
                $tracking->label = 'J36UCJWrt2MQ4vjWvgM';
                $_SESSION["googleAdConversionTracking"] = serialize($tracking); // tracking label
                $user->login();
                if(isset($_SESSION['addrName']) && isset($_SESSION['street']) && isset($_SESSION['apt']) && isset($_SESSION['zip'])) {
                    $address = new \Model\User_Address();
                    $address->addAddrFromSession($_SESSION['em_authentication_id']);
                }
                redirect('/user/dashboard');
            } else {
                $n = new \Notification\ErrorHandler($user->errors);
                $_SESSION["notification"] = serialize($n);
            }
            redirect(SITE_URL);

        }
    }

    public function ajaxReg(){
        $provider = \Model\Provider::getItem(null, ['where' => "active = '1' and email = '" . $_POST['email'] . "'"]);
        $takenUser = \Model\User::getItem(null, ['where' => "active = '1' and email = '" . $_POST['email'] . "'"]);
        if (count($provider) > 0 || count($takenUser) > 0) {
            echo $this->toJson(['status'=>'failed','message'=>'Sorry, that email is already taken!']);
        } else {
            $user = \Model\User::loadFromPost();
            $user->authorized = false;
            $hash = \Emagid\Core\Membership::hash($user->password);
            $user->password = $hash['password'];
            $user->hash = $hash['salt'];
            $user->ref_key = md5(time() . mt_rand(1000000, 99999999));
            $type = \Model\Role::getItem(null, ['where' => ['name' => $_POST['type']]]);
            if ($user->save()) {
                $userRoles = \Model\User_Roles::getList(['where' => 'active = 1 and role_id = 1 and user_id = ' . $user->id]);
                if (count($userRoles) <= 0) {
                    $userRoles = new \Model\User_Roles();
                    $userRoles->role_id = $type->id;
                    $userRoles->user_id = $user->id;
                    $userRoles->save();
                }
                if (isset($_COOKIE['ref_u'])) {

                    if ($user_check = \Model\Provider::getItem(null, ['where' => "active = '1' and ref_key = '" . $_COOKIE['ref_u'] . "'"])) {
                        $user_id_from = $user_check->id;
                        $user_type_from = "provider";
                    }    //provider
                    elseif ($user_check = \Model\User::getItem(null, ['where' => "active = '1' and ref_key = '" . $_COOKIE['ref_u'] . "'"])) {
                        $user_id_from = $user_check->id;
                        $user_type_from = "user";
                    }//user

                    if (isset($user_check)) {
                        $ref = new \Model\Referral();
                        $ref->user_id_from = $user_id_from;
                        $ref->user_id_to = $user->id;
                        $ref->user_type_from = $user_type_from;
                        $ref->user_type_to = "user";
                        $ref->save();
                    }


                    setcookie('ref_u', "", -3600, "/");
                }
                $this->sign_up_mail($user->email, $user->first_name);
                $user->login();
                if(isset($_SESSION['addrName']) && isset($_SESSION['street']) && isset($_SESSION['apt']) && isset($_SESSION['zip'])) {
                    $address = new \Model\User_Address();
                    $address->addAddrFromSession($_SESSION['em_authentication_id']);
                }
                echo $this->toJson(['status'=>'success']);
            } else {
                echo $this->toJson(['status'=>'failed', 'message'=>$user->errors]);
            }
        }
    }

    public function fbHandler()
    {
        $fbUser = \Model\User::getItem(null,['where' => "active = 1 and fb_id = '{$_POST['fb_id']}'"]);
        if (!is_null($fbUser)) {
            if($fbUser->login()){
                if($fbUser->email == '' && isset($_POST['email']) && $_POST['email'] != ''){
                    $fbUser->email = $_POST['email'];
                    $fbUser->save();
                }

                $n = new \Notification\MessageHandler('WELCOME BACK '.$fbUser->first_name);
                $_SESSION["notification"] = serialize($n);

                $fbTracking = new stdClass();
                $fbTracking->label = '6036632535110';
                $_SESSION['facebookAdConversionTracking'] = serialize($fbTracking);
            }
            echo $this->toJson(['status'=>'success']);
//
        } else {
            $provider = \Model\Provider::getItem(null, ['where' => "active = '1' and email = '" . $_POST['email'] . "'"]);
            if (count($provider) > 0) {
                echo $this->toJson(['status' => 'failed', 'message' => 'A provider with this email already exists']);
            } else {
                $user = new \Model\User();
                if (isset($_POST['email']) && $_POST['email'] != "") {
                    $user->email = $_POST['email'];
                }
                $user->first_name = $_POST['first_name'];
                $user->last_name = $_POST['last_name'];
                $user->fb_id = $_POST['fb_id'];
                $user->authorized = false;
                $hash = \Emagid\Core\Membership::hash($_POST['password']);
                $user->password = $hash['password'];
                $user->hash = $hash['salt'];
                $user->ref_key = md5(time() . mt_rand(1000000, 99999999));
                $type = \Model\Role::getItem(null, ['where' => ['name' => $_POST['user_type']]]);
                if ($user->save()) {
                    $userRoles = \Model\User_Roles::getList(['where' => 'active = 1 and role_id = 1 and user_id = ' . $user->id]);
                    if (count($userRoles) <= 0) {
                        $userRoles = new \Model\User_Roles();
                        $userRoles->role_id = $type->id;
                        $userRoles->user_id = $user->id;
                        $userRoles->save();
                    }
                    if (isset($_COOKIE['ref_u'])) {

                        if ($user_check = \Model\Provider::getItem(null, ['where' => "active = '1' and ref_key = '" . $_COOKIE['ref_u'] . "'"])) {
                            $user_id_from = $user_check->id;
                            $user_type_from = "provider";
                        }    //provider
                        elseif ($user_check = \Model\User::getItem(null, ['where' => "active = '1' and ref_key = '" . $_COOKIE['ref_u'] . "'"])) {
                            $user_id_from = $user_check->id;
                            $user_type_from = "user";
                        }//user

                        if (isset($user_check)) {
                            $ref = new \Model\Referral();
                            $ref->user_id_from = $user_id_from;
                            $ref->user_id_to = $user->id;
                            $ref->user_type_from = $user_type_from;
                            $ref->user_type_to = "user";
                            $ref->save();
                        }


                        setcookie('ref_u', "", -3600, "/");
                    }
                    $this->sign_up_mail($user->email, $user->first_name);
                    if($user->login()){
                        $fbTracking = new stdClass();
                        $fbTracking->label = '6036632535110';
                        $_SESSION['facebookAdConversionTracking'] = serialize($fbTracking);

                        $n = new \Notification\MessageHandler('Thank you for joining Wellns - we look forward to helping you create a better self, on every dimension...');
                        $_SESSION["notification"] = serialize($n);
                    }
                    if(isset($_SESSION['addrName']) && isset($_SESSION['street']) && isset($_SESSION['apt']) && isset($_SESSION['zip'])) {
                        $address = new \Model\User_Address();
                        $address->addAddrFromSession($_SESSION['em_authentication_id']);
                    }
                    echo $this->toJson(['status'=>'success']);
                } else {
                    echo $this->toJson(['status'=>'failed', 'message'=>$user->errors]);
                }
            }
        }
    }

    private function sign_up_mail($mail, $first_name_user)
    {
        global $emagid;
        $emagid->email->from->email = 'noreply@wellns.com';
        $mail = \Model\Mail::getItem(2);
        $text = $mail->email;
        $text = str_replace("{{user_first_name}}", $first_name_user, $text);
        $email = new \Emagid\Email();
        ///{{user_first_name}}
        $email->addTo($mail);
        $email->addTo("john@emagid.com");
        $email->addTo("eitan@emagid.com");
        $email->subject('Wellns sign up!');
        $email->body = $text;
        $q = "<html xmlns='http://www.w3.org/1999/xhtml' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
<head style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
<meta name='viewport' content='width=device-width' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
<link href='http://fonts.googleapis.com/css?family=Varela' rel='stylesheet' type='text/css' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
<title style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>Welcome to Arza</title>
<style style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
/* -------------------------------------
		GLOBAL
------------------------------------- */
* {
	margin: 0;
	padding: 0;
	font-size: 100%;
	line-height: 1.6;
}

img {
	max-width: 100%;
}

body {
	-webkit-font-smoothing: antialiased;
	-webkit-text-size-adjust: none;
	width: 100%!important;
	height: 100%;
	letter-spacing: .4px;
	font-family: 'Varela', sans-serif;
}


/* -------------------------------------
		ELEMENTS
------------------------------------- */
a {
	color: #348eda;
}

.btn-primary {
	text-decoration: none;
	color: #FFF;
	background-color: #348eda;
	border: solid #348eda;
	border-width: 10px 20px;
	line-height: 2;
	font-weight: bold;
	margin-right: 10px;
	text-align: center;
	cursor: pointer;
	display: inline-block;
	border-radius: 25px;
}

.btn-secondary {
	text-decoration: none;
	color: #FFF;
	background-color: #aaa;
	border: solid #aaa;
	border-width: 10px 20px;
	line-height: 2;
	font-weight: bold;
	margin-right: 10px;
	text-align: center;
	cursor: pointer;
	display: inline-block;
	border-radius: 25px;
}

.last {
	margin-bottom: 0;
}

.first {
	margin-top: 0;
}

.padding {
	padding: 10px 0;
}


/* -------------------------------------
		BODY
------------------------------------- */
table.body-wrap {
	width: 100%;
	padding: 20px;
}

table.body-wrap .container {
	border: 1px solid transparent;
}


/* -------------------------------------
		FOOTER
------------------------------------- */
table.footer-wrap {
	width: 100%;
	clear: both!important;
}

.footer-wrap .container p {
	font-size: 12px;
	color: #666;

}

table.footer-wrap a {
	color: #a0c2ce;
	text-decoration: none;
}


/* -------------------------------------
		TYPOGRAPHY
------------------------------------- */
h1, h2, h3 {
	color: #000;
	margin: 40px 0 10px;
	line-height: 1.2;
	font-weight: 200;
}

h1 {
	font-size: 36px;
}
h2 {
	font-size: 28px;
}
h3 {
	font-size: 22px;
}

p, ul, ol {
	margin-bottom: 10px;
	font-weight: normal;
	font-size: 14px;
}

ul li, ol li {
	margin-left: 5px;
	list-style-position: inside;
}

/* ---------------------------------------------------
		RESPONSIVENESS
		Nuke it from orbit. It's the only way to be sure.
------------------------------------------------------ */

/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
.container {
	display: block!important;
	max-width: 600px!important;
	margin: 0 auto!important; /* makes it centered */
	clear: both!important;
}

/* Set the padding on the td rather than the div for Outlook compatibility */
.body-wrap .container {
	padding: 20px;
}

</style>
</head>

<body bgcolor='#FFFFFF' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;-webkit-font-smoothing: antialiased;-webkit-text-size-adjust: none;height: 100%;letter-spacing: .4px;font-family: 'Varela', sans-serif;width: 100%!important;'>

<!-- body -->
<table class='body-wrap' bgcolor='#FFFFFF' style='margin: 0;padding: 20px 0;font-size: 100%;line-height: 1.6;width: 100%;padding-bottom:8px;'>
	<tbody style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'><tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
		<td style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'></td>
		<td class='container' bgcolor='#FFFFFF' style='margin: 0 auto!important;padding: 8px 0 20px 0;font-size: 100%;line-height: 1.6;border: 1px solid transparent;display: block!important;max-width: 800px!important;width:100%;clear: both!important;'>

			<!-- content -->
			<div class='content' style='margin: 0 auto;padding: 0;font-size: 100%;line-height: 1.6;max-width: 800px;width:100%;display: block;'>
				<table style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;width: 100%;padding-left:40px;padding-right:40px;'>
					<tbody style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
						<tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
							<td style='text-align: center;margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
								<img style='width: 200px;margin-bottom: 18px;padding: 0;font-size: 100%;line-height: 1.6;max-width: 100%;' src='http://wellns.com/content/frontend/assets/img/email_imgs/logo.png'>
								<p style = 'border-top:1px solid #000000;margin-top:8px;margin-bottom:24px;'></p>
								<a href='http://wellns.com/'><img src='http://wellns.com/content/frontend/assets/img/email_imgs/member_verification_welcome_hero.png' style='padding: 0;font-size: 100%;line-height: 1.6;max-width: 100%;'></a>
								<p style='text-align: center;font-size: 19px;margin: 0;padding: 0;line-height: 1.6;margin-bottom: 10px;padding-top:8px;font-weight: normal;margin-top:12px;'>Welcome $first_name_user</p>
								<p style='font-size: 14px;line-height: 22.5px;text-align: center;padding: 0 12px;margin-bottom: 10px;font-weight: normal;'>Thank you for your interest in joining the Wellns community. The stronger our community becomes, the more wellness we can spread accross the world. We look forward to embarking on this journey with you.
								</p>
								<a href='http://wellns.com/'>
								<img style='margin-top: 24px;margin-bottom: 18px;width: 330px;cursor: pointer;padding: 0;font-size: 100%;line-height: 1.6;max-width: 100%;' src='http://wellns.com/content/frontend/assets/img/email_imgs/mail_user.png'>
								</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- /content -->

		</td>
		<td style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'></td>
	</tr>
</tbody>
</table>
<!-- /body -->

<!-- footer -->
<table class='footer-wrap' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;width: 100%;clear: both!important;'>
	<tbody style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'><tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
		<td style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'></td>
		<td class='container' style='margin: 0 auto!important;padding: 0;font-size: 100%;line-height: 1.6;display: block!important;max-width: 600px!important;clear: both!important;'>

			<!-- content -->
			<div class='content' style='margin: 0 auto;padding: 0;font-size: 100%;line-height: 1.6;max-width: 600px;display: block;'>
				<table style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;width: 100%;'>
					<tbody style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'><tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
						<td align='center' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
							<p style='margin: 0;padding: 0;font-size: 12px;line-height: 1.6;margin-bottom: 10px;font-weight: normal;color: #666;'>
								<a class='text-decoration:none;color:#a0c2ce;' href='https://wellns.com/' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;color: #a0c2ce;text-decoration: none;'><unsubscribe style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>Unsubscribe</unsubscribe></a>
								<span style='color: #000000;margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>·</span>
								<a class='text-decoration:none;color:#a0c2ce;' href='https://wellns.com/' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;color: #a0c2ce;text-decoration: none;'>Contact Us</a>
								<span style='color: #000000;margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>·</span>
								<a class='text-decoration:none;color:#a0c2ce;' href='https://wellns.com/' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;color: #a0c2ce;text-decoration: none;'>Privacy Policy</a>
							</p>
						</td>
					</tr>
					<tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
						<td align='center' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
							<p style='color: #8b8b8b;font-size: 9px;width: 80%;max-width: 600px;margin: 0;padding: 0;line-height: 1.6;margin-bottom: 10px;font-weight: normal;'>
							© 2015 Wellns. The Wellns logo is a registered trademark of Wellns LLC. All rights reserved. This email has been sent to your by Wellns, 225 West 39th Street, New York City, NY 10009. Made by <b style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>Emagid</b>
							</p>
						</td>
					</tr>
				</tbody></table>
			</div>
			<!-- /content -->

		</td>
		<td style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'></td>
	</tr>
</tbody></table>
<!-- /footer -->



</body></html>";

        $email->send(); //sends to the customer


    }

    public function change_password()
    {

        $user = $this->viewData->user;
        $current_pass = $user->password;
        $curr_password = @trim($_POST['curr_password']);
        $new_password = @trim($_POST['new_password']);
        $new_password_2 = @trim($_POST['new_password_2']);

        $hash = \Emagid\Core\Membership::hash($curr_password, $user->hash);

        if ($current_pass !== $hash['password']) {
            echo "<font color='red'>Current password is wrong!</font>";
        } else {
            if ($new_password !== $new_password_2) {
                echo "<font color='red'>New passwords doesn`t match!</font>";
            } elseif (empty($new_password) || empty($new_password_2)) {
                echo "<font color='red'>New passwords are empty!</font>";
            } elseif ((strlen($new_password) < 7) || (strlen($new_password_2) < 7)) {
                echo "<font color='red'>New password should be more than 7 symbols!</font>";
            } else {
                echo "<font color='green'>Password has been changed!</font>";
                $new_pass = \Emagid\Core\Membership::hash($new_password);
                $user->password = $new_pass['password'];
                $user->hash = $new_pass['salt'];
                $user->save();
            }
        }
    }

    public function confirmEmail()
    {
        if(!isset($_POST['email']) || !$_POST['email']){
            $n = new \Notification\MessageHandler("Update email failed");
            $_SESSION["notification"] = serialize($n);
            return $this->toJson(['success' => false]);
        }


        // we don't update existing email
        if($this->viewData->user->email){
            $n = new \Notification\MessageHandler("Update email failed");
            $_SESSION["notification"] = serialize($n);
            return $this->toJson(['success' => false]);
        }

        // email already taken
        if(\Model\User::getItem(null, ['where' => ['email' => $_POST['email']]])){
            $n = new \Notification\MessageHandler("Email exists, please use another one");
            $_SESSION["notification"] = serialize($n);
            return $this->toJson(['success' => false]);
        }

        $this->viewData->user->email = $_POST['email'];
        $this->viewData->user->save();

        $n = new \Notification\MessageHandler("Email updated!");
        $_SESSION["notification"] = serialize($n);
        return $this->toJson(['success' => true]);
    }

    public function add_address()
    {
        if(!empty($_POST['userId'])) {
            $loggedUser = \Model\User::getItem($_POST['userId']);
            $user_address = new \Model\User_Address();

            $user_address->name = @trim($_POST['name']);
            $user_address->street = @trim($_POST['street']);
            $user_address->apt = @trim($_POST['apt']);
            $user_address->zip = @trim($_POST['zip']);
            $user_address->add_info = @trim($_POST['addInfo']);
            $user_address->user_id = $loggedUser->id;
        } else {
            $_SESSION['addrName'] = @trim($_POST['name']);
            $_SESSION['street'] = @trim($_POST['street']);
            $_SESSION['apt'] = @trim($_POST['apt']);
            $_SESSION['zip'] = @trim($_POST['zip']);
            $_SESSION['add_info'] = @trim($_POST['addInfo']);
        }
        if ((trim($_POST['name']) == "") || (trim($_POST['street']) == "") || (trim($_POST['apt']) == "") || (trim($_POST['zip']) == "")) {
            echo "All fields are required";
        } else {

            $user_address->save();
//            echo '
// 					<li class = "user_location_list_item_wrapper">
//						<div class = "user_location">
//							<p class = "user_location_text">
//								<span class = "user_location_label">' . $user_address->name . '</span>
//								<span class = "user_location_address">' . $user_address->street . ' ' . $user_address->apt . ' ' . $user_address->zip . '</span>
//							</p>
//						</div>
//						<div class = "vertical_address_delete_separator"></div>
//						<div class = "delete_address" id-address="' . $user_address->id . '">
//							<span class="delete_address_icon">
//								<svg class="modal_svg" xmlns="http://www.w3.org/2000/svg" version="1.1" width="32" height="32" viewBox="0 0 32 32">
//			                        <path class="modal_svg_path" d="M31.158 32.118c-.246 0-.492-.093-.678-.282L.163 1.52c-.375-.375-.375-.983 0-1.358s.983-.375 1.358 0l30.317 30.316c.375.375.375.983 0 1.358-.187.188-.433.282-.678.282zM.842 32.118c-.246 0-.492-.093-.678-.282-.375-.375-.375-.983 0-1.358L30.48.162c.375-.375.983-.375 1.358 0s.375.983 0 1.358L1.52 31.836c-.186.188-.432.282-.677.282z" fill="#fff"></path>
//			                    </svg>
//							</span>
//						</div>
//					</li>';
            echo '<div class="option_button dropdown_option_button option_button_radio location_ui">
                      <p class="cal_r">' . $user_address->street . ' ' . $user_address->apt . ', ' . $user_address->zip . '</p>
                      <input type="checkbox" value="'.$user_address->id.'">
                  </div>';

        }
    }

    public function respond_message()
    {


        if ((trim($_POST['subject']) == "") || (trim($_POST['text']) == "")) {
            echo "All fields required!";
        } else {


            $message = new \Model\Message();


            $message->insert_time = date("Y-m-d H:i:s");
            $message->text = @trim($_POST['text']);
            $message->subject_id = @trim($_POST['subject']);
            $message->author = "user";
            $message->save();
        }
    }

    public function send_message()
    {


        if ((trim($_POST['subject']) == "") || (trim($_POST['provider']) == "") || (trim($_POST['text']) == "")) {
            echo "All fields required!";
        } else {

            $provider = \Model\Provider::getItem($_POST['provider']);
            $user = $this->viewData->user;
            \Model\Message::send($provider, $user, $_POST['subject'], $_POST['text']);
        }
    }

    public function delete_address()
    {
        $class = new \Model\User_Address();
        $class::delete($_POST['id']);
    }

    public function health_questionaire()
    {
        $user = $this->viewData->user;
        if ($user) {

            foreach ($_POST as $key => $service) {
                if ($key == "yob" || $key == "mob" || $key == "dob") {

                } else {
                    if (!empty($service)) {
                        $user->$key = $service;
                    }

                }

            }
            $user->dob = $dob = $_POST['yob'] . '-' . $_POST['mob'] . '-' . $_POST['dob'];

            $user->save();
            redirect(SITE_URL . 'user/dashboard/');
        }
    }

    public function make_reservation()
    {

        $user = $this->viewData->user;
        if ($user) {
            $provider = $_POST['provider'];
            $service = $_POST['service'];
            $session_location = $_POST['session_location'];
            $session_datetime = $_POST['session_datetime'];
            $or_pr = \Model\Order_Product::getItem($service);
            $or_pr->used = $or_pr->used + 1;
            $or_pr->save();
            $schedule = new \Model\Schedule();
            $schedule->user_id = $user->id;
            $schedule->provider_id = $provider;
            $schedule->status = 0;
            $schedule->service = $or_pr->service;
            $schedule->order_product_id = $service;
            $schedule->session_location = $session_location;
            $schedule->day = date("Y-m-d", strtotime($session_datetime));
            $schedule->time = date("g:ia", strtotime($session_datetime));

            if ($schedule->save()) {
                $this->new_session_request($schedule->id);
                echo "success";
            }

        }
    }

    private function new_session_request($id)
    {
        global $emagid;
        $emagid->email->from->email = 'noreply@wellns.com';
        $schedule = \Model\Schedule::getItem($id);
        $mail = \Model\Mail::getItem(3);
        $text = $mail->email;
        $time = date('F d Y  \a\t g:ia', strtotime($schedule->day . ' ' . $schedule->time));
        //{{time_and_date}}
        $text = str_replace("{{time_and_date}}", $time, $text);

        $service = \Model\Service::getItem($schedule->service);

        //{{service_name}}
        $text = str_replace("{{service_name}}", $service->name, $text);
        $day = date('d', strtotime($schedule->day));
        //{{day}}
        $text = str_replace("{{day}}", $day, $text);
        $month = date('M', strtotime($schedule->day));
        //{{month}}
        $text = str_replace("{{month}}", $month, $text);
        $provider = \Model\Provider::getItem($schedule->provider_id);
        $user = \Model\User::getItem($schedule->user_id);
        $pr_name = $provider->first_name . ' ' . $provider->last_name;
        //{{provider_full_name}}
        $text = str_replace("{{provider_full_name}}", $pr_name, $text);
        $user_name = $user->first_name . ' ' . $user->last_name;
        //{{user_full_name}}
        $text = str_replace("{{user_full_name}}", $user_name, $text);


        //{{location}}
        $text = str_replace("{{location}}", $schedule->session_location, $text);
        $email = new \Emagid\Email();

        $email->addTo($provider->email);
        $email->addTo("john@emagid.com");
        $email->addTo("eitan@emagid.com");
        $email->subject('New session request!');
        $email->body = $text;
        $q = "
<html xmlns='http://www.w3.org/1999/xhtml' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'><head style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
<meta name='viewport' content='width=device-width' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
<link href='http://fonts.googleapis.com/css?family=Varela' rel='stylesheet' type='text/css' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
<title style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>Your Upcoming Session</title>
<style style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>

</style>
</head>

<body bgcolor='#FFFFFF' style='font-family:'Varela', sans-serif;margin: 0;padding: 0;font-size: 100%;line-height: 1.6;-webkit-font-smoothing: antialiased;-webkit-text-size-adjust: none;height: 100%;letter-spacing: .4px;'>

<!-- body -->
<table class='body-wrap' bgcolor='#FFFFFF' style='margin: 0;padding: 20px 0;font-size: 100%;line-height: 1.6;width: 100%;'>
	<tbody style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'><tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
		<td style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'></td>
		<td class='container' bgcolor='#FFFFFF' style='margin: 0 auto!important;padding: 8px 0 20px 0;font-size: 100%;line-height: 1.6;border: 1px solid transparent;display: block!important;max-width: 1200px!important;width:100%;clear: both!important;'>

			<!-- content -->
			<div class='content' style='margin: 0 auto;padding: 0;font-size: 100%;line-height: 1.6;max-width: 600px;display: block;'>
			<table style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;width: 100%;'>
				<tbody style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'><tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
					<td style='text-align: center;margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
						<img style='width: 200px;margin-bottom: 18px;padding: 0;font-size: 100%;line-height: 1.6;max-width: 100%;' src='http://wellns.com/content/frontend/assets/img/email_imgs/logo.png'>
						<p style = 'border-top:1px solid transparent;margin-top:8px;margin-bottom:24px;'></p>
						<img style='width: 170px;margin-bottom: -7px;padding: 0;font-size: 100%;line-height: 1.6;max-width: 100%;' src='http://wellns.com/content/frontend/assets/img/email_imgs/new_session_request.png' alt='UPCOMING SESSION'>
						<p style='text-align: center;font-size: 22px;margin: 0;padding: 0;line-height: 1.6;margin-bottom: 10px;font-weight: normal;margin-top:18px;'>Hi $pr_name,</p>
						<p style='font-size: 14px;line-height: 19.5px;text-align: center;padding: 0;margin-bottom: 10px;font-weight: normal;'>A new session has been requested by $user_name for $time. We have listed the details of this session below.</p>

						<p style='font-size: 16px;line-height: 22.5px;text-align: center;padding: 0 12px;margin-bottom: 10px;font-weight: normal;'><strong>Would you like to approve or reject this session request?</strong><br> <span style = 'font-size:10.5px;color:#8b8b8b;'>You may also do so from your <a style=';text-decoration:none;color:#64cfdb' href='https://wellns.com'>dashboard</a> on Wellns.</span></p>

						<table cellpadding='0' cellspacing='0' style='width:97%;max-width:480px;background-color: #FFFFFF;margin:24px auto;'>
							<tr>
								<td align = 'left' style = 'width:40%;padding-right:10px;'>
									<a  href='https://wellns.com/provider/calendar' style= 'text-align:center;background-color:#64cfdb;display:block;color:#FFFFFF;padding:7px 0;text-transform:uppercase;letter-spacing:1.5px;font-size:12.5px;'>Approve Session</a>
								</td>
								<td align = 'right' style = 'width:40%;padding-left:10px;'>
									<a href='https://wellns.com/provider/calendar' style = 'background:#c3c3c3;text-transform:uppercase;letter-spacing:1.5px;color:#ffffff;text-align:center;display:block;padding:7px 0;font-size:14px;'>Reject</a>
								</td>
							</tr>
						</table>

						<div style='background-color:#FFFFFF;border-top:10px solid #fff;background:#fff;border-bottom: 1px solid #e5e5e5;'>
							<div style='background-color: #FFFFFF;font-weight:bold;border: 8px solid #FFFFFF;color: #000000!important;font-size:16px;border-bottom: 1px solid #e5e5e5;padding-bottom: 6px;margin-bottom: 7px;'>
								<a href='' style='color:#FFFFFF!important;text-decoration:none' target='_blank'>
									<font color='#000000' style='font-size:12px;text-transform:uppercase;position: relative;top: -5px;letter-spacing: 2px; font-weight: light;'>Session Request Details</font>
								</a>
							</div>
							<table cellpadding='0' cellspacing='0' style='width:100%;background-color: #FFFFFF;'>
							<tbody>
							<tr style=''>
								<td>
									<div style='margin:0 auto;width:52px;background-color: #ffffff;vertical-align:top;border-top:1px solid #fff;padding:8px 0 8px 8px'>
										<div style='border: 1px solid #a0c2ce;border-radius: 1px;'>
											<div style='background-color: #a0c2ce;text-transform:uppercase;color: #000!important;font-weight: light;text-align:center;font-size:9px;line-height:14px;min-height:14px;padding: 0 14px;'>
												<font color='#fff' style='color: #ffffff;'>$month</font>
											</div>
											<div style='background-color:#fff;font-size: 21px;line-height:18px;min-height:18px;color: #a0c2ce;font-weight: 100!important;text-align:center;padding: 9px 4px 9px 4px;'>$day</div>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td style='vertical-align:top;padding:8px;padding-left:14px;border-top:1px solid #fff;text-align:center;'>
									<table cellpadding='0' cellspacing='0' style='width:100%'>
										<tbody>
											<tr>
												<td style='position:relative;top:-8px;background-color: #ffffff;vertical-align:top'>
													<a href='' style='font-size:14px;color:#000;text-decoration:none;display:block;text-align:center;' target='_blank'>1 Hour $service->name Session with $pr_name.</a>
													<div style='padding:2px 0 4px 0;font-size:12px;line-height:16px;color: #8b8b8b;display:block;text-align:center;'>$schedule->session_location</div>
													<div style='padding:2px 0 4px 0;font-size:12px;line-height:16px;color: #8b8b8b;display:block;text-align:center;'>$time</div>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							</tbody>
							</table>
							</div>




					</td>
				</tr>
			</tbody></table>
			</div>
			<!-- /content -->

		</td>
		<td style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'></td>
	</tr>
</tbody></table>
<!-- /body -->

<!-- footer -->
<table class='footer-wrap' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;width: 100%;clear: both!important;'>
	<tbody style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'><tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
		<td style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'></td>
		<td class='container' style='margin: 0 auto!important;padding: 0;font-size: 100%;line-height: 1.6;display: block!important;max-width: 600px!important;clear: both!important;'>

			<!-- content -->
			<div class='content' style='margin: 0 auto;padding: 0;font-size: 100%;line-height: 1.6;max-width: 600px;display: block;'>
				<table style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;width: 100%;'>
					<tbody style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'><tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
						<td align='center' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
							<p style='margin: 0;padding: 0;font-size: 12px;line-height: 1.6;margin-bottom: 10px;font-weight: normal;color: #666;'>
								<a class='text-decoration:none;color:#a0c2ce;' href='https://wellns.com/' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;color: #a0c2ce;text-decoration: none;'><unsubscribe style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>Unsubscribe</unsubscribe></a>
								<span style='color: #000000;margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>·</span>
								<a class='text-decoration:none;color:#a0c2ce;' href='https://wellns.com/' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;color: #a0c2ce;text-decoration: none;'>Contact Us</a>
								<span style='color: #000000;margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>·</span>
								<a class='text-decoration:none;color:#a0c2ce;' href='https://wellns.com/' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;color: #a0c2ce;text-decoration: none;'>Privacy Policy</a>
							</p>
						</td>
					</tr>
					<tr style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
						<td align='center' style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>
							<p style='color: #8b8b8b;font-size: 9px;width: 80%;max-width: 600px;margin: 0;padding: 0;line-height: 1.6;margin-bottom: 10px;font-weight: normal;'>
							© 2015 Wellns. The Wellns logo is a registered trademark of Wellns LLC. All rights reserved. This email has been sent to your by Wellns, 225 West 39th Street, New York City, NY 10009. Made by <b style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'>Emagid</b>
							</p>
						</td>
					</tr>
				</tbody></table>
			</div>
			<!-- /content -->

		</td>
		<td style='margin: 0;padding: 0;font-size: 100%;line-height: 1.6;'></td>
	</tr>
</tbody></table>
<!-- /footer -->



</body></html>";

        $email->send();
    }

    public function change_credit_card()
    {

        $user = $this->viewData->user;
        $card_check = \Model\Card::getItem(null, ['where' => "active=1 and user_id = '" . $user->id . "'"]);

        if (!$card_check) {
            $card = new \Model\Card();

            $card->cc_number = $_POST['cc_number'];

        } else {
            $card = \Model\Card::getItem(null, ['where' => "active=1 and user_id = '" . $user->id . "'"]);
            if ('****' . substr($card->cc_number, -4) != $_POST['cc_number']) {
                $card->cc_number = $_POST['cc_number'];
            }
        }
        $card->user_id = $user->id;

        $card->cc_holder = trim($_POST['cc_holder']);

        $card->cc_expiration_month = trim($_POST['cc_expiration_month']);
        $card->cc_expiration_year = trim($_POST['cc_expiration_year']);
        $card->cc_ccv = trim($_POST['cc_ccv']);


        if ($card->save()) {
            echo "<font color='green'>Information has been saved!</font>";
        } else {
            echo "<font color='red'>All fields required!</font>";

        }

    }

    public function update_image()
    {

        $fileName = rand(0, 9999999999999999999);

        foreach ($_FILES as $fileType => $file) {
            $imageKey = $this->viewData->user->uploadPicture($_FILES[$fileType]);
            $this->viewData->user->photo = $imageKey;
            $this->viewData->user->save();

            redirect(SITE_URL . 'user/dashboard');

        }


    }

    public function favorite_provider()
    {

        $user = $this->viewData->user;
        /**
         * Guest can't favorite any provider
         */
        if(!$user){
            return null;
        }
        $favorite = new \Model\User_Favorite();

        $favorite->provider_id = $_POST['provider_id'];
        $favorite->user_id = $user->id;

        $check_provider = \Model\Provider::getItem(null, ['where' => "id = '" . $favorite->provider_id . "' and authorized='TRUE'"]);
        if ($check_provider) {
            $check_fav = \Model\User_Favorite::getItem(null, ['where' => "provider_id = '" . $favorite->provider_id . "' and user_id = '" . $user->id . "'"]);

            if (!empty($check_fav)) {
                if ($check_fav->active == 0) {
                    $favorite->id = $check_fav->id;
                    $favorite->active = 1;
                    $favorite->save();
                } else {
                    $favorite->id = $check_fav->id;
                    $favorite->active = 0;
                    $favorite->save();
                }
            } else {
                $favorite->save();

            }
        }
    }


} 


	 



 