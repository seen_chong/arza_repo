<?php

class matchesController extends siteController
{

    public function index(Array $params = [])
    {
        $zip = @trim($_GET['where']);
        $serviceSlug = @strtolower(trim($_GET['service_category']));
        $b = [];

        if($serviceSlug) {
            $services = \Model\Provider_Services::getServicesByServiceSlug($serviceSlug);
            $providers = \Model\Provider_Services::getAvailableProvidersByServices($services);
            foreach($providers as $provider){
                $b[] = $provider->id;
            }
            $this->viewData->q = $b;
        } else {

            $this->viewData->provider_all = \Model\Provider::getList(['where' => "authorized = 'TRUE'"]);

            foreach ($this->viewData->provider_all as $all) {
                $b[] = $all->id;
            }

            $this->viewData->q = $b;

        }
        if ($serviceSlug) {
            $carbon = \Carbon\Carbon::now();
            $string = $carbon->toDateString();

            $provider_service = \Model\Service::getItem(NULL, ['where' => "lower(slug) = '" . $serviceSlug . "'"]);
            $providerList = $provider_service->getProviderList();
            $this->viewData->filteredService = $provider_service;

            foreach($providerList as $index => $provider){
                if($provider->doesAvailable()){
                    unset($providerList[$index]);
                    $this->viewData->featuredProvider = $provider;
                    break;
                }
            }

            /**
             * If not any provider setup calendar, we default show the first random provider
             */
            if(!isset($this->viewData->featuredProvider)){
                $this->viewData->featuredProvider = $providerList[0];
                array_shift($providerList);
            }


            $_SESSION['matchesReminding'] = serialize($providerList);
            $providerList = array_slice($providerList, 0, 4);

            $this->viewData->allProviders = $providerList;
        }

        $this->configs['Meta Title'] = "Wellns";
        $this->loadView($this->viewData);
    }

    public function getMoreMatches()
    {
        $serviceSlug = $_POST['service'];
        $seq = intval($_POST['seq']);
//        $provider_service = \Model\Service::getItem(NULL, ['where' => "lower(slug) = '" . $serviceSlug . "'"]);

        $providers = unserialize($_SESSION['matchesReminding']);

        $chunkProviders = array_splice($providers, $seq*4, 4);

        $_SESSION['matchesReminding'] = serialize($providers);

        $htmlContent = null;

        foreach($chunkProviders as $allProviders){
            if($this->viewData->user){
                $favored = \Model\User_Favorite::getItem(null,['where'=>"provider_id = {$allProviders->id} and user_id = {$this->viewData->user->id}"])? 'favored':'';
            } else {
                $favored = '';
            }

            if($model->user == null){
                $user_start = 'user_start';
            }else{
                $user_start = '';
            }

            $tmp = "<div class='col matchResultBox'>
                <a class='' href='/provider/profile/{$allProviders->id}?profile_service=$serviceSlug'>
                    <div class='mediaWrapper'>
                        <div class='media' style='background-image:url(".$allProviders->getPhoto().")'></div>
                    </div>
                </a>
                <div class='matchInfo'>
                    <div class='abs_right'>
                        <a class='favorite $favored $user_start' date-id-favorite='{$allProviders->id}'>
                            <icon>
                                <svg version='1.1' id='Layer_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px'
                                 viewBox='0 0 241.3 224.7' enable-background='new 0 0 241.3 224.7' xml:space='preserve'>
                                            <path d='M120.6,34.6c0,0,0.5-0.5,0.8-0.8c9.8-11.2,30.1-30,54.2-30c33.6,0,61.9,28.4,61.9,62c0,22-14.5,50.1-43.3,83.7
                                            c-10.8,12.6-23.5,25.8-37.6,39.3c-10,9.5-18.7,17.1-24.2,21.9c-3.4,3-11.9,10.5-11.9,10.5s-8.3-7.5-11.6-10.4
                                            C77.2,183.6,3.6,115.4,3.6,65.9c0-33.6,28.3-62,61.9-62c24.2,0,44.4,18.7,54.3,29.9L120.6,34.6z'/>
                                        </svg>
                            </icon>
                        </a>
                    </div>
                    <h2 class='cal_m'><a class='turn_dg' href='/provider/profile/{$allProviders->id}?profile_service=$serviceSlug'>".$allProviders->full_name()."</a></h2>
                    <div class='row row_of_2'>
                        <div class='col'>
                            <p class='quote_style libr_r dg'>".(strlen($allProviders->promise_statement) > 230 ? substr($allProviders->promise_statement,0,230).'...': $allProviders->promise_statement)."</p>
                        </div>
                        <div class='col'>
                            <div class='style'>
                                <h4 class='cal_m'>Teaching Style</h4>
                                <p class='cal_r dg'>".(strlen($allProviders->teaching_description) > 87 ? substr($allProviders->teaching_description,0,87).'...': $allProviders->teaching_description)."</p>
                            </div>
                            <div class='style about'>
                                <h4 class='cal_m'>About ".ucfirst($allProviders->first_name)."</h4>
                                <p class='cal_r dg'>".(strlen($allProviders->description) > 87 ? substr($allProviders->description,0,87).'...': $allProviders->description)."</p>
                            </div>
                            <div class='footer row_of_2 row'>
                                <div class='col'>
                                    <a class='inv_btn turn_dg btn_$serviceSlug'>View Profile</a>
                                </div>
                                <div class='col'>
                                    <a class='well_btn btn_full_width btn_$serviceSlug' href='/user/calendar/$allProviders->id'>Book Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>";

            $htmlContent .= $tmp;
        }

//        dd($htmlContent);

        echo $this->toJson(['data' => $htmlContent]);
    }
}