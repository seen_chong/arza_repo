<?php

class resourceController extends siteController
{

    public function index(Array $params = [])
    {
        $this->loadView($this->viewData);
    }

    public function resource(Array $params = [])
    {
        $id = (isset($params['resource_id'])) ? $params['resource_id'] : '';
        $this->viewData->resource = \Model\Ebook::getItem($id);
        if($this->viewData->resource) {
            $this->configs['Meta Title'] = $this->viewData->resource->name . "| Arza Resource";
            $this->loadView($this->viewData);
        }else{
            redirect('/');
        }
    }


}