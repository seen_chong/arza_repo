<?php

class creditsController extends siteController
{
    function __construct()
    {
        parent::__construct();
    }

    public function index(Array $params = [])
    {
        if ($this->viewData->user && $this->viewData->user->isCorporate()) {
            $this->viewData->credits = \Model\Credit::getCorporateCreditList();
        } else {
            $this->viewData->credits = \Model\Credit::getCustomerCreditList();
        }

        $this->loadView($this->viewData);
    }
}