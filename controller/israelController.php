<?php

class israelController extends siteController
{

    public function index(Array $params = [])
    {
        $this->viewData->posts = \Model\Israel::getList(['where' => "status = '1'",'orderBy'=>'date_modified desc']);
        $this->viewData->featured_posts = \Model\Israel::getList(['where' => "status = 1 AND featured = 1"]);
        $this->viewData->recent_posts = \Model\Israel::getList(['where' => "status = 1",'orderBy'=>'insert_time DESC', 'limit'=>4]);

        $this->configs['Meta Title'] = "Israel in the Parasha";
        $this->loadView($this->viewData);
    }

    public function post(Array $params = [])
    {
        $slug = (isset($params['slug'])) ? $params['slug'] : '';
        if(isset($_GET['draft'])&&$_GET['draft']=='true'){
            $this->viewData->post = \Model\Israel::getItem(null, ['where' => "status = '0' and slug = '" . $slug . "'"]);
        }else{
            $this->viewData->post = \Model\Israel::getItem(null, ['where' => "status = '1' and slug = '" . $slug . "'"]);
        }
        if($this->viewData->post) {
            if (!is_null($this->viewData->post->meta_keywords) && $this->viewData->post->meta_keywords != ''){
                $this->configs['Meta Keywords'] = $this->viewData->post->meta_keywords;
            }
            if (!is_null($this->viewData->post->meta_description) && $this->viewData->post->meta_description != ''){
                $this->configs['Meta Description'] = $this->viewData->post->meta_description;
            }
            if (!is_null($this->viewData->post->meta_title) && $this->viewData->post->meta_title != ''){
                $this->configs['Meta Title'] = $this->viewData->post->meta_title;
            }else{
                $this->configs['Meta Title'] = $this->viewData->post->title . ' | Arza Statement';
            }
            $this->loadView($this->viewData);
        }else{
            redirect('/israel');
        }

    }


}