<?php

class confirmationController extends siteController
{

    public function index(Array $params = [])
    {
        $this->viewData->sessionHistory = \Model\SessionHistory::getItem($params['id']);
        parent::index($params);
    }
}