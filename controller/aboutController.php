<?php

class aboutController extends siteController
{

    public function terms(Array $params = [])
    {
        $this->loadView($this->viewData);
    }

    public function index(Array $params = [])
    {
        $this->configs['Meta Title'] = "About Wellns";
        $this->loadView($this->viewData);
    }

    public function faq(Array $params = [])
    {
        $this->configs['Meta Title'] = "Wellns FAQ";
        $this->loadView($this->viewData);
    }

    public function waiver(Array $params = [])
    {
        $this->loadView($this->viewData);
    }

}