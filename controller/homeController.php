<?php

class homeController extends siteController
{


    public function index(Array $params = [])
    {
        $this->viewData->events = \Model\Event::getLiveEvents();
        $splash = \Model\Home::getItem(null,['where'=>"section_name='Splash'"]);
        if($splash){
            $this->viewData->splash = $splash;
        }
        $home = \Model\Home::getCount();
        $this->viewData->news = \Model\News::getList();
        $this->viewData->latests = \Model\Latest::getList(['orderBy'=>'display_order ASC']);
        $this->loadView($this->viewData);


    }

    public function buildRSVP(){
        $event = \Model\Event::getItem($_POST['id']);
        if($event){
            $time = date("m/d/Y g:iA",  $event->start_time).' - '.date("m/d/Y g:iA",  $event->end_time);
            $name = $event->name;
            $description = $event->description;
            $price = number_format($event->price,2);
        }else{
            $time=[];
            $name = [];
            $description=[];
            $price=[];
        }
        $arr = ['time'=>$time,'name'=>$name,'description'=>$description,'price'=>$price];
        $this->toJson($arr);
    }
    public function rsvp_post(){
        $rsvp = \Model\Rsvp::loadFromPost();
        $event = \Model\Event::getItem($_POST['event_id']);
        $rsvp->insert_time = date('Y-m-d H:i:s.u');
        $rsvp->ref_num = generateToken();
        if($event->getRsvpNumber()<$event->capacity){
            if($rsvp->save()){
                if($event->price == 0 ){
                    $rsvp->status = \Model\Rsvp::$status[0];
                    global $emagid;
                    $emagid->email->from->email = 'report@emagid.com';
                    $email = new \Emagid\Email();
                    $email->addTo($rsvp->email);
                    $email->subject('Reserve Successful!');
                    $email->body = '<p><a href="https://arza.org"><img src="https://arza.org/content/frontend/assets/img/navyLogoText.png" /></a></p>'
                        . '<p><b>Dear ' . $rsvp->full_name() . '</b></p>'
                        . '<p>Thank you for attending our events</p>'
                        . '<li> Event Name : '. $event->name .'</li>'
                        . '<li> Time: '. date("m/d/Y g:iA",  $event->start_time).'</li>'
                        . '<li> Address : '.$event->getAddr() .'</li>'
                        . '<p>If you have any questions please feel free to contact us by phone at '.$event->contact_number .' </p>'
                        . '<p>Thanks again!</p>';
                    $email->send();
                    $rsvp->save();
                    $n = new \Notification\ErrorHandler("Successfully Reserved");
                    $_SESSION["notification"] = serialize($n);
                    redirect('/home/confirmation/'.$rsvp->ref_num);
                }else {
                    $PItems = new \PayPal\Api\ItemList();
                    $PItem = new \PayPal\Api\Item();
                    $PItem->setName($event->name)->setPrice($event->price)->setQuantity(1)->setCurrency('USD');
                    $PItems->setItems(array($PItem));
                    $apiContext = new \PayPal\Rest\ApiContext(new \PayPal\Auth\OAuthTokenCredential(PAYPAL_CLIENT_ID,PAYPAL_CLIENT_SECRET));
                    $apiContext->setConfig(array('mode' => 'live'));
                    $payer = new \PayPal\Api\Payer(); $payer->setPaymentMethod('paypal');
                    $PAmount = new \PayPal\Api\Amount(); $PAmount->setTotal(number_format($event->price,2))->setCurrency('USD');
                    $PTransaction = new \PayPal\Api\Transaction(); $PTransaction->setAmount($PAmount)->setDescription($event->name);
                    $PTransArr[] = $PTransaction;
                    //Live
                    $pPRedirectUrl = new \PayPal\Api\RedirectUrls(); $pPRedirectUrl->setReturnUrl("https://arza.org/home/confirmation/$rsvp->ref_num")->setCancelUrl('https://arza.org');
                    //Local
//                    $pPRedirectUrl = new \PayPal\Api\RedirectUrls(); $pPRedirectUrl->setReturnUrl("http://arzafront.app/home/confirmation/$rsvp->ref_num")->setCancelUrl('http://arzafront.app/');
                    $payment = new \PayPal\Api\Payment();
                    $payment = $payment->setIntent('sale')->setPayer($payer)->setTransactions($PTransArr)->setRedirectUrls($pPRedirectUrl)->create($apiContext);
                    if(strtolower($payment->getState()) == 'created'){
                        $redirect = $payment->getLink('approval_url');
                        $rsvp->status = \Model\Rsvp::$status[2];
                        $rsvp->save();
                        redirect($redirect);
                    }else {
                        \Model\Rsvp::delete($rsvp->id);
                        $n = new \Notification\ErrorHandler($payment->getFailureReason());
                        $_SESSION['notification'] = serialize($n);
                        redirect('/');
                    }

                }
            }else {
            	$rsvp->status = \Model\Rsvp::$status[3];
            	$rsvp->save();
                $n = new \Notification\ErrorHandler($rsvp->errors);
                $_SESSION["notification"] = serialize($n);
                redirect('/');
            }
        }else{
            $rsvp->status = \Model\Rsvp::$status[3];
            $rsvp->save();
            $n = new \Notification\ErrorHandler("Sorry,this event is fully booked.");
            $_SESSION["notification"] = serialize($n);
            redirect('/');
        }



    }
    public function rabbinic(){
        $this->loadView($this->viewData);
    }
    public function kehilot(){
        $this->loadView($this->viewData);
    }

    function confirmation(Array $params = []){
        if(($rsvp = \Model\Rsvp::getItem(null,['where'=>"ref_num = '".$params['id']."'"]))){
            $event =  \Model\Event::getItem($rsvp->event_id);
            $this->viewData->event = $event;
            $this->viewData->rsvp = $rsvp;
            if(isset($_GET['paymentId']) && $_GET['paymentId']){
                $apiContext = new \PayPal\Rest\ApiContext(new \PayPal\Auth\OAuthTokenCredential(PAYPAL_CLIENT_ID,PAYPAL_CLIENT_SECRET));
                $apiContext->setConfig(['mode'=>'live']);
                $payment = \PayPal\Api\Payment::get($_GET['paymentId'],$apiContext);
                $paymentExecution = new \PayPal\Api\PaymentExecution();
                $paymentExecution->setPayerId($_GET['PayerID']);
                $payment = $payment->execute($paymentExecution,$apiContext);
                if(strtolower($payment->getState()) == 'approved') {
                    $rsvp->paypal_id = $_GET['paymentId'];
                    $rsvp->status = \Model\Rsvp::$status[0];
                    $rsvp->save();
                    global $emagid;
                    $emagid->email->from->email = 'report@emagid.com';
                    $email = new \Emagid\Email();
                    $email->addTo($rsvp->email);
                    $email->subject('Reserve Successful!');
                    $email->body = '<p><a href="https://arza.org"><img src="https://arza.org/content/frontend/assets/img/navyLogoText.png" /></a></p>'
                        . '<p><b>Dear ' . $rsvp->full_name() . '</b></p>'
                        . '<p>Thank you for attending our events</p>'
                        . '<li> Event Name : '. $event->name .'</li>'
                        . '<li> Time: '. date("m/d/Y g:iA",  $event->start_time).'</li>'
                        . '<li> Address : '.$event->getAddr() .'</li>'
                        . '<p>If you have any questions please feel free to contact us by phone at '.$event->contact_number .' </p>'
                        . '<p>Thanks again!</p>';
                    $email->send();
                } else {
                    $n = new \Notification\ErrorHandler($payment->getFailureReason());
                    $_SESSION['notification'] = serialize($n);
                    \Model\Rsvp::delete($rsvp->id);
                    redirect('/');
                }
            }
        }
        $this->loadView($this->viewData);
    }


}