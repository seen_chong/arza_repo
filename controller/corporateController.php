<?php

class corporateController extends siteController
{


    public function index(Array $params = [])
    {
        $this->viewData->popularServices = \Model\Popular_Service::getList(['where'=>"display_state = 2",'orderBy'=>'id']);
        $this->configs['Meta Title'] = "Wellns Corporate Activities";
        $this->loadView($this->viewData);
    }    

    public function verify(array $params)
    {
        $token = $params['token'];
        $id = $params['id'];
        $corp = \Model\Corporate_Invite::getItem(null, ['where' => "token = '$token' and user_id = '$id'"]);
        $user = \Model\User::getItem(null, ['where' => "email = '$corp->email'"]);

        if (!is_null($user) && $corp->is_verified != 1) {
            redirect("/login/corporate_accept?corp_token=$token");
//            redirect("/login?corp_token=$token");
        } else if (is_null($user)) {
            redirect("/signup?corp_token=$token");
        } else if ($corp->is_verified == 1) {
            redirect('/login');
        }
    }

    public function credit_send()
    {
        $cEmployee = $_POST['cEmployee'];
        $creditAmt = intval($_POST['creditAmt']);

        $corpEmployee = \Model\Corporate_Employee::getItem(null, ['where' => "id = $cEmployee"]);
        $employee = \Model\User::getItem($corpEmployee->employee_id);
        $employee->corporate_credit += $creditAmt;

        $corporate = \Model\User::getItem($corpEmployee->employer_id);
        $corporate->credit -= $creditAmt;

        $corporateCredit = new \Model\Corporate_Credit();
        $corporateCredit->corporate_employee_id = $cEmployee;
        $corporateCredit->credit = $creditAmt;

        $employee->save();
        $corporate->save();
        $corporateCredit->save();

        $email = new Email\MailMaster();
        $mergeFields = [
            'CREDIT' => $creditAmt,
            'COMPANY_NAME' => $corporate->company
        ];
        $email->setTo(['email' => $employee->email, 'name' => $employee->full_name(), 'type' => 'to'])->setMergeTags($mergeFields)->setTemplate('send-credits-to-employee')->send();
        echo '/user/dashboard';
    }
}