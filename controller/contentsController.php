<?php

class contentsController extends siteController {
	
//    public function index(Array $params = [])
//    {
//
//        $this->configs['Meta Title'] = "Category";
//        $this->loadView($this->viewData);
//    }

//    public function subcategory(Array $params = [])
//    {
//
//        $this->configs['Meta Title'] = "Subcategory";
//        $this->loadView($this->viewData);
//    }

	public function content(Array $params = []){
		$slug = $params['content_slug'];
        if(isset($_GET['draft'])&&$_GET['draft']=='true'){
            $this->viewData->content = \Model\Content::getItem(null,['where'=>"display is null and slug = '{$slug}'"]);
        }else {
            $this->viewData->content = \Model\Content::getItem(null,['where'=>"display = 1 and slug = '{$slug}'"]);
        }

//		// /collections/{main category}
//        if(isset($params['category_slug']) && $params['category_slug'] != '' && !isset($params['subcategory'])){
//
//        }
        if($this->viewData->content) {

            if (!is_null($this->viewData->content->meta_keywords) && $this->viewData->content->meta_keywords != '') {
                $this->configs['Meta Keywords'] = $this->viewData->content->meta_keywords;
            }
            if (!is_null($this->viewData->content->meta_description) && $this->viewData->content->meta_description != '') {
                $this->configs['Meta Description'] = $this->viewData->content->meta_description;
            }
            if (!is_null($this->viewData->content->meta_title) && $this->viewData->content->meta_title != '') {
                $this->configs['Meta Title'] = $this->viewData->content->meta_title;
            } else {
                $this->configs['Meta Title'] = $this->viewData->content->name . ' | Arza';
            }
            $this->viewData->sections = \Model\Section::getList(['where' => "display = 1 and content_name = {$this->viewData->content->id}", 'orderBy' => 'display_order ASC']);
            $this->loadView($this->viewData);
        }else{
            redirect('/');
        }
	}

}