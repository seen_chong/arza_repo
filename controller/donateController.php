<?php

class donateController extends siteController
{

    function confirmation(Array $params = []){
        $date = \Carbon\Carbon::now();
        $date = $date->format('F d, Y');
        if(($donation = \Model\Donation::getItem(null,['where'=>"ref_num = '".$params['id']."'"]))){
            $this->viewData->donation = $donation;
            $this->viewData->donatePage = \Model\Donate::getItem($donation->type);
            if(isset($_GET['paymentId']) && $_GET['paymentId']){
                $apiContext = new \PayPal\Rest\ApiContext(new \PayPal\Auth\OAuthTokenCredential(PAYPAL_CLIENT_ID,PAYPAL_CLIENT_SECRET));
                $apiContext->setConfig(['mode'=>'live']);
                $payment = \PayPal\Api\Payment::get($_GET['paymentId'],$apiContext);
                $paymentExecution = new \PayPal\Api\PaymentExecution();
                $paymentExecution->setPayerId($_GET['PayerID']);
                $payment = $payment->execute($paymentExecution,$apiContext);
                if(strtolower($payment->getState()) == 'approved') {
                    $donation->paypal_id = $_GET['paymentId'];
                    $donation->status = \Model\Donation::$status[0];
                    $donation->agreement_id = $_GET['paymentId'];
                    $donation->save();
                    if($donation->type == 0) {
                        global $emagid;
                        $emagid->email->from->email = 'report@emagid.com';
                        $email = new \Emagid\Email();
                        $email->addTo($donation->email);
                        $confirm_email = \Model\Confirm_Email::getItem(null,[['where'=>"name = 'Register Confirmation'"]]);
                        if($confirm_email){
                            $temp = \Model\Confirm_Email::$dynamicElements;
                            $real = [$date, $donation->full_name(), number_format($donation->amount,2)];
                            $emailTemplate = str_replace($temp,$real,$confirm_email->email_template);
                            $subject = str_replace($temp,$real,$confirm_email->subject);
                            $email->subject(trim($subject));
                            $email->body = trim($emailTemplate);

                        }else{
                            $email->subject('Welcome, ' . $donation->full_name() . '!');
                            $email->body = '<p><a href="https://arza.org"><img src="http://beta.arza.org/content/frontend/assets/img/navyLogoText.png" /></a></p>'
                                . '<p><b>Dear ' . $donation->full_name() . '</b></p>'
                                . '<p>Welcome to <a href="arza.org">Arza.org</a></p>'
                                . '<p>If you have any questions about your account or any other matter, please feel free to contact us by phone at 212.650.4289.</p>'
                                . '<p>Thanks again!</p>';
                        }
                        $email->send();
                    }else {
                        global $emagid;
                        $emagid->email->from->email = 'report@emagid.com';
                        $email = new \Emagid\Email();
                        $email->addTo($donation->email);
                        $confirm_email = \Model\Confirm_Email::getItem(null,[['where'=>"name = 'Donation Confirmation'"]]);
                        if($confirm_email){
                            $temp = \Model\Confirm_Email::$dynamicElements;
                            $real = [$date, $donation->full_name(), number_format($donation->amount,2)];
                            $emailTemplate = str_replace($temp,$real,$confirm_email->email_template);
                            $subject = str_replace($temp,$real,$confirm_email->subject);
                            $email->subject(trim($subject));
                            $email->body = trim($emailTemplate);

                        }else{
                            $email->subject('Thank you, ' . $donation->full_name() . '!');
                            $email->body = '<p><a href="https://arza.org"><img src="http://beta.arza.org/content/frontend/assets/img/navyLogoText.png" /></a></p>'
                                .'<p>'.$date .'</p>'
                                .'<p>8 Tishrei 5777</p>'
                                . '<p>Dear ' . $donation->full_name() . ',</p>'
                                .'<p>Thank you so much for your gift of $'.number_format($donation->amount,2).' to  <a href="http://arza.org">Arza.</a> With your help, we are fostering a vibrant Reform Zionism in the US and supporting the growing Reform Movement and its congregations in Israel.  
                                Together we will continue to build the Israel that is the Homeland to all Jews, representing equality, pluralism and religious freedom.</p>'
                                .'<p>It’s 5777: With your generosity, we are Taking Back the Z.  
                                We are proud Zionists: our love of Israel compels us to advocate for the betterment of Israeli society.  
                                May this be the year where there is quality under the law for all in Israel.  
                                May this be the year we make significant strides toward peace for Israel and her neighbors.</p>'
                                .'<p>Shana Tova.  We wish you and your family a Happy and Healthy New Year.</p>'
                                .'<p>Todah Rabah.  Thank you very much.</p><br>'
                                .'<p>Rabbi John Rosove, Chair</p>'
                                .'<p>Rabbi Josh Weinberg, President</p>'
                                .'<p><em>Your generous contribution to ARZA is tax-deductible as allowed by law.</em></p>';
                        }
                        $email->send();
                    }
                } else {
                    $n = new \Notification\ErrorHandler($payment->getFailureReason());
                    $_SESSION['notification'] = serialize($n);
                    if($donation->type==0){
                        \Model\User::delete($donation->user_id);
                        redirect('/join');
                    }else{
                        redirect(SITE_URL . 'donation/' .\Model\Donate::getItem($donation->type)->slug);
                    }
                }
            }elseif(isset($_GET['success']) && $_GET['success'] == 'true'){
                $apiContext = new \PayPal\Rest\ApiContext(new \PayPal\Auth\OAuthTokenCredential(PAYPAL_CLIENT_ID,PAYPAL_CLIENT_SECRET));
                $apiContext->setConfig(['mode'=>'live']);
                $token = $_GET['token'];
                $myAgreement = new \PayPal\Api\Agreement();
                $myAgreement->execute($token, $apiContext);
                $agreement = \PayPal\Api\Agreement::get($myAgreement->getId(), $apiContext);
                if(strtolower($agreement->getState()) == 'active') {
                    $donation->status = \Model\Donation::$status[0];
                    $donation->monthly = 1;
                    $donation->agreement_id = $myAgreement->getId();

                    $donation->save();
                    global $emagid;
                    $emagid->email->from->email = 'report@emagid.com';
                    $email = new \Emagid\Email();
                    $email->addTo($donation->email);
                    $confirm_email = \Model\Confirm_Email::getItem(null,[['where'=>"name = 'Monthly Donation Confirmation'"]]);
                    if($confirm_email){
                        $temp = \Model\Confirm_Email::$dynamicElements;
                        $real = [$date, $donation->full_name(), number_format($donation->amount,2)];
                        $emailTemplate = str_replace($temp,$real,$confirm_email->email_template);
                        $subject = str_replace($temp,$real,$confirm_email->subject);
                        $email->subject(trim($subject));
                        $email->body = trim($emailTemplate);

                    }else{
                    $email->subject('Thank you, ' . $donation->full_name() . '!');
                    $email->body = '<p><a href="https://arza.org"><img src="http://beta.arza.org/content/frontend/assets/img/navyLogoText.png" /></a></p>'
                        .'<p>'.$date .'</p>'
                        .'<p>8 Tishrei 5777</p>'
                        . '<p>Dear ' . $donation->full_name() . ',</p>'
                        .'<p>Thank you so much for your gift of $'.number_format($donation->amount,2).' to  <a href="http://arza.org">Arza.</a> With your help, we are fostering a vibrant Reform Zionism in the US and supporting the growing Reform Movement and its congregations in Israel.  
                                Together we will continue to build the Israel that is the Homeland to all Jews, representing equality, pluralism and religious freedom.</p>'
                        .'<p>It’s 5777: With your generosity, we are Taking Back the Z.  
                                We are proud Zionists: our love of Israel compels us to advocate for the betterment of Israeli society.  
                                May this be the year where there is quality under the law for all in Israel.  
                                May this be the year we make significant strides toward peace for Israel and her neighbors.</p>'
                        .'<p>Shana Tova.  We wish you and your family a Happy and Healthy New Year.</p>'
                        .'<p>Todah Rabah.  Thank you very much.</p><br>'
                        .'<p>Rabbi John Rosove, Chair</p>'
                        .'<p>Rabbi Josh Weinberg, President</p>'
                        .'<p><em>Your generous contribution to ARZA is tax-deductible as allowed by law.</em></p>';
                    }
                    $email->send();

                }else{
                    $n = new \Notification\ErrorHandler('Failed to donate');
                    $_SESSION["notification"] = serialize($n);
                    redirect(SITE_URL . 'donation/' .\Model\Donate::getItem($donation->type)->slug);
                }
            }
        }

        $this->loadView($this->viewData);
    }

    public function crisis(Array $params = [])
    {
        $this->loadView($this->viewData);
    }
    public function crisis_post(){
        $donate = \Model\Donation::loadFromPost();
        if($this->viewData->user){
            $donate->user_id = $this->viewData->user->id;
        }
        $donate->type = 3;
        $donate->status = \Model\Donation::$status[3];
        $donate->ref_num = generateToken();

        if($donate->save()){

            if($donate->monthly == 1){

                $myPlan = new \PayPal\Api\Plan();
                $value = $donate->amount;
                $myPlan->setName('Donate to Kehilat')->setDescription('Donate to Kehilat monthly.')->setType('fixed');
                $paymentDefinition = new \PayPal\Api\PaymentDefinition();
                $paymentDefinition->setName('Monthly Donation')->setType('REGULAR')->setFrequency('Month')->setFrequencyInterval("1")->setCycles("12")->setAmount(new \PayPal\Api\Currency(array('value' => $value, 'currency' => 'USD')));
                $merchantPreferences = new \PayPal\Api\MerchantPreferences();
                $apiContext = new \PayPal\Rest\ApiContext(new \PayPal\Auth\OAuthTokenCredential(PAYPAL_CLIENT_ID,PAYPAL_CLIENT_SECRET));

                $apiContext->setConfig(array('mode' => 'live'));

//                $merchantPreferences->setReturnUrl("http://arzafront.app/donate/confirmation/$donate->ref_num?success=true")
//                    ->setCancelUrl("http://arzafront.app/donate/confirmation/$donate->ref_num?success=false")
//                    ->setAutoBillAmount("yes")
//                    ->setInitialFailAmountAction("CONTINUE")
//                    ->setMaxFailAttempts("0")
//                    ->setSetupFee(new \PayPal\Api\Currency(array('value' => 1, 'currency' => 'USD')));
                //TODO: Change the Url to live site
                $merchantPreferences->setReturnUrl("http://arza.org/donate/confirmation/$donate->ref_num?success=true")
                    ->setCancelUrl("http://arza.org/donate/confirmation/$donate->ref_num?success=false")
                    ->setAutoBillAmount("yes")
                    ->setInitialFailAmountAction("CONTINUE")
                    ->setMaxFailAttempts("0")
                    ->setSetupFee(new \PayPal\Api\Currency(array('value' => 1, 'currency' => 'USD')));

                $myPlan->setPaymentDefinitions(array($paymentDefinition));
                $myPlan->setMerchantPreferences($merchantPreferences)->create($apiContext);
                if (strtolower($myPlan->getState()) == 'created') {
                    $patch = new \PayPal\Api\Patch();
                    $value = new \PayPal\Common\PayPalModel('{
                    "state":"ACTIVE"
                    }');
                    $patch->setOp('replace')->setPath('/')->setValue($value);
                    $patchRequest = new \PayPal\Api\PatchRequest();
                    $patchRequest->addPatch($patch);
                    $myPlan->update($patchRequest, $apiContext);
                    $myPlan = \PayPal\Api\Plan::get($myPlan->getId(), $apiContext);
                    if (strtolower($myPlan->getState()) == 'active') {
                        $agreement = new \PayPal\Api\Agreement();
                        $tomorrow = strtotime('+1 day');
                        $date = date("Y-m-d", $tomorrow).'T'. date("H:i:s", $tomorrow).'Z';
                        $agreement->setName('Monthly Donate Agreement')->setDescription('Donate to Kehilat every month')->setStartDate($date);
                        $plan = new \PayPal\Api\Plan();
                        $plan->setId($myPlan->getId());
                        $agreement->setPlan($plan);
                        $payer = new \PayPal\Api\Payer();
                        $payer->setPaymentMethod('paypal');
                        $agreement->setPayer($payer)->create($apiContext);

                        if($agreement->getApprovalLink()!=''){
                            redirect($agreement->getApprovalLink());
                        }else{
                            $n = new \Notification\ErrorHandler('Failed to donate');
                            $_SESSION["notification"] = serialize($n);
                            redirect('/donate/crisis');
                        }

                    }else{
                        $n = new \Notification\ErrorHandler('Failed to donate');
                        $_SESSION["notification"] = serialize($n);
                        redirect('/donate/crisis');
                    }

                } else {
                    $n = new \Notification\ErrorHandler('Failed to donate');
                    $_SESSION["notification"] = serialize($n);
                    redirect('/donate/crisis');
                }

            }else {


                $PItems = new \PayPal\Api\ItemList();
                $PItem = new \PayPal\Api\Item();
                $PItem->setName('Donate to Kehilat')->setPrice($donate->amount)->setQuantity(1)->setCurrency('USD');
                $PItems->setItems(array($PItem));
                $apiContext = new \PayPal\Rest\ApiContext(new \PayPal\Auth\OAuthTokenCredential(PAYPAL_CLIENT_ID, PAYPAL_CLIENT_SECRET));

                $apiContext->setConfig(array('mode' => 'live'));
                $payer = new \PayPal\Api\Payer();
                $payer->setPaymentMethod('paypal');
                $PAmount = new \PayPal\Api\Amount();
                $PAmount->setTotal($donate->amount)->setCurrency('USD');
                $PTransaction = new \PayPal\Api\Transaction();
                $PTransaction->setAmount($PAmount)->setDescription("Donations");
                $PTransArr[] = $PTransaction;

                $pPRedirectUrl = new \PayPal\Api\RedirectUrls();
//                $pPRedirectUrl->setReturnUrl("http://arzafront.app/donate/confirmation/$donate->ref_num")->setCancelUrl('http://arzafront.app/donate');//TODO:TODO: Change the Url to live site
                $pPRedirectUrl->setReturnUrl("http://arza.org/donate/confirmation/$donate->ref_num")->setCancelUrl('http://arza.org/donate');

                $payment = new \PayPal\Api\Payment();

                $payment = $payment->setIntent('sale')->setPayer($payer)->setTransactions($PTransArr)->setRedirectUrls($pPRedirectUrl)->create($apiContext);

                if (strtolower($payment->getState()) == 'created') {
                    $redirect = $payment->getLink('approval_url');
                    redirect($redirect);
                } else {
                    $donate->status = \Model\Donation::$status[2];
                    $donate->save();
                    $n = new \Notification\ErrorHandler($payment->getFailureReason());
                    $_SESSION['notification'] = serialize($n);
                    redirect(SITE_URL . 'donate/crisis');
                }

            }
        }else {
            $n = new \Notification\ErrorHandler('Failed to donate');
            $_SESSION["notification"] = serialize($n);
            redirect('/donate/crisis');
        }
    }

}