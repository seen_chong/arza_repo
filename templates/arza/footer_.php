<footer class="primaryFooter">

	<div class="contentWidthExtreme">

		<div class="pull_left">
			<p>ARZA</p>
			<p>633 Third Avenue, New York, NY 10017</p>
			<p class="sub_links_contained"><span>Phone</span> <a href="tel:2126504280">212.650.4280</a></p>
		</div>

		<div class="abs_trans_center">
			<a class="footerLogo">
				<img src="<?=FRONT_IMG?>navyStackedLogo.jpg">
			</a>
		</div>

		<div class="pull_right">
			<div class="legal">
				<p>&#169; ARZA, 2016. All Rights Reserved</p>
				<?php foreach (\Model\Page::getList() as $cs) { ?>
					<a href="<?= SITE_URL ?>page/<?=$cs->slug?>"><?=$cs->title?></a>
				<?}?>
<!--				<a>Privacy Policy</a>-->
<!--				<a>Reform Movement Affairs</a>-->
			</div>
			<div class="social socialIconsStacked">
				<div>
					<a href="https://www.facebook.com/ARZAUS" target="_blank" class="facebookIcon">
						<icon class="icon phone_icon" style="background-image:url('<?=FRONT_IMG?>facebookLogo.png')"></icon>
					</a>

<!-- 					<a class="instaIcon">
						<icon class="icon phone_icon" style="background-image:url('<?=FRONT_IMG?>instagramLogo.png')"></icon>
					</a> -->
					<a href="https://twitter.com/arzaus" target="_blank" class="twitterIcon">

						<icon class="icon phone_icon" style="background-image:url('<?=FRONT_IMG?>twitterLogo.png')"></icon>
					</a>
					<a href="https://www.youtube.com/user/ARZAUSA" target="_blank" class="youtubeIcon">
						<icon class="icon phone_icon" style="background-image:url('<?=FRONT_IMG?>youtubeLogo.png')"></icon>
					</a>
				</div>
			</div>
		</div>

	</div>

</footer>

<script type="text/javascript">
$(function(){
		$('#mob-menu').on('click', function(e) {
				e.preventDefault();
				$('ul').slideToggle();
			});

	});
</script>

<script type="text/javascript">
$(document).ready(function(){
 $('a.has-mobile-dropdown').click(function(e) {
 	e.preventDefault();
 	$('.mobile-dropdown').toggle();
 	});
 });

</script>