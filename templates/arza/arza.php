<!DOCTYPE html>

<html>

	<head>

	    <meta charset="utf-8">

	    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

	    <meta name="apple-mobile-web-app-capable" content="yes"/>

	    <title><?= $this->configs['Meta Title']; ?></title>

	    <meta name="Description" content="<?= $this->configs['Meta Description']; ?>">

	    <meta name="Keywords" content="<?= $this->configs['Meta Keywords']; ?>">

	    <? require_once('includes.php'); ?>

	</head>


	<body class="<?=$this->emagid->route['controller']?> <?=$this->emagid->route['action']?>">

		<div class="body_template_notification_wrapper" style="position: absolute;"><? display_notification(); ?></div>
		<div class="error-notification body_template_notification_wrapper" style="display: none;"></div>


		<div class="mainPageContainer">
			<? require_once('header.php'); ?>
			<?php $emagid->controller->renderBody($model); ?>
			<? require_once('footer.php'); ?>
		</div>

	</body>

</html>

