<header class="primaryHeader">

	<div class="contentWidthPrimary">

		<div class="pull_left">
			<a href="<?=SITE_URL?>" class="headerLogoWrapper">
				<img src="<?=FRONT_IMG?>navyLogoText.png">
			</a>
		</div>
		<div class="pull_right">
			<?$contents = \Model\Content::getList(['where'=>'display=1']);
			$work=[];
			$mission=[];
			$mission=[];
			$nav=[];

			foreach ($contents as $obj){
				if(strcmp($obj->parent_section,'work')==0){
					$work[]=$obj;
				}elseif(strcmp($obj->parent_section,'mission')==0){
					$mission[]=$obj;
				}elseif(strcmp($obj->parent_section,'join')==0){
					$join[]=$obj;
				}elseif (strcmp($obj->parent_section,'nav')==0){
					$nav[]=$obj;
				}
			} ?>
			<ul class="header_tab_list">

				<li class="<?=empty($work)?"":"has-dropdown"?>">
					<a href="/work" class="header_tab header_tab_gray">
						<h4 class="proxSc">What we do <br>מה אנחנו עושים
						</h4>
					</a>
					<div class="dropdown-content">
						<?foreach ($work as $obj){?>
							<a href="<?=SITE_URL.'contents'.DS.$obj->slug?>"><?=$obj->name?></a>
						<?}?>
					</div>
<!-- 					  <div class="dropdown-content">
					    <a href="/work">The Reform Movement </a>
					    <a href="/work#middle">Affinity Group</a>
					    <a href="/work#middle">Israel in the Parasha</a>
					    <a href="/work#last">Rabbinic Council</a>

					    <a class="joinBtn" href="/join"><button>Join Us</button></a>
					  </div> -->
				</li>
				<li class="<?=empty($mission)?"":"has-dropdown"?>">
					<a href="/mission" class="header_tab header_tab_gray">
						<h4 class="proxSc">Who we Are<br>מי אנחנו </h4>
					</a>
					<div class="dropdown-content">
						<?foreach ($mission as $obj){?>
							<a href="<?=SITE_URL.'contents'.DS.$obj->slug?>"><?=$obj->name?></a>
						<?}?>
					</div>

				</li>
				<?foreach ($nav as $obj){
					$nests = \Model\Content::getList(['where'=>"active=1 AND parent_section ='".$obj->id."'"]);;?>
				<li class="<?=empty($nests)?"":"has-dropdown"?>">
					<a href="<?=SITE_URL.'contents'.DS.$obj->slug?>">
						<h4 class="proxSc"><?=$obj->name?></h4>
					</a>
					<div class="dropdown-content">
						<?foreach ($nests as $nest){?>
							<a href="<?=SITE_URL.'contents'.DS.$nest->slug?>"><?=$nest->name?></a>
						<?}?>
					</div>
				</li>
				<?}?>

				<li class="<?=empty($join)?"":"has-dropdown"?>">
					<a href="/join" class="header_tab header_tab_gray">
						<h4 class="proxSc">Join the Community<br>
						הצטרפו לקהילה
						</h4>
					</a>
					<div class="dropdown-content">
						<?foreach ($join as $obj){?>
							<a href="<?=SITE_URL.'contents'.DS.$obj->slug?>"><?=$obj->name?></a>
						<?}?>
					</div>
				</li>
				<li>
					<a href="/blog" class="header_tab header_tab_gray">
						<h4 class="proxSc">Blog<br>  בלוג</h4>
					</a>
				</li>
				<li>
					<a href="/" class="header_tab header_tab_navy">
						<icon class="icon phone_icon" style="background-image:url('<?=FRONT_IMG?>house-outline.png')"></icon>
					</a>
				</li>
				<li>
					<a href="/contact" class="header_tab header_tab_navy">
						<icon class="icon phone_icon" style="background-image:url('<?=FRONT_IMG?>headerPhoneIcon.png')"></icon>
					</a>
				</li>
				<li>
					<a href="/donate" class="header_tab header_tab_green header_tab_primary">
						<h4 class="futura">Donate</h4>
					</a>
				</li>
					<?if($model->user == 0){?>
				<li>
					<a href="/login" class="header_tab header_tab_green header_tab_primary">
						<h4 class="futura">Sign In</h4>
					</a>
						<div class="dropdown-content">
								<form class="form" method="post" action="/login/signin" enctype="multipart/form-data">
						    <p>
						    	Email Address <br>
						    	<input type="email" name="email" required/>
						    </p>
						    <p>
						    	Password<br>
						    	<input type="password" name="password" required/>
								<input hidden name="uri" value="<?=$emagid->uri?>">
						    </p>

						    <p>
                            	<input type="submit" id="signin" value="Sign In">
                       		</p>
							</form>
					  </div>
					</li>
					<?}else{?>
						<li class="has-dropdown">
					<a class="header_tab header_tab_green header_tab_primary">
						<h4 class="futura">Welcome</h4>
						</a>
					<div class="dropdown-content">
						<a id="signout">Log Out</a>
						</div>
							</li>
					<?}?>

			</ul>
		</div>

	</div>
<div id="mob-menu">Menu</div>
<div id="nav">
	<div>
    <ul>
    	<li>
        	<a href="/home" class="header_tab header_tab_gray">
				<h4 class="proxSc">Home</h4>
			</a>
		</li>
        <li class="<?=empty($work)?"":"has-mobile-dropdown"?>">
        	<a href="/work" class="header_tab header_tab_gray">
				<h4 class="proxSc">What We Do</h4>
			</a>
			<div class="mobile-dropdown" style="display:none;">
				<?foreach ($work as $obj){?>
					<a href="<?=SITE_URL.'contents'.DS.$obj->slug?>"><?=$obj->name?></a>
				<?}?>
			</div>
		</li>
		<li class="<?=empty($mission)?"":"has-mobile-dropdown"?>">
			<a href="/mission" class="header_tab header_tab_gray">
				<h4 class="proxSc">Who we Are</h4>
			</a>
			<div class="mobile-dropdown" style="display:none;">
			   		<?foreach ($mission as $obj){?>
						<a href="<?=SITE_URL.'contents'.DS.$obj->slug?>"><?=$obj->name?></a>
					<?}?>
			  </div>
		</li>
		<li class="<?=empty($join)?"":"has-mobile-dropdown"?>">
			<a href="/join" class="header_tab header_tab_gray">
				<h4 class="proxSc">Join the Community</h4>
			</a>
			<div class="mobile-dropdown" style="display:none;">
			    	<?foreach ($join as $obj){?>
						<a href="<?=SITE_URL.'contents'.DS.$obj->slug?>"><?=$obj->name?></a>
					<?}?>
			  </div>
		</li>
		<li>
			<a href="/blog" class="header_tab header_tab_gray">
				<h4 class="proxSc">Blog</h4>
			</a>
		</li>
		<li>
			<a href="tel:212-650-4280" class="header_tab header_tab_navy"><h4 class="proxSc">Call Us 212-650-4280</h4>
			</a>
		</li>
		<li>
			<a href="/donate" class="header_tab header_tab_green header_tab_primary">
				<h4 class="proxSc" style="font-size:16px;color:#152231;">Donate</h4>
			</a>
		</li>
    </ul>
    </div>
</div>



</header>
<script>
	$(document).ready(function(){
		$('#signout').on('click',function(){
			$.post('/login/logout','',function(){
				window.location.replace('/');
			})
		});
//		$('#signin').on('click',function(e){
//			e.preventDefault();
//			var email = $('input[name=email]').val();
//			var password = $('input[name=password]').val();
//			var uri = $('input[name=uri]').val();
//			if(email.length>0 && email.indexOf('@')>0&&password.length>0){
//				$("form#sign").submit();
//			}
//
//		});

	})
</script>

<script>
	$(document).ready(function(){
		$('li.has-mobile-dropdown').on('click',function(e){
			e.preventDefault();
			$('.mobile-dropdown').hide();
			$('.mobile-dropdown', this).toggle();
			$(this).unbind('click').click()
		});
	});
</script>