<?php

namespace Model;



class Event extends \Emagid\Core\Model {
    static $tablename = 'event';
    public static $fields = [
        'name',
        'start_time',
        'end_time',
        'description',
        'address',
        'address2',
        'city',
        'state',
        'country',
        'capacity',
        'price',
        'contact_name',
        'contact_number',
        'featured_image',
        'zip'
    ];
    public function getRsvpNumber(){
        return count(Rsvp::getList(['where'=>'event_id = '.$this->id . "and (status ='".\Model\Rsvp::$status[0] ."' or status = '" . \Model\Rsvp::$status[1] ."')"]));
    }
    public static function getLiveEvents(){
        $allEvents = self::getList(['orderBy'=>'start_time asc']);
        foreach ($allEvents as $event){
            if(time()<$event->end_time){
                $events[]= $event;
            }
        }
        return $events;
    }
    function getAddr(){
        return $this->address .' '. $this->address2 .' '. $this->city .' '. $this->state .', '.$this->country .', '. $this->zip;
    }

}





















































































