<?php



namespace Model;



class Mission extends \Emagid\Core\Model {



    static $tablename = 'mission';



    public static $fields =  [

        'section_name',

        'title',

        'subtitle',

        'description',

        'sub_description',

        'featured_image',

        'video_link',

        'link',

        'button'

    ];



}