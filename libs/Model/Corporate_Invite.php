<?php
namespace Model;
class Corporate_Invite extends \Emagid\Core\Model {

    static $tablename = 'public.corporate_invite';

    static $fields = [
        'user_id',
        'email',
        'token',
        'is_verified',
    ];

    public function verifiedStatus(){
        if($this->is_verified == 0){
            return 'Pending';
        } else if($this->is_verified == 1) {
            return 'Verified';
        } else {
            return 'Declined';
        }
    }
}