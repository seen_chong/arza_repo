<?php
namespace Model;
class Latest extends \Emagid\Core\Model {
    static $tablename = "latest";
    public static $fields  =  [
        'time',
        'name',
    	'description',
    	'display_order',
		'url'
    ];

	public function getUrl(){
		return $this->url != "" && !is_null($this->url)? $this->url: '#';
	}
    public function buildSortable(){
        $html = '<li data-name="'.$this->name.'" data-id="'.$this->id.'">'.$this->name.'</li>';
//        $html .= '</ol></li>';
        return $html;
    }
}

















































