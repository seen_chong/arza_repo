<?php
namespace Model;
use Carbon\Carbon;
use Email\MailMaster;
use s3Bucket\s3Handler;

class Provider extends \Emagid\Core\Model {
    static $tablename = "public.provider";
    public static $fields  =  [
        'authorized'=>['type'=>'boolean'],
        'email'=>['required'=>true,'type'=>'email','unique'=>true],
        'password'=>['required'=>true],
        'hash',
        'first_name'=>['required'=>true,'name'=>'First Name'],
        'last_name'=>['required'=>true,'name'=>'Last Name'],
        'zip',
        'phone', 
        'key',
        'step_2_completed'=>['type'=>'boolean'],
        'dob',
        'experience',
        'description',
        'teaching_description',
        'promise_statement',
        'certificates',
        'lifestyle',
        'testimonial_1',
        'testimonial_1_reference',
        'testimonial_2',
        'testimonial_2_reference',
        'testimonial_3',
        'testimonial_3_reference',
        'photo',
        'license_photo',
        'extra_photo_1',
        'extra_photo_2',
        'agreed_manifesto_ethics',
        'agreed_contractor_agreement',
        'agreed_terms',
        'agreed_privacy',
        'title',
        'description_1',
        'description_2',
        'description_3' ,
        'ssn',
        'back_check',
        'candidate_id',
        'report_id',
        'sign',
        'oklahoma_check',   
        'minnesota_check',      
        'disclosure_check',
        'remind_password',
		'bank_name',
		'bank_address',
		'bank_city',
		'routing_number',
		'photo_position',
		'extra_photo_position',
		'extra_photo_position_2',
        'e_signature',
        'ref_key',
        'match_photo_position',
        'match_extra_photo_position',
        'match_extra_photo_position_2',
        'account_number',
        'last_login',
        'avg_response_time',
        'equipment',
        'expect',
        'payout_rate'
    ];

    public function updateSteps(){
        $fields = [
            'email',
            'password',
            'first_name',
            'last_name',
            'zip',
            'phone', 
            'dob',
            'experience',
            'description',
            'teaching_description',
            'promise_statement',
            'certificates',
            'lifestyle',
            'testimonial_1',
            'testimonial_1_reference',
            'testimonial_2',
            'testimonial_2_reference',
            'testimonial_3',
            'testimonial_3_reference',
            'photo',
            'license_photo',
            'extra_photo_1',
            'extra_photo_2',
            'agreed_manifesto',
            'agreed_ethics',
            'agreed_terms',
            'agreed_privacy',
            'title',
            'description_1',
            'description_2',
            'description_3',
            'cellphone'
        ];
        $valid = true;
        foreach ($fields as $field){
            if (is_null($this->$field) || trim($this->$field) == ""){
                $valid = false;
            }
        }
        $packages = \Model\Package::getList(['where'=>'provider_id = '.$this->id]);
        if ($valid && count($packages > 0)){
            $this->step_2_completed = true;
            $this->save();
        }
    }

    /**
     * @var array
     * Don't delete it, otherwise we can't update provider services
     */
    static $relationships = [
      [
        'name'=>'provider_service',
        'class_name' => '\Model\Provider_Services',
        'local'=>'id',
        'remote'=>'provider_id',
        'remote_related'=>'service_id',
        'relationship_type' => 'many'
      ]
    ];
    /**
    * concatenates first name and last name to create full name string and returns it
    * @return type: string of full name
    */
    function full_name() {
        return $this->first_name.' '.$this->last_name[0];
    }
      public static function search($keywords, $limit = 20){
        $sql = "select id,first_name, last_name, email from provider where active = 1 and (";
        if (is_numeric($keywords)){
            $sql .= "id = ".$keywords." or ";
        }
        $sql .= " lower(email) like '%".strtolower(urldecode($keywords))."%' or lower(first_name) like '%".strtolower(urldecode($keywords))."%' or lower(last_name) like '%".strtolower(urldecode($keywords))."%') limit ".$limit;
        return self::getList(['sql' => $sql]);
    }
    /**
    * Verify login and create the authentication cookie / session 
    * Accepts no parameter to login the instantiated object or 2 parameters (login and password) 
    * to get an object from the database and log it in
    */
    public function login(){
        if (count(func_get_args()) == 0){
            $roles = \Model\Provider_Roles::getList(['where' => 'active = 1 and provider_id = '.$this->id]);
            if (!is_null($roles)){
                $rolesIds = [];
                foreach($roles as $role){
                    $rolesIds[] = $role->role_id;
                }
                $rolesIds = implode(',', $rolesIds);
                $roles = \Model\Role::getList(['where' => 'active = 1 and id in ('.$rolesIds.')']);
                $rolesNames = [];
                foreach($roles as $role){
                    $rolesNames[] = $role->name;
                }
                \Emagid\Core\Membership::setAuthenticationSession($this->id, $rolesNames, $this);
                return true;
            } else {
                return false;
            }
        } else if (count(func_get_args()) == 2){
            $email = func_get_arg(0);
            $password = func_get_arg(1);
            $user = self::getItem(null, ['where'=>"LOWER(email) = LOWER('".$email."')"]);
            if (!is_null($user)){
                $hash = \Emagid\Core\Membership::hash($password, $user->hash);            
                if ($hash['password'] == $user->password) {
                    $user->login();
                    $user->last_login = date('Y-m-d h:i:s');
                    $user->save();
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getPrimaryService()
    {
        $ser = [];
        if(!($services = Provider_Services::getList(['sql'=>"select DISTINCT ON (service_id) * from provider_services where provider_id = $this->id and active = 1"]))){
//        if(!($services = Provider_Services::getList(['where'=>"provider_id = $this->id"]))){
            return $ser;
        }
        foreach($services as $service){
            if(($serv = Service::getItem($service->service_id)->sub_service) == 0){
                $ser[] = Service::getItem($service->service_id);
            }
        }

        return $ser;
    }

    public function getSubService(){
        $list = [];
        foreach(Provider_Services::getList(['sql'=>"select DISTINCT ON (service_id) * from provider_services where provider_id = $this->id and active = 1"]) as $obj){
            $service = Service::getItem($obj->service_id);
            if($service && $service->sub_service != 0){
                $list[] = $service;
            }
        }
        return $list;
    }

    public function getServicesObjectList()
    {
        $ser = [];
        if(!($services = Provider_Services::getList(['sql' => "SELECT DISTINCT ON (service_id) * from provider_services where provider_id = $this->id and active = 1"]))){
            return $ser;
        }

        foreach($services as $service){
            $ser[] = Service::getItem($service->service_id);
        }

        return $ser;
    }

    public function getServicesNameList()
    {
        $ser = [];
        if(!($services = Provider_Services::getList(['where' => ['provider_id' => $this->id]]))){
            return $ser;
        }

        foreach($services as $service){
            $ser[] = Service::getItem($service->service_id)->name;
        }

        return $ser;
    }

    public function getAllRequestedSessions()
    {
        $providerId = $this->id;
        $today = date('Y-m-d', time());
        return SessionHistory::getList(['where' => "active='1' and provider_id = " . $providerId. " and date>='$today' and status = 0 ", 'orderBy' => 'date asc']);
    }

    public function getUpcomingSessions()
    {
        $providerId = $this->id;
        $today = date('Y-m-d', time());
        return SessionHistory::getList(['where' => "active='1' and provider_id = $providerId and date>='$today' and status = 1", 'orderBy' => 'date asc']);
    }


    public function getWaitingConfirmSessions()
    {
        $providerId = $this->id;
        $today = date('Y-m-d', time());
        return SessionHistory::getList(['where' => "active='1' and provider_id = $providerId and date < '$today' and (status = 1 or status = 3)", 'orderBy' => 'date asc']);
    }

    public function getHistorySessions()
    {
        $providerId = $this->id;
        $today = date('Y-m-d', time());
        return SessionHistory::getList(['where' => "active='1' and provider_id = $providerId and date < '$today' and status in (4,8,9)", 'orderBy' => 'date asc']);
    }

    public function getTimes($date)
    {
        $date = new Carbon($date);
        //get calendar time
        $lists = Calendar::getList(['where' => ['date_of_session' => $date->toDateString(), ['provider_id' => $this->id]]]);
        
    }

    public function sendCancelSessionEmail($session)
    {
        $provider = $session->provider();
        $service = $session->service();
        $customer = $session->user();
        $parent_service = $service->getParentService();
        $email = new \Email\MailMaster();
        $mergeFields = [
            'PROVIDER_NAME' => $provider->full_name(),
            'SERVICE_NAME' => $service->name,
            'SERVICE_SLUG' => $parent_service->slug,
            'SESSION_TIME' => $session->getTimeFormat(),
            'SESSION_DATE' => $session->getDateFormat(),
            'CUSTOMER_NAME' => $customer->full_name(),
        ];

        $email->setTo(['email' => $customer->email, 'name' => $customer->full_name(),  'type' => 'to'])->setTemplate('book-session-cancel-provider')->setMergeTags($mergeFields)->send();

        \Email\Message::send($customer->phone, \Email\Message::getMessageContent('session-cancelled-to-user', $mergeFields));
    }

    public function sendAcceptSessionEmail($session)
    {
        $provider = $session->provider();
        $service = $session->service();
        $parent_service = $service->getParentService();
        $customer = $session->user();
        $email = new \Email\MailMaster();
        $mergeFields = [
            'PROVIDER_NAME' => $provider->full_name(),
            'SERVICE_NAME' => $service->name,
            'SESSION_TIME' => $session->getTimeFormat(),
            'SESSION_DATE' => $session->getDateFormat(),
            'SERVICE_SLUG' => $parent_service->slug,
            'CUSTOMER_NAME' => $customer->full_name(),
        ];

        $email->setTo(['email' => $customer->email, 'name' => $customer->full_name(),  'type' => 'to'])->setTemplate('book-session-accept-provider')->setMergeTags($mergeFields)->send();

        \Email\Message::send($customer->phone, \Email\Message::getMessageContent('session-confirmed-to-user', $mergeFields));
    }

    public function sendIncompleteSessionEmail($session)
    {
        $provider = $session->provider();
        $service = $session->service();
        $customer = $session->user();
        $parent_service = $service->getParentService();
        $email = new \Email\MailMaster();
        $mergeFields = [
            'PROVIDER_NAME' => $provider->full_name(),
            'SERVICE_NAME' => $service->name,
            'SERVICE_SLUG' => $parent_service->slug,
            'SESSION_TIME' => $session->getTimeFormat(),
            'SESSION_DATE' => $session->getDateFormat(),
            'CUSTOMER_NAME' => $customer->full_name(),
        ];

        $email->setTo(['email' => $customer->email, 'name' => $customer->full_name(),  'type' => 'to'])->setTemplate('book-session-incomplete-provider')->setMergeTags($mergeFields)->send();
    }

    public function sendConfirmSessionEmail($session)
    {
        $provider = $session->provider();
        $service = $session->service();
        $parent_service = $service->getParentService();
        $customer = $session->user();
        $email = new \Email\MailMaster();
        $mergeFields = [
            'PROVIDER_NAME' => $provider->full_name(),
            'SERVICE_NAME' => $service->name,
            'SERVICE_SLUG' => $parent_service->slug,
            'SESSION_TIME' => $session->getTimeFormat(),
            'SESSION_DATE' => $session->getDateFormat(),
            'CUSTOMER_NAME' => $customer->full_name(),
        ];

        $email->setTo(['email' => $customer->email, 'name' => $customer->full_name(),  'type' => 'to'])->setTemplate('book-session-confirm-provider')->setMergeTags($mergeFields)->send();
    }

    public function getPhoto()
    {
        return S3_URL."{$this->photo}";
    }

    public function getDashboard()
    {
        return '/provider/dashboard';
    }

    public function getRelatedProvidersByService($limit = 4){
        $provService = Provider_Services::getList(['where'=>"active = 1 and provider_id = {$this->id}"]);
        $list = [];
        foreach($provService as $provider){
            $list[] = $provider->service_id;
        }
        $list = implode(',',$list);
        $sql = "select distinct on(provider_id) * from provider_services where service_id in ({$list}) and active = 1 and provider_id in (select id from provider where authorized = 'TRUE' and active = 1 and id != $this->id);";
        $provider = Provider_Services::getList(['sql'=>"$sql"]);
        shuffle($provider);
        if($limit != 0){
            return array_slice($provider,0,$limit);
        } else {
            return $provider;
        }
    }

    public function getRole()
    {
        return 3;
    }

    public function getProfileImage()
    {
        $s3 = new s3Handler();
        if($this->photo){
            return $s3->getUrlByKey($this->photo);
        } else {
            return null;
        }
    }

    /**
     * @param FILE $file it has to be $_FILE[index]
     * @return bool
     */
    public function uploadPicture($file)
    {
        $s3 = new s3Handler();
        return $s3->upload($file);
    }


    /**
     * get provider all completed session
     */
    /*public function getPaymentHistory()
    {
        $sessions = $this->getHistorySessions();
        $formatArray = [];
        $thisMonth = Carbon::now()->startOfMonth();
        foreach($sessions as $session){
            $date = new Carbon($session->date);
            $key = $date->month.'_'.$date->year;
            if($date->timestamp < $thisMonth->timestamp){
                // past
                $formatArray['past'][$key][] = $session;
            } else {
                $formatArray['current'][] = $session;
            }
        }

        return $formatArray;
    }*/

    /**
     * Return provider details for API request
     */
    public function getApiDetails()
    {
        $prov_service = Provider_Services::getList(['where'=>"provider_id = $this->id"]);
        $arr = [];

        foreach($prov_service as $provider_services) {
            if ($service = Service::getItem($provider_services->service_id)) {
                $arr[] = $service->ide;
            }
        }

        return [
            'firstName' => $this->first_name,
            'lastName' => $this->last_name,
            'authorized' => $this->authorized,
            'services' => $arr
        ];
    }

    public function getPaymentHistory(){
        $sessions = SessionHistory::getList(['where'=>"provider_id = {$this->id} AND status in (4,8,9)"]);
        $formatArray = ['past'=>[], 'current'=>[]];
        $thisMonth = Carbon::now()->startOfMonth();
        foreach($sessions as $session){
            $date = new Carbon($session->date);
            $key = $date->month.'_'.$date->year;
            if($date->timestamp < $thisMonth->timestamp && $session->status == 9){
                $formatArray['past'][$key][] = $session;    //sessions already paid for
            } else {
                $formatArray['current'][] = $session;       //sessions pending payment
            }
        }
        return $formatArray;
    }

    /**
     * @param $period should be '2016-01'
     * @return bool
     */
    public function markPaidForPeriodSessions($period)
    {
        $date = new Carbon($period);
        $dateFormat = $date->toDateString();
        $nextMonth = $date->copy()->addMonth();
        $nextMonthFormat = $nextMonth->toDateString();
        $sessions = SessionHistory::getList(['where' => "provider_id = {$this->id} AND status = 4 AND date >= '$dateFormat' AND date<= '$nextMonthFormat'"]);
        foreach($sessions as $session){
            $session->markStatus(9);
        }

        return true;
    }

    public function doesAvailable($time = null)
    {
        $today = Carbon::now()->toDateString();
        if(!$time){
            $sql = "SELECT DISTINCT date FROM calendar where provider_id='$this->id' and date >='$today' order by date asc limit 1";
            return \Model\Calendar::getList(['sql' => $sql])?true:false;
        } else {
            $date = new Carbon($time);
            $dateString = $date->toDateString();
            $sql = "SELECT * FROM calendar where provider_id='$this->id' and date_of_session = '$dateString'";
            $calendars = \Model\Calendar::getList(['sql' => $sql]);

            if(!$calendars){
                return false;
            }

            if(count($calendars) == 1 && $calendars[0]->full_day_av == 'true'){
                return true;
            }

            // just compare with start and end
            foreach($calendars as $calendar){
                $start = new Carbon($calendar->date_of_session.' '.$calendar->time_start);
                $end = new Carbon($calendar->date_of_session.' '.$calendar->time_end);

                // if both true, time works
                if($date->gte($start) && $date->lte($end)){
                    return true;
                }
            }

            return false;
        }


    }

    public function getFutureNoEmailAppointment()
    {
        $now = Carbon::now()->toDateString();
        $providerServices = Provider_Services::getList(['where' => ['provider_id' => $this->id]]);
        $providerServiceIds = [];
        foreach($providerServices as $providerService){
            $providerServiceIds[] = $providerService->id;
        }

        $idString = implode(",", $providerServiceIds);
        return Appointment::getList(['where' => "provider_service_id in ($idString) AND date >= '$now' AND send_email = 0"]);
    }

    public function checkAppointmentRequest()
    {
        $lists = $this->getFutureNoEmailAppointment();
        foreach($lists as $list){
            if($this->doesAvailable($list->date)){
                // TODO: send email to user


            }
        }
    }
}