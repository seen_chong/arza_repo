<?php

namespace Model;



class Donation extends \Emagid\Core\Model {

	static $tablename = "donation";

    

    public static $status = ['New', 'Paid', 'Payment Declined', 'Incomplete PayPal', 'Complete', 'Pending Paypal', 'Cancelled', 'Imported as Processing', 'Returned'];
	public static $transactionStatus = ['New Order', 'Paid', 'Payment Declined','Transaction Error'];


	public static $fields  = [
        'viewed'=>['type'=>'boolean'],
        'insert_time',
        'first_name',
        'last_name',
        'address',
        'city',
        'state',
        'zip',
        'email'=>['type'=>'email'],
        'phone',
        'cc_number'=>['name'=>'Credit Card Number'],
        'cc_expiration_month'=>['name'=>'Credit Card Expiration Month'],
        'cc_expiration_year'=>['name'=>'Credit Card Expiration Year'],
        'cc_ccv'=>['type'=>'numeric', 'name'=>'Credit Card CCV'],
        'amount',
        'user_id',
        'type',
        'status',
        'paypal_id',
        'ref_num',
        'monthly',
        'preference',
        'agreement_id',
        'designation',
        'gift_designation',
        'gift_designation_contact'

	];
    function full_name()
    {

        return $this->first_name . ' ' . $this->last_name;

    }
    function getType(){
        if($this->type == 0){
            return 'Register';
        }else{
            return \Model\Donate::getItem($this->type)->name;
        }

    }
    public static function search($keywords,$limit){

        $sql = "select * from public.donation where active = 1 and (";
        if (is_numeric($keywords)){
            $sql .= "id = ".$keywords." or ";
        }
        $sql .= "ref_num = '".$keywords."' or lower(first_name) like '%".strtolower(urldecode($keywords))."%' or lower(last_name) like '%".strtolower(urldecode($keywords))."%') limit ".$limit;

        return self::getList(['sql' => $sql]);


//        $sql = "select * from donation where active = 1 and id::varchar like '%".$keywords."%' order by id asc limit 20";
//
//        return self::getList(['sql' => $sql]);

    }
    function getAddr()
    {
        return $this->address . ' ' . $this->city . ' ' . $this->state . ', ' . $this->zip;
    }



}





















































































