<?php



namespace Model;



class Service extends \Emagid\Core\Model {

    static $tablename = "public.service";



    public static $fields  =  [
    	'name'=>['required'=>true],
    	'description',
		'slogan',
    	'photo',
    	'sub_service' => ['type'=>'numeric'],
		'slug' => ['required'=>true,'unique'=>true],
		'meta_title', 
		'meta_keywords', 
		'meta_description',
		'display_order' => ['type'=>'numeric'],
		'featured_image',
		'description_landing',
		'description_home',
      	'slider_1',
      	'slider_2',
      	'slider_3'
    ];    



    static $relationships = [
		[
			'name' => 'testimonial_services',
			'class_name' => '\Model\Testimonial_Services',
			'local' => 'id',
			'remote' => 'service_id',
			'remote_related' => 'testimonial_id',
			'relationship_type' => 'many'
		],
	];

    public function getParentService(){
    	if(($this->sub_service)==0){
            $parent_service=$this->slug;
        }else{
            $parent_service  = \Model\Service::getItem($this->sub_service)->slug;
        }
        return $parent_service;
    } 

	public function getProviderList(){
		$providerServices = Provider_Services::getList(['where'=>"service_id = {$this->id} and active = 1"]);
		$providers = [];
		foreach($providerServices as $providerService) {
			$provider = Provider::getItem($providerService->provider_id);
			if($provider->authorized == 'True'){
				$providers[] = $provider;
			}
		}
		shuffle($providers);
		return $providers;
	}

	public function getNumberProviderList($number = 3){
		$sql = "SELECT DISTINCT ON (provider_id)* from provider_services WHERE service_id = {$this->id} AND active = 1";
		$providerServices = Provider_Services::getList(['sql'=>$sql]);
		$providers = [];
		foreach($providerServices as $providerService){
			if($providerService->getProvider()) {
				$providers[] = $providerService->getProvider();
			}
		}
		shuffle($providers);
		return array_slice($providers, 0, $number);
	}

	public function getSubServices(){
		return self::getList(['where'=>"sub_service = $this->id"]);
	}

	public static function getParentChildId(){
		$arr = [];
		$mainServices = self::getList(['where'=>'sub_service = 0']);
		foreach($mainServices as $mainservice){
			$arr[$mainservice->name][] = $mainservice->id;
			foreach($mainservice->getSubServices() as $subService){
				$arr[$mainservice->name][] = $subService->id;
			}
		}
		return $arr;
	}

	public static function getAllSubServices(){
		$mainServices = self::getList(['where'=>"sub_service = '0'"]);
		$subServices = [];
		foreach($mainServices as $mainService){
			$subServices[$mainService->name] = $mainService->getSubServices();
		}

		return $subServices;
	}
}