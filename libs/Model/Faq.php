<?php
namespace Model;

use Emagid\Core\Model;

class Faq extends Model{
    static $tablename = 'faq';
    static $fields = [
        'title',
        'description',
        'display_order'
    ];
}