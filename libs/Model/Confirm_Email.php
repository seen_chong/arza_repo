<?php

namespace Model;

use Emagid\Core\Model;

class Confirm_Email extends Model{
    static $tablename = 'confirm_email';
    static $fields = [
        'name',
        'email_template',
        'subject',
        'confirm_email_images'
    ];

    static $dynamicElements = ['*|DATE|*','*|NAME|*','*|AMOUNT|*'];
    public static $name = ['Donation Confirmation','Monthly Donation Confirmation','Register Confirmation','Join Newsletter'];

}