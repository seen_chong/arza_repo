<?php 
namespace Model; 
class Israel extends \Emagid\Core\Model {
	static $tablename = 'israel';
	
	public static $fields =  [
		'insert_time', 
		'title', 
		'slug',
		'preview_description', 
		'description',
        'introduction',
		'featured_image',
		'meta_title', 
		'meta_keywords', 
		'meta_description',
		'date_modified',
		'author',
        'category',
		'status',
        'content_images',
        'author_description',
        'author_image',
		'featured' => [
        'type' => 'checkbox'
      	],

	];
	
}