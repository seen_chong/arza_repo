<?php

namespace Model;

use Emagid\Core\Model;

class External_Brand extends Model{
    static $tablename = 'external_brand';
    static $fields = [
        'name',
        'featured_image'
    ];
}