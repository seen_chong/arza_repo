<?php

namespace Model;

class Category extends \Emagid\Core\Model {

  	static $tablename = "category";

  	public static $fields = [ 
	    'description',
	    'name' => ['required'=>true],
	    'subtitle',
	    'parent_category',
	    'slug' => ['required'=>true,'unique'=>true],
	    'meta_title', 
		'meta_keywords', 
		'meta_description',
		'display_order' => ['type'=>'numeric']
	];
  	
	static $relationships = [
		[
			'name'=>'product_category',
			'class_name' => '\Model\Product_Category',
			'local'=>'id',
			'remote'=>'category_id',
			'remote_related'=>'product_id',
			'relationship_type' => 'many'
		],
    ];

	public static function getNested($parentId){
		$categories = self::getList(['where'=>"active = 1 AND parent_category = {$parentId} "]);
		
		foreach($categories as $category){
			$category->children = self::getNested($category->id);
		};
			
		return $categories;
	}
  
}