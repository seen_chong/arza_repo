<?php
namespace Model;

use Email\MailMaster;
use s3Bucket\s3Handler;

class Career extends \Emagid\Core\Model{

    static $tablename = "career";


    public static $fields = [
        'insert_time',
        'first_name' => ['required' => true, 'name' => 'First Name'],
        'last_name' => ['required' => true, 'name' => 'Last Name'],
        'email'=>['unique'=>'true'],
        'phone',
        'comment',
        'resume'
    ];

    /**
     * concatenates first name and last name to create full name string and returns it
     * @return type: string of full name
     */

    function full_name()
    {

        return $this->first_name . ' ' . $this->last_name;

    }

}