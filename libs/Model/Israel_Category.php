<?php



namespace Model;



class Israel_Category extends \Emagid\Core\Model {

    static $tablename = "israel_category";



    public static $fields  =  [
        'name'=>['required'=>true],
		'slug' => ['required'=>true,'unique'=>true],
		'meta_title',
		'meta_keywords',
		'meta_description'
    ];
}