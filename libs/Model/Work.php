<?php



namespace Model;



class Work extends \Emagid\Core\Model {



    static $tablename = 'work_page_section';



    public static $fields =  [

        'section_name',

        'title',

        'subtitle',

        'featured_image',

        'link',

        'button',

        'display',

        'display_order'

    ];
    public function buildSortable(){
        $html = '<li data-name="' . $this->section_name . '" data-id="' . $this->id . '">' . $this->section_name . '</li>';
        return $html;
    }


}