<?php

namespace Model;

class Card extends \Emagid\Core\Model {
	static $tablename = 'card';

	public static $fields = [
		'user_id',
//		'name',
		'address'=>['required'=>true],
		'address2',
		'city'=>['required'=>true],
		'state'=>['required'=>true],
		'zip'=>['required'=>true, 'type'=>'numeric'],
		'country'=>['required'=>true],
		'cc_number'=>['type'=>'numeric'],
		'cc_expiration_month'=>['type'=>'numeric'],
		'cc_expiration_year'=>['type'=>'numeric'],
		'cc_ccv'=>['type'=>'numeric'],
		'cc_holder'=>['required'=>true]
	];
}