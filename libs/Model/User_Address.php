<?php

namespace Model;



class User_Address extends \Emagid\Core\Model {

	static $tablename = "user_address";

	public static $fields = [
		'user_id',
		'name',
		'street',
		'apt',
		'zip',
		'add_info'
  	];

	public function getAddress(){
		return $this->street.' '.$this->apt.', '.$this->zip;
	}

	public function addAddrFromSession($userId){
		$address = new self;
		$address->name = $_SESSION['addrName'];
		$address->street = $_SESSION['street'];
		$address->apt = $_SESSION['apt'];
		$address->zip = $_SESSION['zip'];
		$address->user_id = $userId;
		$address->save();
		$address->unsetAddrSession();
	}

	public function unsetAddrSession(){
		unset($_SESSION['addrName']);unset($_SESSION['street']);unset($_SESSION['apt']);unset($_SESSION['zip']);
	}

	public function apiDetails()
	{
		return [
			'ide' => $this->ide,
			'name'=> $this->name,
			'street'=> $this->street,
			'apt'=> $this->apt,
			'zip'=> $this->zip,
		];
	}
}