<?php



namespace Model;



class Blog_Category extends \Emagid\Core\Model {

    static $tablename = "blog_category";



    public static $fields  =  [
        'name'=>['required'=>true],
		'slug' => ['required'=>true,'unique'=>true],
		'meta_title',
		'meta_keywords',
		'meta_description',
		'featured_image',
        'sub_cat' => ['type'=>'numeric']
    ];    




    public function getParentCategory(){
    	if(($this->sub_cat)==0){
            $parent_cat=$this->slug;
        }else{
            $parent_cat  = Blog_Category::getItem($this->sub_cat)->slug;
        }
        return $parent_cat;
    } 



	public function getSubCategory(){
		return self::getList(['where'=>"sub_cat = $this->id"]);
	}

	public static function getParentChildId(){
		$arr = [];
		$mainCategories = self::getList(['where'=>'sub_cat = 0']);
		foreach($mainCategories as $maincatgory){
			$arr[$maincatgory->name][] = $maincatgory->id;
			foreach($maincatgory->getSubCategory() as $subCat){
				$arr[$maincatgory->name][] = $subCat->id;
			}
		}
		return $arr;
	}

	public static function getAllSubServices(){
		$mainCategory = self::getList(['where'=>"sub_cat = '0'"]);
		$subCat = [];
		foreach($mainCategory as $main){
            $subCat[$main->name] = $main->getSubServices();
		}

		return $subCat;
	}
}