<?php
namespace Model;

use Email\MailMaster;
use s3Bucket\s3Handler;

class Position extends \Emagid\Core\Model{

    static $tablename = "position";


    public static $fields = [
        'name',
        'description',
        'amount',
        'display'
    ];

}