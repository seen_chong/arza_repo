<?php
namespace Model;

use Email\MailMaster;
use s3Bucket\s3Handler;

class Officer extends \Emagid\Core\Model{

    static $tablename = "officer";


    public static $fields = [

        'first_name' => ['required' => true, 'name' => 'First Name'],
        'last_name' => ['required' => true, 'name' => 'Last Name'],
        'title',
        'display',
        'display_order'


    ];

    /**
     * concatenates first name and last name to create full name string and returns it
     * @return type: string of full name
     */

    function full_name()
    {

        return $this->first_name . ' ' . $this->last_name;

    }

    public function buildSortable(){
        $html = '<li data-id="'.$this->id.'">'.$this->full_name().'</li>';
//        $html .= '</ol></li>';
        return $html;
    }
}