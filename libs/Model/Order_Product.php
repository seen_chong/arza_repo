<?php

namespace Model;

class Order_Product extends \Emagid\Core\Model {
    static $tablename = "order_products";
  
    public static $fields = [
  		'order_id',
  		'product_id',
  		'quantity',
  		'unit_price',
  		'color',
  		'size',
  		'type_product',
      'provider_id',
      'date_of_pack',
      'service',
      'used',
      'max_using',
      'user_id'
    ];
	
	 
}