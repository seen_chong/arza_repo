<?php
namespace Model;

use Email\MailMaster;
use s3Bucket\s3Handler;

class Volunteer extends \Emagid\Core\Model{

    static $tablename = "volunteer";


    public static $fields = [

        'name',
        'description',
        'url',
        'display'
    ];

}