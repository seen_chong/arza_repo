<?php
namespace Model;
class Message extends \Emagid\Core\Model {
  	static $tablename = "message";
  	public static $fields = [ 
  		'insert_time',
	    'subject_id',
	    'text',
		'read_status',
		'author'
	];
  	
	/*static $relationships = [
		[
			'name'=>'product_category',
			'class_name' => '\Model\Product_Category',
			'local'=>'id',
			'remote'=>'category_id',
			'remote_related'=>'product_id',
			'relationship_type' => 'many'
		],
    ];
	public static function getNested($parentId){
		$categories = self::getList(['where'=>"active = 1 AND parent_category = {$parentId} "]);
		
		foreach($categories as $category){
			$category->children = self::getNested($category->id);
		};
			
		return $categories;
	}*/

	public function getUnread($to, $role){
		$message = self::getList(['where'=>['to_id'=>$to,'to_type_id'=>$role,'read_status'=>0]]);
		return $message;
	}

	public function getRead($to, $role){
		$message = self::getList(['where'=>['to_id'=>$to,'to_type_id'=>$role,'read_status'=>1]]);
		return $message;
	}

	public static function send($to, $from, $subject, $text){
		$message = new Message();
		if(!is_numeric($subject)) {
			$message->sendNewMessage($to, $from, $subject, $text);
		} else {
			$message->reply($to, $from, $subject, $text);
		}

		return $message;
	}

	public function sendNewMessage($to, $from, $subject, $text){
		$reflectTo = new \ReflectionClass($to);
		$toRole = $reflectTo->getShortName();

		$sub = new Subject();

		if($toRole == 'User'){
			$sub->user_id = $to->id;
			$sub->provider_id = $from->id;
		} else {
			$sub->provider_id = $to->id;
			$sub->user_id = $from->id;
		}
		$sub->subject_name = $subject;
		$sub->save();

		$this->subject_id = $sub->id;
		$this->text = $text;
		$this->author = $from->full_name();
		$this->save();
	}

	public function reply($to, $from, $subject, $text){
		$sub = Subject::getItem($subject);
		$this->subject_id = $sub->id;
		$this->text = $text;
		$this->author = $from->full_name();
		$this->save();
	}

	public function isRead()
	{
		return $this->read_status;
	}

	public function markRead()
	{
		$this->read_status = 1;
		return $this->save();
	}
}