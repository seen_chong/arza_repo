<?php 
namespace Model; 
class Mission_Display extends \Emagid\Core\Model {
	static $tablename = 'mission_display';
	
	public static $fields =  [
		'section_name',
        'display_order',
        'display'
	];
    public function buildSortable(){
            $html = '<li data-name="' . $this->section_name . '" data-id="' . $this->id . '">' . $this->section_name . '</li>';
        return $html;
    }

}