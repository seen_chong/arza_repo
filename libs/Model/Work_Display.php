<?php 
namespace Model; 
class Work_Display extends \Emagid\Core\Model {
	static $tablename = 'work_display';
	
	public static $fields =  [
		'section_name',
        'display_order',
        'display'
	];
    public function buildSortable(){
            $html = '<li data-name="' . $this->section_name . '" data-id="' . $this->id . '">' . $this->section_name . '</li>';
        return $html;
    }

}