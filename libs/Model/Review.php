<?php

namespace Model;



class Review extends \Emagid\Core\Model {

	static $tablename = "reviews";

	public static $fields = [
	  'insert_time',
      'session_id',
      'review', 
      'author'

  	];

}