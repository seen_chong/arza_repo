<?php 
namespace Model; 
class Blog extends \Emagid\Core\Model {
	static $tablename = 'blog'; 
    static $status = [0=>'Not approved',1=>'Approved',2=>'Scheduled'];
	public static $fields =  [
		'insert_time', 
		'title', 
		'slug',
		'preview_description', 
		'description',
        'introduction',
		'featured_image',
		'meta_title', 
		'meta_keywords', 
		'meta_description',
		'date_modified',
		'author',
		'status',
        'blog_images',
//		'service',
//		'date_modified',
        'category',
        'author_description',
        'author_image',
		'featured' => [
        'type' => 'checkbox'
      	],
        'post_date'

	];
	
}