<?php
namespace Model;

use Email\MailMaster;
use s3Bucket\s3Handler;

class User extends \Emagid\Core\Model{

    static $tablename = "public.user";


    public static $fields = [

        'email'=>['required'=>'true','unique'=>'true','type'=>'email'],
        'password',
        'hash',
        'first_name' => ['required' => true, 'name' => 'First Name'],
        'last_name' => ['required' => true, 'name' => 'Last Name'],
        'company',
        'phone',
        'subtype',
        'donation_amount',
        'token'
    /* General members=>individual=>1, General members=>congregational=>2 ;
    Rabbinic Council Members=>new rabbis => 3, Rabbinic Council Members=>other rabbis=>4 */

    ];

    /**
     * concatenates first name and last name to create full name string and returns it
     * @return type: string of full name
     */

    function full_name()
    {

        return $this->first_name . ' ' . $this->last_name;

    }

//    function first_name()
//    {
//
//        return $this->first_name;
//
//    }


    /**
     * Verify login and create the authentication cookie / session
     * Accepts no parameter to login the instantiated object or 2 parameters (login and password)
     * to get an object from the database and log it in
     */

    public static function search($keywords, $limit = 20)
    {
        $sql = "select id,first_name,last_name,email from public.user where active = 1 and (";
        if (is_numeric($keywords)) {
            $sql .= "id = " . $keywords . " or ";
        }
        $sql .= "lower(email) like '%" . strtolower(urldecode($keywords)) . "%' or lower(first_name) like '%" . strtolower(urldecode($keywords)) . "%' or lower(last_name) like '%" . strtolower(urldecode($keywords)) . "%') limit " . $limit;

        return self::getList(['sql' => $sql]);
    }

    public static function login($email , $password)
    {

//        if (count(func_get_args()) == 0) {
//            $roles = \Model\User_Roles::getList(['where' => 'active = 1 and user_id = ' . $this->id]);
//            if (!is_null($roles)) {
//                $rolesIds = [];
//                foreach ($roles as $role) {
//                    $rolesIds[] = $role->role_id;
//                }
//                $rolesIds = implode(',', $rolesIds);
//                $roles = \Model\Role::getList(['where' => 'active = 1 and id in (' . $rolesIds . ')']);
//                $rolesNames = [];
//                foreach ($roles as $role) {
//                    $rolesNames[] = $role->name;
//                }
//                \Emagid\Core\Membership::setAuthenticationSession($this->id, $rolesNames, $this);
//
//                return true;
//            } else {
//                return false;
//            }
//
//        } else if (count(func_get_args()) == 2) {
//            $email = func_get_arg(0);
//            $password = func_get_arg(1);
//            $user = self::getItem(null, ['where' => "LOWER(email) = LOWER('" . $email . "')"]);
//            if (!is_null($user)) {
//                $hash = \Emagid\Core\Membership::hash($password, $user->hash);
//                if ($hash['password'] == $user->password) {
//                    $user->login();
//                    $user->last_login = date('Y-m-d h:i:s');
//                    return true;
//                } else {
//                    return false;
//                }
//            } else {
//                return false;
//            }
//        } else {
//            return false;
//        }

        $user = self::getList(['where'=>"email = '".$email."'"]);
        if (count($user)>0){
            $user = $user[0];
            $hash = \Emagid\Core\Membership::hash($password, $user->hash);
            if ($hash['password'] == $user->password) {
                $userRoles = \Model\User_Roles::getList(['where' => 'active = 1 and user_id = '.$user->id]);
                $rolesIds = [];
                foreach($userRoles as $role){
                    $rolesIds[] = $role->role_id;
                }
                $rolesIds = implode(',', $rolesIds);

                $roles = \Model\Role::getList(['where' => 'active = 1 and id in ('.$rolesIds.')']);
                $rolesNames = [];
                foreach($roles as $role){
                    $rolesNames[] = $role->name;
                }

                \Emagid\Core\Membership::setAuthenticationSession($user->id, $rolesNames, $user);

                return true;
            } else {
                $n = new \Notification\ErrorHandler('Incorrect email or password.');
                $_SESSION["notification"] = serialize($n);
            }
        } else {
            $n = new \Notification\ErrorHandler('Email not found.');
            $_SESSION["notification"] = serialize($n);
        }
    }


    public function getAllUsersByRole($role)
    {
        $userRoles = User_Roles::getList(['where' => "role_id = $role"]);
        $users = [];
        foreach ($userRoles as $role) {
            if($user = User::getItem(null, ['where' => "id = $role->user_id"])) {
                $users[] = $user;
            }
        }
        return $users;
    }
    public function getMemberType(){
        return Member_Type::getItem($this->subtype)->name;
    }


}