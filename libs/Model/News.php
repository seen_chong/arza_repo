<?php
namespace Model;
class News extends \Emagid\Core\Model {
  	static $tablename = "news";
  	public static $fields = [
	    'title',
	    'author',
		'time',
        'url',
        'featured_image'
	];
  	

}