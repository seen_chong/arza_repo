<?php
namespace Model;

class Role extends \Emagid\Core\Model {
	static $tablename = "role";
	public static $fields = ['name'];
}