<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 12/18/15
 * Time: 2:55 PM
 */

namespace Model;


class CreditHistory extends \Emagid\Core\Model {
    static $tablename = 'credit_history';

    public static $fields =  [
        'insert_time',
        'order_id'  => ['required'=>true, 'type'=>'numeric' ],
        'credit_id' => ['required'=>true,'type'=>'numeric' ],
        'user_id',
    ];
}