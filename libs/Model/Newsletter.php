<?php

namespace Model;

class Newsletter extends \Emagid\Core\Model {
	static $tablename = "newsletter";
	public static $fields = [
		'insert_time',
        'email'=>['required'=>true, 'type'=>'email']
	];
    static function deleteCoupon($newsletter){
        $nl = self::getItem($newsletter->id);
        $nl->is_expired = 0;
        $nl->save();
    }
}
