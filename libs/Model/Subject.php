<?php
namespace Model;
class Subject extends \Emagid\Core\Model {
  	static $tablename = "subject";
  	public static $fields = [ 
	    'user_id',
	    'provider_id',
	    'subject_name' 
	];
  	
	/*static $relationships = [
		[
			'name'=>'product_category',
			'class_name' => '\Model\Product_Category',
			'local'=>'id',
			'remote'=>'category_id',
			'remote_related'=>'product_id',
			'relationship_type' => 'many'
		],
    ];
	public static function getNested($parentId){
		$categories = self::getList(['where'=>"active = 1 AND parent_category = {$parentId} "]);
		
		foreach($categories as $category){
			$category->children = self::getNested($category->id);
		};
			
		return $categories;
	}*/

	public function getUnread()
	{
		return Message::getList(['where' => ['subject_id' => $this->id, 'read_status' => 0]]);
	}

	public function getRead()
	{
		return Message::getList(['where' => ['subject_id' => $this->id, 'read_status' => 1]]);
	}

	public function getMessages()
	{
		return Message::getList(['where' => ['subject_id' => $this->id]]);
	}

    public function getNotAuthorMessages($author)
    {
        return Message::getList(['where' => "subject_id = {$this->id} AND author != '{$author->full_name()}'"]);
    }


	/**
	 * Based on subject messages status, split to read and unread two array,
	 * each array will contain subject object, and all associated messages,
	 * if any of subject messages is unread, that subject will belongs to unread
	 * @return array
     */
	public static function getSubjectArray($author, $whereOption)
	{
		$unreadArray = [];
		$readArray = [];

		$where = array_merge([], $whereOption);
		foreach(self::getList(['where'=>$where]) as $subject){
			$messages = $subject->getNotAuthorMessages($author);
            $allMessages = $subject->getMessages();;

			if(!(self::isAllRead($messages))){
				$subject->messages = $allMessages;
				$unreadArray[] = $subject;
			} else {
				$subject->messages = $allMessages;
				$readArray[] = $subject;
			}
		}

		return ['read' => $readArray, 'unread' => $unreadArray];

	}

	public function markAllMessageRead($author)
	{
		$messages = $this->getNotAuthorMessages($author);
		foreach($messages as $message){
			$message->markRead();
		}

		return true;

	}

    private static function isAllRead($messages)
    {
        foreach($messages as $message){
            if(!$message->isRead()){
                return false;
            }
        }

        return true;
    }

	public function getLatestTime(){
		return Message::getItem(null,['where'=>"subject_id = $this->id"])->insert_time;
	}
  
}