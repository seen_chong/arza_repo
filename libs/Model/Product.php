<?php
namespace Model;

class Product extends \Emagid\Core\Model {
  
  	public static $tablename = "product";
  
  	public static $fields = [
  		'name' => ['required'=>true],
  		'sku',
  		'price' => ['type'=>'numeric', 'required'=>true],
  		'list_price' => ['type'=>'numeric'],
  		'wholesale_price' => ['type'=>'numeric', 'name'=>'Wholesale Price'],
  		'summary',
  		'description',
  		'brand_id',
  		'manufacturer',
  		'mpn',
  		'gender_id', //[1=>"Unisex", 2=>"Men", 3=>"Women"]
  		'msrp' => ['type'=>'numeric'],
  		'quantity' => ['type'=>'numeric'],
  		'slug' => ['required'=>true, 'unique'=>true],
        'tags',
  		'meta_title',
  		'meta_keywords',
  		'meta_description',
  		'featured_image',
  		'colors',
  		'sizes',
  		'featured' => [
        	'type' => 'checkbox'
      	],
  		'length',
  		'width',
  		'height',
  		'video_url',
		'video_thumbnail',
  		'google_product_category',
  		'availability'
  	];

    static $relationships = [
      [
        'name'=>'product_category',
        'class_name' => '\Model\Product_Category',
        'local'=>'id',
        'remote'=>'product_id',
        'remote_related'=>'category_id',
        'relationship_type' => 'many'
      ],
      // [
      //   'name'=>'product_collection',
      //   'class_name' => '\Model\Product_Collection',
      //   'local'=>'id',
      //   'remote'=>'product_id',
      //   'remote_related'=>'collection_id',
      //   'relationship_type' => 'many'
      // ],
      // [
      //   'name'=>'product_material',
      //   'class_name' => '\Model\Product_Material',
      //   'local'=>'id',
      //   'remote'=>'product_id',
      //   'remote_related'=>'material_id',
      //   'relationship_type' => 'many'
      // ]
    ];

    public function beforeValidate(){
    	if(is_array($this->colors)){
    		$this->colors = implode(',', $this->colors);
    	}
    	if(is_array($this->sizes)){
    		$this->sizes = implode(',', $this->sizes);
    	}
    }

    public static function search($keywords, $limit = 20){
        $sql = "select id, name, featured_image, mpn, price, slug from product where active = 1 and (";
        if (is_numeric($keywords)){
            $sql .= "id = ".$keywords." or ";
        }
        $sql .= " lower(name) like '%".strtolower(urldecode($keywords))."%') limit ".$limit;
        return self::getList(['sql' => $sql]);
    }

    public static function getPriceSum($productsStr){
        $total = 0;
        foreach(explode(',', $productsStr) as $productId){
            $product = self::getItem($productId);
            $total += $product->price;
        }
        return $total;
    }

}


































