<?php
namespace Model;
class Banner extends \Emagid\Core\Model {
    static $tablename = "public.banner";
    public static $fields  =  [
    	'title',
    	'image',
    	'description',
    	'featured' => ['type' => 'checkbox'],
		'url'
    ];

	public function getUrl(){
		return $this->url != "" && !is_null($this->url)? $this->url: '#';
	}
}

















































