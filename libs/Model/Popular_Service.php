<?php

namespace Model;

use Emagid\Core\Model;

class Popular_Service extends Model{
    static $tablename = 'popular_service';
    static $fields = [
        'service_id',
        'title',
        'description',
        'featured_image',
        'url',
        'display_state'
    ];
    static $display_state = [1=>'Home', 2=>'Corporate'];

    function getService(){
        return Service::getItem($this->service_id);
    }
}