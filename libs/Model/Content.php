<?php


namespace Model;


class Content extends \Emagid\Core\Model
{


    static $tablename = "content";


    public static $fields = [

        'name' => ['required' => true],

        'hebrew_name',

        'parent_section',

        'title',

        'description',

        'slug' => ['required' => true, 'unique' => true],

        'meta_title',

        'meta_keywords',

        'meta_description',

//        'display_order' => ['type'=>'numeric'],

        'display' => ['type' => 'numeric']

    ];


    public static function getNested($parentId)
    {

        $categories = self::getList(['where' => "active = 1 AND parent_section = {$parentId} "]);

        return $categories;

    }

    public function getLocation()
    {
//      $loc = self::getItem($this->parent_section)->name;
        $location = ["nav" => "Navbar", "work" => "What We Do", "mission" => "Who We Are", "join" => "Join the Community"];
        foreach ($location as $key => $value) {
            if (strcmp($this->parent_section, $key) == 0) {
                $loc = $value;
            }
        }


        return $loc;
    }

    public function getHebrew()
    {
        $name = '';
        if ($this->hebrew_name != '') {
            $name .= '<br/>' . $this->hebrew_name;
        }
        return $name;
    }

}