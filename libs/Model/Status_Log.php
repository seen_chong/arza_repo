<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 4/4/16
 * Time: 10:38 AM
 */

namespace Model;

use Carbon\Carbon;
class Status_Log extends \Emagid\Core\Model
{

    static $tablename = "public.status_log";

    public static $fields = [
        'provider_id', 'start_status', 'end_status', 'start_time', 'end_time', 'summary', 'session_id'
    ];

    public static function create(array $data)
    {
        $model = new Status_Log();
        foreach($data as $field => $val){
            $model->$field = $val;
        }

        $model->save();

        return $model;
    }
}