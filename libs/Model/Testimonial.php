<?php

namespace Model;

use Emagid\Core\Model;

class Testimonial extends Model
{
    static $tablename = 'testimonial';
    static $fields = [
        'name',
        'content',
        'state',
        'status'
    ];
    static $status = [1 => 'Active', 2 => 'Inactive'];
    static $relationships = [
        [
            'name' => 'testimonial_services',
            'class_name' => '\Model\Testimonial_Services',
            'local' => 'id',
            'remote' => 'testimonial_id',
            'remote_related' => 'service_id',
            'relationship_type' => 'many'
        ]
    ];


    /**
     * @param int $service
     * @return array|\Emagid\Core\Array /Model/Testimonial
     */
    public static function getTestimonials($service_id = null){
        if(is_numeric($service_id)){
            if($testSer = Testimonial_Services::getList(['where'=>"service_id = $service_id"])){
                $testId = implode(',',array_map(function($item){return $item->testimonial_id;},$testSer));
                $test = Testimonial::getList(['where'=>"id in ($testId) and status = 1", 'limit'=>3]);
            } else {
                $sql = "select * from testimonial where id not in (select DISTINCT testimonial_id from testimonial_services) and active = 1 and status = 1 limit 3";
                $test = Testimonial::getList(['sql'=>$sql]);
            }
        } else if($service_id && ($service = Service::getItem(null,['where'=>"slug = '$service_id'"]))){
            if($testSer = Testimonial_Services::getList(['where'=>"service_id = $service->id"])){
                $testId = implode(',',array_map(function($item){return $item->testimonial_id;},$testSer));
                $test = Testimonial::getList(['where'=>"id in ($testId) and status = 1", 'limit'=>3]);
            } else {
                $sql = "select * from testimonial where id not in (select DISTINCT testimonial_id from testimonial_services) and active = 1 and status = 1 limit 3";
                $test = Testimonial::getList(['sql'=>$sql]);
            }
        } else {
            $sql = "select * from testimonial where id not in (select DISTINCT testimonial_id from testimonial_services) and active = 1 and status = 1 limit 3";
            $test = Testimonial::getList(['sql'=>$sql]);
        }
        return $test;
    }
}