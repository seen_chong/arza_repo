<?php
namespace Model;
class Payout extends \Emagid\Core\Model {
	static $tablename = "withdraw";
	public static $fields = [
      'provider_id',
      'amount',
      'status', 
      'period'
  	];
}