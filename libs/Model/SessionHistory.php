<?php
namespace Model;

use Carbon\Carbon;
class SessionHistory extends \Emagid\Core\Model {

    static $tablename = "public.session_history";

    public static $CREDIT_TYPE = [
        'customer' => 1,
        'corporate' => 2
    ];

    CONST WELLNS_KEEP = 0.7;

//    CONST AUTO_CANCEL_DAY = 3;
    // 0 pending session
    // 1 provider accepted requested session
    // 2 provider declined requested session
    // 3 provider confirmed completed session
    // 4 customer confirmed completed session(customer only can confirm it after provider confirmed it)
    // 5 provider/customer rejected session, it required cancellation reason, it won't get instantly refund, system administrator have to look into it
    // 6 refunded
    // 7 cancelled, usually after cancelled, status should change to refunded instantly, but if it doesn't, means troubles...
    // 8 unpaid, session is complete and provider is pending payment
    // 9 paid, session is complete and provider is paid
    // 10 payment issue, Wellns had issues paying that session
    public static $fields  =  [
        'user_id' => ['required'=>true],
        'date' => ['required'=>true],
        'service_id' => ['required'=>true],
        'provider_id' => ['required'=>true],
        'location' => ['required'=>true],
        'credit_type_id' => ['required'=>true],
        'credit' => ['required'=>true],
        'insert_time',
        'status',
        'cancel_message_id',
        'credit_package_id',
        'status_updated_at'
    ];

    public function getCreditConsume()
    {
        return 1;
    }

    public function getFormattedDate()
    {
        return date("g:i A F d, Y", strtotime($this->date));
    }

    /**
     * 5:20 PM, Wednesday, June 1, 2016
     * @return bool|string
     */
    public function getTimeFormat()
    {
        return date("g:i A", strtotime($this->date));
    }

    /**
     * Wednesday, 6/1/16
     * @return bool|string
     */
    public function getDateFormat()
    {
        return date("l, n/j/y", strtotime($this->date));
    }

    public static function getByDateTime($provider, $date)
    {

        return self::getItem(null, ['where' => "provider_id = {$provider->id} and date = '$date'"]);
    }



    /**
     * Provider or customer only can cancel session 48 hours(2 days) before session date
     */
    public function isAbleCancel()
    {
        if($this->status != 1){
            return false;
        }
        $now = Carbon::now();
        $sessionDate = new Carbon($this->date);
        $daysDiff = $sessionDate->diffInDays($now, false);
        return $daysDiff > 2;
    }

    /**
     * @param $status
     * @param bool|false $isProvider => is status made by provider
     * @return bool
     */
    public function markStatus($status, $isProvider = false)
    {
        $status = intval($status);
        if(is_numeric($status) && $status >= 0){

            // we only log provider status log
            if(in_array($status, [1, 2, 3, 5, 7]) && $isProvider) {
                $oldStatus = $this->status;
                $newStatus = $status;

                // This is UTC time
                if($this->status_updated_at == $this->insert_time){
                    $startTime = Carbon::createFromFormat('Y-m-d H:i:s.u', $this->status_updated_at, 'UTC');
                    $startTime->setTimezone('America/New_York');
                } else {
                    $startTime = new Carbon($this->status_updated_at);
                }

                $endTime = Carbon::now();

                $summary = $endTime->diffInSeconds($startTime);

                Status_Log::create([
                    'start_status' => $oldStatus,
                    'end_status' => $newStatus,
                    'start_time' => $startTime->toDateTimeString(),
                    'end_time' => $endTime->toDateTimeString(),
                    'provider_id' => $this->provider_id,
                    'session_id' => $this->id,
                    'summary' => $summary
                ]);

                // this means latest provider update status time
                $this->status_updated_at = $endTime->toDateTimeString();
            }

            /**
             * Pending to accept, we log provider time slot as book
             */
            if($this->status == 0 && $status == 1){
                $date = new Carbon($this->date);
                Calendar::create($this->provider_id, $date->toDateString(), $date->hour, Calendar::$STATUS_MAP['book']);
            }

            if($this->status == 1 && $status == 7){
                $date = new Carbon($this->date);
                Calendar::create($this->provider_id, $date->toDateString(), $date->hour, Calendar::$STATUS_MAP['available']);
            }

            $this->status = $status;

            if($status == 2 || $status == 5 || $status == 7){
                $this->refund();
            }


            return $this->save();
        }

        return false;
    }

    public function markCancelMessage($messageId)
    {
        $messageId = intval($messageId);
        if(is_numeric($messageId) && $messageId >= 0){
            $this->cancel_message_id = $messageId;
            return $this->save();
        }

        return false;
    }

    public function user()
    {
        return User::getItem(null, ['where' => ['id' => $this->user_id]]);
    }

    public function provider()
    {
        return Provider::getItem(null, ['where' => ['id' => $this->provider_id]]);
    }

    public function service(){
        return Service::getItem($this->service_id);
    }

    public function status(){
        return ['Pending','Accepted Session','Declined Session','Completed by provider','Completed by customer','Denial of Completion','Refunded','Canceled','Unpaid','Paid','Payment Issues'];
    }

    public function getSessionCredits()
    {
        return $this->creditPackage()->credit;
    }

    public static function getByStatus($status){
        return self::getList(['where'=>['active'=>1,'status'=>$status]]);
    }

    public function refund()
    {
        // check refund status, we only refund when status is 0, 1, 2, 5
        if(!in_array($this->status, [0, 1, 2, 5, 7])){
            return false;
        }
        $credit = $this->credit;
        $user = $this->user();
        if($this->credit_type_id == 1){
            $user->credit += $credit;
        } else {
            $user->corporate_credit += $credit;
        }

        return $this->markStatus(6) && $user->save();
    }

    public static function confirmRelationship($userId, $providerId){
        $relation = self::getList(['where'=>"user_id = $userId and provider_id = $providerId"]);
        return count($relation)>0 ? true: false;
    }

    public static function getExpiredPendingSession()
    {
        $today = date('Y-m-d', time());
        return self::getList(['where' => "active='1' and date<='$today' and status = 0 ", 'orderBy' => 'date asc']);
    }

    public function getCreditPackage(){
        return CreditPackage::getItem($this->credit_package_id);
    }

    public function sendAutoCanceledSessionEmail()
    {
        $provider = $this->provider();
        $service = $this->service();
        $customer = $this->user();
        $email = new \Email\MailMaster();
        $mergeFields = [
            'PROVIDER_NAME' => $provider->full_name(),
            'SERVICE_NAME' => $service->name,
            'SESSION_TIME' => $this->getFormattedDate(),
            'CUSTOMER_NAME' => $customer->full_name(),
        ];

        $email->setTo(['email' => $customer->email, 'name' => $customer->full_name(),  'type' => 'to'])->setTemplate('system-auto-cancel-session')->setMergeTags($mergeFields)->send();
        $email->setTo(['email' => $provider->email, 'name' => $provider->full_name(),  'type' => 'to'])->setTemplate('system-auto-cancel-session')->setMergeTags($mergeFields)->send();
    }

    public function getCreditUnitPrice()
    {
        return 100;
    }

    //24hr & 3hr before session
    public function sendSessionWarningEmail($hours = null){
        $provider = $this->provider();
        $service = $this->service();
        $customer = $this->user();
        switch($this->getCreditPackage()->credits){
            case 1:
                $customerCount = 'This is a one-on-one session';break;
            case 2:
                $customerCount = 'There are multiple students attending this session (2-9).';break;
            case 3:
                $customerCount = 'There are multiple students attending this session (10-20).';break;
            default:
                $customerCount = '';break;
        }
        $email = new \Email\MailMaster();
        if($customer->getRole() == 4 && $hours == 24){
            $carbon = Carbon::now()->minute(0)->second(0);
            $date = $carbon->addDay(1)->toDateString();
            $time = $carbon->toTimeString();
            $mergeFields = [
                'PROVIDER_NAME'=>$provider->full_name(),
                'DATE'=>$date,
                'SERVICE'=>$service->name,
                'COMPANY'=>$customer->company,
                'CUSTOMER_COUNT'=>$customerCount,
                'LOCATION'=>$this->getLocation(),
                'TIME'=>$time
            ];
            $email->setTo(['email' => $provider->email, 'name' => $provider->full_name(), 'type' => 'to'])->setTemplate('corporate-session-reminder-to-provider')->setMergeTags($mergeFields)->send();
        } else {
            $mergeFields = [
                'PROVIDER_NAME' => $provider->full_name(),
                'SERVICE_NAME' => $service->name,
                'SESSION_TIME' => $this->getFormattedDate(),
                'CUSTOMER_NAME' => $customer->full_name(),
                'CUSTOMER_COUNT' => $customerCount,
                'TIME_REMAINING' => $hours
            ];
            $email->setTo(['email' => $provider->email, 'name' => $provider->full_name(), 'type' => 'to'])->setTemplate('getTimeFormat')->setMergeTags($mergeFields)->send();
        }
    }

    //1hr after session
    public function sendAfterSessionWarningEmail(){
        $provider = $this->provider();
        $service = $this->service();
        $customer = $this->user();
        $customerCount = $this->getCreditPackage()->credits == 1?'Single':"{$this->getCreditPackage()->name}";
        $email = new \Email\MailMaster();
        $mergeFields = [
            'PROVIDER_NAME' => $provider->full_name(),
            'SERVICE_NAME' => $service->name,
            'SESSION_TIME' => $this->getFormattedDate(),
            'CUSTOMER_NAME' => $customer->full_name(),
            'CUSTOMER_COUNT' => $customerCount
        ];
        $email->setTo(['email' => $customer->email, 'name' => $customer->full_name(),  'type' => 'to'])->setTemplate('notify-confirm-session-provider')->setMergeTags($mergeFields)->send();
    }

    //new address uses user_address id as ref, this should catch all previous locations written as address strings
    public function getLocation(){
        if(is_numeric($this->location)){
            $ua = User_Address::getItem($this->location);
            $temp = [];
            array_push($temp,$ua->street,$ua->apt,$ua->zip);
            $merge = implode(' ',$temp);
            return $merge;
        } else {
            return $this->location;
        }

    }

    public function locationInfo(){
        $ua = User_Address::getItem($this->location);
        return $ua->add_info != null && $ua->add_info != ''?$ua->add_info:null;
    }

    public static function getAllHistoryUnpaidSessions()
    {
        $data = [];
        // get distinct providers
        $sessions = self::getList(['sql'=>"SELECT DISTINCT provider_id FROM session_history where status = 4"]);
        $thisMonth = Carbon::now()->startOfMonth()->toDateString();
        foreach($sessions as $session){
            $provider = $session->provider();
            $historySessions = self::getList(['sql'=>"SELECT * FROM session_history where status = 4 AND provider_id = {$provider->id} AND date <= '{$thisMonth}'"]);
            $formatSession = [];
            $paymentArray = [];
            $paymentAmount = 0;
            foreach($historySessions as $historySession){
                $date = new Carbon($historySession->date);
                $key = $date->year.'_'.$date->month;
                $formatSession[$key][] = $session;
                $paymentAmount += $historySession->credit * $session->getCreditUnitPrice();
                $paymentArray[$key]['amount'] = isset($paymentArray[$key]['amount'])?($paymentArray[$key]['amount'] + $historySession->credit * floatval($provider->payout_rate)):($historySession->credit * floatval($provider->payout_rate));
            }

            if($formatSession){
                $data[] = ['provider' => $provider, 'past' => $formatSession, 'payment' => $paymentArray];
            }
        }

        return $data;
    }

    public function providerApiDetails()
    {
        $location = User_Address::getItem($this->location);

        $service = $this->service();
        $user = $this->user();
        return [
            'date' => $this->date,
            'user' => $user->full_name(),
            'ide' => $this->ide,
            'location' => $location->apiDetails(),
            'service' => $service->ide
        ];
    }
}