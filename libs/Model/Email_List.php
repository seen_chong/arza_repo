<?php 

namespace Model; 

class Email_List extends \Emagid\Core\Model {
	
	static $tablename = 'email_list'; 
	
	public static $fields =  [
		'email'=>[
			 'type'=>'email',
			 'required'=>true,
			 'unique'=>true
		]	
	];
}




