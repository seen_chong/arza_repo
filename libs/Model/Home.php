<?php



namespace Model;



class Home extends \Emagid\Core\Model {



    static $tablename = 'home';



    public static $fields =  [

        'section_name',

        'title',

        'subtitle',

        'description',

        'sub_description',

        'featured_image',

        'image',

        'display',

        'link',

        'display_order',
        'button',
        'about',
        'about_link',
        'sub_about'

    ];


    public function buildSortable(){
        if($this->section_name=='Splash'){
            $html='';
        }else {
            $html = '<li data-name="' . $this->section_name . '" data-id="' . $this->id . '">' . $this->section_name . '</li>';
        }
        return $html;
    }

}