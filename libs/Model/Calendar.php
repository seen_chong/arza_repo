<?php

namespace Model;



use Carbon\Carbon;

class Calendar extends \Emagid\Core\Model {

	static $tablename = "calendar";

	public static $fields = [
  		'date',
  		'time',
  		'provider_id',
		'status'
  	];

	/**
	 * Calendar status map, default 0
	 * @var int
	 */
	static $REQUEST = 0;
	static $AVAILABLE = 1;
	static $UNAVAILABLE = 2;
	static $BOOKED = 3;

	static $STATUS_MAP = [
		'request' => 0,
		'available' => 1,
		'unavailable' => 2,
		'book' => 3
	];

	/**
	 * We only accept calendar hours as below
	 * @var array
	 */
	static $ACCEPT_HOUR = [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24];

	/**
	 * @param $providerId
	 * @param $date
	 * @param $hour
	 * @param $status
	 * @return bool
	 */
	public static function create($providerId, $date, $hour, $status = null)
	{
		$hour = intval($hour);
		if(!self::isValidateHour($hour)){
			return false;
		}
		$calendar = new Calendar();
		if($existingCalendar = Calendar::getItem(null, ['sql' => "select * from calendar where provider_id = $providerId and date = '$date' and time = $hour and active = 1"])){
			$calendar->id = $existingCalendar->id;
		}

		$calendar->provider_id = $providerId;
		$calendar->date = $date;
		$calendar->time = $hour;
		if($status){
			$calendar->status = $status;
		}

		return $calendar->save();
	}
	/**
	 * Hours requires full hours from same date
	 * @param $date 2016-01-01
	 * @param Array $hours
	 * @param $providerId
	 */
	public static function set($providerId, $date, array $hours, $status)
	{
		/**
		 * Clear existing calendar
		 */
//		self::clear($providerId, $date);

		$status = self::$STATUS_MAP[$status];
		/**
		 * Start adding hours
		 */
		foreach($hours as $hour){
			self::create($providerId, $date, $hour, $status);
		}
	}

	/**
	 * @param $providerId
	 * @param $date
	 * @param $hours
	 */
	public static function deleteCalendar($providerId, $date, array $hours)
	{

		$hoursString = implode(',', $hours);
		global $emagid;
		$db = $emagid->getDb();
		$sql = "delete from calendar where provider_id = $providerId AND date = '$date' AND time in ($hoursString)";
		$result = $db->getResults($sql);
	}

	/**
	 * Clear all available hours, not any booked
	 * @param $providerId
	 * @param $date
	 */
	public static function clear($providerId, $date)
	{
		global $emagid;
		$db = $emagid->getDb();
		$availableStatus = self::$AVAILABLE;
		$sql = "delete from calendar where provider_id = $providerId AND date = '$date' AND status = $availableStatus";
		$result = $db->getResults($sql);
	}

	/**
	 * @param $hour
	 * @return bool
	 */
	public static function isValidateHour($hour)
	{
		return in_array($hour, self::$ACCEPT_HOUR);
	}

	public static $calendarTimes = [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];

//	public static $calendarTimes =
//		['05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00', '24:00'];

	/**
	 * If date is not provided, we get all future calendar, otherwise only for that day
	 * @param $provider
	 * @param null $date
	 * @return array [['time' => 5, 'status' => 'request'], ['time' => 6, 'status' => 'available'], ['time' => 7, 'status' => 'unavailable'], ['time' => 8, 'status' => 'book']]
	 */
	public static function getTimes($provider, $date)
	{
		if(!is_a($date, 'Carbon')){
			$date = new Carbon($date);
		}

		// return all times
		$times = [];
		$existingTime = [];
		$sql = "select * from calendar where active = 1 and provider_id = {$provider->id} and date = '{$date->toDateString()}'";
		$calendars = self::getList(['sql' => $sql]);
		foreach($calendars as $calendar){
			$times[] = ['time' => $calendar->time, 'status' => array_search($calendar->status, self::$STATUS_MAP)];
			$existingTime[] = $calendar->time;
		}
		
		foreach(self::$calendarTimes as $time){
			if(!in_array($time, $existingTime)){
				$times[] = ['time' => $time, 'status' => 'request'];
			}
		}


		/**
		 * Sort the time from 5 to 24
		 */
		usort($times, function($a, $b) {
			return $a['time'] - $b['time'];
		});

		return $times;
	}

//	public static function getTimes($provider, Carbon $date)
//	{
//		// return all times
////		return ['05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00', '24:00'];
//		$date = new Carbon($date);
//		$times = [];
//		$calendars = self::getList(['where' => ['provider_id' => $provider->id, 'date_of_session' => $date->toDateString()]]);
//		foreach($calendars as $calendar){
//			if($calendar->full_day_av == 'true' || $calendar->full_day_av == 1){
//				return ['05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00', '24:00'];
//			}
//			$start = new Carbon($date->toDateString().' '.$calendar->time_start);
//			$end = new Carbon($date->toDateString().' '.$calendar->time_end);
//
//			// looping to get hours
//			while($start->hour < $end->hour){
//				$times[] = $start->format('H:i');
//				$start->addHour();
//			}
//
//			// sort times from small to big
//			sort($times, SORT_STRING);
//		}
//
//		return $times;
//	}

	public static function getProvidersBeforeSessionByDate($date){
		if(isset($date) && $date != ""){
			$sql = "SELECT distinct on(provider_id) * from calendar where date_of_session = '$date' AND provider_id in (SELECT id FROM provider where authorized = 'TRUE')";
			$calendar = self::getList(['sql'=>$sql]);
			$providersFiltered = [];
			if(count((array) $calendar) > 4){
				shuffle($calendar);
				for($i = 0; $i < 4; $i++){
					$providersFiltered[] = $calendar[$i]->getProvider();
				}
			} else {
				foreach($calendar as $cal) {
					$providersFiltered[] = $cal->getProvider();
				}
			}
			return $providersFiltered;
		} else {
			return null;
		}
	}

	public function getProvider(){
		return Provider::getItem(null,['where'=>"id = '$this->provider_id' and authorized = 't'"]);
	}

	public static function getRandomTime($provider, Carbon $date)
	{
		$times = self::getTimes($provider, $date);
		shuffle($times);
		return $times[0];
	}
}