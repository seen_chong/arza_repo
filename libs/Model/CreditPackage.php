<?php
namespace Model;
class CreditPackage extends \Emagid\Core\Model{

    static $tablename = "public.credit_package";

    public static $fields = [
        'credits'=>['requred'=>true],
        'name'=>['requred'=>true],
        'description'
    ];
}