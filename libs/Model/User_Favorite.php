<?php

namespace Model;

class User_Favorite extends \Emagid\Core\Model {
  
    static $tablename = "user_favorite";
    public static $fields = ['active','user_id','provider_id'];

}
