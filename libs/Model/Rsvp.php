<?php

namespace Model;



class Rsvp extends \Emagid\Core\Model {
    static $tablename = 'rsvp';
    public static $status = ['New','Success','Incomplete PayPal','Failed'];
    public static $fields = [
        'insert_time',
        'first_name',
        'last_name',
        'address',
        'city',
        'state',
        'zip',
        'cc_number'=>['name'=>'Credit Card Number'],
        'cc_expiration_month'=>['type'=>'numeric', 'name'=>'Credit Card Expiration Month'],
        'cc_expiration_year'=>['type'=>'numeric', 'name'=>'Credit Card Expiration Year'],
        'cc_ccv'=>['type'=>'numeric', 'name'=>'Credit Card CCV'],
        'email'=>['type'=>'email'],
        'phone'=>['required'=>true],
        'event_id',
        'user_id',
        'ref_num',
        'paypal_id',
        'congregation_name',
        'congregation_city',
        'congregation_state',
        'position',
        'status'
    ];

    function full_name()
    {

        return $this->first_name . ' ' . $this->last_name;

    }
    function getAddr(){
        return $this->address .' '. $this->city .' '. $this->state .', '. $this->zip;
    }
}





















































































