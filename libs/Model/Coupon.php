<?php

namespace Model;

class Coupon extends \Emagid\Core\Model {

  static $tablename = "coupon";
  public static $fields = [
  		'name',
  		'code'=>['unique'=>true],
  		'discount_type'=>	['type'=>'numeric', 'required'=>true, 'name'=>'Discount Type'], //1 - $, 2 - %
  		'discount_amount'=>	['type'=>'numeric', 'required'=>true, 'name'=>'Discount Amount'],
  		'min_amount'=>		['type'=>'numeric', 'required'=>true, 'name'=>'Minimum Amount'],
  		'num_uses_all'=>	['type'=>'numeric', 'required'=>true, 'name'=>'Number of uses'],
  		'uses_per_user'=>	['type'=>'numeric', 'name'=>'Uses per user'],
      	'start_time',
  		'end_time'
  ];
  
}
