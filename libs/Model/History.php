<?php



namespace Model;



class History extends \Emagid\Core\Model {



    static $tablename = 'history';



    public static $fields =  [


        'time',

        'title',

        'description'
    ];



}