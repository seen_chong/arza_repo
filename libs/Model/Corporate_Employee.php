<?php
namespace Model;
class Corporate_Employee extends \Emagid\Core\Model{

    static $tablename = 'public.corporate_employee';

    static $fields = [
        'employer_id',
        'employee_id'
    ];

    public static function getEmployers($employeeId)
    {
        return self::getList(['where' => ['employee_id' => $employeeId]]);
    }

    public static function getEmployeeCorporate($employer, $employee)
    {
        return self::getItem(null, ['where' => ['employer_id' => $employer, 'employee_id' => $employee]]);
    }

    public static function getEmployerUser($employeeId){
        $corporate = self::getItem(null,['where'=>"employee_id = $employeeId"]);
        return User::getItem($corporate->employer_id);
    }
}