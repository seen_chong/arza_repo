<?php

namespace Model;



class Calendar extends \Emagid\Core\Model {

	static $tablename = "calendar";

	public static $fields = [

  		'date_of_session' => ['required'=>true ],

  		'time',

  		'time_end',

  		'provider_id' ,
  		'full_day_av',
  		'user_id'

  	];

}