<?php
namespace Model;
class Corporate_Credit extends \Emagid\Core\Model {

    static $tablename = 'public.corporate_credit';

    static $fields = [
        'insert_time',
        'corporate_employee_id',
        'credit'
    ];

//    public function getUserById(){
//        $cEmployee = Corporate_Employee::getItem($this->corporate_employee_id);
//        return User::getItem($cEmployee->employee_id);
//    }

    public static function getEmployeeCredits($corporateEmployeeId)
    {
        $credit = 0;
        $list = self::getList(['where' => ['corporate_employee_id' => $corporateEmployeeId]]);
        if(!$list){
            return $credit;
        }

        foreach($list as $record){
            $credit += $record->credit;
        }

        return $credit;
    }
}