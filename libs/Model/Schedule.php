<?php

namespace Model;



class Schedule extends \Emagid\Core\Model {

	static $tablename = "schedule";

	public static $fields = [

  		 
  	  'user_id',
      'provider_id',
      'day',
      'time',
      'status',
      'service',
  	'session_location',
      'order_product_id',
      'rating','problem'
  	];

}