<?php

namespace Model;



class Community extends \Emagid\Core\Model {
    static $tablename = 'community';
    public static $fields = [
        'name',
        'description',
        'address',
        'city',
        'state',
        'country',
        'zip',
        'lat',
        'lng',
        'display_order'
    ];


    public function buildSortable(){
        $html = '<li data-name="'.$this->name.'" data-id="'.$this->id.'">'.$this->name.'</li>';
        return $html;
    }

    public static function search($keywords, $limit = 20)
    {
        $sql = "select id, name, city, state, country, display_order from community where active = 1 and (";
        if (is_numeric($keywords)) {
            $sql .= "CAST(community.id as TEXT) like '" . $keywords . "%' or ";
        }
        $explodeTags = explode(' ',strtolower(urldecode($keywords)));

        $tags = "'%".implode("%','%",$explodeTags)."%'";

        $sql .= " lower(community.name) like '%" . strtolower(urldecode($keywords)) .
                "%' or lower(community.country) like '%" . strtolower(urldecode($keywords)) .
                "%' or lower(community.city) like '%" . strtolower(urldecode($keywords)) .
            "%' or lower(community.description) like all(array[". $tags."]) 
             ) order BY community.display_order ASC limit " . $limit;
        return self::getList(['sql' => $sql]);
    }
}





















































































