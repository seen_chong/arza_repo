<?php

namespace Model;



class Referral extends \Emagid\Core\Model {

	static $tablename = "referrals";

	public static $fields = [
	  'insert_time',
      'user_id_from',
      'user_id_to',
      'user_type_from',
      'user_type_to'

  	];

}