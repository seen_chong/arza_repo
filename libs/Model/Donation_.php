<?php

namespace Model;



class Donation extends \Emagid\Core\Model {

	static $tablename = "donation";

    

    public static $status = ['New', 'Paid', 'Payment Declined', 'Incomplete PayPal', 'Complete', 'Pending Paypal', 'Canceled', 'Imported as Processing', 'Returned'];
	public static $transactionStatus = ['New Order', 'Paid', 'Payment Declined','Transaction Error'];


	public static $fields  = [
        'viewed'=>['type'=>'boolean'],
        'insert_time',
        'first_name'=>['required'=>true],
        'last_name'=>['required'=>true],
        'address'=>['required'=>true],
        'city',
        'state',
        'zip',
        'email'=>['type'=>'email'],
        'phone'=>['required'=>true],
        'cc_number'=>['name'=>'Credit Card Number'],
        'cc_expiration_month'=>['name'=>'Credit Card Expiration Month'],
        'cc_expiration_year'=>['name'=>'Credit Card Expiration Year'],
        'cc_ccv'=>['type'=>'numeric', 'name'=>'Credit Card CCV'],
        'amount',
        'user_id',
        'type',
        'status',
        'paypal_id',
        'ref_num',
        'monthly'

	];
    function full_name()
    {

        return $this->first_name . ' ' . $this->last_name;

    }
    function getType(){
        return $this->type == 1? 'Register':'Donation';
    }
    public static function search($keywords){

        $sql = "select * from donation where active = 1 and id::varchar like '%".$keywords."%' order by id asc limit 20";

        return self::getList(['sql' => $sql]);

    }
    function getAddr()
    {
        return $this->address . ' ' . $this->city . ' ' . $this->state . ', ' . $this->zip;
    }



}





















































































