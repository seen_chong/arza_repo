<?php
namespace Model;

use Email\MailMaster;
use s3Bucket\s3Handler;

class Applicant extends \Emagid\Core\Model{

    static $tablename = "applicant";


    public static $fields = [
        'insert_time',
        'first_name' => ['required' => true, 'name' => 'First Name'],
        'last_name' => ['required' => true, 'name' => 'Last Name'],
        'email'=>['unique'=>'true'],
        'phone',
        'comment',
        'resume',
        'position_id'
    ];

    /**
     * concatenates first name and last name to create full name string and returns it
     * @return type: string of full name
     */

    function full_name()
    {

        return $this->first_name . ' ' . $this->last_name;

    }

}