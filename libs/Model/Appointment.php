<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 12/14/15
 * Time: 5:09 PM
 */

namespace Model;


class Appointment extends \Emagid\Core\Model {
    static $tablename = 'appointment';
    public static $fields = [
        'active',
        'provider_service_id' => ['required'=>true ],
        'user_id' => ['required'=>true ],
        'date' => ['required'=>true ],
        'message'
    ];


}