<?php



namespace Model;



class Section extends \Emagid\Core\Model {



  	static $tablename = "section";



  	public static $fields = [ 

	    'content_name',

  	    'name' => ['required'=>true],

        'title',

        'description',

        'subtitle',

        'sub_description',

        'featured_image',

		'display' => ['type'=>'numeric'],

		'display_order' => ['type'=>'numeric'],

        'content_style',
        'images',
        'notes',
        'link',
        'button',
        'image_description',
        'options'

	];



	public static function getNested($parentId){

		$categories = self::getList(['where'=>"active = 1 AND parent_category = {$parentId} "]);

		

		foreach($categories as $category){

			$category->children = self::getNested($category->id);

		};

			

		return $categories;

	}

    public function getContentName(){

        if(Content::getItem($this->content_name)){
            return Content::getItem($this->content_name)->name;
        }else{
            return false;
        }

    }
    public function buildSortable(){
        $html = '<li data-name="'.$this->name.'" data-id="'.$this->id.'">'.$this->name.'</li>';
//        $html .= '</ol></li>';
        return $html;
    }

}