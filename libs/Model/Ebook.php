<?php



namespace Model;



class Ebook extends \Emagid\Core\Model {



    static $tablename = 'ebook';



    public static $fields =  [

        'name',

        'author',

        'description',

        'member_type',

        'file_name',

        'featured_image',

        'publish_date'

    ];

    public static function search($keywords, $limit = 20)
    {
        $sql = "select id, name, author, publish_date from ebook where active = 1 and (";
        if (is_numeric($keywords)) {
            $sql .= "CAST(ebook.id as TEXT) like '" . $keywords . "%' or ";
        }
        $explodeTags = explode(' ',strtolower(urldecode($keywords)));
        $tags = "'%".implode("%','%",$explodeTags)."%'";
        $sql .= " lower(ebook.name) like '%" . strtolower(urldecode($keywords)) . "%' or lower(ebook.author) like '%" . strtolower(urldecode($keywords)) ."%' or
            lower(ebook.name) like all(array[". $tags."]) or lower(ebook.publish_date) like '%". strtolower(urldecode($keywords)) . "%' 
            ) order BY ebook.name ASC limit " . $limit;
        return self::getList(['sql' => $sql]);
    }

    public static function getBooks($typeId){
        $member_type = Member_Type::getParent($typeId)->id;
        $array=[];
        $books = self::getList();
        foreach ($books as $book){
            $obj = json_decode($book->member_type);
            if(in_array($member_type,$obj)){
                array_push($array,$book);
            }
        }
        return $array;
    }

}