<?php

namespace Model;



class Donate extends \Emagid\Core\Model {

    static $tablename = "donate";
    public static $form = ['First Name'=>'first_name','Last Name'=>'last_name','Address'=>'address','City'=>'city',
        'State'=>'state','ZipCode'=>'zip','Email'=>'email','Phone Number'=>'phone','Donation Preference'=>'preference',
        "I'd like to repeat this gift every month"=>'monthly','This gift is honor of/in memory of _'=>'specify'];
    public static $type = [1=>'URL',2=>'Regular'];

    public static $fields  = [
        'insert_time',
        'name',
        'title',//donation_description
        'subtitle',
        'image',
        'logo_image',
        'logo_description',
        'donate_amount',
        'background_image',
        'is_default',
        'slug',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'display',
        'default_template',
        'donate_form',
        'display_order',
        'type'

    ];

    static function clearDefault($except = 0){
        self::execute('update '.self::$tablename.' set is_default = 0 where id <> '.$except);
    }
    static function getDefault(){
        return self::getItem(null,['where'=>'is_default = 1']);
    }
}


















































