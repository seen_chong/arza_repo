<?php

namespace Model;

use Emagid\Core\Model;

class Testimonial_Services extends Model
{
    static $tablename = 'testimonial_services';
    static $fields = [
        'testimonial_id',
        'service_id'
    ];
    static $relationships = [
        [
            'name' => 'testimonial',
            'class_name' => '\Model\Testimonial',
            'local' => 'testimonial_id',
            'remote' => 'id',
            'relationship_type' => 'one'
        ],
        [
            'name' => 'service',
            'class_name' => '\Model\Service',
            'local' => 'service_id',
            'remote' => 'id',
            'relationship_type' => 'one'
        ],
    ];
}