<?php

namespace Model;
class Corporate extends \Emagid\Core\Model {

    static $tablename = "public.user";

    public static $fields = [
        'email' => ['required' => true, 'type' => 'email', 'unique' => true],
        'insert_time',
        'password',
        'hash',
        "dob",
        'first_name' => ['required' => true, 'name' => 'First Name'],
        'last_name' => ['required' => true, 'name' => 'Last Name'],
        'bank_name',
        'bank_address',
        'bank_city',
        'voided_check',
        'routing_number',
        'photo',
        'remind_password',
        "height_feet",
        "height_inches",
        "weight",
        "six_month_ago_weight",
        "one_year_ago_weight",
        "satisfied_weight",
        "different_weight_value",
        "health_concerns",
        "health_goals",
        "sleep_well",
        "wake_up_at_night",
        "explanation_sleep",
        "explanation_pain",
        "medications_list",
        "explanation_fitness",
        "important_diet_change",
        "injuries_conditions",
        "ref_key",
        "company",
        "credit",
        "corporate_credit",
    ];
    function full_name() {

        return $this->first_name.' '.$this->last_name;

    }

    public function getCorporateList(){
        $users = new User();
        return $users->getAllUsersByRole('4');
    }

}