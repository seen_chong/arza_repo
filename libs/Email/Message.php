<?php

namespace Email;

require(__DIR__.'/../twilio-php/Services/Twilio.php');


class Message {

    static $FROM_PHONE = '6466634577';

    public static function send($to, $content){
        if(!$to || $to == '' ||!$content){
            return false;
        }

        $to = preg_replace("/[^0-9]/", "", $to);
        $client = new \Services_Twilio(TWILLIO_SID, TWILLIO_TOKEN);
        $message = $client->account->messages->sendMessage(self::$FROM_PHONE, $to, $content);

        return $message->sid;
    }

    /**
     * - upcoming session reminder to users 48 hours prior to the session
     * WELLNS REMINDER: you have a (TYPE OF SESSION) session with (SPECIALIST NAME) at (TIME) (DATE). To reschedule, click wellns.com/login.
     *
     * - upcoming session reminder to users 24 hours prior to the session
     * WELLNS REMINDER: you have a (TYPE OF SESSION) session with (SPECIALIST NAME) at (DATE) (TIME).
     *
     * - upcoming session reminder to users 3 hours prior to the session
     * Get ready! Your (TYPE OF SESSION) session with (SPECIAL NAME) is starting in 3 hours.
     *
     * - upcoming session reminder to users 48 hours prior to the session
     * WELLNS REMINDER: you have a (TYPE OF SESSION) session with (USER NAME) at (TIME) (DATE). Address is (ADDRESS). To reschedule, click wellns.com/login.
     *
     * - upcoming session reminder to specialists 24 hours prior to the session
     * WELLNS REMINDER: you have a (TYPE OF SESSION) session with (USER NAME) tomorrow at (TIME). Address is (ADDRESS).
     *
     * - session request from a user
     * You have a session request from a WELLNS user! Click wellns.com/login and log in to your dashboard to accept/reject this request.
     *
     * - session confirmation from a specialist
     * WELLNS specialist (NAME) has confirmed your session request! You now have a (TYPE OF SESSION) booked for (DATE) at (TIME)
     *
     * - session cancelled by a user
     * WELLNS member (NAME) has cancelled your session on (DATE) at (TIME). Please log in to your dashboard for rescheduling options. wellns.com/login
     *
     * - session cancelled by a specialist
     * WELLNS user (NAME) has cancelled your session on (DATE) at (TIME). Please log in to your dashboard for rescheduling options. wellns.com/login
     *
     * - new inbox message notifications
     * You have a new message in your WELLNS inbox! Please log in to your account at wellns.com/login to view this message.
     *
     * - when specialists open their calendars/request a session
     * (SPECIALIST NAME) has a newly available time slots! Click wellns.com/login and log in to your WELLNS account to request a session!
     * @param $type
     * @param $mergeTags
     * @return null|string
     */
    public static function getMessageContent($type, $mergeTags = []){
        $message = null;
        switch($type){
            case 'new-session-to-provider':
                $message = 'You have a new *|SERVICE_NAME|* session request from a WELLNS member on *|SESSION_DATE|* at *|SESSION_TIME|*! Click wellns.com/login and log in to your dashboard to accept/reject this request.';
                break;
            case 'session-confirmed-to-user':
                $message = "WELLNS specialist *|PROVIDER_NAME|* has confirmed your session request! You now have a *|SERVICE_NAME|* booked for *|SESSION_DATE|* at *|SESSION_TIME|*";
                break;
            case 'session-cancelled-to-provider':
                $message = "WELLNS member *|CUSTOMER_NAME|* has cancelled your *|SERVICE_NAME|* session on *|SESSION_DATE|* at *|SESSION_TIME|*. Please log in to your dashboard for rescheduling options. wellns.com/login";
                break;
            case 'session-cancelled-to-user':
                $message = "WELLNS specialist *|PROVIDER_NAME|* has cancelled your *|SERVICE_NAME|* session on *|SESSION_DATE|* at *|SESSION_TIME|*. Please log in to your dashboard for rescheduling options. wellns.com/login";
                break;
            case 'new-inbox-message':
                $message = "You have a new message from *|NAME|* in your WELLNS inbox! Please log in to your account at wellns.com/login to view this message.";
                break;
        }

        foreach($mergeTags as $tag => $value){
            $pattern = '/\*\|'.$tag.'\|\*/';
            $message = preg_replace($pattern, $value, $message);
        }

        return $message;
    }
}