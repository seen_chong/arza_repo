<?php

namespace Notification;

class ErrorHandler extends \Notification\NotificationsHandler
{
  /**
   * 
   * Construct the error object
   * 
   */
  public function __construct($messages)
  {
    if (is_array($messages)){
      $message = '<ul>';
      foreach($messages as $m){
        $message .= '<li>';
        $message .= $m['message'];
        $message .= '</li>';
      }
      $message .= '</ul>';
    } else {
      $message = $messages;
    }
    $this->message = $message;
    $this->buildHTML();
  }
  
  /**
   * 
   * Display the error
   * 
   */
  protected function buildHTML()
  {
    $this->html .= "<div class='in_page_alert_wrapper' id='in_page_alert_wrapper_nofitication'>";
    $this->html .= "<script type='text/javascript'>";
    $this->html .= "hide_in_page_alert_notification()";
    $this->html .= "</script>";
    $this->html .= "<div class='in_page_alert_container'>";
    $this->html .= "<h1 class='alert alert-danger'>";
    $this->html .= $this->message;
    $this->html .= "</h1>";
    $this->html .= "</div>";
    $this->html .= "</div>";    
  }
  
}

