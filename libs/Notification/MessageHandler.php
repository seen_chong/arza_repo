<?php

namespace Notification;

class MessageHandler extends \Notification\NotificationsHandler
{
  /**
   * 
   * Construct the error object
   * 
   */
  public function __construct($message)
  {
    $this->message = $message;
    $this->buildHTML();
  }
  
  /**
   * 
   * Display the error
   * 
   */
  protected function buildHTML()
  {
    $this->html .= "<div class='in_page_alert_wrapper' id='in_page_alert_wrapper_nofitication'>";
    $this->html .= "<script type='text/javascript'>";
    $this->html .= "hide_in_page_alert_notification()";
    $this->html .= "</script>";
    $this->html .= "<div class='in_page_alert_container in_page_alert_container_success'>";
    $this->html .= "<h1 class='alert alert-success'>";
    $this->html .= $this->message;
    $this->html .= "</h1>";
    $this->html .= "</div>";
    $this->html .= "</div>";  
  }
  
}

 