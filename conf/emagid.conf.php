<?php

include("emagid.db.php");
include("emagid.consts.php");

$site_routes = [
    'routes' => [
        [
            'name' => 'login',
            'area' => 'admin',
            'template' => 'admin',
            'pattern' => "admin/login/{?action}",
            'controller' => 'login',
            'action' => 'login',
        ],
        [
            'name' => 'email',
            'area' => 'admin',
            'template' => 'admin',
            'pattern' => "admin/emails",
            'controller' => 'email_list',
            'action' => 'redir',
            'authorize' => [
                'roles' => ['admin'],
                'login_url' => '/admin/login'
            ]
        ],
        [
            'name' => 'admin',
            'area' => 'admin',
            'template' => 'admin',
            'pattern' => "admin/{?controller}/{?action}/{?id}",
            'controller' => 'dashboard',
            'action' => 'index',
            'authorize' => [
                'roles' => ['admin'],
                'login_url' => '/admin/login'
            ]
        ],
        [
            'name' => 'page',
            'pattern' => "page/{page_slug}",
            'controller' => 'pages',
            'action' => 'page'
        ],
        [
            'name' => 'content',
            'pattern'=>"contents/{content_slug}",
            'controller'=>'contents',
            'action'=>'content'
        ],
        [
            'name' => 'event',
            'pattern' => "event/{id}",
            'controller' => 'event',
            'action' => 'event'
        ],
        [
            'name' => 'donate',
            'pattern' => "donation/{donation_slug}",
            'controller' => 'donation',
            'action' => 'donate'
        ],
        [
            'name' => 'service',
            'pattern' => "service/{service_slug}",
            'controller' => 'services',
            'action' => 'service'
        ],
        [
            'name' => 'shop',
            'pattern' => "shop/{category_slug}",
            'controller' => 'shop',
            'action' => 'index'
        ], [
            'name' => 'blog',
            'pattern' => "blog/{?action}/{slug}",
            'controller' => 'blog',
            'action' => 'post'
        ],
        [
            'name' => 'blog2',
            'pattern' => "blog/{?action}/{category_id}",
            'controller' => 'blog',
            'action' => 'category'
        ],
        [
            'name' => 'israel',
            'pattern' => "israel/{?action}/{slug}",
            'controller' => 'israel',
            'action' => 'post'
        ],
        [
            'name' => 'resource',
            'pattern' => "resource/{resource_id}",
            'controller' => 'resource',
            'action' => 'resource'
        ]

    ]
];

$emagid_config = array(
    'debug' => true,          // When debug is enabled, Kint debugging plugin is enabled.
    //  you can use d($my_var) , dd($my_var) , Kint::Trace() , etc...
    //  documentation available here : http://raveren.github.io/kint/

    'root' => SITE_URL,
    'template' => 'arza',  // template must be found in /temlpates/<template_name>/<template_name>.php
    // so in this example we will have /templates/default/default.php
    // please open the template file to see how the view is being rendered .

    'connection_string' => array(
        'driver' => DB_DRIVER,
        'db_name' => DB_NAME,
        'username' => DB_USER,
        'password' => DB_PWD,
        'host' => DB_HOST
    ),

    'include_paths' => array(
        'libs/Mandrill'
    ),

    'email' => array(
//        'api_key' => '_Uv1USpTRK57oz-E48slog',
        'api_key' => '85UZ0rPwBOR5chLjxQiYaw', //emagid key
        'from' => array(
            'email' => 'info@arza.com',
            'name' => 'ARZA Team'
        )
    )
); 
