<?php
$signupEmail = md5(time())."@gmail.com";

$I = new AcceptanceTester($scenario);
$I->wantTo('Test user signup');
$I->amOnPage('/signup');

// it should have signup modal
//$I->seeElement('.onboarding_slide_content');

$I->fillField(['name' => 'first_name'], 'test');
$I->fillField(['name' => 'last_name'], 'test');
$I->fillField(['name' => 'email'], $signupEmail);
$I->fillField(['name' => 'password'], 'test5343');

// click submit
// TODO: cancel out mandrill email
$I->click('.onboarding_slide_continue_button.submit_btn');

$I->wait(1);

$I->seeInCurrentUrl('/user/dashboard');
