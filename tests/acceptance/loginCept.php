<?php

$I = new AcceptanceTester($scenario);
$I->wantTo('Test user login');
$I->amOnPage('/');
$I->click('#signin_link_container .login_btn');
// I should be redirect to login page
$I->seeInCurrentUrl('/login');
$I->wait(1);
$I->fillField(['name' => 'user_email'], 'corporate@test.com');
$I->fillField(['name' => 'user_password'], 'test5343');
$I->click('.big_signin_btn');
$I->seeInCurrentUrl('/user');
