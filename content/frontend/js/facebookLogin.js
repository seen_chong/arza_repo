$(document).ready(function(){
    $('#facebook_login_button').click(function() {
        FB.login(function(response) {
            if (response.authResponse) {
                FB.api('/me?fields=email, first_name, last_name, name', function(response) {
                    console.log(response);
                    // picture comes from here: graph.facebook.com/response.id/picture?type=large
							var url = '/user/fbHandler';
							var user_type = "customer";
							var first_name = response.first_name;
							var last_name = response.last_name;
							var email = response.email;
							var facebook_id = response.id;
							var password = (Math.round(Math.random()*1E16)).toString();
//
							$.post(url, {user_type: user_type, first_name: first_name, last_name: last_name, email: email, fb_id: facebook_id, password:password}, function(data){
								console.log(data);
								//var json_data = JSON.parse(data);
								//console.log(json_data);
								if(data.status == 'success'){
									window.location.href="/user";
								}else{
									alert("Error: " + data.message);
								}//close if
							});//close post
                });
            //FB.api('/me/permissions', function(response) {
            //    var declined = [];
            //    for (i = 0; i < response.data.length; i++) {
            //        if (response.data[i].status == 'declined') {
            //            declined.push(response.data[i].permission)
            //        }
            //    }
            //    alert(declined.toString())
            //});
            }
        }, {scope:'email', return_scopes:true})
    });//close click function
});
//