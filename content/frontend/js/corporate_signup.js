$(document).ready(function() {
	$("div").on("click", ".option_button", function(event){

		event.stopImmediatePropagation()
		event.preventDefault();
		var $inner_checkbox = $(this).find("input");
		var $option_clicked = $(this);

		if($option_clicked.hasClass("option_button_radio")){
			var $option_wrapper = $option_clicked.closest(".checkbox_option_button_wrapper");
			$option_wrapper.find(".option_button_radio").removeClass("selected");
			$option_wrapper.find(".option_button_radio").find("input").attr('checked', false);
		}
		if($option_clicked.hasClass("selected")){
			$option_clicked.removeClass("selected");
			if($inner_checkbox.length > 0){
				$inner_checkbox.attr('checked', false);
			}
		} else{
			$option_clicked.addClass("selected");
			if($inner_checkbox.length > 0){
				$inner_checkbox.attr('checked', true);
			}
		}
	});
	$(".corporate_signup_continue").click(function(){
		$('#user-register').submit();
		//var $credits_purchase_wrapper = $(".corporate_signup_credits_wrapper");
		//var first_name=$("#corporate_user_first_name").val();
		//
		//var company_name=$("#corporate_company_name").val();
		//$(".corporate_full_page_signup").addClass("credits_purchase_step_active");
        //
		//$credits_purchase_wrapper.show().delay(300).queue(function(nextX){
		//	$(".big_title .company_name").text(company_name);
		//	$(".big_title .corporate_user_first_name").text(first_name);
		//	$credits_purchase_wrapper.addClass("corporate_signup_credits_wrapper_active");
		//	$(".signup_form_wrapper").hide();
		//	nextX();
		//});
	});
});